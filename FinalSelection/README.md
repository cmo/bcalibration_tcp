# FinalSelection
This part selects events from TopCPToolkit nTuples, store them as histograms which will be used during likelihood-fit.

# Quick Start

## Step0: basic setup
Set up environment:
```bash
source setup.sh
```
Configure `options_container` in `options_file.py`. **The following items should be taken care of:**
```python
self.AT_release
self.version_tag
self.dust_dir
```

Get the list of DatasetNames of TopCPToolkit nTuples:
```bash
rucio ls --short "user.USERNAME.*.DAOD_FTAG2.*DSTAG_output" > dsname_list.txt
```

## Step1: build work directory
Build your work directory:
```bash
python LocalFolderBuilder.py --RSE [RSE_storage] dsname_list.txt
```
The path you specified in `option_file.py options_container.dust_dir` will be created and is now your work directory. `LocalFolderBuilder.py` will retrive the sample list, categorize them by the sample DSID, and record path to the ROOT files stored in RSE. You can check the details in `$dust_dir/input_files`.

## Step2: event selection 
Event selection is carried by `finalSelection.py`. To test if everything is set up properly. Create a txt file which contains the path to one of the `$dust_dir/*/*.txt`:
```bash
ls $dust_dir/[!d]*/*.txt | head -n1 > tmp.txt # replace $dust_dir with the correct path
python finalSelection.py tmp.txt --hadronization 410250 
```
If you find `$dust_dir/histograms/tmp/tmp_nominal_combination.root` successfully created, then everything is correct. Now we can submit htcondor jobs to implement the event selection over all samples:
```bash
cd htcondor_submit
python submit_to_condor.py
```
The final output ROOT files will be stored in `$dust_dir/histograms` once all jobs completed.