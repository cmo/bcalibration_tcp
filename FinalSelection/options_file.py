import ROOT
import os
import sys,glob,getpass,socket
class sample:
    _lastFillColor =0
    _fillColors=[8, 9, 2, 41, 800, 0, 880] + list(range(30)) # a personal taste of cosmetics :)
    def __init__(self, name, boot_strap_available = False, name_short = ""):
        self.name = name
        self.root_file_path = ""
        # self.boot_strap_available = boot_strap_available
        # self.do_bootstrap=boot_strap_available  
        #TODO: Update these with new ttbar DSIDs in the future once light-jet SFs are available
        if "Py8" in name:
            self.hadronization=410470 #601230
        elif "Sherpa22" in name:
            self.hadronization=410250 #700660
        elif "HW7" in name:
            self.hadronization=410250 #601415
        else:
            print(f"Warning: Hadronization DSID not found for sample: {name}. Setting to 410470")
            self.hadronization=410470

        self.fillColor = sample._fillColors[sample._lastFillColor]
        sample._lastFillColor=sample._lastFillColor+1
    
    def setHistOptions(self, hist):
        hist.SetMarkerStyle(1)
        hist.SetFillStyle(1001)
        hist.SetLineColor(1)
        hist.SetFillColor(self.fillColor)
        hist.SetLineStyle(1)



class options_container:
    def __init__(self):
        self.name="Summary of options"

        # Modify the following options
        self.AT_release = "r25.2.40"
        # self.version_tag=self.AT_release+ "_emujet-v3.0_mc23a/"
        self.version_tag=self.AT_release+ "_emujet-v3.0_mc23a-splitSingletop/"
        self.dust_dir="/eos/user/c/cmo/project/Calibration/FTAGResults/topcp_sample/"+self.version_tag

        self.data_name = "data22"
        self.data_lumi = 29.0493
        self.data_CME = "13.6"

        self.output_dir= self.dust_dir+"/histograms/"
        self.input_selection_dir= self.dust_dir+"input_files/"
        self.plot_dir= self.dust_dir+"/plots/"

        self.nominal_samples = {
            "ZJets":    sample('Zjets_Sherpa22', True, "Zjets"),
            "Wjets":    sample('Wjets_Sherpa22', True, "Wjets"),
            "Diboson":  sample('Diboson_Sherpa22', True, "Diboson"),
            "Singletop":sample('Singletop_PowPy8', True,"Singletop"),
            "ttbar":    sample('ttbar_PhPy8_nominal',True,"ttbar"),
            "Singletop_schan":sample('Singletop_schan_PowPy8', True,"Singletop_schan"),
            "Singletop_tchan":sample('Singletop_tchan_PowPy8', True,"Singletop_tchan"),
            "Singletop_tW":sample('Singletop_tW_PowPy8', True,"Singletop_tW"),
        }

        self.alternative_samples = {
            "ttbar_PhPy8_hdamp": sample('ttbar_PhPy8_hdamp', True, "ttbar_hdamp"),
            "ttbar_HW7": sample('ttbar_HW7', True, "ttbar_HW7"),
            "ttbar_PhPy8_pThard": sample('ttbar_PhPy8_pThard', True, "ttbar_PhPy8_pThard"),
            "Singletop_tW_PowPy8_DS": sample('Singletop_tW_PowPy8_DS', True, "Singletop_tW_PowPy8_DS"),
        }


options=options_container()
