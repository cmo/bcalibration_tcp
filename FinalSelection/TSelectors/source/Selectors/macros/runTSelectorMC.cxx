// A simple program that computes the square root of a number
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "TH1.h"
#include "TFile.h"
#include "TChain.h"
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include "libSelectorMC/finalSelectionMC.h"
#include "cxxopts.hpp"
#include "libSelectorMC/ConfigManager.h"

using namespace std;

int main(int argc, char *argv[])
{
  // Parse the command line arguments
  ConfigManager::Instance()->ParseArgs(argc, argv);

  TChain* chain = new TChain("reco");
  auto input_file = ConfigManager::Instance()->input_file();
  ifstream myfile(input_file);
  string line;
  if (myfile.is_open()){
    while (getline(myfile, line))
    {
      auto file = line.c_str();
      // Check if the file is empty
      TFile *f = TFile::Open(file);
      TTree *t = (TTree*)f->Get("reco");
      if ((t == nullptr) || (t->GetEntries() == 0)){
        cout << "File: " << line << " is empty. Skipping." << endl;
        continue;
      }
      cout << "Adding File: " << line << "|" << endl << std::flush;
      chain->Add(line.c_str());
    }
    myfile.close();
  }
  else{
    cout << "Could not open inputfile: " << input_file << endl;
    return 1;
  }

  // check if the chain is empty
  if (chain->GetEntries() == 0){
    cout << "Chain is empty. Exiting." << endl;
  } else {
    finalSelectionMC* selectorMC = new finalSelectionMC(ConfigManager::Instance()->systematic_name());
    std::cout << "selector formed" << std::endl;
    selectorMC->output = ConfigManager::Instance()->output_file();
    std::cout << "set output attribute" << std::endl;
    // selectorMC->Begin(commandLineStr.c_str());
    selectorMC->Begin();
    std::cout << "Begin method done" << std::endl;
    cout << "Beginning TChain processing ... " << endl << std::flush;
    chain->Process(selectorMC);
    
    delete selectorMC;
    cout << "deleted TSelector" << endl;
    delete chain;
    cout << "deleted chain" << endl;
  }
  return 0;
}
