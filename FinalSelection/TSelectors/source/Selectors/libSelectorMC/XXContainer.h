// XXContainer.h

#pragma once

#include <TLorentzVector.h>
#include <string>
#include <vector>
#include "VariableContainer.h"

class XX_Container
{
public:
	std::string name;
	Variable_Container *xx_m; //! 
	Variable_Container *xx_mT; //! 
	Variable_Container *xx_pT; //! 
	Variable_Container *xx_d_eta; //! 
	Variable_Container *xx_d_phi; //! 
	Variable_Container *xx_d_R; //! 
public:
	// Constructor
	XX_Container(std::string name, int n_jets, bool data, std::vector<std::string> eflavours, double xx_m_start = 0.0);

	// Destructor
	~XX_Container();

	// Writes something
	void Write();

	// Adds an event to something
	void addEvent(TLorentzVector first4vec, TLorentzVector sec4vec, double weight, std::vector<int> *jet_truthflav);

	ClassDef(XX_Container,1);
};
