// VariableContainer.h

#pragma once

#include <vector>
#include <string>
#include <map>
#include <THnSparse.h>
#include <TAxis.h>
// Potentially
// #include <THnSparseD.h>

class Variable_ptBins_Correlation_Container
{
public:
	std::string name;
	bool data;
	static const int NBINS_pt = 5;
	const double pt_bins[NBINS_pt + 1] = {20., 40., 60., 140., 250., 400.}; //{20., 30., 40., 60., 85., 110., 140., 175., 250., 400.};
	Int_t bins[4] = {1, 1, NBINS_pt, NBINS_pt};
	std::map<std::string, THnSparseD *> histograms;

public:
	// Constructor
	Variable_ptBins_Correlation_Container(std::string name, int n_jets, std::string x_name, int nxbins, const double *xbins, std::string y_name, int nybins, const double *ybins, bool data, std::vector<std::string> eflavours);

	// Destructor
	~Variable_ptBins_Correlation_Container();

    // Writes histograms it seems
	void Write();

    // Adds event
    void addEvent(double xvalue, double yvalue, std::vector<float> *jet_pt, double weight, std::vector<int> *jet_truthflav);

    // Gets event flavour
    std::string getEventFlav(std::vector<int> *jet_truthflav);

    ClassDef(Variable_ptBins_Correlation_Container,1);
};
