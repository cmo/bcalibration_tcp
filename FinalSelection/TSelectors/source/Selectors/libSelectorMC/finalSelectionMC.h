// finalSelectionMC.h
#pragma once

#include "TObject.h"
#include "JetContainer.h"
#include "DilepChannel.h"
#include "TreeReader.h"
#include "macros/cxxopts.hpp"
#include "CalibrationDataInterface/CalibrationDataInterfaceROOT.h"
#include "CalibrationDataInterface/CalibrationDataContainer.h"
#include "CalibrationDataInterface/CalibrationDataVariables.h"
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"
#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"
#include "PATInterfaces/ISystematicsTool.h"
#include "PATInterfaces/SystematicSet.h"

//namespace finalSelectionMC{
class finalSelectionMC : public TSelector
{
public:
    TTree *fChain;               //!pointer to the analyzed TTree or TChain
    int num_events = 0;
    bool data = false;
    std::vector<std::string> eflavours = {"bb", "bc", "bl", "cb", "cc", "cl", "lb", "lc", "ll"};
    int hadronization = 410470;
    const double m_jl_CutValue = 175.;
    std::string bTagSystName = "";
    int n_weight_shower = 0;
    int n_weight_muRmuF = 0;
    TDirectory *f_out;
    TH1F *jet_multiplicity; //!
    TH1F* histo_reweight; //!
    Double_t w_lumi = 0.;
    bool save_fits = false;
    bool boot_strap = false;
    int bsbin = 0;
    bool run_fakes = false;
    bool correctFakes = false;
    bool apply_lf_calib = false;
    bool save_bin_means_for_sr_histo = false;
    double NPel_bin1 = 1., NPel_bin2 = 1., NPel_bin3 = 1.;
    const double m_ll_CutValue = 50.;
    const double min_m_jl_CutValue = 20.;
    int m_n_boot_strap_min = 0;
    int m_n_boot_strap_max = 100;
    bool use_pt_bins_as_eta = false;
    std::string jet_collection="AntiKt4EMPFlowJets";
    std::string jet_collection_lightjets="AntiKt4EMPFlowJets";
    std::string output;

    TreeReader* m_treeReader; //!

    DiLepChannel *emu_OS_J2;//!
    DiLepChannel *emu_OS_J2_CR_bl;//!
    DiLepChannel *emu_OS_J2_CR_lb;//!
    DiLepChannel *emu_OS_J2_CR_ll;//!
    DiLepChannel *emu_OS_J2_m_lj_cut;//!
    DiLepChannel *emu_SS_J2;//!

public:
    // Default Constructor
    finalSelectionMC(string systematic_name = "NOSYS");

    // Destructor
    virtual ~finalSelectionMC();

    virtual Int_t   Version() const
    {
        return 2;
    }

    // Called every time a loop on the tree starts, a convenient place to create your histograms.
    void Begin();

    // Clled after Begin(), when on PROOF called only on the slave servers.
    virtual void    SlaveBegin();

    // virtual void    Init(TTree *tree);
    // virtual Bool_t  Notify();

    // called for each event, in this function you decide what to read and fill your histograms.
    virtual Bool_t  Process(Long64_t entry);

    virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0)
    {
        return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0;
    }

    virtual void    SetOption(const char *option)
    {
        fOption = option;
    }

    virtual void    SetObject(TObject *obj)
    {
        fObject = obj;
    }

    virtual void    SetInputList(TList *input)
    {
        fInput = input;
    }

    virtual TList *GetOutputList() const
    {
        return fOutput;
    }

    // Called at the end of the loop on the tree, when on PROOF called only on the slave servers.
    virtual void    SlaveTerminate();

    // called at the end of the loop on the tree, a convenient place to draw/fit your histograms.
    virtual void    Terminate();

    Bool_t Notify()
    {
        // The Notify() function is called when a new file is opened. This
        // can be either for a new TTree in a TChain or when when a new TTree
        // is started when using PROOF. It is normally not necessary to make changes
        // to the generated code, but the routine can be extended by the
        // user if needed. The return value is currently not used.

        return kTRUE;
    }

    // Initialise
    void Init(TTree* tree)
    {
        m_treeReader->Init(tree);
    };

    // Gets event flavour
    std::string getEventFlav(std::vector<int> *jet_truthflav);

    // ClassDef(finalSelectionMC,1);
};
