// BTagToolContainer.h
#pragma once

#include "CalibrationDataInterface/CalibrationDataInterfaceROOT.h"
#include "CalibrationDataInterface/CalibrationDataContainer.h"
#include "CalibrationDataInterface/CalibrationDataVariables.h"
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"
#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"
#include "PATInterfaces/ISystematicsTool.h"
#include "PATInterfaces/SystematicSet.h"

class BtagTool_Container 
{
public:
	//class tp conbin btagging tools for 1 Tagger and 1 type of WP (Fixed or hybrid cuts)
	std::vector<BTaggingSelectionTool *> btagTools; 
	std::vector<std::string> wp_boundrys;
	BTaggingEfficiencyTool *btageff = NULL;
	bool apply_lf_calib = true;
	Analysis::CalibrationDataVariables *vars_jet; 
	std::string toolName = "";
    std::string cdi_file_path; 
	std::string custom_cdi_file_path = "/afs/cern.ch/work/f/fdibello/public/2017-21-13TeV-MC16-CDI-2018-10-19_v1_testcutDl1r.root";
	std::string bjet_calibsystset = "SFEigen";
	std::string m_jet_collection="AntiKt4EMPFlowJets";
	std::string m_jet_collection_lightjets="AntiKt4EMPFlowJets";
	int m_hadronization;
	std::string m_bTagSystName;
	bool m_data;

public:
	// Constructor
	BtagTool_Container(const char *toolName, const char *taggerName, const char *cutName, int hadronization, std::string bTagSystName, bool data,std::string jet_collection,std::string jet_collection_lightjets,bool apply_lf_calib);

	// Destructor
	~BtagTool_Container();

    // Gets the SF
	double get_scale_factor(Double_t jet_pt, Double_t jet_eta, int jet_truthflav, Double_t jet_tag_weight);

    // Gets the Btagging Bin
    int get_Btagging_bin(Double_t jet_pt, Double_t jet_eta, Double_t jet_tag_weight);

    ClassDef(BtagTool_Container,1);
};
