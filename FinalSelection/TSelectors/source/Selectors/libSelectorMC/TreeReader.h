//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sat Sep 21 17:52:39 2024 by ROOT version 6.28/12
// from TTree reco/xAOD->NTuple tree
//////////////////////////////////////////////////////////

// Branches required for the analysis:
//    ULong64_t       eventNumber;
//    UInt_t          mcChannelNumber;
//    UInt_t          runNumber;
//    vector<float>   *el_charge;
//    vector<float>   *el_eta;
//    vector<float>   *el_phi;
//    vector<float>   *jet_GN2v01;
//    vector<float>   *jet_GN2v01_pb;
//    vector<float>   *jet_GN2v01_pc;
//    vector<float>   *jet_GN2v01_ptau;
//    vector<float>   *jet_GN2v01_pu;
//    vector<int>     *jet_TruthFlavouExtended;
//    vector<int>     *jet_truthflav;
//    vector<float>   *jet_eta;
//    vector<float>   *jet_phi;
//    vector<float>   *mu_charge;
//    vector<float>   *mu_eta;
//    vector<float>   *mu_phi;
//    Float_t         weight_pileup;
//    Float_t         weight_mc;
//    Float_t         weight_mc_NOSYS;
//    Float_t         weight_mc_93300; // weight for PDF4LHC
//    Float_t         weight_jvt_effSF;
//    Float_t         weight_leptonSF_tight;
   // Float_t         globalTriggerEffSF;
//    vector<float>   *el_e;
//    vector<float>   *el_pt;
//    vector<float>   *jet_e;
//    vector<float>   *jet_pt;
//    vector<float>   *mu_e;
//    vector<float>   *mu_pt;
//    Float_t         met_met;
//    Int_t nPV;
//    Float_t mu;
//    vector<int>    *mu_true_type;
//    vector<int>    *mu_true_origin;
//    vector<int>    *el_true_type;
//    vector<int>    *el_true_origin;
//    vector<int>    *HadronConeExclExtendedTruthLabelID;
//    vector<bool>   *mu_true_isPrompt ;
//    vector<bool>   *el_true_isPrompt;
//    vector<float>  *el_cl_eta;
//    vector<float>  *jet_m_jl;
//   bool           m_storeTracks=false;

#ifndef TreeReader_h
#define TreeReader_h

#include <TROOT.h>
#include <TChain.h>
#include <TSelector.h>
#include <TFile.h>
#include <TString.h>

// Header file for the classes stored in the TTree if any.
#include "string"
#include "vector"
using std::vector;
using std::string;

class TreeReader : public TSelector{
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   ULong64_t       eventNumber;
   UInt_t          mcChannelNumber = -1;
   UInt_t          runNumber;
   vector<float>   *el_charge;
   vector<float>   *el_eta;
   vector<float>   *el_phi;
   vector<float>   *jet_GN2v01;
   vector<float>   *jet_GN2v01_pb;
   vector<float>   *jet_GN2v01_pc;
   vector<float>   *jet_GN2v01_ptau;
   vector<float>   *jet_GN2v01_pu;
   vector<int>     *jet_TruthFlavouExtended;
   vector<int>     *jet_TruthFlavour;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *mu_charge;
   vector<float>   *mu_eta;
   vector<float>   *mu_phi;
   Float_t         weight_pileup = 1;
   Float_t         weight_mc = 1;
   // weight_mc_NOSYS and weight_mc_93300 should only be used by PDF4LHC systematics
   Float_t         weight_mc_NOSYS = 1;
   Float_t         weight_mc_93300 = 1;
   Float_t         weight_jvt_effSF = 1;
   Float_t         weight_leptonSF_tight = 1;
   Float_t         globalTriggerEffSF = 1;
   vector<float>   *el_e;
   vector<float>   *el_pt;
   vector<char>    *el_select_tight;
   vector<float>   *jet_e;
   vector<float>   *jet_pt;
   vector<char>    *jet_select_baselineJvt;
   vector<float>   *mu_e;
   vector<float>   *mu_pt;
   vector<char>    *mu_select_tight;
   Float_t         met_met;
   vector<float>   *PrimaryVerticesPosX;
   float           actualInteractionsPerCrossing;

   // To suit the needs of the analysis. Following parts are added.
   Int_t nPV;
   Float_t mu;

   // List of branches
   TBranch        *b_eventNumber;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_jet_GN2v01;   //!
   TBranch        *b_jet_GN2v01_pb;   //!
   TBranch        *b_jet_GN2v01_pc;   //!
   TBranch        *b_jet_GN2v01_ptau;   //!
   TBranch        *b_jet_GN2v01_pu;   //!
   TBranch        *b_jet_TruthFlavouExtended;   //!
   TBranch        *b_jet_TruthFlavour;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_weight_mc;   //!
   TBranch        *b_weight_mc_NOSYS;   //!
   TBranch        *b_weight_mc_93300;   //!
   TBranch        *b_weight_jvt_effSF;   //!
   TBranch        *b_weight_leptonSF_tight;   //!
   TBranch        *b_globalTriggerEffSF;   //!
   TBranch        *b_el_e;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_select_tight;   //!
   TBranch        *b_jet_e;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_select_baselineJvt;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_select_tight;   //!
   TBranch        *b_met_met;   //!
   TBranch        *b_PrimaryVerticesPosX;   //!
   TBranch        *b_actualInteractionsPerCrossing;   //!

   TreeReader(string systematic_name = "NOSYS");
   virtual ~TreeReader();
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);

   // To deal with systematics
   TString fSysSuffix = "NOSYS";
   template <class T> void SysAwareSetBranchAddress(const char *var_name, T *add, TBranch **ptr = nullptr);

   // TODO: these variables are missing from current TopCPToolkit
   vector<int>    *mu_true_type;
   vector<int>    *mu_true_origin;
   vector<int>    *el_true_type;
   vector<int>    *el_true_origin;
   vector<int>    *HadronConeExclExtendedTruthLabelID;
   vector<bool>   *mu_true_isPrompt ;
   vector<bool>   *el_true_isPrompt;
   vector<float>  *el_cl_eta;
   vector<int>    *jet_truthflav;

   // Extra variables
	std::vector<float> *jet_m_jl= new std::vector<float>();
   bool           m_storeTracks=false;

   void SelectAndSortJetsByPt();
   void SelectAndSortElectronByPt();
   void SelectAndSortMuonByPt();

   using TSelector::Begin;
   using TSelector::SlaveBegin;
   void initialize_m_ljs();
   // Called every time a loop on the tree starts, a convenient place to create your histograms.                                                                                                       
   virtual void    Begin();
   // Clled after Begin(), when on PROOF called only on the slave servers.                                                                                                                             
   virtual void    SlaveBegin();
   // called for each event, in this function you decide what to read and fill your histograms.                                                                                                        
   virtual Bool_t  Process(Long64_t entry);
   virtual void    SetOption(const char *option){fOption = option;}
   virtual void    SetObject(TObject *obj){fObject = obj;}
   virtual void    SetInputList(TList *input){fInput = input;}
   virtual TList   *GetOutputList() const{return fOutput;}
   // Called at the end of the loop on the tree, when on PROOF called only on the slave servers.                                                                                                       
   virtual void    SlaveTerminate(){};

   // called at the end of the loop on the tree, a convenient place to draw/fit your histograms.                                                                                                       
   virtual void    Terminate(){};

	// ClassDef(TreeReader,1);
};

#endif

#ifdef TreeReader_cxx
TreeReader::TreeReader(string systematic_name) : fChain(0) 
{
   fSysSuffix = systematic_name;
}

TreeReader::~TreeReader()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}


Int_t TreeReader::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t TreeReader::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
   }
   return centry;
}

void TreeReader::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   el_charge = 0;
   el_eta = 0;
   el_phi = 0;
   jet_GN2v01 = 0;
   jet_GN2v01_pb = 0;
   jet_GN2v01_pc = 0;
   jet_GN2v01_ptau = 0;
   jet_GN2v01_pu = 0;
   jet_TruthFlavouExtended = 0;
   jet_TruthFlavour = 0;
   jet_eta = 0;
   jet_phi = 0;
   mu_charge = 0;
   mu_eta = 0;
   mu_phi = 0;
   el_e = 0;
   el_pt = 0;
   el_select_tight = 0;
   jet_e = 0;
   jet_pt = 0;
   jet_select_baselineJvt = 0;
   mu_e = 0;
   mu_pt = 0;
   mu_select_tight = 0;
   PrimaryVerticesPosX = 0;
   mu_true_type = 0;
   mu_true_origin = 0;
   el_true_type = 0;
   el_true_origin = 0;
   HadronConeExclExtendedTruthLabelID = 0;
   mu_true_isPrompt  = 0;
   el_true_isPrompt = 0;
   el_cl_eta = 0;
   jet_truthflav = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
   fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
   fChain->SetBranchAddress("jet_GN2v01", &jet_GN2v01, &b_jet_GN2v01);
   fChain->SetBranchAddress("jet_GN2v01_pb", &jet_GN2v01_pb, &b_jet_GN2v01_pb);
   fChain->SetBranchAddress("jet_GN2v01_pc", &jet_GN2v01_pc, &b_jet_GN2v01_pc);
   fChain->SetBranchAddress("jet_GN2v01_ptau", &jet_GN2v01_ptau, &b_jet_GN2v01_ptau);
   fChain->SetBranchAddress("jet_GN2v01_pu", &jet_GN2v01_pu, &b_jet_GN2v01_pu);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
   fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("PrimaryVerticesPosX", &PrimaryVerticesPosX, &b_PrimaryVerticesPosX);
   fChain->SetBranchAddress("actualInteractionsPerCrossing", &actualInteractionsPerCrossing, &b_actualInteractionsPerCrossing);

   // systematics aware branches
   SysAwareSetBranchAddress("el_e", &el_e, &b_el_e);
   SysAwareSetBranchAddress("el_pt", &el_pt, &b_el_pt);
   SysAwareSetBranchAddress("el_select_tight", &el_select_tight, &b_el_select_tight);
   SysAwareSetBranchAddress("jet_e", &jet_e, &b_jet_e);
   SysAwareSetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   SysAwareSetBranchAddress("jet_select_baselineJvt", &jet_select_baselineJvt, &b_jet_select_baselineJvt);
   SysAwareSetBranchAddress("mu_e", &mu_e, &b_mu_e);
   SysAwareSetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   SysAwareSetBranchAddress("mu_select_tight", &mu_select_tight, &b_mu_select_tight);
   SysAwareSetBranchAddress("met_met", &met_met, &b_met_met);

   // MC only branches
   if (fChain->GetListOfBranches()->FindObject("mcChannelNumber")){
      fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
      fChain->SetBranchAddress("jet_TruthFlavouExtended", &jet_TruthFlavouExtended, &b_jet_TruthFlavouExtended);
      fChain->SetBranchAddress("jet_TruthFlavour", &jet_TruthFlavour, &b_jet_TruthFlavour);
      SysAwareSetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
      SysAwareSetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
      SysAwareSetBranchAddress("weight_jvt_effSF", &weight_jvt_effSF, &b_weight_jvt_effSF);
      SysAwareSetBranchAddress("weight_leptonSF_tight", &weight_leptonSF_tight, &b_weight_leptonSF_tight);
      SysAwareSetBranchAddress("globalTriggerEffSF", &globalTriggerEffSF, &b_globalTriggerEffSF);

      // Should only be used by PDF4LHC systematics
      if(fSysSuffix.First("GEN_MUR1_MUF1_PDF933")!=-1 && fSysSuffix.First("GEN_MUR1_MUF1_PDF93300")==-1){
         fChain->SetBranchAddress("weight_mc_NOSYS", &weight_mc_NOSYS, &b_weight_mc_NOSYS);
         if(fChain->GetBranch("weight_mc_GEN_MUR1_MUF1_PDF93300"))
            fChain->SetBranchAddress("weight_mc_GEN_MUR1_MUF1_PDF93300", &weight_mc_93300, &b_weight_mc_93300);
         else
            printf("WARNING: weight_mc_93300 branch not found\n");
      }
   }
}

template <class T> void TreeReader::SysAwareSetBranchAddress(const char *var_name, T *add, TBranch **ptr){
   // check if this var is suffixed with fSysSuffix
   TString var_sys_branch_name = TString::Format("%s_%s", var_name, fSysSuffix.Data());
   if(fChain->GetBranch(var_sys_branch_name)){
      fChain->SetBranchAddress(var_sys_branch_name, add, ptr);
      printf("DEBUG SYST aware var  : Set %s as %s\n", var_name, var_sys_branch_name.Data());
   }else{
      fChain->SetBranchAddress(TString::Format("%s_NOSYS", var_name), add, ptr);
      printf("DEBUG SYST nominal var: Set %s as %s\n", var_name, TString::Format("%s_NOSYS", var_name).Data());
   }
}

#endif // #ifdef TreeReader_cxx
