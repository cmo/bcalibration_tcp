#include <iostream>
#include "macros/cxxopts.hpp"
#include "ConfigManager.h"


void ConfigManager::ParseArgs(int argc, char *argv[]){
  try
  {
    cxxopts::Options options(argv[0], " - example command line options");
    options.positional_help("");
    options.add_options()
    ("i,input", "Input", cxxopts::value<std::string>(), "input_file")
    ("o,output", "outputFilename", cxxopts::value<std::string>()->default_value(""), "output_file") 
    ("save_fit_input", "save the inputs for the fit later on.", cxxopts::value<bool>(m_save_fits))
    ("syst", "name of Systematic", cxxopts::value<std::string>()->default_value("NOSYS"), "systName")
    ("f,NPLeptons_file", "NPLeptons_file", cxxopts::value<std::string>()->default_value(""), "NPLeptons_file")
    ("l,lumiWeight", "lumiWeight", cxxopts::value<double>(m_w_lumi), "lumiWeight")
    ("data", "run over data", cxxopts::value<bool>(m_is_data))
    ("boot_strap", "save boot strap weights. takes sapce and time!", cxxopts::value<bool>(m_boot_strap))
    ("bsbin", "choose which  boot strap weights to save!", cxxopts::value<int>(m_bsbin))
    ("run_fakes", "run over fakes", cxxopts::value<bool>(m_run_fakes))
    ("apply_lf_calib", "apply_lf_calib", cxxopts::value<bool>(m_apply_lf_calib))
    ("jet_collection", "jet_collection", cxxopts::value<std::string>(m_jet_collection)->default_value("AntiKt4EMPFlowJets"))
    ("jet_collection_lf", "jet_collection_lf", cxxopts::value<std::string>(m_jet_collection_lightjets)->default_value("AntiKt4EMPFlowJets"))
    ("hadronization", "hadronization use for mc/mc sf", cxxopts::value<int>(m_hadronization), "int")
    ("save_bin_means_for_sr_histo", "needs space, but nice for nominal files.", cxxopts::value<bool>(m_save_bin_means_for_sr_histo))
    ("h, help", "Print help");
    
    auto results = options.parse(argc, argv);

    m_input_file = results["input"].as<std::string>();
    m_output_file = results["output"].as<std::string>();
    m_systematic_name = results["syst"].as<std::string>();
    m_NPLepton_file = results["NPLeptons_file"].as<std::string>();

    if (results.count("input")) {
      std::cout << "Input file = " << m_input_file << std::endl;
    } else {
      std::cout << "please give input file!" << std::endl;
      std::cout << options.help({ "" }) << std::endl;
      exit(0);
    }

    std::cout << "Output file = " << m_output_file << std::endl;
    std::cout << "systematic_name = " << m_systematic_name << std::endl;
    std::cout << "NPLeptons_file = " << m_NPLepton_file << std::endl;
    std::cout << "w_lumi = " << m_w_lumi << std::endl;
    m_jet_pt_bins = {20., 40., 60., 140., 250., 400.};//{20., 30., 40., 60., 85., 110., 140., 175., 250., 400.};
    // m_jet_pt_bins = {20., 40., 60., 140., 400.};//{20., 30., 40., 60., 85., 110., 140., 175., 250., 400.};
    m_num_pt_bins = m_jet_pt_bins.size() - 1;
    std::cout << "num_pt_bins = " << m_num_pt_bins << std::endl;
    std::cout << "Pt bins: " << std::endl;
    for (int i = 0; i < m_num_pt_bins; i++)
    {
      std::cout << "  bin " << i << ": " << m_jet_pt_bins[i] << " - " << m_jet_pt_bins[i + 1] << std::endl;
    }
  } catch (const cxxopts::exceptions::parsing& err){
		std::cout << "Error parsing options: " << err.what() << std::endl;
		exit(1);
	}
}