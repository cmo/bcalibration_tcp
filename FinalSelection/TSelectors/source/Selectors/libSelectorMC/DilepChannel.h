// DilepChannel.h
#pragma once

#include "TreeReader.h"
#include "VariableContainer.h"
#include "JetContainer.h"
#include "HistsForFitContainer.h"
#include "JLMinMSumCombination.h"
#include "JLMinMSubCombination.h"
#include "JLdRCombination.h"
#include "XXContainer.h"
#include "VariablePtContainer.h"
#include "VariablePtBinsContainer.h"

class DiLepChannel
{
public:
  std::string name;
  bool m_save_fits = false;
  int n_jets;
  int n_el;    
  int n_mu;
  int n_lep;
  int m_n_boot_strap_min;
  int m_n_boot_strap_max;
  bool m_storeTracks;
  bool m_use_pt_bins_as_eta;
  bool m_data;
  std::string m_jet_collection="";
  std::string m_jet_collection_lightjets="";
  bool m_apply_lf_calib = true;

  TDirectory *channel_dir; 
  TH1F *cutflow; //!  

  Variable_Container *h_met= NULL;
  Variable_Container *h_mu= NULL;
  Variable_Container *h_mu_shifted= NULL;
  Variable_Container *h_nPV= NULL;

  Variable_Container *h_ip2dtrk_lead= NULL;
  Variable_Container *h_ip3dtrk_lead= NULL;
  Variable_Container *h_ip2dnegtrk_lead= NULL;
  Variable_Container *h_ip3dnegtrk_lead= NULL;
  Variable_Container *h_jffliptrk_lead= NULL;
  Variable_Container *h_sv1fliptrk_lead= NULL;
  Variable_Container *h_jftrk_lead= NULL;
  Variable_Container *h_sv1trk_lead= NULL;
  Variable_Container *h_ip2dtrk_sublead= NULL;
  Variable_Container *h_ip3dtrk_sublead= NULL;
  Variable_Container *h_ip2dnegtrk_sublead= NULL;
  Variable_Container *h_ip3dnegtrk_sublead= NULL;
  Variable_Container *h_jffliptrk_sublead= NULL;
  Variable_Container *h_sv1fliptrk_sublead= NULL;
  Variable_Container *h_jftrk_sublead= NULL;
  Variable_Container *h_sv1trk_sublead= NULL;
  Variable_pt_Container *h_pt_both= NULL;
  Variable_pt_Container *h_pt_eta_lead= NULL;
  Variable_pt_Container *h_pt_phi_lead= NULL;
  Variable_pt_Container *h_pt_ip2dtrk_lead= NULL;
  Variable_pt_Container *h_pt_ip3dtrk_lead= NULL;
  Variable_pt_Container *h_pt_eta_sublead= NULL;
  Variable_pt_Container *h_pt_phi_sublead= NULL;
  Variable_pt_Container *h_pt_ip2dtrk_sublead= NULL;
  Variable_pt_Container *h_pt_ip3dtrk_sublead= NULL;
  // Hist_for_fit_Container *hist_for_fit_DL1d_fixedCut= NULL;
  // Hist_for_fit_Container *hist_for_fit_GN1_fixedCut= NULL;
  Hist_for_fit_Container *hist_for_fit_GN2_fixedCut= NULL;
  JL_min_m_sum_Combination *jl_min_m_sum_Combination= NULL;
  JL_min_m_sub_Combination *jl_min_m_sub_Combination= NULL;
  JL_closest_dR_Combination *jl_closest_dR_Combination= NULL;

  Variable_ptBins_Container *m_jl_max= NULL;
  Variable_ptBins_Container *m_jl_sqsum= NULL;
  Variable_ptBins_Container *m_jl_sum= NULL;
  XX_Container *ll_con= NULL;

  std::vector<jet_Container *> jets;
  std::vector<Variable_Container *> el_pt;
  std::vector<Variable_Container *> el_eta;
  std::vector<Variable_Container *> el_cl_eta;
  std::vector<Variable_Container *> el_phi;
  std::vector<Variable_Container *> mu_pt;
  std::vector<Variable_Container *> mu_eta;
  std::vector<Variable_Container *> mu_phi;
  std::vector<Variable_Container *> el_true_type;
  std::vector<Variable_Container *> el_true_origin;
  std::vector<Variable_Container *> mu_true_type;
  std::vector<Variable_Container *> mu_true_origin;
  
 public:
  // Constructor
  DiLepChannel(std::string name, std::vector<std::string> cutFlowLabels, int n_jets, int n_el, int n_mu, TDirectory *f_out, bool data, std::vector<std::string> eflavours, int n_boot_strap_min, int n_boot_strap_max, bool storeTracks, bool use_pt_bins_as_eta,int hadronization, const double m_jl_CutValue, std::string bTagSystName, bool m_save_fits = false, bool save_cr_hist = false,bool save_bin_means_for_sr_histo = false, std::string jet_collection="", std::string jet_collection_lightjets="",bool apply_lf_calib = true);
  
  // Destructor
  virtual ~DiLepChannel();
  
  // Adds to cutflow
  void addToCutflow(int bin, double weight);
  
  // Saves TFile
  void Save(TFile* file);

  // Adds event
  void AddEvent(TreeReader *selector, double weight, Long64_t entry);

  // Calculates Discriminant by hand due to issues found in some AT releases
  std::vector<float> DiscriminantHand(std::vector<float> pb,std::vector<float> pc,std::vector<float> pu, double fraction_c, double fraction_tau=-999.);

  ClassDef(DiLepChannel,1);
};
