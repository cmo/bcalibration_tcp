// PtBinMeanContainer.cxx

#include "PtBinMeanContainer.h"

ClassImp(Pt_bin_mean_container)

Pt_bin_mean_container::Pt_bin_mean_container(std::string name, std::string taggerName, TDirectory* chan_dir, int n_pt_bins, const Double_t* pt_bins, int n_tagger, const Double_t* tagger_bins, bool data, std::vector<std::string> eflavours, std::string pt_label_jet1, std::string pt_label_jet2)
{
  // this->chan_dir=chan_dir;
  // dir = chan_dir->mkdir(name.c_str());
  // dir->cd();
  bin_edges=pt_bins;
  this->m_name=name;
  this->n_pt_bins=n_pt_bins;
  this->data = data;
  for (int i=0; i<n_pt_bins; i++)
    {
      for (int j=0; j<=i; j++)
	{
	  std::string bin_name= name+"_pt1_"+std::to_string(i+1)+"_pt2_"+std::to_string(j+1);
	  v_single_pt_bins.push_back(new  Pt_bin_single(bin_name, taggerName, 10, bin_edges[i], bin_edges[i+1], bin_edges[j], bin_edges[j+1], n_tagger, tagger_bins, data, eflavours, pt_label_jet1, pt_label_jet2) );
	}
    }
  //  chan_dir->cd();
}

void Pt_bin_mean_container::Fill(Double_t* where_vector, double weight,  std::vector<int> *jet_truthflav)
{
  bool filled = false;
  unsigned int i = 0;
  do{
    filled = v_single_pt_bins.at(i)->Fill(where_vector, weight, jet_truthflav);
    i++;
  }while ( (!filled) && (i<v_single_pt_bins.size()) );
}

void Pt_bin_mean_container::Write()
{

  this->chan_dir=gDirectory;
  dir=gDirectory->mkdir(m_name.c_str());

  dir->cd();
  for (const auto &s_pt_bin : v_single_pt_bins)
    {
      s_pt_bin->Write();
    }
  chan_dir->cd();
  
}
