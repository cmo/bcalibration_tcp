// finalSelectionMC.cxx

#include "finalSelectionMC.h"
#include "libSelectorMC/ConfigManager.h"

using namespace std;
using std::vector;
//using CP::SystematicCode;

// Constructor
finalSelectionMC::finalSelectionMC(string systematic_name) : fChain(0)
{
    m_treeReader = new TreeReader(systematic_name);
}

// Destrcutor
finalSelectionMC::~finalSelectionMC()
{
}

std::string finalSelectionMC::getEventFlav(std::vector<int> *jet_truthflav)
{
    std::string eventFlav = "";
    if (data)
    {
        eventFlav = "data";
    }
    else
    {
        for (int i = 0; i < 2; i++)
        {
            if (jet_truthflav->at(i) == 4)
                eventFlav = eventFlav + "c";
            else if (jet_truthflav->at(i) == 5)
                eventFlav = eventFlav + "b";
            else
                eventFlav = eventFlav + "l"; //this is also tau jets!
        }
    }
    return eventFlav;
}

void finalSelectionMC::Begin()
{
    f_out = gDirectory;
    std::string blah = f_out->GetPath();
    std::cout << "Printing f_out: " << std::endl;
    num_events = 0;

    string input_file = ConfigManager::Instance()->input_file();
    string output_file = ConfigManager::Instance()->output_file();
    string systematic_name = ConfigManager::Instance()->systematic_name();
    string NPLeptons_file = ConfigManager::Instance()->NPLepton_file();
    jet_collection = ConfigManager::Instance()->jet_collection();
    jet_collection_lightjets = ConfigManager::Instance()->jet_collection_lightjets();
    w_lumi = ConfigManager::Instance()->weight_lumi();
    hadronization = ConfigManager::Instance()->hadronization();
    bsbin = ConfigManager::Instance()->bsbin();
    data = ConfigManager::Instance()->is_data();
    run_fakes = ConfigManager::Instance()->run_fakes();
    boot_strap = ConfigManager::Instance()->boot_strap();
    apply_lf_calib = ConfigManager::Instance()->apply_lf_calib();
    save_fits = ConfigManager::Instance()->save_fits();
    save_bin_means_for_sr_histo = ConfigManager::Instance()->save_bin_means_for_sr_histo();

    if (!boot_strap)
    {
        m_n_boot_strap_max = 0;
    } else
    {
        m_n_boot_strap_min=bsbin > 0 ? 10*(bsbin-1):0;
        m_n_boot_strap_max=10*bsbin;
        std::cout << "m_n_boot_strap_max = " << m_n_boot_strap_max << std::endl;
    }
    if (run_fakes)
    {
        std::cout << "running fake estimation!. " << std::endl;
    }
    if (save_fits)
    {
        std::cout << "saving the input histogramms for the fits. " << std::endl;
    }

    // Initialize the tree
    std::cout << "Beginning to initialise TreeReader" << std::endl;
    m_treeReader->Begin();
    std::cout << "Finished begin method" << std::endl;

    if ((NPLeptons_file != "") && (!data))
    {
        TFile *npleptonsfile = TFile::Open(NPLeptons_file.c_str());
        if (!npleptonsfile)
        {
            cout << "File " << NPLeptons_file.c_str() << " not found!" << endl;
            return;
        }
        TH1D *npleptonshist = (TH1D *)npleptonsfile->Get("hist_SS_data_minus_MC2PL_over_MC1PL");
        if (!npleptonshist)
        {
            cout << "Histogram 'hist_SS_data_minus_MC2PL_over_MC1PL' could not be found in " << NPLeptons_file.c_str() << endl;
            return;
        }
        cout << "Data/MC correction factors for OS emu events with a non-prompt electron will be applied from file " << NPLeptons_file.c_str() << endl;
        NPel_bin1 = npleptonshist->GetBinContent(1);
        NPel_bin2 = npleptonshist->GetBinContent(2);
        NPel_bin3 = npleptonshist->GetBinContent(3);
        correctFakes = true;
        npleptonsfile->Close();
    }


    std::cout << "Progressed this far" << std::endl;
    vector<string> cutFlowLabels;
    jet_multiplicity = new TH1F("h_jet_multiplicity", "Jet Multiplicity before jet cut", 10, 0, 10);
    std::cout << "Jet multiplicty th1f created" << std::endl;

    // emu_OS_J2 channel
    cutFlowLabels = {"inputs", "2 leptons", "emu Channel", "OS", "2 or 3 Jets", "2 Jets"};
    std::cout << "Begin emu_OS_J2 channel" << std::endl;
    std::cout << "cutFlowLabels[0]: " << cutFlowLabels[0] << std::endl;
    std::cout << "f_out: " << &f_out << std::endl;
    std::cout << "data: " << data << std::endl;
    std::cout << "eflavours[0]: " << eflavours[0] << std::endl;
    std::cout << "m_n_boot_strap_min: " << m_n_boot_strap_min << std::endl;
    std::cout << "m_n_boot_strap_max: " << m_n_boot_strap_max << std::endl;
    std::cout << "m_treeReader->m_storeTracks: " << m_treeReader->m_storeTracks << std::endl;
    std::cout << "use_pt_bins_as_eta: " << use_pt_bins_as_eta << std::endl;
    std::cout << "hadronization" << hadronization << std::endl;
    std::cout << "m_jl_CutValue: " << m_jl_CutValue << std::endl;
    std::cout << "bTagSystName: " << bTagSystName << std::endl;
    std::cout << "save_fits: " << save_fits << std::endl;
    std::cout << "jet_collection: " << jet_collection << std::endl;
    std::cout << "jet_collection_lightjets: " << jet_collection_lightjets << std::endl;
    emu_OS_J2= new DiLepChannel( "emu_OS_J2",cutFlowLabels,2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, save_fits,true,save_bin_means_for_sr_histo,jet_collection,jet_collection_lightjets,apply_lf_calib);
    std::cout << "Finished emu_OS_J2" << std::endl;

    cutFlowLabels.push_back("1 lep_comb_cut");
    cutFlowLabels.push_back("2 lep_comb_cut");
    //this is a channel we potentially want to fit later, so lets save the histograms for the fits!
    emu_OS_J2_m_lj_cut = new DiLepChannel("emu_OS_J2_m_lj_cut", cutFlowLabels, 2, 1, 1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, save_fits,false,save_bin_means_for_sr_histo,jet_collection,jet_collection_lightjets,apply_lf_calib);
    emu_OS_J2_CR_bl= new DiLepChannel( "emu_OS_J2_CR_bl",cutFlowLabels, 2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, save_fits,false,false,jet_collection,jet_collection_lightjets,apply_lf_calib);
    emu_OS_J2_CR_lb= new DiLepChannel( "emu_OS_J2_CR_lb",cutFlowLabels, 2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, save_fits,false,false,jet_collection,jet_collection_lightjets,apply_lf_calib);
    emu_OS_J2_CR_ll= new DiLepChannel( "emu_OS_J2_CR_ll",cutFlowLabels, 2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, save_fits,false,false,jet_collection,jet_collection_lightjets,apply_lf_calib);

    std::cout << "Finished begin method" << std::endl;
}


void finalSelectionMC::SlaveBegin()
{
    // The SlaveBegin() function is called after the Begin() function.
    // When running with PROOF SlaveBegin() is called on each slave server.
    // The tree argument is deprecated (on PROOF 0 is passed).

    TString option = GetOption();
}


Bool_t finalSelectionMC::Process(Long64_t entry)
{
    // The Process() function is called for each entry in the tree (or possibly
    // keyed object in the case of PROOF) to be processed. The entry argument
    // specifies which entry in the currently loaded tree is to be processed.
    // It can be passed to either finalSelectionMC::GetEntry() or TBranch::GetEntry()
    // to read either all or the required parts of the data. When processing
    // keyed objects with PROOF, the object is already loaded and is available
    // via the fObject pointer.
    //
    // This function should contain the "body" of the analysis. It can contain
    // simple or elaborate selection criteria, run algorithms on the data
    // of the event and typically fill histograms.
    //
    // The processing can be stopped by calling Abort().
    //
    // Use fStatus to set the return value of TTree::Process().
    //
    // The return value is currently not used.

    m_treeReader->Process(entry);
    if (!(num_events % 10000))
        cout << "Processing Event: " << num_events + 1 << endl << std::flush;
    num_events++;

    int n_el = m_treeReader->el_charge->size();
    int n_mu = m_treeReader->mu_charge->size();

    TLorentzVector lep4vec;
    TLorentzVector seclep4vec;
    double weight = 1;
    if (!data)
    {
        weight = w_lumi;

        // weight_mc
        weight = weight * m_treeReader->weight_mc;
        // weight_pileup
        weight = weight * m_treeReader->weight_pileup;

        // weight_leptonSF_tight
        weight = weight * m_treeReader->weight_leptonSF_tight;
        
        // weight_jvt_effSF
        weight = weight * m_treeReader->weight_jvt_effSF;

        // globalTriggerEffSF
        weight = weight * m_treeReader->globalTriggerEffSF;

        // Special treat for PDF4LHC
        if (ConfigManager::Instance()->systematic_name().find("GEN_MUR1_MUF1_PDF933") != std::string::npos &&
            ConfigManager::Instance()->systematic_name().find("GEN_MUR1_MUF1_PDF93300") == std::string::npos)
        {
            weight = weight * m_treeReader->weight_mc_NOSYS / m_treeReader->weight_mc_93300;
        }
    }

    int nNonpromptel = 0, nNonpromptmu = 0;

    emu_OS_J2->addToCutflow(1, weight);
    emu_OS_J2_m_lj_cut->addToCutflow(1, weight);
    emu_OS_J2_CR_bl->addToCutflow(1, weight);
    emu_OS_J2_CR_lb->addToCutflow(1, weight);
    emu_OS_J2_CR_ll->addToCutflow(1, weight);


    //2 lepton cut:
    if (n_el + n_mu != 2)
        return kTRUE;

    emu_OS_J2->addToCutflow(2, weight);
    emu_OS_J2_m_lj_cut->addToCutflow(2, weight);
    emu_OS_J2_CR_bl->addToCutflow(2, weight);
    emu_OS_J2_CR_lb->addToCutflow(2, weight);
    emu_OS_J2_CR_ll->addToCutflow(2, weight);


    int n_jet = m_treeReader->jet_pt->size();
    if (n_jet < 9)
        jet_multiplicity->Fill(n_jet, weight);
    else
        jet_multiplicity->Fill(9, weight);

    //el mu channel *************************************************************
    if ((n_el == 1) && (n_mu == 1))
    {
        emu_OS_J2->addToCutflow(3, weight);
        emu_OS_J2_m_lj_cut->addToCutflow(3, weight);
        emu_OS_J2_CR_bl->addToCutflow(3, weight);
        emu_OS_J2_CR_lb->addToCutflow(3, weight);
        emu_OS_J2_CR_ll->addToCutflow(3, weight);


        lep4vec.SetPtEtaPhiM(m_treeReader->mu_pt->at(0), m_treeReader->mu_eta->at(0), m_treeReader->mu_phi->at(0), 0);
        seclep4vec.SetPtEtaPhiM(m_treeReader->el_pt->at(0), m_treeReader->el_eta->at(0), m_treeReader->el_phi->at(0), 0);
        double mll = (lep4vec + seclep4vec).M();

        if (mll < m_ll_CutValue*1000) {
            return kTRUE;
        } 

        if ((run_fakes || correctFakes) && !data)
        {
            if (!m_treeReader->el_true_isPrompt->at(0)) {
                nNonpromptel++;
                if (m_treeReader->el_true_type->at(0) == 6 && m_treeReader->el_true_origin->at(0) == 13) {	//these are Z->mumu events where a muon is reconstructed as an electron! - consider as prompt!
                    nNonpromptel--;
                }
            }
            if (!m_treeReader->mu_true_isPrompt->at(0)) {
                nNonpromptmu++;
            }
        }


        if ((m_treeReader->el_charge->at(0) * m_treeReader->mu_charge->at(0)) < 0)
        {
            emu_OS_J2->addToCutflow(4, weight);
            emu_OS_J2_m_lj_cut->addToCutflow(4, weight);
            emu_OS_J2_CR_bl->addToCutflow(4, weight);
            emu_OS_J2_CR_lb->addToCutflow(4, weight);
            emu_OS_J2_CR_ll->addToCutflow(4, weight);
        }
        else
        {
            return kTRUE;
        }

        //Only OS events left now:
        if (n_jet < 2 || n_jet > 3) return kTRUE;
        
        if (n_jet == 2){
            if (m_treeReader->jet_m_jl->at(0)/1000. < min_m_jl_CutValue || m_treeReader->jet_m_jl->at(1) / 1000. < min_m_jl_CutValue) {
                return kTRUE;
            }
        }
        emu_OS_J2->addToCutflow(5, weight);
        emu_OS_J2_m_lj_cut->addToCutflow(5, weight);
        emu_OS_J2_CR_bl->addToCutflow(5, weight);
        emu_OS_J2_CR_lb->addToCutflow(5, weight);
        emu_OS_J2_CR_ll->addToCutflow(5, weight);

        if (n_jet == 2 ){
            if (m_treeReader->jet_m_jl->at(0) / 1000. < min_m_jl_CutValue || m_treeReader->jet_m_jl->at(1) / 1000. < min_m_jl_CutValue){
                return kTRUE; 
            }			
            if (correctFakes && nNonpromptel == 1 && nNonpromptmu == 0)
            {
                if (m_treeReader->el_pt->at(0) / 1000. < 150.)
                    weight *= NPel_bin1;
                else if (m_treeReader->el_pt->at(0) / 1000. >= 150. && m_treeReader->el_pt->at(0) / 1000. < 300.)
                    weight *= NPel_bin2;
                else
                    weight *= NPel_bin3;
            }
            emu_OS_J2->addToCutflow(6, weight);
            emu_OS_J2_m_lj_cut->addToCutflow(6, weight);
            emu_OS_J2_CR_bl->addToCutflow(6, weight);
            emu_OS_J2_CR_lb->addToCutflow(6, weight);
            emu_OS_J2_CR_ll->addToCutflow(6, weight);
            emu_OS_J2->AddEvent(m_treeReader, weight, entry);

            //lets try the lj combi cut
            if (m_treeReader->jet_m_jl->at(0) / 1000. < m_jl_CutValue)
            {
                emu_OS_J2_m_lj_cut->addToCutflow(7, weight);
                emu_OS_J2_CR_bl->addToCutflow(7, weight);

                if (m_treeReader->jet_m_jl->at(1) / 1000. < m_jl_CutValue)
                {
                    emu_OS_J2_m_lj_cut->addToCutflow(8, weight);
                    emu_OS_J2_m_lj_cut->AddEvent(m_treeReader, weight, entry);
                }else{
                    emu_OS_J2_CR_bl->addToCutflow(8, weight);
                    emu_OS_J2_CR_bl->AddEvent(m_treeReader, weight, entry);
                }
            }
            else
            {
                if (m_treeReader->jet_m_jl->at(1) / 1000. < m_jl_CutValue)
                {
                    emu_OS_J2_CR_lb->AddEvent(m_treeReader, weight, entry);
                }
                else
                {
                    emu_OS_J2_CR_ll->AddEvent(m_treeReader, weight, entry);
                }
            }
        }
    }

    return kTRUE;
}

void finalSelectionMC::SlaveTerminate()
{
    // The SlaveTerminate() function is called after all entries or objects
    // have been processed. When running with PROOF SlaveTerminate() is called
    // on each slave server.
}

void finalSelectionMC::Terminate()
{
    // The Terminate() function is the last function to be called during
    // a query. It always runs on the client, it can be used to present
    // the results graphically or save the results to file.
    std::cout << "ran over " << num_events << " events" << std::endl;
    TFile* file=TFile::Open(output.c_str(),"RECREATE");
    file->cd();
    jet_multiplicity->Write();
    // cout<<"wrote multiplicity" << endl;
    TVectorD *lumi_weight;
    lumi_weight = new TVectorD(1);
    lumi_weight[0][0] = w_lumi;
    lumi_weight->Write("applied_lumi_weight");

    emu_OS_J2->Save(file);
    emu_OS_J2_m_lj_cut->Save(file);
    emu_OS_J2_CR_ll->Save(file);
    emu_OS_J2_CR_bl->Save(file);
    emu_OS_J2_CR_lb->Save(file);

    std::cout << "TSelector: End." << endl;
    file->Close();
    std::cout << " Closing the TFile with name " << output.c_str()<<std::endl;
}
