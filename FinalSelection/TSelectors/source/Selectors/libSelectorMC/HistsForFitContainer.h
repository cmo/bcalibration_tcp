// HistsForFitContainer.h

#pragma once

#include "VariableContainer.h"
#include "BTagToolContainer.h"
#include "PtBinMeanContainer.h"
#include <vector>
#include <string>
#include <TDirectory.h>
#include <TDirectory.h>
#include <TH2D.h>
#include <THnSparse.h>
// Potentially:
// #include <THnSparseI.h>
// #include <THnSparseD.h>

class Hist_for_fit_Container
{
public:
	std::string name;
	int n_jets;
	bool m_data;
	std::vector<std::string> eflavours;
	bool m_save_bin_means_for_sr_histo=true;
	int m_n_boot_strap_min;
	int m_n_boot_strap_max;
	bool m_use_pt_bins_as_eta;
	bool m_apply_lf_calib = true;
	std::string m_pt_label_jet1 = "p_{T,1} [GeV]";
	std::string m_pt_label_jet2 = "p_{T,2} [GeV]";
	std::string m_pt_label_single = "p_{T} [GeV]";
	
	std::map<std::string, std::vector<THnSparseD *>> sr_histograms;
	Pt_bin_mean_container* bin_means_for_sr_histo; 
	Variable_Container* j1_sf_weight;
	Variable_Container* j2_sf_weight;
	std::map<std::string, std::vector<THnSparseD *>> cr_histograms;
	//same histograms for unweighted events, in order to be able to see how many events per bin we have in those histograms.
	std::map<std::string, THnSparseI *> sr_histogram_uw;
	std::map<std::string, THnSparseI *> cr_histogram_uw;
	TH2D *h_l_jet;
	TH2D *h_b_jet;
	bool split_cr_regions;
	BtagTool_Container *btagtoolCont;

public:
	// Constructor
	Hist_for_fit_Container(std::string name, int n_jets, const char *taggerName, const char *cutName, TDirectory* chan_dir, bool data, std::vector<std::string> eflavours, int n_boot_strap_min, int n_boot_strap_max, bool use_pt_bins_as_eta,int hadronization, const double m_jl_CutValue, std::string bTagSystName, bool split_cr_regions,bool save_bin_means_for_sr_histo,std::string jet_collection="def",std::string jet_collection_lightjets="def",bool apply_lf_calib=true);
	
	// Destructor
	~Hist_for_fit_Container();

	// Writes to something
	void Write();

	// Adds Event
	void addEvent(std::vector<float> *jet_tag_weight, std::vector<float> *jet_pt, std::vector<float> *jet_eta, std::vector<float> *jet_m_jl, double weight, std::vector<int> *jet_truthflav, std::vector<int> *weight_poisson);

	// Gets event flavour
	std::string getEventFlav(std::vector<int> *jet_truthflav);
	ClassDef(Hist_for_fit_Container,1);
};
