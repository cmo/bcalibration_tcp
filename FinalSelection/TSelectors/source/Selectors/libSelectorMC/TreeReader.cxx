#define TreeReader_cxx
#include "TreeReader.h"
#include <TLorentzVector.h>
#include <numeric>  



ClassImp(TreeReader)
void TreeReader::initialize_m_ljs()
{
    //lets try the lj combi cut
    TLorentzVector lep_4vec[2];
    TLorentzVector jet_4vec[2];

    //clear the vectors in the this:
    this->jet_m_jl->clear();
    if (this->jet_pt->size() != 2)
        return;
    for (uint i = 0; i < this->jet_pt->size(); i++)
        jet_4vec[i].SetPtEtaPhiE(this->jet_pt->at(i), this->jet_eta->at(i), this->jet_phi->at(i), this->jet_e->at(i));
    if ((this->el_pt->size() == 2) && (this->mu_pt->size() == 0))
    {
        lep_4vec[0].SetPtEtaPhiM(this->el_pt->at(0), this->el_eta->at(0), this->el_phi->at(0), 0);
        lep_4vec[1].SetPtEtaPhiM(this->el_pt->at(1), this->el_eta->at(1), this->el_phi->at(1), 0);
    }
    else if ((this->el_pt->size() == 1) && (this->mu_pt->size() == 1))
    {
        lep_4vec[0].SetPtEtaPhiM(this->el_pt->at(0), this->el_eta->at(0), this->el_phi->at(0), 0);
        lep_4vec[1].SetPtEtaPhiM(this->mu_pt->at(0), this->mu_eta->at(0), this->mu_phi->at(0), 0);
    }
    else if ((this->el_pt->size() == 0) && (this->mu_pt->size() == 2))
    {
        lep_4vec[0].SetPtEtaPhiM(this->mu_pt->at(0), this->mu_eta->at(0), this->mu_phi->at(0), 0);
        lep_4vec[1].SetPtEtaPhiM(this->mu_pt->at(1), this->mu_eta->at(1), this->mu_phi->at(1), 0);
    }
    double m_l1_j1, m_l2_j2, m_l1_j2, m_l2_j1;
    m_l1_j1 = (lep_4vec[0] + jet_4vec[0]).M2();
    m_l1_j2 = (lep_4vec[0] + jet_4vec[1]).M2();
    m_l2_j1 = (lep_4vec[1] + jet_4vec[0]).M2();
    m_l2_j2 = (lep_4vec[1] + jet_4vec[1]).M2();
    if ((m_l1_j1 + m_l2_j2) < (m_l1_j2 + m_l2_j1))
    {
        //so m_l1_j1 and m_l2_j2 right combinations
        this->jet_m_jl->push_back(sqrt(m_l1_j1));
        this->jet_m_jl->push_back(sqrt(m_l2_j2));
    }
    else
    {
        //so m_l1_j2 and m_l2_j1 right combinations. Be carefull with the order now!
        this->jet_m_jl->push_back(sqrt(m_l2_j1));
        this->jet_m_jl->push_back(sqrt(m_l1_j2));
    }
}

void TreeReader::Begin()
{
}

void TreeReader::SlaveBegin()
{
    // The SlaveBegin() function is called after the Begin() function.
    // When running with PROOF SlaveBegin() is called on each slave server.
    // The tree argument is deprecated (on PROOF 0 is passed).

    TString option = GetOption();
}

Bool_t TreeReader::Process(Long64_t entry)
{
    // The Process() function is called for each entry in the tree (or possibly
    // keyed object in the case of PROOF) to be processed. The entry argument
    // specifies which entry in the currently loaded tree is to be processed.
    // It can be passed to either finalSelectionMC::GetEntry() or TBranch::GetEntry()
    // to read either all or the required parts of the data. When processing
    // keyed objects with PROOF, the object is already loaded and is available
    // via the fObject pointer.
    //
    // This function should contain the "body" of the analysis. It can contain
    // simple or elaborate selection criteria, run algorithms on the data
    // of the event and typically fill histograms.
    //
    // The processing can be stopped by calling Abort().
    //
    // Use fStatus to set the return value of TTree::Process().
    //
    // The return value is currently not used.

    fChain->GetTree()->GetEntry(entry);

    // rename the variables
    jet_truthflav = jet_TruthFlavour;
    el_cl_eta = el_eta;
    HadronConeExclExtendedTruthLabelID = jet_TruthFlavouExtended;

    // TODO: below are not correctly implemented
    nPV=PrimaryVerticesPosX->size();
    mu = actualInteractionsPerCrossing;
    
    delete mu_true_type;
    mu_true_type = new std::vector<int>(mu_eta->size(), 6);
    delete mu_true_origin;
    mu_true_origin = new std::vector<int>(mu_eta->size(), 43);
    delete mu_true_isPrompt;
    mu_true_isPrompt = new std::vector<bool>(mu_eta->size(), true);
    delete el_true_type;
    el_true_type = new std::vector<int>(el_eta->size(), 2);
    delete el_true_origin;
    el_true_origin = new std::vector<int>(el_eta->size(), 43);
    delete el_true_isPrompt;
    el_true_isPrompt = new std::vector<bool>(el_eta->size(), true);

    // Sort the jets, electrons and muons by Pt
    SelectAndSortJetsByPt();
    SelectAndSortElectronByPt();
    SelectAndSortMuonByPt();

    if(this->el_pt->size() + this->mu_pt->size() >=2 )
        this->initialize_m_ljs();
    return kTRUE;
}


void TreeReader::SelectAndSortJetsByPt(){
    // Sort jet indices by Pt
    std::vector<int> jet_idx(jet_pt->size());
    std::iota(jet_idx.begin(), jet_idx.end(), 0);
    std::sort(jet_idx.begin(), jet_idx.end(), [&](int i, int j) { return jet_pt->at(i) > jet_pt->at(j); });

    // Define sorted variables
    vector<float>   jet_GN2v01_sorted, jet_GN2v01_pb_sorted, jet_GN2v01_pc_sorted, jet_GN2v01_ptau_sorted, jet_GN2v01_pu_sorted;
    vector<int>     jet_TruthFlavouExtended_sorted, jet_TruthFlavour_sorted, HadronConeExclExtendedTruthLabelID_sorted, jet_truthflav_sorted;
    vector<float>   jet_eta_sorted, jet_phi_sorted, jet_e_sorted, jet_pt_sorted;
    vector<char>    jet_select_baselineJvt_sorted;

    // Sort all variables by jet_pt
    for (size_t i = 0; i < jet_idx.size(); i++)
    {
        // cut on jet_pt, jet_eta and jet quality
        if (
            jet_pt->at(jet_idx[i]) < 20000 || 
            std::abs(jet_eta->at(jet_idx[i])) > 2.5 ||
            !jet_select_baselineJvt->at(jet_idx[i])
        )
            continue;
        jet_GN2v01_sorted.push_back(jet_GN2v01->at(jet_idx[i]));
        jet_GN2v01_pb_sorted.push_back(jet_GN2v01_pb->at(jet_idx[i]));
        jet_GN2v01_pc_sorted.push_back(jet_GN2v01_pc->at(jet_idx[i]));
        jet_GN2v01_ptau_sorted.push_back(jet_GN2v01_ptau->at(jet_idx[i]));
        jet_GN2v01_pu_sorted.push_back(jet_GN2v01_pu->at(jet_idx[i]));
        if (mcChannelNumber != -1){
            jet_TruthFlavouExtended_sorted.push_back(jet_TruthFlavouExtended->at(jet_idx[i]));
            jet_TruthFlavour_sorted.push_back(jet_TruthFlavour->at(jet_idx[i]));
            jet_truthflav_sorted.push_back(jet_truthflav->at(jet_idx[i]));
            HadronConeExclExtendedTruthLabelID_sorted.push_back(HadronConeExclExtendedTruthLabelID->at(jet_idx[i]));
        }
        jet_eta_sorted.push_back(jet_eta->at(jet_idx[i]));
        jet_phi_sorted.push_back(jet_phi->at(jet_idx[i]));
        jet_e_sorted.push_back(jet_e->at(jet_idx[i]));
        jet_pt_sorted.push_back(jet_pt->at(jet_idx[i]));
        jet_select_baselineJvt_sorted.push_back(jet_select_baselineJvt->at(jet_idx[i]));
    }

    // Assign the sorted variables to the original variables
    *jet_GN2v01 = std::move(jet_GN2v01_sorted);
    *jet_GN2v01_pb = std::move(jet_GN2v01_pb_sorted);
    *jet_GN2v01_pc = std::move(jet_GN2v01_pc_sorted);
    *jet_GN2v01_ptau = std::move(jet_GN2v01_ptau_sorted);
    *jet_GN2v01_pu = std::move(jet_GN2v01_pu_sorted);
    if (mcChannelNumber != -1){
        *jet_TruthFlavouExtended = std::move(jet_TruthFlavouExtended_sorted);
        *jet_TruthFlavour = std::move(jet_TruthFlavour_sorted);
        *jet_truthflav = std::move(jet_truthflav_sorted);
        *HadronConeExclExtendedTruthLabelID = std::move(HadronConeExclExtendedTruthLabelID_sorted);
    }
    *jet_eta = std::move(jet_eta_sorted);
    *jet_phi = std::move(jet_phi_sorted);
    *jet_e = std::move(jet_e_sorted);
    *jet_pt = std::move(jet_pt_sorted);
    *jet_select_baselineJvt = std::move(jet_select_baselineJvt_sorted);
}


void TreeReader::SelectAndSortElectronByPt(){
    // Sort electron indices by Pt
    std::vector<int> el_idx(el_pt->size());
    std::iota(el_idx.begin(), el_idx.end(), 0);
    std::sort(el_idx.begin(), el_idx.end(), [&](int i, int j) { return el_pt->at(i) > el_pt->at(j); });

    // Define sorted variables
    vector<float>   el_e_sorted, el_pt_sorted, el_eta_sorted, el_phi_sorted, el_cl_eta_sorted, el_charge_sorted;
    vector<int>     el_true_type_sorted, el_true_origin_sorted;
    vector<bool>    el_true_isPrompt_sorted;
    vector<char>    el_select_tight_sorted;

    // Sort all variables by el_pt
    for (size_t i = 0; i < el_idx.size(); i++)
    {
        // cut on el_pt, el_eta and el quality
        if (
            el_pt->at(el_idx[i]) < 27000 || 
            std::abs(el_eta->at(el_idx[i])) > 2.47 ||
            !el_select_tight->at(el_idx[i])
        )
            continue;
        el_e_sorted.push_back(el_e->at(el_idx[i]));
        el_pt_sorted.push_back(el_pt->at(el_idx[i]));
        el_eta_sorted.push_back(el_eta->at(el_idx[i]));
        el_phi_sorted.push_back(el_phi->at(el_idx[i]));
        el_cl_eta_sorted.push_back(el_cl_eta->at(el_idx[i]));
        el_charge_sorted.push_back(el_charge->at(el_idx[i]));
        el_true_type_sorted.push_back(el_true_type->at(el_idx[i]));
        el_true_origin_sorted.push_back(el_true_origin->at(el_idx[i]));
        el_true_isPrompt_sorted.push_back(el_true_isPrompt->at(el_idx[i]));
        el_select_tight_sorted.push_back(el_select_tight->at(el_idx[i]));
    }

    // Assign the sorted variables to the original variables
    *el_e = std::move(el_e_sorted);
    *el_pt = std::move(el_pt_sorted);
    *el_eta = std::move(el_eta_sorted);
    *el_phi = std::move(el_phi_sorted);
    *el_cl_eta = std::move(el_cl_eta_sorted);
    *el_charge = std::move(el_charge_sorted);
    *el_true_type = std::move(el_true_type_sorted);
    *el_true_origin = std::move(el_true_origin_sorted);
    *el_true_isPrompt = std::move(el_true_isPrompt_sorted);
    *el_select_tight = std::move(el_select_tight_sorted);
}


void TreeReader::SelectAndSortMuonByPt(){
    // Sort muon indices by Pt
    std::vector<int> mu_idx(mu_pt->size());
    std::iota(mu_idx.begin(), mu_idx.end(), 0);
    std::sort(mu_idx.begin(), mu_idx.end(), [&](int i, int j) { return mu_pt->at(i) > mu_pt->at(j); });

    // Define sorted variables
    vector<float>   mu_e_sorted, mu_pt_sorted, mu_eta_sorted, mu_phi_sorted, mu_charge_sorted;
    vector<int>     mu_true_type_sorted, mu_true_origin_sorted;
    vector<bool>    mu_true_isPrompt_sorted;
    vector<char>    mu_select_tight_sorted;

    // Sort all variables by mu_pt
    for (size_t i = 0; i < mu_idx.size(); i++)
    {
        // cut on mu_pt mu_eta and mu quality
        if (
            mu_pt->at(mu_idx[i]) < 27000 || 
            std::abs(mu_eta->at(mu_idx[i])) > 2.5 ||
            !mu_select_tight->at(mu_idx[i])
        )
            continue;
        mu_e_sorted.push_back(mu_e->at(mu_idx[i]));
        mu_pt_sorted.push_back(mu_pt->at(mu_idx[i]));
        mu_eta_sorted.push_back(mu_eta->at(mu_idx[i]));
        mu_phi_sorted.push_back(mu_phi->at(mu_idx[i]));
        mu_charge_sorted.push_back(mu_charge->at(mu_idx[i]));
        mu_true_type_sorted.push_back(mu_true_type->at(mu_idx[i]));
        mu_true_origin_sorted.push_back(mu_true_origin->at(mu_idx[i]));
        mu_true_isPrompt_sorted.push_back(mu_true_isPrompt->at(mu_idx[i]));
        mu_select_tight_sorted.push_back(mu_select_tight->at(mu_idx[i]));
    }

    // Assign the sorted variables to the original variables
    *mu_e = std::move(mu_e_sorted);
    *mu_pt = std::move(mu_pt_sorted);
    *mu_eta = std::move(mu_eta_sorted);
    *mu_phi = std::move(mu_phi_sorted);
    *mu_charge = std::move(mu_charge_sorted);
    *mu_true_type = std::move(mu_true_type_sorted);
    *mu_true_origin = std::move(mu_true_origin_sorted);
    *mu_true_isPrompt = std::move(mu_true_isPrompt_sorted);
    *mu_select_tight = std::move(mu_select_tight_sorted);
}