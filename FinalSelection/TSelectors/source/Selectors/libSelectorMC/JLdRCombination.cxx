// JLdRCombination.cxx

#include "JLdRCombination.h"

ClassImp(JL_closest_dR_Combination)

JL_closest_dR_Combination::JL_closest_dR_Combination(std::string name, int n_jets, bool data, std::vector<std::string> eflavours ) : JL_Combination::JL_Combination(name, n_jets, data, eflavours)
{
}

JL_closest_dR_Combination::~JL_closest_dR_Combination()
{
}

void JL_closest_dR_Combination::Write()
{
	JL_Combination::Write();
}

void JL_closest_dR_Combination::addEvent(TLorentzVector *lep_4vecArray, TLorentzVector *jet_4vecArray, double weight, std::vector<int> *jet_truthflav)
{
	if (this->n_jets == 2) {
		double dR11,dR12;
		dR11=abs(lep_4vecArray[0].DeltaR(jet_4vecArray[0]) )+abs(lep_4vecArray[1].DeltaR(jet_4vecArray[1]) );
		dR12=abs(lep_4vecArray[1].DeltaR(jet_4vecArray[0]) )+abs(lep_4vecArray[0].DeltaR(jet_4vecArray[1]) );
		if (dR11 < dR12) {
			this->jet1_combination->addEvent(lep_4vecArray[0], jet_4vecArray[0], weight, jet_truthflav);
			this->jet2_combination->addEvent(lep_4vecArray[1], jet_4vecArray[1], weight, jet_truthflav);

		} else {
			this->jet1_combination->addEvent(lep_4vecArray[1], jet_4vecArray[0], weight, jet_truthflav);
			this->jet2_combination->addEvent(lep_4vecArray[0], jet_4vecArray[1], weight, jet_truthflav);
		}
	}
}
