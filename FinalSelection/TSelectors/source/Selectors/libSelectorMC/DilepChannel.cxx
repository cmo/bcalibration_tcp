// DiLepChannel.cxx

#include "DilepChannel.h"

ClassImp(DiLepChannel)

DiLepChannel::DiLepChannel(std::string name, std::vector<std::string> cutFlowLabels, int n_jets, int n_el, int n_mu, TDirectory *f_out, bool data, std::vector<std::string> eflavours, int n_boot_strap_min, int n_boot_strap_max, bool storeTracks, bool use_pt_bins_as_eta, int hadronization, const double m_jl_CutValue, std::string bTagSystName, bool m_save_fits, bool save_cr_hist, bool save_bin_means_for_sr_histo,std::string jet_collection,std::string jet_collection_lightjets,bool apply_lf_calib)
{
  this->name = name;
  this->m_save_fits = m_save_fits;
  channel_dir = f_out->GetDirectory(((name).c_str()));
  if (channel_dir == 0)
    channel_dir = f_out->mkdir(((name).c_str()));
  channel_dir->cd();

  this->n_jets = n_jets;
  this->n_el = n_el;
  this->n_mu = n_mu;
  this->n_lep = n_el + n_mu;
  this->m_n_boot_strap_min = n_boot_strap_min;
  this->m_n_boot_strap_max = n_boot_strap_max;
  this->m_n_boot_strap_min = n_boot_strap_min;
  this->m_jet_collection = jet_collection;
  this->m_jet_collection_lightjets = jet_collection_lightjets;
  this->m_storeTracks = storeTracks;
  this->m_use_pt_bins_as_eta = use_pt_bins_as_eta;
  this->m_data = data;
  this->m_apply_lf_calib = apply_lf_calib;

  //  int cs = 11;
  int cs = cutFlowLabels.size();


  cutflow = new TH1F(("h_" + name + "_CutFlow").c_str(), (name + "_CutFlow").c_str(), cs, 0, cs);
  cutflow->Sumw2();

  for (uint i = 0; i < cutFlowLabels.size(); i++)
    cutflow->GetXaxis()->SetBinLabel(i + 1, cutFlowLabels.at(i).c_str());
  if (this->n_jets == 2)
  {
    if (this->m_save_fits)
    {
      hist_for_fit_GN2_fixedCut = new Hist_for_fit_Container(name + "_hist_for_fit", n_jets, "GN2v01", "FixedCutBEff", channel_dir, m_data, eflavours, m_n_boot_strap_min, m_n_boot_strap_max, m_use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, save_cr_hist,save_bin_means_for_sr_histo,jet_collection,jet_collection_lightjets,apply_lf_calib);
    }
    const Int_t NBINS_im = 20;
    const Double_t im_bins[NBINS_im + 1] = {0., 20., 40., 60., 80., 100., 120., 140., 160., 180., 200., 220., 240., 260., 280., 300., 320., 340., 360., 380., 400.};
    m_jl_max = new Variable_ptBins_Container(name + "_m_jl_max", n_jets, NBINS_im, im_bins, m_data, eflavours);
    m_jl_sqsum = new Variable_ptBins_Container(name + "_m_jl_sqsum", n_jets, NBINS_im, im_bins, m_data, eflavours);
    m_jl_sum = new Variable_ptBins_Container(name + "_m_jl_sum", n_jets, NBINS_im, im_bins, m_data, eflavours);
    jl_min_m_sum_Combination = new JL_min_m_sum_Combination(name + "_jl_min_m_sum_Combination", n_jets, m_data, eflavours);
    jl_min_m_sub_Combination = new JL_min_m_sub_Combination(name + "_jl_min_m_sub_Combination", n_jets, m_data, eflavours);
    jl_closest_dR_Combination = new JL_closest_dR_Combination(name + "_jl_closest_dR_Combination", n_jets, m_data, eflavours);
  }
  if( m_n_boot_strap_max == 0 ){
    ll_con = new XX_Container(name + "_ll", n_jets, m_data, eflavours);

    h_met = new Variable_Container(name + "_met", n_jets, 40, 0, 400, m_data, eflavours);
    h_mu = new Variable_Container(name + "_mu", n_jets, 100, 0, 100, m_data, eflavours);
    h_mu_shifted = new Variable_Container(name + "_mu_shifted", n_jets, 100, 0, 100, m_data, eflavours);
    h_nPV = new Variable_Container(name + "_nPV", n_jets, 100, 0, 100, m_data, eflavours);

    h_pt_both = new Variable_pt_Container(name + "_lead_sublead_ptbins", n_jets, 100,0,400,100,0,400,m_data,eflavours);

  
    for (int i = 0; i < n_jets; i++)
      jets.push_back(new jet_Container(name + "_jet" + std::to_string(i + 1), n_jets, m_data, eflavours));
    for (int i = 0; i < n_el; i++)
    {
      std::string histName = name + "_el" + std::to_string(i + 1);
      el_pt.push_back(new Variable_Container(histName + "_pt"+std::to_string(i + 1), n_jets, 60, 0, 400, m_data, eflavours));
      el_eta.push_back(new Variable_Container(histName + "_eta"+std::to_string(i + 1), n_jets, 24, -3, 3, m_data, eflavours));
      el_cl_eta.push_back(new Variable_Container(histName + "_cl_eta"+std::to_string(i + 1), n_jets, 24, -3, 3, m_data, eflavours));
      el_phi.push_back(new Variable_Container(histName + "_phi"+std::to_string(i + 1), n_jets, 28, -3.5, 3.5, m_data, eflavours));
      if (!m_data)
      {
        el_true_type.push_back(new Variable_Container(histName + "_true_type"+std::to_string(i + 1), n_jets, 39, 0, 38, m_data, eflavours));
        el_true_origin.push_back(new Variable_Container(histName + "_true_origin"+std::to_string(i + 1), n_jets, 46, 0, 45, m_data, eflavours));
      }
    }
    for (int i = 0; i < n_mu; i++)
    {
      std::string histName = name + "_mu" + std::to_string(i + 1);
      mu_pt.push_back(new Variable_Container(histName + "_pt"+std::to_string(i + 1), n_jets, 60, 0, 400, m_data, eflavours));
      mu_eta.push_back(new Variable_Container(histName + "_eta"+std::to_string(i + 1), n_jets, 24, -3, 3, m_data, eflavours));
      mu_phi.push_back(new Variable_Container(histName + "_phi"+std::to_string(i + 1), n_jets, 28, -3.5, 3.5, m_data, eflavours));
      if (!m_data)
      {
        mu_true_type.push_back(new Variable_Container(histName + "_true_type"+std::to_string(i + 1), n_jets, 39, 0, 38, m_data, eflavours));
        mu_true_origin.push_back(new Variable_Container(histName + "_true_origin"+std::to_string(i + 1), n_jets, 46, 0, 45, m_data, eflavours));
      }
    }
  }
}

void DiLepChannel::addToCutflow(int bin, double weight)
{
  cutflow->Fill(bin - 1, weight);
}

void DiLepChannel::Save(TFile* file)
{
  channel_dir = file->GetDirectory(((name).c_str()));
  if (channel_dir == 0)
    channel_dir = file->mkdir(((name).c_str()));
  channel_dir->cd();
  cutflow->Write();
  if(m_n_boot_strap_max == 0){
    ll_con->Write();
    h_met->Write();
    h_mu->Write();
    h_mu_shifted->Write();
    h_nPV->Write();
  }
  if (this->n_jets == 2)
  {
    if(m_n_boot_strap_max == 0 )
      h_pt_both->Write();
    if (this->m_save_fits)
    {
      hist_for_fit_GN2_fixedCut->Write();
    }
      if(m_n_boot_strap_max == 0 ){
        m_jl_max->Write();
        m_jl_sqsum->Write();
        m_jl_sum->Write();
        jl_min_m_sum_Combination->Write();
        jl_min_m_sub_Combination->Write();
        jl_closest_dR_Combination->Write();
      }
    }

  for (int i = 0; i < n_jets; i++)
    jets.at(i)->Write();

  if(m_n_boot_strap_max == 0){
    for (int i = 0; i < n_el; i++)
    {
      el_pt.at(i)->Write();
      el_eta.at(i)->Write();
      el_cl_eta.at(i)->Write();
      el_phi.at(i)->Write();
      if (!m_data)
      {
        el_true_type.at(i)->Write();
        el_true_origin.at(i)->Write();
      }
    }
    for (int i = 0; i < n_mu; i++)
    {
      mu_pt.at(i)->Write();
      mu_eta.at(i)->Write();
      mu_phi.at(i)->Write();
      if (!m_data)
      {
        mu_true_type.at(i)->Write();
        mu_true_origin.at(i)->Write();
      }
    }
  }
}

DiLepChannel::~DiLepChannel()
{
}

void DiLepChannel::AddEvent(TreeReader *selector, double weight, Long64_t entry)
{
  selector->fChain->GetTree()->GetEntry(entry);
  std::vector<float> GN2 = *selector->jet_GN2v01;

  if(m_n_boot_strap_max == 0){
    h_met->addEvent(selector->met_met / 1000, weight, selector->jet_truthflav);
    h_mu->addEvent(selector->mu, weight, selector->jet_truthflav);
    double mu_shift=selector->mu;
    if (!m_data){
      mu_shift=mu_shift*1.03;
    }
    h_mu_shifted->addEvent(mu_shift, weight, selector->jet_truthflav);
    h_nPV -> addEvent(selector -> nPV, weight, selector->jet_truthflav);
  }

  if (this->n_jets == 2)
  {
    if(m_n_boot_strap_max == 0 ){
      h_pt_both->addEvent(selector->jet_pt->at(0), selector->jet_pt->at(1), weight, selector->jet_truthflav);

      m_jl_max->addEvent(std::max(selector->jet_m_jl->at(0), selector->jet_m_jl->at(1)) / 1000., selector->jet_pt, weight, selector->jet_truthflav);
      m_jl_sqsum->addEvent(sqrt(pow(selector->jet_m_jl->at(0), 2) + pow(selector->jet_m_jl->at(1), 2)) / 1000., selector->jet_pt, weight, selector->jet_truthflav);
      m_jl_sum->addEvent((selector->jet_m_jl->at(0) + selector->jet_m_jl->at(1)) / 1000., selector->jet_pt, weight, selector->jet_truthflav);
    }
    if (this->m_save_fits)
    {
        hist_for_fit_GN2_fixedCut->addEvent(&GN2, selector->jet_pt, selector->jet_eta, selector->jet_m_jl, weight, selector->jet_truthflav, 0);
    }
  }
  if(m_n_boot_strap_max == 0 ){
    for (int i = 0; i < n_mu; i++)
    {
      mu_pt.at(i)->addEvent(selector->mu_pt->at(i) / 1000, weight, selector->jet_truthflav);
      mu_eta.at(i)->addEvent(selector->mu_eta->at(i), weight, selector->jet_truthflav);
      mu_phi.at(i)->addEvent(selector->mu_phi->at(i), weight, selector->jet_truthflav);
      if (!m_data)
      {
        mu_true_type.at(i)->addEvent(selector->mu_true_type->at(i), weight, selector->jet_truthflav);
        mu_true_origin.at(i)->addEvent(selector->mu_true_origin->at(i), weight, selector->jet_truthflav);
      }
    }
    for (int i = 0; i < n_el; i++)
    {
      el_pt.at(i)->addEvent(selector->el_pt->at(i) / 1000, weight, selector->jet_truthflav);
      el_eta.at(i)->addEvent(selector->el_eta->at(i), weight, selector->jet_truthflav);
      el_cl_eta.at(i)->addEvent(selector->el_cl_eta->at(i), weight, selector->jet_truthflav);
      el_phi.at(i)->addEvent(selector->el_phi->at(i), weight, selector->jet_truthflav);
      if (!m_data)
      {
        el_true_type.at(i)->addEvent(selector->el_true_type->at(i), weight, selector->jet_truthflav);
        el_true_origin.at(i)->addEvent(selector->el_true_origin->at(i), weight, selector->jet_truthflav);
      }
    }
    for (int i = 0; i < n_jets; i++)
    {
      jets.at(i)->addJet(i, selector, weight);
    }
    TLorentzVector lep_4vec[2];
    TLorentzVector jet_4vec[3];
    for (int i = 0; i < n_jets; i++)
      jet_4vec[i].SetPtEtaPhiE(selector->jet_pt->at(i), selector->jet_eta->at(i), selector->jet_phi->at(i), selector->jet_e->at(i));
    if (n_el == 2)
    {
      lep_4vec[0].SetPtEtaPhiM(selector->el_pt->at(0), selector->el_eta->at(0), selector->el_phi->at(0), 0);
      lep_4vec[1].SetPtEtaPhiM(selector->el_pt->at(1), selector->el_eta->at(1), selector->el_phi->at(1), 0);
    }
    if ((n_el == 1) && (n_mu == 1))
    {
      if (selector->el_pt->at(0) > selector->mu_pt->at(0))
      {
        lep_4vec[0].SetPtEtaPhiM(selector->el_pt->at(0), selector->el_eta->at(0), selector->el_phi->at(0), 0);
        lep_4vec[1].SetPtEtaPhiM(selector->mu_pt->at(0), selector->mu_eta->at(0), selector->mu_phi->at(0), 0);
      }
      else
      {
        lep_4vec[1].SetPtEtaPhiM(selector->el_pt->at(0), selector->el_eta->at(0), selector->el_phi->at(0), 0);
        lep_4vec[0].SetPtEtaPhiM(selector->mu_pt->at(0), selector->mu_eta->at(0), selector->mu_phi->at(0), 0);
      }
    }
    if ((n_el == 0) && (n_mu == 2))
    {
      lep_4vec[0].SetPtEtaPhiM(selector->mu_pt->at(0), selector->mu_eta->at(0), selector->mu_phi->at(0), 0);
      lep_4vec[1].SetPtEtaPhiM(selector->mu_pt->at(1), selector->mu_eta->at(1), selector->mu_phi->at(1), 0);
    }
    ll_con->addEvent(lep_4vec[0], lep_4vec[1], weight, selector->jet_truthflav);
    if (this->n_jets == 2)
    {
      jl_min_m_sum_Combination->addEvent(lep_4vec, jet_4vec, weight, selector->jet_truthflav);
      jl_min_m_sub_Combination->addEvent(lep_4vec, jet_4vec, weight, selector->jet_truthflav);
      jl_closest_dR_Combination->addEvent(lep_4vec, jet_4vec, weight, selector->jet_truthflav);
    }
  }
}
