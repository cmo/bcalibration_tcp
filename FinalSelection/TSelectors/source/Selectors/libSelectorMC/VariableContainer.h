// VariableContainer.h

#pragma once

#include <string>
#include <vector>
#include <map>
#include <TH1F.h>
#include <iostream>

class Variable_Container
{
 public:
  std::string name;
  std::map<std::string, TH1F *> m_histograms;
  bool m_data;
  
 public:
  // Constructor 1
  Variable_Container(std::string name, int n_jets, int bins, double xmin, double xmax, bool data, std::vector<std::string> eflavours);
  
  // Constructor 2
  Variable_Container(std::string name, int n_jets, int nbins, const double* xbins, bool data, std::vector<std::string> eflavours);
  
  // Destructor
  ~Variable_Container();
  
  // Writes histograms it seems
  void Write();
  
  // Adds event
  void addEvent(double value, double weight, std::vector<int> *jet_truthflav);
  
  // Gets event flavour
  std::string getEventFlav(std::vector<int> *jet_truthflav);

  ClassDef(Variable_Container,1);
};
