// VariablePtContainer.h

#pragma once

#include <vector>
#include <string>
#include <map>
#include <TH2F.h>

class Variable_pt_Container
{
public:
	std::string name;
	std::map<std::string, TH2F *> histograms;
	static const int NBINS_pt = 5;
	const double pt_bins[NBINS_pt + 1] = {20., 40., 60., 140., 250., 400.}; //{20., 30., 40., 60., 85., 110., 140., 175., 250., 400.};
	bool data;

public:
	// Constructor
	Variable_pt_Container(std::string name, int n_jets, int bins, double xmin, double xmax,int ybins, double ymin, double ymax, bool data, std::vector<std::string> eflavours);
	
	// Destructor
	~Variable_pt_Container();

    // Writes the histos it seems
	void Write();

    // Adds the event
    void addEvent(double value, float pt, double weight, std::vector<int> *jet_truthflav);

    // Gets event flavour
    std::string getEventFlav(std::vector<int> *jet_truthflav);

    ClassDef(Variable_pt_Container,1);
};
