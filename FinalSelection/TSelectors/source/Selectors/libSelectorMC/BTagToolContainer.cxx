// BTagToolContainer.cxx

#include "BTagToolContainer.h"
#include "PATCore/AcceptData.h"

//using CP::SystematicCode;

ClassImp(BtagTool_Container)
 
BtagTool_Container::BtagTool_Container(const char *toolName, const char *taggerName, const char *cutName, int hadronization, std::string bTagSystName, bool data,std::string jet_collection,std::string jet_collection_lightjets,bool apply_lf_calib)
{
	std::string sCutName(cutName);
	std::string sToolName(toolName);
	std::string staggerName(taggerName);
	// get the WP boundaries
	// {85, 77, 70, 60} for all taggers except GN2v01
	// {90, 85, 77, 70, 65} for GN2v01
	if (taggerName != "GN2v01") {
		wp_boundrys = {"85", "77", "70", "60"};
		cdi_file_path = "./13p6TeV/2023-22-13p6TeV-MC21-CDI_Test_2023-08-1_v1.root";
	}
	else {
		wp_boundrys = {"90", "85", "77", "70", "65"};
		cdi_file_path = "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13p6TeV/2023-02_MC23_CDI_GN2v01-noSF.root";
	}
	int count_wp=0;
	this->toolName = sToolName + "_" + staggerName + "_" + sCutName;
	this->m_hadronization = hadronization;
	this->m_bTagSystName = bTagSystName;
	this->m_data = data;
	this->m_jet_collection=jet_collection;
	this->m_jet_collection_lightjets=jet_collection_lightjets;
	this->apply_lf_calib=apply_lf_calib;
	for (const auto &wp_boundry : wp_boundrys)
	{
		std::string operatingPoint = sCutName + "_" + wp_boundry;
		std::string full_tool_name = sToolName + "_" + staggerName + "_" + operatingPoint;
		std::cout << "Initialisation of Tool "<< sToolName<<" with wp: " << operatingPoint << std::endl;
		BTaggingSelectionTool *btagtool = new BTaggingSelectionTool(full_tool_name.c_str());
		btagtool->setProperty("MaxEta", 2.5);
		btagtool->setProperty("MinPt", 20000.);
		btagtool->setProperty("JetAuthor", m_jet_collection);
		btagtool->setProperty("TaggerName", taggerName);
		btagtool->setProperty("FlvTagCutDefinitionsFileName", cdi_file_path);
		btagtool->setProperty("OperatingPoint", operatingPoint.c_str());
		if (btagtool->initialize().isSuccess())
		{
			std::cout << "Btagging selection tool inititialization successfull: "<< sToolName  <<std::endl;
		}
		else
		{
			std::cout << "ERROR! " << full_tool_name << " not inititialized properly!!!!!." << std::endl;
			
		}
		btagTools.push_back(btagtool);
	}
	if ( this->apply_lf_calib && (!m_data) )
	{
		std::string btageff_tool_name = sToolName + "_BTaggingEfficiencyTool_" + staggerName + "_" + sCutName + "_" + "Continuous";
		vars_jet = new Analysis::CalibrationDataVariables();
		btageff = new BTaggingEfficiencyTool(btageff_tool_name);
		btageff->setProperty("JetAuthor", m_jet_collection_lightjets);
		btageff->setProperty("TaggerName", taggerName);
		btageff->setProperty("EfficiencyFileName", cdi_file_path);
		btageff->setProperty("ScaleFactorFileName", cdi_file_path);
		btageff->setProperty("OperatingPoint", "Continuous");
		btageff->setProperty("MinPt", 20000.0);
		btageff->setProperty("EfficiencyLightCalibrations", std::to_string(m_hadronization).c_str());
		btageff->setProperty("EfficiencyBCalibrations", std::to_string(m_hadronization).c_str());
		btageff->setProperty("EfficiencyCCalibrations", std::to_string(m_hadronization).c_str());
		btageff->setProperty("EfficiencyTCalibrations", std::to_string(m_hadronization).c_str());
		btageff->setProperty("SystematicsStrategy", bjet_calibsystset.c_str());

		if (btageff->initialize().isSuccess())
		{
			std::cout << sToolName << " inititialized successfull." << std::endl;
			std::cout << "-----------------------------------------------------" << std::endl;
			const std::map<CP::SystematicVariation, std::vector<std::string>> allowed_variations = btageff->listSystematics();
			std::cout << "Allowed systematics variations for tool " << btageff->name() << ":" << std::endl;
			CP::SystematicSet bTagSyst;
			CP::SystematicVariation var_to_apply;
			for (auto var : allowed_variations)
			{
				std::cout << std::setw(40) << std::left << var.first.name() << ":";
				if (var.first.name() == m_bTagSystName)
				{
					bTagSyst.insert(var.first);
					var_to_apply = var.first;
				}
				for (auto flv : var.second)
					std::cout << " " << flv;
				std::cout << std::endl;
			}
			std::cout << "-----------------------------------------------------" << std::endl;

			if (bTagSyst.size() != 0)
			{
				std::cout << "found : " << m_bTagSystName << " applying" << std::endl;
				StatusCode sresult = btageff->applySystematicVariation(bTagSyst);
				if (sresult != StatusCode::SUCCESS)
				{
					std::cout << var_to_apply.name() << " apply systematic variation FAILED " << std::endl;
					std::exit(1);
				}
				std::cout << "New b-tagging SF to a shifted systematic set : " << var_to_apply.name() << " size: " << bTagSyst.size() << " retruned: " << sresult << " joined names: " << bTagSyst.name() << std::endl;
			}
		}
		else
		{
			std::cout << "ERROR! " << btageff_tool_name << " not inititialized properly!!!!!." << std::endl;
		}
	}
}

double BtagTool_Container::get_scale_factor(Double_t jet_pt, Double_t jet_eta, int jet_truthflav, Double_t jet_tag_weight)
{
	float scale_factor = 1;
	if (btageff)
	{
		vars_jet->jetPt = jet_pt;
		vars_jet->jetEta = jet_eta;
		vars_jet->jetTagWeight = jet_tag_weight;
		btageff->getScaleFactor(jet_truthflav, *vars_jet, scale_factor);
	}
	if (scale_factor == 0)
	{
		std::cout << "Got scale_factor= " << scale_factor << " from tool: " << toolName << std::endl;
	}
	return scale_factor;
}

int BtagTool_Container::get_Btagging_bin(Double_t jet_pt, Double_t jet_eta, Double_t jet_tag_weight)
{
	int btag_bin = 1;

	for (const auto &btagtool : btagTools)
	{
	  bool isBtagged = btagtool->accept(jet_pt, jet_eta, jet_tag_weight).getCutResult("WorkingPoint") == true ;
	  
	  if (isBtagged)
	    //btagtool->print();
	    btag_bin++;
	  else
	    break;
	}
	//std::cout<<"Returning Btagbin: "<<btag_bin<<endl;
	return btag_bin;
}

BtagTool_Container::~BtagTool_Container()
{
	for (const auto &btagtool : btagTools)
	{
		delete btagtool;
	}
	delete btageff;
}
