// JLdRCombination.h

#pragma once

#include <TLorentzVector.h>
#include "JLCombination.h"
#include <string>

class JL_closest_dR_Combination : public JL_Combination
{
 public:
  int n_jets  = 0;
  bool data;
  std::vector<std::string> eflavours;

public:
	// Constructor
  JL_closest_dR_Combination(std::string name, int n_jets, bool data, std::vector<std::string> eflavours);

	// Destructor
	~JL_closest_dR_Combination();

	// Writes
	void Write();

	// Adds an event to something
	void addEvent(TLorentzVector *lep_4vecArray, TLorentzVector *jet_4vecArray, double weight, std::vector<int> *jet_truthflav);

	ClassDef(JL_closest_dR_Combination,1);
};
