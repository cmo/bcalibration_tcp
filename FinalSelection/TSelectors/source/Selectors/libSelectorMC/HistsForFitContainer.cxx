// HistsForFitContainer.cxx

#include "ConfigManager.h"
#include "HistsForFitContainer.h"

ClassImp(Hist_for_fit_Container)

Hist_for_fit_Container::Hist_for_fit_Container(std::string name, int n_jets, const char *taggerName, const char *cutName, TDirectory* chan_dir, bool data, std::vector<std::string> eflavours, int n_boot_strap_min, int n_boot_strap_max, bool use_pt_bins_as_eta,int hadronization, const double m_jl_CutValue, std::string bTagSystName, bool split_cr_regions = false,bool save_bin_means_for_sr_histo = true,std::string jet_collection,std::string jet_collection_lightjets,bool apply_lf_calib)
{ //add a bool to split cr here. we need only one cr histogramm container!
	this->name = name + "_" + taggerName + "_" + cutName;
	this->n_jets = n_jets;
	this->split_cr_regions = split_cr_regions;
	this->m_data = data;
	this->m_n_boot_strap_min = n_boot_strap_min;
	this->m_n_boot_strap_max = n_boot_strap_max;
	this->m_use_pt_bins_as_eta = use_pt_bins_as_eta;
	this->m_save_bin_means_for_sr_histo = save_bin_means_for_sr_histo;
	this->m_apply_lf_calib = apply_lf_calib;
	if (use_pt_bins_as_eta)
	{
		m_pt_label_jet1 = "#eta_{1}";
		m_pt_label_jet2 = "#eta_{2}";
		m_pt_label_single = "#eta";
	}
	if (m_data)
	{
		eflavours.clear();
		eflavours.push_back("data");
	}
	else{
		j1_sf_weight= new Variable_Container( this->name + "j1_sf_weight", n_jets, 150, -1, 2., m_data, eflavours);
		j2_sf_weight= new Variable_Container( this->name + "j2_sf_weight", n_jets, 150, -1, 2., m_data, eflavours);
	}
	const Int_t NBINS_tagger = 6;
	const Double_t tagger_bins[NBINS_tagger + 1] = {1, 2, 3, 4, 5, 6, 7};
	const Int_t NBINS_mjl = 2;
	const Double_t mjl_bins[NBINS_mjl + 1] = {0., m_jl_CutValue, 600};
	const Int_t NBINS_pt = ConfigManager::Instance()->num_pt_bins();
	auto jet_pt_bins_vec = ConfigManager::Instance()->jet_pt_bins();
	double *pt_bins = jet_pt_bins_vec.data();

	if (m_use_pt_bins_as_eta)
	{
		for (int i = 0; i <= NBINS_pt; i++)
		{
			pt_bins[i] = -2.5 + i * 5. / NBINS_pt;
		}
	}
	Int_t bins_sr[4] = {NBINS_tagger, NBINS_tagger, NBINS_pt, NBINS_pt};
	Int_t bins_cr[4] = {NBINS_mjl, NBINS_mjl, NBINS_pt, NBINS_pt};

	if (n_jets == 2)
	{

	  // hack to deal with LegacyWP and NewAliasWP working point names for GN2 CDI
	  const char* cdiName = (taggerName == "GN2v00") ? "GN2v00LegacyWP" : taggerName;

	  btagtoolCont = new BtagTool_Container((this->name + "_BTaggingSelectionTool").c_str(), cdiName, cutName, hadronization, bTagSystName, m_data,jet_collection,jet_collection_lightjets,apply_lf_calib);
		if (!m_data)
		{ 
			if (m_save_bin_means_for_sr_histo && ( this->name.find("emu_OS_J2")!= std::string::npos))
			{
				bin_means_for_sr_histo = new Pt_bin_mean_container(this->name+"_bin_mean", taggerName, chan_dir, NBINS_pt, pt_bins, NBINS_tagger, tagger_bins, m_data, eflavours, m_pt_label_jet1, m_pt_label_jet2);
			}
			else{
				bin_means_for_sr_histo =0;
			}
			h_l_jet = new TH2D(("hff_" + this->name + "_light").c_str(), ("hff_" + this->name + "_light").c_str(), NBINS_tagger, tagger_bins, NBINS_pt, pt_bins);
			h_l_jet->GetXaxis()->SetTitle(std::string(taggerName).c_str());
			h_l_jet->GetYaxis()->SetTitle(m_pt_label_single.c_str());
			h_l_jet->Sumw2();
			h_b_jet = new TH2D(("hff_" + this->name + "_b").c_str(), ("hff_" + this->name + "_b").c_str(), NBINS_tagger, tagger_bins, NBINS_pt, pt_bins);
			h_b_jet->GetXaxis()->SetTitle(std::string(taggerName).c_str());
			h_b_jet->GetYaxis()->SetTitle(m_pt_label_single.c_str());
			h_b_jet->Sumw2();
		}
		for (const auto &eventFlav : eflavours)
		{
			if(!m_data){
				std::string hist_name = "hf4_" + this->name + "_" + eventFlav;
				THnSparseI *sr_hist_uw_this = new THnSparseI((hist_name+"_unweighted").c_str(), (hist_name+"_unweighted").c_str(), 4, bins_sr, NULL, NULL);
				sr_hist_uw_this->SetBinEdges(0, tagger_bins);
				sr_hist_uw_this->GetAxis(0)->SetTitle((std::string(taggerName) + "_1").c_str());
				sr_hist_uw_this->SetBinEdges(1, tagger_bins);
				sr_hist_uw_this->GetAxis(1)->SetTitle((std::string(taggerName) + "_2").c_str());
				sr_hist_uw_this->SetBinEdges(2, pt_bins);
				sr_hist_uw_this->GetAxis(2)->SetTitle(m_pt_label_jet1.c_str());
				sr_hist_uw_this->SetBinEdges(3, pt_bins);
				sr_hist_uw_this->GetAxis(3)->SetTitle(m_pt_label_jet2.c_str());
				sr_hist_uw_this->Sumw2();
				sr_histogram_uw[eventFlav]=sr_hist_uw_this;
				if (split_cr_regions)
				{
					std::string cr_hist_name = "hf4_Ncr_" + this->name + "_" + eventFlav;
					THnSparseI *cr_hist_uw_this = new THnSparseI((cr_hist_name+"_unweighted").c_str(), (cr_hist_name+"_unweighted").c_str(), 4, bins_cr, NULL, NULL);
					cr_hist_uw_this->SetBinEdges(0, mjl_bins);
					cr_hist_uw_this->GetAxis(2)->SetTitle("m_{lj,1} [GeV]");
					cr_hist_uw_this->SetBinEdges(1, mjl_bins);
					cr_hist_uw_this->GetAxis(2)->SetTitle("m_{lj,2} [GeV]");
					cr_hist_uw_this->SetBinEdges(2, pt_bins);
					cr_hist_uw_this->GetAxis(2)->SetTitle(m_pt_label_jet1.c_str());
					cr_hist_uw_this->SetBinEdges(3, pt_bins);
					cr_hist_uw_this->GetAxis(3)->SetTitle(m_pt_label_jet2.c_str());
					cr_hist_uw_this->Sumw2();
					cr_histogram_uw[eventFlav]=cr_hist_uw_this;
				}
			}

			for (int n_boot_strap = n_boot_strap_min; n_boot_strap <= m_n_boot_strap_max; n_boot_strap++)
			{
				std::string hist_name = "hf4_" + this->name + "_" + eventFlav;
				if (n_boot_strap > 0)
				{
					hist_name = hist_name + "_boot_strap_" + std::to_string(n_boot_strap - 1);
				}
				THnSparseD *sr_hist_this = new THnSparseD(hist_name.c_str(), hist_name.c_str(), 4, bins_sr, NULL, NULL);
				sr_hist_this->SetBinEdges(0, tagger_bins);
				sr_hist_this->GetAxis(0)->SetTitle((std::string(taggerName) + "_1").c_str());
				sr_hist_this->SetBinEdges(1, tagger_bins);
				sr_hist_this->GetAxis(1)->SetTitle((std::string(taggerName) + "_2").c_str());
				sr_hist_this->SetBinEdges(2, pt_bins);
				sr_hist_this->GetAxis(2)->SetTitle(m_pt_label_jet1.c_str());
				sr_hist_this->SetBinEdges(3, pt_bins);
				sr_hist_this->GetAxis(3)->SetTitle(m_pt_label_jet2.c_str());
				sr_hist_this->Sumw2();
				sr_histograms[eventFlav].push_back(sr_hist_this);
				if (split_cr_regions)
				{
					std::string cr_hist_name = "hf4_Ncr_" + this->name + "_" + eventFlav;
					if (n_boot_strap > 0)
					{
						cr_hist_name = cr_hist_name + "_boot_strap_" + std::to_string(n_boot_strap - 1);
					}
					THnSparseD *cr_hist_this = new THnSparseD(cr_hist_name.c_str(), cr_hist_name.c_str(), 4, bins_cr, NULL, NULL);
					cr_hist_this->SetBinEdges(0, mjl_bins);
					cr_hist_this->GetAxis(2)->SetTitle("m_{lj,1} [GeV]");
					cr_hist_this->SetBinEdges(1, mjl_bins);
					cr_hist_this->GetAxis(2)->SetTitle("m_{lj,2} [GeV]");
					cr_hist_this->SetBinEdges(2, pt_bins);
					cr_hist_this->GetAxis(2)->SetTitle(m_pt_label_jet1.c_str());
					cr_hist_this->SetBinEdges(3, pt_bins);
					cr_hist_this->GetAxis(3)->SetTitle(m_pt_label_jet2.c_str());
					cr_hist_this->Sumw2();
					cr_histograms[eventFlav].push_back(cr_hist_this);
				}
			}
		}
	}
}

Hist_for_fit_Container::~Hist_for_fit_Container()
{
  delete btagtoolCont;
}

void Hist_for_fit_Container::Write()
{
	if (n_jets == 2)
	{
		for (const auto &flav_vec : sr_histograms)
		{
			for (const auto &hist : flav_vec.second)
			{
				hist->Write();
			}
		}
		if (split_cr_regions)
		{
			for (const auto &flav_vec : cr_histograms)
			{
				for (const auto &hist : flav_vec.second)
				{
					hist->Write();
				}
			}
		}
		if (!m_data)
		{
			j1_sf_weight->Write();
			j2_sf_weight->Write();
			if (bin_means_for_sr_histo)
				bin_means_for_sr_histo->Write();
			h_l_jet->Write();
			h_b_jet->Write();
		
			for (const auto &flav_vec : sr_histogram_uw)
			{
				flav_vec.second->Write();					
			}
			if (split_cr_regions)
			{
				for (const auto &flav_vec : cr_histogram_uw)
				{
					flav_vec.second->Write();									
				}
			}
		}
	}
}

void Hist_for_fit_Container::addEvent(std::vector<float> *jet_tag_weight, std::vector<float> *jet_pt, std::vector<float> *jet_eta, std::vector<float> *jet_m_jl, double weight, std::vector<int> *jet_truthflav, std::vector<int> *weight_poisson)
{
	//defing string flavour "bb","bl"...:
	std::string eventFlav = getEventFlav(jet_truthflav);
	if (!m_data && m_use_pt_bins_as_eta && (jet_eta->at(0) < jet_eta->at(1)))
	{
		std::reverse(eventFlav.begin(), eventFlav.end());
	}
	double m_jl_1, m_jl_2;
	if (n_jets == 2)
	{
		std::vector<int> btagbin;
		btagbin.clear();
		for (int j = 0; j < 2; j++)
		{
			btagbin.push_back(btagtoolCont->get_Btagging_bin(jet_pt->at(j), jet_eta->at(j), jet_tag_weight->at(j)));
			if (!m_data)
			{
				if (jet_truthflav->at(j) < 5)
				{ //its a light jet
					double jet_sf_weight = btagtoolCont->get_scale_factor(jet_pt->at(j), jet_eta->at(j), jet_truthflav->at(j), jet_tag_weight->at(j));
					//multiplying the jet sf up.
					if (j==0){
						j1_sf_weight-> addEvent( jet_sf_weight, weight, jet_truthflav);
					}else{
						j2_sf_weight-> addEvent( jet_sf_weight, weight, jet_truthflav);
					}
					weight = weight * jet_sf_weight;
					//cout<<"found sf for jet_flav: "<<jet_truthflav->at(j) <<" pt: " << jet_pt->at(j)<<" eta: " << jet_eta->at(j)<< " sf: "<<scale_factor <<" tool: "<<this->name<<endl;
				}
				else{
					if (j==0){
						j1_sf_weight-> addEvent( 1, weight, jet_truthflav);
					}else{
						j2_sf_weight-> addEvent( 1, weight, jet_truthflav);
					}
				}
			}
		}

		if (split_cr_regions)
		{
			m_jl_1 = jet_m_jl->at(0) / 1000.;
			m_jl_2 = jet_m_jl->at(1) / 1000.;
			if (m_jl_1 > 599.)
				m_jl_1 = 599.;
			if (m_jl_2 > 599.)
				m_jl_2 = 599.;
			Double_t where_vector_cr[4] = {m_jl_1, m_jl_2, jet_pt->at(0) / 1000., jet_pt->at(1) / 1000.};
			if (m_use_pt_bins_as_eta)
			{
				if (jet_eta->at(0) > jet_eta->at(1))
				{
					where_vector_cr[2] = jet_eta->at(0);
					where_vector_cr[3] = jet_eta->at(1);
				}
				else
				{
					where_vector_cr[2] = jet_eta->at(1);
					where_vector_cr[3] = jet_eta->at(0);
					double save = where_vector_cr[0];
					where_vector_cr[0] = where_vector_cr[1];
					where_vector_cr[1] = save;
				}
			}
			cr_histograms[eventFlav.c_str()].at(0)->Fill(where_vector_cr, weight);
			if(!m_data){
				cr_histogram_uw[eventFlav.c_str()]->Fill(where_vector_cr);
			}
			int count=0;
			for (int n_boot_strap = m_n_boot_strap_min; n_boot_strap < m_n_boot_strap_max; n_boot_strap++)
			{
				if (weight_poisson == 0)
				{
					// cout << "no bootstrapweights found!" << endl;
					exit(2);
				}
				cr_histograms[eventFlav.c_str()].at(count + 1)->Fill(where_vector_cr, weight * weight_poisson->at(n_boot_strap));
				++count;
			}
		}

		Double_t where_vector_sr[4] = {Double_t(btagbin.at(0)), Double_t(btagbin.at(1)), jet_pt->at(0) / 1000., jet_pt->at(1) / 1000.};
		if (m_use_pt_bins_as_eta)
		{
			if (jet_eta->at(0) > jet_eta->at(1))
			{
				where_vector_sr[2] = jet_eta->at(0);
				where_vector_sr[3] = jet_eta->at(1);
			}
			else
			{
				where_vector_sr[2] = jet_eta->at(1);
				where_vector_sr[3] = jet_eta->at(0);
				double save = where_vector_sr[0];
				where_vector_sr[0] = where_vector_sr[1];
				where_vector_sr[1] = save;
			}
		}
		sr_histograms[eventFlav].at(0)->Fill(where_vector_sr, weight);
		if(!m_data){
			sr_histogram_uw[eventFlav]->Fill(where_vector_sr);
			if (bin_means_for_sr_histo)
				bin_means_for_sr_histo->Fill(where_vector_sr, weight, jet_truthflav);
		}
		int count=0;
		for (int n_boot_strap = m_n_boot_strap_min; n_boot_strap < m_n_boot_strap_max; n_boot_strap++)
		{
			sr_histograms[eventFlav].at(count + 1)->Fill(where_vector_sr, weight * weight_poisson->at(n_boot_strap));
			++count;
		}
		if (!m_data)
		{
			for (int j = 0; j < 2; j++)
			{
				if (m_use_pt_bins_as_eta)
				{
					if (jet_truthflav->at(j) == 5)
						h_b_jet->Fill(btagbin.at(j), jet_eta->at(j), weight);
					else
						h_l_jet->Fill(btagbin.at(j), jet_eta->at(j), weight);
				}
				else
				{
					if (jet_truthflav->at(j) == 5)
						h_b_jet->Fill(btagbin.at(j), jet_pt->at(j) / 1000., weight);
					else
						h_l_jet->Fill(btagbin.at(j), jet_pt->at(j) / 1000., weight);
				}
			}
		}
	}
}

std::string Hist_for_fit_Container::getEventFlav(std::vector<int> *jet_truthflav)
{
	std::string eventFlav = "";
	if (m_data)
	{
		eventFlav = "data";
	}
	else
	{
		for (int i = 0; i < 2; i++)
		{
			if (jet_truthflav->at(i) == 4)
				eventFlav = eventFlav + "c";
			else if (jet_truthflav->at(i) == 5)
				eventFlav = eventFlav + "b";
			else
				eventFlav = eventFlav + "l"; //this is also tau jets!
		}
	}
	return eventFlav;
}
