// JetContainer.h
#pragma once

#include "VariableContainer.h"
#include "VariablePtBinsContainer.h"
#include "VariablePtBinsCorrelationContainer.h"
#include "TreeReader.h"
#include <string>

class jet_Container : public TObject
{
 public:
  std::string name;
  int n_jets;
  bool m_data;
  Variable_Container *jet_pt;
  Variable_Container *jet_eta;
  Variable_Container *jet_eta_pt_gt_60;
  Variable_Container *jet_phi;
  // Variable_Container *jet_DL1d;
  // Variable_Container *jet_GN1;
  Variable_Container *jet_GN2;
  Variable_Container *jet_m_jl;
  Variable_Container *HadronConeExclExtendedTruthLabelID;
  Variable_ptBins_Container *jet_HadronConeExclExtendedTruthLabelID_pt;
  
 public:
  // constructor
  jet_Container(std::string name, int n_jets, bool data, std::vector<std::string> eflavours);
  
  // Destructor
  ~jet_Container();
  
  // Write something
  void Write();
  
  // Add Jet to something
  void addJet(int jet_n,TreeReader *selector, double weight);
  
  // Gets DArray
  double* getDArray(int bins, double lower, double upper);


  // Calculate DL1 by Hand
  std::vector<float> DiscriminantHand(std::vector<float> pb,std::vector<float> pc,std::vector<float> pu, double fraction_c, double fraction_tau=-999.);
  ClassDef(jet_Container,1);
};
