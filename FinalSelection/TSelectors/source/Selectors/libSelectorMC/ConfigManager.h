/*
 * OutputManager.hh
 * 
 * Created on: 2024.09.29
 * Author: Cen Mo
 */

#ifndef CONFIGMANAGER_HH
#define CONFIGMANAGER_HH

#include <vector>
#include "macros/Singleton.hpp"

class ConfigManager : public Singleton<ConfigManager>
{
public:
  void ParseArgs(int argc, char *argv[]);
  double weight_lumi() { return m_w_lumi; }
  int hadronization() { return m_hadronization; }
  int bsbin() { return m_bsbin; }
  std::string input_file() { return m_input_file; }
  std::string output_file() { return m_output_file; }
  std::string systematic_name() { return m_systematic_name; }
  std::string NPLepton_file() { return m_NPLepton_file; }
  std::string jet_collection() { return m_jet_collection; }
  std::string jet_collection_lightjets() { return m_jet_collection_lightjets; }
  bool is_data() { return m_is_data; }
  bool run_fakes() { return m_run_fakes; }
  bool boot_strap() { return m_boot_strap; }
  bool apply_lf_calib() { return m_apply_lf_calib; }
  bool save_fits() { return m_save_fits; }
  bool save_bin_means_for_sr_histo() { return m_save_bin_means_for_sr_histo; }
  int num_pt_bins() { return m_num_pt_bins; }
  std::vector<double> jet_pt_bins() { return m_jet_pt_bins; }

private:
  friend class Singleton<ConfigManager>;
  ConfigManager(){};

  double m_w_lumi = 1.;
  int m_hadronization=410470;
  int m_bsbin;
  std::string m_input_file, m_output_file, m_systematic_name, m_NPLepton_file;
  std::string m_jet_collection, m_jet_collection_lightjets;
  bool m_is_data, m_run_fakes, m_boot_strap, m_apply_lf_calib, m_save_fits, m_save_bin_means_for_sr_histo;
  
  int m_num_pt_bins;
  std::vector<double> m_jet_pt_bins;

};


#endif
