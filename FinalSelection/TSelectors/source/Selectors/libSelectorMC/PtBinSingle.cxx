// PtBinSingle.cxx

#include "PtBinSingle.h"
#include "TDirectory.h"
ClassImp(Pt_bin_single)

Pt_bin_single::Pt_bin_single(std::string name, std::string taggerName, int n_pt, Double_t pt_1_low, Double_t pt_1_high, Double_t pt_2_low, Double_t pt_2_high, int n_tagger, const Double_t* tagger_bins, bool data, std::vector<std::string> eflavours, std::string pt_label_jet1, std::string pt_label_jet2)
{
	this->name=name;
	this-> pt_1_low = pt_1_low;
	this-> pt_1_high = pt_1_high;
	this-> pt_2_low=pt_2_low;
	this-> pt_2_high= pt_2_high;
	this->data = data;
	this->m_pt_label_jet1 = pt_label_jet1;
	this->m_pt_label_jet2 = pt_label_jet2;
	Int_t bins_sr[4] = {n_tagger, n_tagger, n_pt, n_pt};
	for (const auto &eventFlav : eflavours)
	{
		std::string hist_name="h_" + name + "_" + eventFlav;
		THnSparseD *h_this = new THnSparseD(hist_name.c_str(), hist_name.c_str(), 4, bins_sr, NULL, NULL);
		h_this->SetBinEdges(0, tagger_bins);
		h_this->GetAxis(0)->SetTitle((std::string(taggerName) + "_1").c_str());
		h_this->SetBinEdges(1, tagger_bins);
		h_this->GetAxis(1)->SetTitle((std::string(taggerName) + "_2").c_str());
		h_this->GetAxis(2)->Set(n_pt, pt_1_low, pt_1_high);
		h_this->GetAxis(2)->SetTitle(m_pt_label_jet1.c_str());
		h_this->GetAxis(3)->Set(n_pt, pt_2_low, pt_2_high);
		h_this->GetAxis(3)->SetTitle(m_pt_label_jet2.c_str());
		h_this->Sumw2();
		h_pt_bin[eventFlav] = h_this;
	}
}

bool Pt_bin_single::Fill(Double_t* where_vector, double weight,std::vector<int> *jet_truthflav)
{
	Double_t jet_pt_1= where_vector[2];
	Double_t jet_pt_2= where_vector[3];
	if ((jet_pt_1>pt_1_low) && (jet_pt_1 < pt_1_high))
	{
		if ((jet_pt_2>pt_2_low) && (jet_pt_2 < pt_2_high))
		{	
			std::string eventFlav="";
			if (!data)
			{
				for (int i = 0; i < 2; i++)
				{
					if (jet_truthflav->at(i) == 5)
						eventFlav = eventFlav + "b";
					else
						eventFlav = eventFlav + "l"; //this is also c and tau jets!
				}
			}
			h_pt_bin[eventFlav]->Fill(where_vector, weight);
			return true;
		}
	}
	return false;
}

void Pt_bin_single::Write()
{
  std::cout<<" .... saving : " << name <<  " flav: ";

  for (const auto &x : h_pt_bin)
    {
      std::cout<< x.first<<", ";
      x.second->Write();
    }
  std::cout<<std::endl;
}
