// VariablePtBinsContainer.h
#pragma once

#include <TH3F.h>
#include <string>
#include <map>

class Variable_ptBins_Container
{
public:
	std::string name;
	bool data;
	static const int NBINS_pt = 5;
	const double pt_bins[NBINS_pt + 1] = {20., 40., 60., 140., 250., 400.}; //{20., 30., 40., 60., 85., 110., 140., 175., 250., 400.};
	std::map<std::string, TH3F *> histograms; 
public:
	// Constructor
	Variable_ptBins_Container(std::string name, int n_jets, int nxbins, const double *xbins, bool data, std::vector<std::string> eflavours);

	// Destructor
	~Variable_ptBins_Container();

    // Writes the histos it seems
	void Write();

    // Adds the event
    void addEvent(double value, std::vector<float> *jet_pt, double weight, std::vector<int> *jet_truthflav);

    // Gets event flavour
    std::string getEventFlav(std::vector<int> *jet_truthflav);

    ClassDef(Variable_ptBins_Container,1);
};
