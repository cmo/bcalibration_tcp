#include "libSelectorMC/finalSelectionMC.h"
#include "libSelectorMC/DilepChannel.h"
#include "libSelectorMC/TreeReader.h"
#include "libSelectorMC/HistsForFitContainer.h"
#include "libSelectorMC/BTagToolContainer.h"
#include "libSelectorMC/PtBinMeanContainer.h"
#include "libSelectorMC/PtBinSingle.h"
#include "libSelectorMC/JetContainer.h"
#include "libSelectorMC/VariableContainer.h"
#include "libSelectorMC/VariablePtBinsContainer.h"
#include "libSelectorMC/VariablePtBinsCorrelationContainer.h"
#include "libSelectorMC/VariablePtContainer.h"
#include "libSelectorMC/XXContainer.h"
#include "libSelectorMC/JLCombination.h"
#include "libSelectorMC/JLMinMSubCombination.h"
#include "libSelectorMC/JLMinMSumCombination.h"
#include "libSelectorMC/JLdRCombination.h"
#include "libSelectorMC/ConfigManager.h"


#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

for loading the object selection at run time                                                                                                                                                                      
#pragma link C++ class finalSelectorMC+;
#pragma link C++ class DiLepChannel+;
#pragma link C++ class TreeReader+;
#pragma link C++ class Hist_for_fit_Container+;
#pragma link C++ class Pt_bin_mean_container+;
#pragma link C++ class Pt_bin_single+;
#pragma link C++ class BtagTool_Container+;
#pragma link C++ class XX_Container+;
#pragma link C++ class jet_Container+;
#pragma link C++ class Variable_ptBins_Container+;
#pragma link C++ class Variable_ptBins_Correlation_Container+;
#pragma link C++ class Variable_Container+;
#pragma link C++ class Variable_pt_Container+;
#pragma link C++ class JL_Combination+;
#pragma link C++ class JL_min_m_sub_Combination+;
#pragma link C++ class JL_min_m_sum_Combination+;
#pragma link C++ class JL_closest_dR_Combination+;


#endif
