import ROOT
import os
import glob
from options_file import options
import RetrieveSampleList
import argparse

# Write the folder you want to create here
folderToCreate = options.version_tag
RSE = 'CERN-PROD_PERF-FLAVTAG'

parser = argparse.ArgumentParser(description='Processes the final selection and plots some cuts.')
parser.add_argument('file', type=str, default='./Dir20240705/FTAG-DiLepMET-v2.0.0', help='input file')
parser.add_argument('-r', '-R', '--RSE', help='specify RSE storage element which should be read (default: %s)' % RSE, default=RSE)
args = parser.parse_args()

folderToCreate=options.input_selection_dir
print("OPTIONS DATA NAME  : ", options.data_name)
os.system("mkdir -p %s "%(folderToCreate))
if options.data_name == "dataRun3" or options.data_name == "data22" or options.data_name =="data23":
    run_tag = "run3"
else:
    run_tag = "run2"

RetrieveSampleList.createDSFolders(args.file, args.RSE, run_tag, folderToCreate)