import os
import subprocess
import argparse
import fnmatch
import sys,glob,getpass,socket

from utils.DSParser import DSLabelParser


def prettyPrint(preamble, data, width=30, separator=":"):
    """
    Prints uniformly-formatted lines of the type "preamble : data".
    """
    preamble = preamble.ljust(width)
    print(f'{preamble}{separator} {data}')


def getDataSamplePaths(dsname, RSE):
    """
    Get the list of ntuple paths for a given dataset name and RSE.
    """
    prettyPrint("Get the Dataset Files for", dsname)
    prettyPrint("on RSE", RSE)
    command = f"rucio list-file-replicas --protocol root " + (f"--rse {RSE} " if RSE else "") + dsname
    print(f"{command}")
    DSReplicas = subprocess.getoutput(command)
    prettyPrint("commandOut: ", DSReplicas)

    # get the list of files
    file_dict = {}
    for part in DSReplicas.split():
        flag = False
        if part.startswith("srm://") or part.startswith("https://") or part.startswith("dcap://"):
            flag = True
            print(f"Warning: {part} not start with root://")
        elif part.startswith("root://"):
            if part.startswith("root://eosatlas.cern.ch:1094//"):
                part = part.replace("root://eosatlas.cern.ch:1094/", "")
            flag = True
        if flag:
            key = part.split('/')[-1]
            if file_dict.get(key) is None:
                file_dict[key] = part
    DS = list(file_dict.values())
    return DS


def getPathListFromDSName(dsname, RSE):
    prettyPrint('Creating file list for', dsname)
    DS = getDataSamplePaths(dsname, RSE)
    if len(DS) == 0:
        print(f"No datasets found for {dsname}")
    return DS


def getPathDictFromTXT(file_name, RSE):
    """
    Get dictionary of dataset names and their corresponding ntuple paths from a text file.
    return: {dsname: [path1, path2, ...], ...}
    """
    dsfile = open(file_name)
    ds_path_dict = {}
    for line in dsfile:
        if line.startswith('#'): continue
        realline = line.strip()
        if not realline: continue
        ds_path_dict[realline] = getPathListFromDSName(realline, RSE)
    return ds_path_dict


def createDSFolders(file_name, RSE, run_tag='run3', out_path=''):
    """
    Create folder for current sample list and put the list into the folder.
    """
    inpath, inFileName = os.path.split(file_name)
    out_path = out_path if out_path!='' else inpath

    ds_path_dict = getPathDictFromTXT(file_name, RSE)

    ds_label_parser = DSLabelParser[run_tag]
    list_not_found = []
    for ds_string, path_list in ds_path_dict.items():
        if path_list is None:
            print(f"ERROR: {ds_string} not found")
            continue
        ds_found = False
        for label, ds_list in ds_label_parser.items():
            for dsid in ds_list:
                if fnmatch.fnmatch(ds_string, dsid):
                    ds_label = label
                    print(f"INFO: {ds_string} -> {ds_label}")
                    cur_folder = f"{out_path}/{ds_label}"
                    os.makedirs(cur_folder, exist_ok=True)

                    ds_name = ds_string.split(':')[-1]
                    file_name = f"{cur_folder}/{ds_name}.txt"
                    if os.path.exists(file_name):
                        print(f"WARN: {file_name} duplicated")
                    with open(f"{file_name}", 'w') as file:
                        for path in path_list:
                            file.write(path + '\n')
                    ds_found = True
                    # break # Singletop sample goes into different folder. So we don't break here.
        if not ds_found:
            list_not_found.append(ds_string)
    
    if len(list_not_found) > 0:
        print(f"ERROR: {len(list_not_found)} datasets not found:")
        for ds in list_not_found:
            print(f"    {ds}")

    # create a summary txt file for each ds_label.
    # The summary file contains the list of paths for all txt files in the ds_label.
    os.system(f"""
    for folder in `ls -d {out_path}/*/`; do
        ds_label=`basename $folder`
        sumary_file={out_path}/$ds_label.txt
        if [ -f $sumary_file ]; then
            rm $sumary_file
        fi
        for file in `ls $folder/*.txt`; do
            fpath=`realpath $file`
            echo $fpath >> $sumary_file
        done
    done
    """)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='This script lists datasets located at a RSE location. Futher patterns to find or exclude can be specified.', prog='ListGroupDisk', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-d', '-l', '-D', '-L', '--dataset_name', help='dataset_name to collect data for', default='')
    parser.add_argument('-o', '-O', '--OutDir', help='specify output directory to put file list(s) into', default='')
    parser.add_argument('-r', '-R', '--RSE', help='specify RSE storage element which should be read (default: "")', default='')
    parser.add_argument('-t', '-T', '--run_tag', help='specify run tag to use for dataset labelling', default='run3')
    parser.add_argument('input_files', nargs='*', help='input files')
    
    RunOptions = parser.parse_args()
    outDir = RunOptions.OutDir
    RSE = RunOptions.RSE
    run_tag = RunOptions.run_tag

    if RunOptions.input_files:
        for input_file in RunOptions.input_files:
            createDSFolders(input_file, RSE, run_tag, outDir)
    else:
        path_list = getPathListFromDSName(RunOptions.dataset_name, RunOptions.RSE)
        print(f'File list for {RunOptions.dataset_name}:')
        for path in path_list:
            print(path)
        
