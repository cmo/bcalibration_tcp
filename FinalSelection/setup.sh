SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

setupATLAS
lsetup git
cd $SCRIPT_DIR/TSelectors

# If the directory doesn't exist, create it and run the build steps
if [[ ! -d build ]];
then
    mkdir build
    cd build
    asetup AnalysisBase,25.2.4,here
    cmake ../source/
    make -j8
else
    cd build
    asetup AnalysisBase,25.2.4,here
fi

# Dynamically source the setup using wildcard match
setupDir=$(ls -d x86_64* | head -n 1)
echo "Sourcing directory: $setupDir"
source $setupDir/setup.sh
cd ../../

# Export LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PWD/TSelectors/build/lib

# VOMS Proxy Management Function
_voms_proxy_long ()
{
    if ! type voms-proxy-info &> /dev/null; then
        echo "voms not set up!" 1>&2
        return 1
    fi

    local VOMS_ARGS="--voms atlas"
    local PROXY_PATH=""
    
    # Check if proxy exists, if not, prompt for passphrase and create one
    if ! voms-proxy-info --exists; then
        VOMS_ARGS+=" --valid 96:00"
        
        # Prompt for the passphrase securely
        echo -n "Enter your GRID passphrase: "
        read -s GRID_PASSPHRASE
        echo
        
        # Use the passphrase with voms-proxy-init, display the output and capture it
        OUTPUT=$(echo "$GRID_PASSPHRASE" | voms-proxy-init $VOMS_ARGS --pwstdin 2>&1 | tee /dev/tty)
        PROXY_PATH=$(echo "$OUTPUT" | grep -oP 'Created proxy in \K.*(?=\.)')
    else
        echo "Using existing proxy"
        PROXY_PATH=$(voms-proxy-info --path)
    fi

    if [[ -n $PROXY_PATH ]]; then
        # Copy proxy to condor submission directory
        cp "$PROXY_PATH" "htcondor_submit/"
    else
        echo "Failed to create or locate the proxy"
        return 1
    fi
}

# Initialize VOMS Proxy
if ! _voms_proxy_long; then
    echo "Failed to set up VOMS proxy!" 1>&2
    return 1
fi

lsetup rucio
export PYTHONPATH=$PYTHONPATH:$SCRIPT_DIR