import ROOT
import os
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import  TCanvas, TH1F
import argparse,sys
import shutil
from utils import SampleInfoRetriever, DSParser
from options_file import options
## Parallelisation 

from functools import partial
import multiprocessing as mp
import logging
logging.basicConfig(level = 'INFO', format = ">>> [%(levelname)s] %(module).s%(name)s: %(message)s")
log = logging.getLogger("FinalSelection")


def get_kfactor_crossection(dsid):
    # Get the kfactor and cross section for the given dsid
    # # commented are codes that used by cmo for his qualification task
    # if options.data_name == "dataRun3":
    #     PathToXSection = "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/AnalysisTop/TopDataPreparation/XSection-MC21-13p6TeV.data"
    # elif options.data_name == "data22":
    #     PathToXSection= "/afs/cern.ch/user/c/cmo/private/workspace/qualification_task/bjets_ttbardilepton_PDF/AnalysisTop/grid/MyXSection-MC23-13p6TeV.data"
    if options.data_name == "data22" or options.data_name == "data23" or options.data_name == "data24" or options.data_name == "dataRun3":
        PathToXSection= "/afs/cern.ch/user/c/cmo/private/workspace/qualification_task/bjets_ttbardilepton_PDF/AnalysisTop/grid/MyXSection-MC23-13p6TeV.data"
    else:
        PathToXSection = "/afs/cern.ch/user/c/cmo/private/workspace/qualification_task/bjets_ttbardilepton_PDF/AnalysisTop/grid/MyXSection-MC20-13TeV.data"
    log.info(f"PathToXSection : {PathToXSection}")

    kfactor, crossection = -1, -1
    with open(PathToXSection,"r") as f:
        for line in f:
            if line.startswith("#"):
                continue
            if str(dsid) in line:
                split=line.split()
                log.info(f"Information for {dsid}: {split}")
                if kfactor!=-1 and crossection!=-1:
                    log.error(f"Found multiple entries for dsid: {dsid}")
                    sys.exit(1)
                kfactor = float(split[2])
                crossection = float(split[1])
    if kfactor == -1:
        log.error(f"kfactor not found for dsid: {dsid}")
        sys.exit(1)
    return kfactor, crossection


def get_weight_to_luminosity(sample_info_retriever, dataset_name, systematic):
    # Get the weight to luminosity and DSID for the given dataset
    log.info(f"Getting weight to luminosity for sample: {dataset_name} with systematic: {systematic}")
        
    # get dsid
    dsid_list = sample_info_retriever.dsids.values()
    if len(set(dsid_list)) > 1:
        log.error(f"Multiple DSIDs found in the dataset: {dataset_name}")
        sys.exit(1)
    dsid = list(set(dsid_list))[0]

    # generated sum of weights
    # generated_SOW = sample_info_retriever['NOSYS']["initial sum of weights"]
    generated_SOW = sample_info_retriever[systematic]['initial sum of weights'] 

    # TODO: systematics

    log.info(f"DSID: {dsid}; sum_of_weights: {generated_SOW}")

    # get kfactor and crossection
    kfactor, crossection = get_kfactor_crossection(dsid)
    # TODO: deal with systematics

    # get mc luminoisty
    lumi_mc = generated_SOW/(crossection * kfactor)

    # get corresponding data luminosity
    year_mc = DSParser.get_year_MC(dataset_name)
    if not year_mc:
        log.error(f"Year not found for dataset: {dataset_name}")
        sys.exit(1)
    lumi_data = DSParser.get_lumi(year_mc)
    log.info(f"Data luminosity: {lumi_data}; MC luminosity: {lumi_mc}")

    # get weight to luminosity
    w_lumi = lumi_data/lumi_mc if lumi_mc!=0 else 1.
    log.info(f"Weight to luminosity: {w_lumi}. Cross section: {crossection}. kfactor: {kfactor}. Sum of initial MC weights: {generated_SOW}")
    return w_lumi, dsid


def final_select_sample(args, sample):
    # Run the TSelector to do the event selection for the sample
    dsid=sample[0]
    lumi_weight = sample[1]
    sample_ntuple_list_file = sample[2]
    output_file_name = sample[3]

    log.info(f"Running final selection for dataset: {sample_ntuple_list_file}")

    cmd=shutil.which("RunTSelectorMC")    
    if cmd == "":
        log.error("Cannot find the RunTSelectorMC. Did you forget to run source ./TSelectors/build/x86_64-centos7-gcc11-opt/setup.sh ?")
        sys.exit(-1)
    
    # common options
    full_command = [
        cmd,
        "-i", sample_ntuple_list_file,
        "-o", output_file_name,
        "--save_fit_input",
        "save_bin_means_for_sr_histo",
        f"-l {lumi_weight}",
    ]

    # options specific to the mc/data
    if args.isData:
        log.info("Running on data")
        full_command.extend([
            "--data",
        ])
    else:
        log.info(f"Running on MC.")
        full_command.extend([
            "--run_fakes",
            f"--hadronization {args.hadronization}",
            # "--use_pt_bins_as_eta",
            # "-f NPLeptonsFile", # this option is used only if correctFakes is used
            # "--apply_lf_calib",
            # following 2 options are used only if apply_lf_calib is used
            "--jet_collection AntiKt4EMPFlowJets",
            "--jet_collection_lf AntiKt4EMPFlowJets",
            "--save_bin_means_for_sr_histo"
        ])
        # TODO: deal with systematics properly
        if args.systematic != "nominal":
            full_command.extend([
                f"--syst {args.systematic}",
            ])


    full_command = " ".join(full_command)
    log.info(f"Running command: {full_command}")

    # run the command
    results = os.popen(full_command).readlines()

    for resline in results:
        print(resline.split("\n")[0])
        if any(x in resline for x in ["Break","illegal"]):
            return "\033[91m Job failed : %s \033[0m"%resline.split("\n")[0]
    log.info("Job ran successfully")
    return "\033[92m Job ran successfully \033[0m"


def post_process(samples, isData, combined_output_file_path):
    # Post process the output of the final selection
    log.info("Post processing the output of the final selection")
    log.info(f"Samples: {samples}")

    # record the weight to luminosity for each sample
    file_list = []
    w_lumi_hist = TH1F("w_lumi_hist", "Weight to luminosity", len(samples), 0, len(samples))
    for isample, sample in enumerate(samples, 1):
        dsid, w_lumi, sample_ntuple_list_file, output_file_name = sample
        log.info(f"Post processing for dataset: {output_file_name}")

        # check if the output file is correctly created
        if not os.path.exists(output_file_name):
            log.error(f"Output file not found: {output_file_name}. Skipping {dsid}")
        else:
            file_list.append(output_file_name)
            if not isData:
                # record the weight to luminosity in the output file
                root_file = ROOT.TFile(output_file_name, "read")
                w_lumi_hist.AddBinContent(isample, root_file.Get("applied_lumi_weight")[0])
                w_lumi_hist.GetXaxis().SetBinLabel(isample, str(dsid))
                root_file.Close()
    
    log.info("Combining the output files")
    command="hadd -f " + combined_output_file_path + " " + " ".join(file_list)
    log.info(f"Running command: {command}")
    os.system(command)
    if not isData:
        c = TCanvas()
        outfile= ROOT.TFile(combined_output_file_path,"update")
        w_lumi_hist.Write()
        w_lumi_hist.SetMaximum(3)
        w_lumi_hist.Draw()
        outfile.Close()
    log.info("Post processing done")


def main(args):
    # set input and output
    input_file = args.input_file
    sample_common_name = input_file.split("/")[-1].split(".")[0]
    output_dir = f"{options.output_dir}/{sample_common_name}/"
    output_sub_dir = f"{output_dir}/split_by_sample/"
    if not os.path.exists(output_sub_dir):
        os.makedirs(output_sub_dir)
    
    # set systematics
    if args.systematic=="nominal" or args.isData:
        systematic_name = 'nominal'
        log.info("Running nominal only")
    else:
        systematic_name = args.systematic if args.systematic_name=="" else args.systematic_name
        log.info(f"Running systematic: {args.systematic} with name: {systematic_name}")

    log.info(f"Running final selection for: {input_file}")

    # get information of all samples
    samples=[]
    for sample_ntuple_list_file in open(input_file, "r"):
        # sample_ntuple_list_file: File that records the path to the ntuples for the current DSID
        sample_ntuple_list_file = sample_ntuple_list_file.strip() # remove the newline character
        log.info(f"Running final selection for dataset: {sample_ntuple_list_file}")

        dataset_name = sample_ntuple_list_file.split("/")[-1] # dataset name is the last part of the path. But ends with .txt
        dataset_name = ".".join(dataset_name.split(".")[:-1]) # remove the .txt
        output_file_name = f"{output_sub_dir}/{dataset_name}_{systematic_name}.root"

        # Get the information of the dataset
        ntuple_list = []

        ## Get the weight to luminosity and the dsid of the current dataset
        if not args.isData:
            with open(sample_ntuple_list_file) as f:
                for line in f:
                    ntuple_path = line.strip() # remove the newline character
                    ntuple_list.append(ntuple_path)
                sample_info_retriever = SampleInfoRetriever.SampleInfoRetriever(ntuple_list)
                weight_syst_name = args.systematic if args.is_mcweight else "NOSYS"
                w_lumi, dsid = get_weight_to_luminosity(sample_info_retriever, dataset_name, weight_syst_name)
        else:
            w_lumi, dsid = 1, -1
        samples.append((dsid, w_lumi, sample_ntuple_list_file, output_file_name))
    
    # run the final selection in parallel for all samples
    results=None
    num_cores = 4
    filledFunc = partial(final_select_sample, args)
    with mp.Pool(processes = num_cores, maxtasksperchild = 1) as pool:
        results = pool.map(filledFunc, samples)
        
    log.info("The following warnings were issued: ")
    for i,res in enumerate(results):
        log.info("Job %d: "%i+res)
    
    # post process the output
    combined_output_file_path = f"{output_dir}/{sample_common_name}_{systematic_name}_combination.root"
    post_process(samples, args.isData, combined_output_file_path)


if __name__=='__main__':
    parser = argparse.ArgumentParser(description='Run the final selection')
    parser.add_argument("--log-level", "-l", type = str, default = "INFO" )
    parser.add_argument('--isData', action='store_true', help='the input is data (default: false)')
    parser.add_argument('--systematic', default="nominal", help='systematic suffix to run over. default=nominal.')
    parser.add_argument('--systematic_name', default='', help='name of systematic to save the output. default is the same as the systematic')
    parser.add_argument('--is_mcweight', action='store_true', help='the input is mcweight (default: false)')
    parser.add_argument('--hadronization', default="410470",
                help='hadronization model used for the Generator. needed for MC/MC sf." \
                "https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTagCalib2017#MC_MC_Scale_Factors_for_Analysis')
    parser.add_argument('input_file', 
                help='txt file that records a list of path to the files that record the path to the ntuples. Each line corresponds to a DSID')
    args = parser.parse_args()

    # set log level
    log.setLevel(args.log_level)

    # print args
    log.info(f"Arguments: {args}")

    main(args)
