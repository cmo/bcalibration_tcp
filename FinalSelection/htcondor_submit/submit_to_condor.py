import os
import subprocess
import shutil, socket
import options_file
import getpass,socket
import argparse
import random
import utils.SystematicsManager as SystematicsManager

#we have to create file structure first
def writeShellScript(job_dir,job_name,inFile,runOptions):
    username = getpass.getuser()
    fsh = open(job_dir+job_name+'.sh', 'w')
    fsh.write("#!/bin/bash\n")
    fsh.write('cd '+os.getcwd()+'\n')
    fsh.write('cd ..\n')
    fsh.write("export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase\n")
    fsh.write("source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh\n")
    if "alopezs" in username and 'zeuthen.desy.de' in socket.gethostname():
        fsh.write('source  setup_desy.sh\n')
    else:
        fsh.write('source  setup.sh\n')
    fsh.write('lsetup \"emi -w\" \n')
    run_command = 'python finalSelection.py '+inFile+ ' ' + runOptions +' \n'
    fsh.write(run_command)
    fsh.close()
    return run_command


def writeJobSubmit(job_dir,job_name):
    fsubmit = open(job_dir+job_name+'.submit', 'w')
    cwd=os.getcwd()
    fsubmit.write("executable     = "+cwd+"/"+job_dir+job_name+".sh\n")
    # if (not job_name.endswith("1up") and not job_name.endswith("1down")) or (random.random() < 0.3):
    fsubmit.write("output         = "+cwd+"/"+job_dir+job_name+".out\n")
    fsubmit.write("error          = "+cwd+"/"+job_dir+job_name+".error\n")
    fsubmit.write("log            = "+cwd+"/"+job_dir+job_name+".log\n")
    fsubmit.write("RequestCpus = 4\n")
    fsubmit.write("use_x509userproxy = true\n")
    if 'cern.ch' in socket.gethostname():
        if "alopezs" in getpass.getuser():
            fsubmit.write("x509userproxy="+os.getcwd()+"/x509.proxy\n")
        elif "wojang" in getpass.getuser():
            fsubmit.write("x509userproxy = $ENV(HOME)/.globus/gridproxy.cert\n")
            fsubmit.write('environment = "X509_CERT_DIR=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/etc/grid-security-emi/certificates"\n')
        elif "cmo" in getpass.getuser():
            fsubmit.write("x509userproxy="+os.getcwd()+"/x509up_u157706\n")  
        else:
            fsubmit.write("x509userproxy="+os.getcwd()+"/x509up_u145473\n")  
        fsubmit.write("+JobFlavour = \"tomorrow\" \n")
        fsubmit.write('+RequestRuntime         =  '+str(60*60*10) +'\n') 
        fsubmit.write('Request_Memory          =  '+str(1024*10) +'\n')    
    else:
        fsubmit.write("x509userproxy = $ENV(HOME)/.globus/gridproxy.cert\n")
        fsubmit.write('environment = "X509_CERT_DIR=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/etc/grid-security-emi/certificates"\n')
        fsubmit.write("job_queue  = q4h \n")      
    fsubmit.write("universe       = vanilla\n")
    fsubmit.write("queue 1 \n")
    fsubmit.close()


def submitFinalSelection(sample_name, run_options, job_name_suffix=""):
    job_name_suffix = f"_{job_name_suffix}" if job_name_suffix!="" else ""
    job_name = f"{options.data_name}_{sample_name}{job_name_suffix}"
    in_file = f"{options.input_selection_dir}{sample_name}.txt"
    job_dir='condor_logs/'+job_name+'/'

    cwd=os.getcwd()
    if not os.path.exists(job_dir):
        os.makedirs(job_dir)
        run_command = writeShellScript(job_dir, job_name, in_file, run_options)
        writeJobSubmit(job_dir, job_name)
        subprocess.check_call(["chmod", "u+x", cwd+"/"+job_dir+job_name+".sh"])
        subprocess.check_call(["chmod", "u+x", cwd+"/"+job_dir+job_name+".submit"])
        subprocess.check_call(["condor_submit", cwd+"/"+job_dir+job_name+".submit"])
        print("submitted: "+ run_command)
    else:
        # shutil.rmtree(job_dir)
        # os.makedirs(job_dir)
        print("already submitted: "+ 'python finalSelection.py '+in_file+ ' ' + run_options +' \n')



class RunOptionsManager:
    common_run_options_MC = " "
    @classmethod
    def get_run_options_data(cls):
        return " --isData"

    @classmethod
    def get_run_options_nominal(cls, sample: options_file.sample):
        return cls.common_run_options_MC + f" --hadronization {sample.hadronization}"

    @classmethod
    def get_run_options_branch_syst(cls, sample: options_file.sample, syst: str) -> str:
        return f"{cls.common_run_options_MC} --hadronization {sample.hadronization} --systematic {syst}"

    @classmethod
    def get_run_options_weight_syst(cls, sample: options_file.sample, syst: str, syst_name: str="") -> str:
        syst_name = syst_name if syst_name!="" else syst
        return f"{cls.common_run_options_MC} --hadronization {sample.hadronization} --systematic {syst} --systematic_name {syst_name} --is_mcweight"

if __name__=='__main__':
    parser = argparse.ArgumentParser(description='Submit jobs to condor')
    parser.add_argument('--no_data', dest='no_data', action='store_true', help='Do not run data. Default is False')
    parser.add_argument('--run_branch_syst', dest='run_branch_syst', action='store_true', help='Run systematics. Default is False')
    parser.add_argument('--run_weight_mc_syst', dest='run_weight_mc_syst', action='store_true', help='Run systematics. Default is False')
    parser.add_argument('--run_alternative_sample', dest='run_alternative_sample', action='store_true', help='Run systematic samples. Default is False')
    # parser.add_argument('--run_pdf_syst', dest='run_pdf_syst', action='store_true', help='Run PDF systematics. Default is False')
    # parser.add_argument('--run_QCD_syst', dest='run_QCD_syst', action='store_true', help='Run QCD systematics. Default is False')
    # parser.add_argument('--run_weight_syst', dest='run_weight_syst', action='store_true', help='Run weight systematics. Default is False')
    # parser.add_argument('--run_systs-for-fake-plots', dest='run_systs_for_fake_plots', action='store_true', help='Run systematics for fake plots. Default is False')

    args = parser.parse_args()
    print(args)

    default_run_options = ' '

    options = options_file.options
    input_folder=options.input_selection_dir

    # submit jobs for data
    if not args.no_data:
        run_options = RunOptionsManager.get_run_options_data()
        submitFinalSelection(options.data_name, run_options)

    samples_to_run = list(options.nominal_samples.values())  

    # submit jobs for MC nominal
    for sample in samples_to_run + (list(options.alternative_samples.values()) if args.run_alternative_sample else []):
        run_options = RunOptionsManager.get_run_options_nominal(sample)
        submitFinalSelection(sample.name, run_options)

    # detector systematics
    if args.run_branch_syst:
        # run branch systematics
        systematics = SystematicsManager.get_systematics()
        for sample in samples_to_run:
            for syst in systematics:
                run_options = RunOptionsManager.get_run_options_branch_syst(sample, syst.suffix)
                submitFinalSelection(sample_name=sample.name, run_options=run_options, job_name_suffix=syst.name_for_histos)

    if args.run_weight_mc_syst:
        systematics = SystematicsManager.get_mc_weight_systematics()
        for sample_name in systematics.keys():
            sample = options.nominal_samples.get(sample_name, None)
            for syst in systematics[sample_name]:
                run_options = RunOptionsManager.get_run_options_weight_syst(sample, syst=syst.suffix, syst_name=syst.name_for_histos)
                submitFinalSelection(sample_name=sample.name, run_options=run_options, job_name_suffix=syst.name_for_histos)

