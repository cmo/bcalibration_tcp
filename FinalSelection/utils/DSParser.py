
DSLabelParser = {
    "run3":{ 
        # updated list May 2024
        'ttbar_HW7': ["*601415*", "*601229.PhPy8EG*"],
        'ttbar_PhPy8_hdamp': ["*601399*", "*601229.PhPy8EG*"],
        'ttbar_PhPy8_pthard': ["*601491*", "*601229.PhPy8EG*"],
        'Singletop_tW_PowPy8_DS': [
            "*601627*", "*601631*" # DS
            ],
        'Zjets_Sherpa22': [
            "*700786.Sh*", "*700787.Sh*", "*700788.Sh*",
            "*700789.Sh*", "*700790.Sh*", "*700791.Sh*",
            "*700792.Sh*", "*700793.Sh*", "*700794.Sh*"
            #"*700795.Sh*","*700796.Sh*", "*700797.Sh*" # neutrino
            ],
        'Wjets_Sherpa22': [
            "*700777.Sh*", "*700778.Sh*", "*700779.Sh*",
            "*700780.Sh*", "*700781.Sh*", "*700782.Sh*",
            "*700783.Sh*", "*700784.Sh*", "*700785.Sh*"
            ],
        'Diboson_Sherpa22': [
            "*701055.Sh*","*701040.Sh*","*701060.Sh*", "*701045.Sh*", "*701050.Sh*", # Sherpa 2.2.14 fully leptonic decay di-boson samples are reported buggy
            "*701125.Sh*", "*701085.Sh*", "*701115.Sh*",
            "*701090.Sh*", "*701105.Sh*", "*701120.Sh*", 
            ],
        'Singletop_PowPy8': [
            "*601348.PhPy8EG*", "*601349.PhPy8EG*", # s-channel
            "*601350.PhPy8EG*", "*601351.PhPy8EG*", # t-channel
            "*601353.PhPy8EG*", "*601354.PhPy8EG*"  # tW
            ],
        'Singletop_schan_PowPy8': [
            "*601348.PhPy8EG*", "*601349.PhPy8EG*", # s-channel
            ],
        'Singletop_tchan_PowPy8': [
            "*601350.PhPy8EG*", "*601351.PhPy8EG*", # t-channel
            ],
        'Singletop_tW_PowPy8': [
            "*601353.PhPy8EG*", "*601354.PhPy8EG*"  # tW
            ],
        'ttbar_PhPy8_nominal' : ["*601230.PhPy8EG*", "*601229.PhPy8EG*"], ### 601229: single lepton ttbar
        'data22' : [
            "*00428648*",
            "*00428700*",
            "*00428747*",
            "*00428759*",
            "*00428770*",
            "*00428776*",
            "*00428777*",
            "*00428855*",
            "*00429018*",
            "*00429027*",
            "*00429137*",
            "*00429142*",
            "*00429452*",
            "*00429469*",
            "*00429470*",
            "*00429603*",
            "*00429606*",
            "*00429612*",
            "*00429658*",
            "*00429697*",
            "*00429716*",
            "*00429782*",
            "*00429929*",
            "*00429940*",
            "*00429993*",
            "*00430036*",
            "*00430178*",
            "*00430183*",
            "*00430336*",
            "*00430341*",
            "*00430488*",
            "*00430490*",
            "*00430526*",
            "*00430536*",
            "*00430542*",
            "*00430580*",
            "*00430644*",
            "*00430647*",
            "*00430648*",
            "*00430702*",
            "*00430896*",
            "*00430897*",
            "*00431037*",
            "*00431178*",
            "*00431179*",
            "*00431215*",
            "*00431228*",
            "*00431241*",
            "*00431341*",
            "*00431493*",
            "*00431810*",
            "*00431812*",
            "*00431850*",
            "*00431885*",
            "*00431894*",
            "*00431906*",
            "*00431914*",
            "*00432180*",
            "*00435816*",
            "*00435831*",
            "*00435854*",
            "*00435931*",
            "*00435946*",
            "*00436041*",
            "*00436169*",
            "*00436182*",
            "*00436195*",
            "*00436354*",
            "*00436377*",
            "*00436422*",
            "*00436496*",
            "*00436550*",
            "*00436582*",
            "*00436584*",
            "*00436602*",
            "*00436656*",
            "*00436799*",
            "*00436863*",
            "*00436983*",
            "*00437010*",
            "*00437034*",
            "*00437062*",
            "*00437065*",
            "*00437079*",
            "*00437124*",
            "*00437484*",
            "*00437522*",
            "*00437548*",
            "*00437580*",
            "*00437623*",
            "*00437692*",
            "*00437711*",
            "*00437744*",
            "*00437756*",
            "*00437778*",
            "*00437780*",
            "*00437834*",
            "*00437837*",
            "*00437881*",
            "*00437898*",
            "*00437944*",
            "*00437971*",
            "*00437991*",
            "*00438181*",
            "*00438219*",
            "*00438234*",
            "*00438252*",
            "*00438266*",
            "*00438277*",
            "*00438298*",
            "*00438323*",
            "*00438364*",
            "*00438411*",
            "*00438446*",
            "*00438481*",
            "*00438502*",
            "*00438532*",
            "*00438638*",
            "*00438737*",
            "*00438786*",
            "*00439519*",
            "*00439529*",
            "*00439607*",
            "*00439676*",
            "*00439798*",
            "*00439830*",
            "*00439859*",
            "*00439911*",
            "*00439927*",
            "*00440199*",
            "*00440407*",
            "*00440499*",
            "*00440543*",
            "*00440570*",

            "*00440485*",
            ]
    },

    "run2": {
        # nominal
        'ttbar_PhPy8_nominal' : ["*410470*"], # nonallhad
        'Singletop_PowPy8' : ["*410644*", "*410645*", # s-channel
                              "*410658*", "*410659*", # t-channel
                              "*601353*", "*601354*"  # tW
                              ],
        'Wjets_Sherpa22' : ["*700338*", "*700339*", "*700340*", # Wenu
                            "*700341*", "*700342*", "*700343*", # Wmunu
                            "*700344*", "*700345*", "*700346*", # Wtaunu L
                            "*700347*", "*700348*", "*700349*"  # Wtaunu H
                            ],
        'Zjets_Sherpa22' : ["*700320*", "*700321*", "*700322*", # Zee
                            "*700323*", "*700324*", "*700325*", # Zmumu
                            "*700326*", "*700327*", "*700328*", # Ztautau LL
                            "*700329*", "*700330*", "*700331*", # Ztautau LH
                            "*700332*", "*700333*", "*700334*", # Ztautau HH
                            "*700335*", "*700336*", "*700337*"  # Znunu. Neutrinos??? Why not...
                            ],
        'Diboson_Sherpa22' : ["*700488*", # WW semi-leptonic
                              "*700489*", "*700490*", "*700491*", "*700492*", # WZ semi-leptonic
                              "*700493*", "*700494*", "*700495*", "*700496*", # ZZ semi-leptonic
                              "*364302*", "*364303*", "*364304*", "*364305*", # gluon-induced
                              "*700600*", "*700601*", "*700602*", "*700603*", "*700604*", "*700605*" # fully leptonic
                              ],
        'data1516' : ["*grp15*","*grp16*"],
        'data17'   : ["*grp17*"],
        'data18'   : ["*grp18*"],
        'dataRun2' : ["*grp15*","*grp16*","*grp17*","*grp18*"],
        
        # alternative coming soon..
    }
}

"""
Below codes store functions useful for extracting metadata from sample filenames
"""
import logging
log = logging.getLogger(__name__)

def run2_or_run3(isMC, file):
    run_period = get_run_period(isMC, file=file)
    if "mc23" in run_period or "data2" in run_period:
        return "Run3"
    elif "mc20" in run_period or "data1" in run_period:
        return "Run2"
    else:
        log.warning("cannot determine if sample is run2 or run3, behaviour may be unexpected")
        return None

def get_run_period(isMC, run_period=None, file=None):
    if run_period is None:
        if isMC:
            return get_year_MC(file)
        else:
            return get_year_data(file)
    else:
        return run_period

def get_year_MC(file):
    if (("r13167" in file) or ("r14859" in file)):
        return "mc20a"
    elif (("r13144" in file)  or ("r14860" in file) ):
        return "mc20d"
    elif (("r13145" in file) or ("r14861" in file)):
        return "mc20e"
    elif (("r14622" in file) or ("r14668" in file)): #r14668 is the r-tag for the trigger signal sample we're using until we have actual ggF HH samples for Run 3
        return "mc23a"
    elif ("r14799" in file): #r14799 is the r-tag used for some W samples in Run 3
        return "mc23c"
    elif ("r15224" in file):
        return "mc23d"
    elif ("r15540" in file):
        return "mc23a"
    elif ("r15530" in file):
        return "mc23d"

def get_year_data(file):
    if ("data15" in file):
        return "data15"
    elif ("data16" in file):
        return "data16"
    elif ("data17" in file):
        return "data17"
    elif ("data18" in file):
        return "data18"
    elif ("data22" in file):
        return "data22"
    elif ("data23" in file):
        return "data23"

def get_lumi(mc_year):
    if mc_year == "mc20a":
        return 32988.1 + 3219.56
    elif mc_year == "mc20d":
        return 44307.4
    elif mc_year == "mc20e":
        return 59937.2
    elif mc_year == "mc23a":
        return 29049.3
    elif mc_year in ["mc23c", "mc23d"]:
        return 25767.5
    else:
        # we expect to end up here for data files
        log.warning("not found the lumi via run year setting it to 1, be careful")
        return 1


def set_runperiods(run_periods):
    if isinstance(run_periods, str):
        run_periods = [run_periods]

    outperiods = []
    for period in run_periods:
        if period.upper() == "ALL":
            log.warning("You are running both RUN 2 and RUN 3 periods. This is not recommended")
            log.warning("It is better to run a seperate command for RUN 2 and for RUN 3") 
            log.warning("I will continue with both RUN 2 and RUN 3 enabled")           
            return set_runperiods(["RUN2", "RUN3"])
        elif period.upper() == "RUN2":
            outperiods += ["mc20a", "mc20d", "mc20e", "data15", "data16", "data17", "data18"]
        elif period.upper() == "RUN3":
            outperiods += ["mc23a", "mc23d", "data22", "data23"]
        else:
            outperiods += period
    return outperiods