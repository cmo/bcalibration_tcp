import ROOT as R
import argparse
import logging
log = logging.getLogger(__name__)

class SampleInfoRetriever(dict):
    def __init__(self, files, nominal_only=False):
        super().__init__()
        self.files = files
        self.run_numbers = {}
        self.dsids = {}
        self.nominal_only = nominal_only
        if self.nominal_only:
            log.debug(f"reading event numbers for nominal only={nominal_only}")
        self.retrieve_sample_info()

    def retrieve_sample_info(self):
        for file in self.files:
            file_data, run_numbers, dsids = self.get_file_data(file, self.nominal_only)
            self += file_data
            
            self.run_numbers[file] = run_numbers[file]
            self.dsids[file] = dsids[file]

    @staticmethod
    def get_file_data(file, nominal_only=False):
        file_data = {}
        run_numbers = {}
        dsids = {}
        read_file = R.TFile.Open(file,"READ")
        for object in read_file.GetListOfKeys():
            object_name = object.GetName()
            if nominal_only and not object_name.endswith("NOSYS"):
                continue

            syst_info = {}

            if object_name.startswith("CutBookkeeper"):
                hist = read_file.Get(object_name)
                #log.debug(f"reading {hist}")
                syst_info["initial events"] = hist.GetBinContent(1)
                syst_info["initial sum of weights"] = hist.GetBinContent(2)
                syst_info["initial sum of weights squared"] = hist.GetBinContent(3)
                variation_name = '_'.join(object_name.split('_')[3:])  # This unpacks all parts of the object_name string, ignores everything up to <run_number>, and takes the rest as 'variation_name'
                if object_name.endswith("NOSYS"):
                    syst_name = "NOSYS"
                else:
                    syst_name = variation_name

                file_data[syst_name] = syst_info
                
                # we need the run number(s) later on, read from the cutbookkeeper names: 
                object_name_parts = object_name.split('_')
                run_number = int(object_name_parts[2])
                run_numbers[file] = run_number
                dsid = int(object_name_parts[1])
                dsids[file] = dsid

        read_file.Close()

        return file_data, run_numbers, dsids

    def __add__(self,file_data):
        for key in file_data:
            if key not in self.keys():
                self[key] = {}
            for ikey,ivalue in file_data[key].items():
                if ikey in self[key].keys():
                    self[key][ikey] += ivalue
                else:
                    self[key][ikey] = ivalue
        return self

if __name__ == "__main__":
    R.gROOT.SetBatch(True)
    parser = argparse.ArgumentParser(description="Collect number of processed input events from files", add_help=False)
    parser.add_argument("files", nargs="*", help="full path to input file(s)", default=["/afs/cern.ch/user/c/cmo/private/workspace/qualification_task/bcalibration_tcp/TopCPToolkit/run-toptoolkit/output.root"])

    args = parser.parse_args()

    data = SampleInfoRetriever(args.files)
    print(f"Sample run numbers: {data.run_numbers}\n")
    print(f"Sample dsids: {data.dsids}\n")
    for key, value in data.items():
        print(key, value)
        print()


