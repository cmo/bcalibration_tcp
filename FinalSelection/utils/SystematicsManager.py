from collections import namedtuple
Systematic = namedtuple('Systematic', ['suffix', 'name_for_histos'])

systematics = [
        "EG_RESOLUTION_ALL__1down",
        "EG_RESOLUTION_ALL__1up",
        "EG_SCALE_AF2__1down",
        "EG_SCALE_AF2__1up",
        "EG_SCALE_ALL__1down",
        "EG_SCALE_ALL__1up",
        "JET_EtaIntercalibration_TotalStat__1down",
        "JET_EtaIntercalibration_TotalStat__1up",
        # "JET_EtaIntercalibration_NonClosure_PreRec__1down",
        # "JET_EtaIntercalibration_NonClosure_PreRec__1up",
        # "JET_GroupedNP_1__1down",
        # "JET_GroupedNP_1__1up",
        # "JET_GroupedNP_2__1down",
        # "JET_GroupedNP_2__1up",
        # "JET_GroupedNP_3__1down",
        # "JET_GroupedNP_3__1up",
        "JET_BJES_Response__1down",
        "JET_BJES_Response__1up",
        "JET_EffectiveNP_Detector1__1down",
        "JET_EffectiveNP_Detector1__1up",
        "JET_EffectiveNP_Detector2__1down",
        "JET_EffectiveNP_Detector2__1up",
        "JET_EffectiveNP_Mixed1__1down",
        "JET_EffectiveNP_Mixed1__1up",
        "JET_EffectiveNP_Mixed2__1down",
        "JET_EffectiveNP_Mixed2__1up",
        "JET_EffectiveNP_Mixed3__1down",
        "JET_EffectiveNP_Mixed3__1up",
        "JET_EffectiveNP_Modelling1__1down",
        "JET_EffectiveNP_Modelling1__1up",
        "JET_EffectiveNP_Modelling2__1down",
        "JET_EffectiveNP_Modelling2__1up",
        "JET_EffectiveNP_Modelling3__1down",
        "JET_EffectiveNP_Modelling3__1up",
        "JET_EffectiveNP_Modelling4__1down",
        "JET_EffectiveNP_Modelling4__1up",
        "JET_JER_EffectiveNP_Statistical1__1down",
        "JET_JER_EffectiveNP_Statistical1__1up",
        "JET_JER_EffectiveNP_Statistical2__1down",
        "JET_JER_EffectiveNP_Statistical2__1up",
        "JET_JER_EffectiveNP_Statistical3__1down",
        "JET_JER_EffectiveNP_Statistical3__1up",
        "JET_JER_EffectiveNP_Statistical4__1down",
        "JET_JER_EffectiveNP_Statistical4__1up",
        "JET_JER_EffectiveNP_Statistical5__1down",
        "JET_JER_EffectiveNP_Statistical5__1up",
        "JET_EffectiveNP_Statistical6__1down",
        "JET_EffectiveNP_Statistical6__1up",
        "JET_InSitu_NonClosure_PreRec__1down",
        "JET_InSitu_NonClosure_PreRec__1up",
        "JET_JESUnc_Noise_PreRec__1down",
        "JET_JESUnc_Noise_PreRec__1up",
        "JET_JESUnc_VertexingAlg_PreRec__1down",
        "JET_JESUnc_VertexingAlg_PreRec__1up",
        # "JET_JESUnc_mc20vsmc21_MC21_PreRec__1down",
        # "JET_JESUnc_mc20vsmc21_MC21_PreRec__1up",
        # "JET_RelativeNonClosure_MC21__1down",
        # "JET_RelativeNonClosure_MC21__1up",
        "MET_SoftTrk_ResoPara",
        "MET_SoftTrk_ResoPerp",
        "MET_SoftTrk_Scale__1down",
        "MET_SoftTrk_Scale__1up",
        "MUON_CB__1down",
        "MUON_CB__1up",
        "MUON_SAGITTA_DATASTAT__1down",
        "MUON_SAGITTA_DATASTAT__1up",
        "MUON_SAGITTA_GLOBAL__1down",
        "MUON_SAGITTA_GLOBAL__1up",
        "MUON_SAGITTA_PTEXTRA__1down",
        "MUON_SAGITTA_PTEXTRA__1up",
        "MUON_SAGITTA_RESBIAS__1down",
        "MUON_SAGITTA_RESBIAS__1up",
        "MUON_SCALE__1down",
        "MUON_SCALE__1up",

        "JET_JERUnc_Noise_PreRec__1down",
        "JET_JERUnc_Noise_PreRec__1up",
        "JET_JERUnc_Noise_PreRec_PseudoData__1down",
        "JET_JERUnc_Noise_PreRec_PseudoData__1up",

        # "JET_JERUnc_mc20vsmc21_MC21_PreRec__1down",
        # "JET_JERUnc_mc20vsmc21_MC21_PreRec__1up",
        "JET_JER_DataVsMC_MC16__1down",
        "JET_JER_DataVsMC_MC16__1up",
        "JET_JER_DataVsMC_MC16_PseudoData__1down",
        "JET_JER_DataVsMC_MC16_PseudoData__1up",

        "JET_JER_EffectiveNP_1__1down",
        "JET_JER_EffectiveNP_1__1up",
        "JET_JER_EffectiveNP_2__1down",
        "JET_JER_EffectiveNP_2__1up",
        "JET_JER_EffectiveNP_3__1down",
        "JET_JER_EffectiveNP_3__1up",
        "JET_JER_EffectiveNP_4__1down",
        "JET_JER_EffectiveNP_4__1up",
        "JET_JER_EffectiveNP_5__1down",
        "JET_JER_EffectiveNP_5__1up",
        "JET_JER_EffectiveNP_6__1down",
        "JET_JER_EffectiveNP_6__1up",
        "JET_JER_EffectiveNP_7__1down",
        "JET_JER_EffectiveNP_7__1up",
        "JET_JER_EffectiveNP_8__1down",
        "JET_JER_EffectiveNP_8__1up",
        "JET_JER_EffectiveNP_9__1down",
        "JET_JER_EffectiveNP_9__1up",
        "JET_JER_EffectiveNP_1_PseudoData__1down",
        "JET_JER_EffectiveNP_1_PseudoData__1up",
        "JET_JER_EffectiveNP_2_PseudoData__1down",
        "JET_JER_EffectiveNP_2_PseudoData__1up",
        "JET_JER_EffectiveNP_3_PseudoData__1down",
        "JET_JER_EffectiveNP_3_PseudoData__1up",
        "JET_JER_EffectiveNP_4_PseudoData__1down",
        "JET_JER_EffectiveNP_4_PseudoData__1up",
        "JET_JER_EffectiveNP_5_PseudoData__1down",
        "JET_JER_EffectiveNP_5_PseudoData__1up",
        "JET_JER_EffectiveNP_6_PseudoData__1down",
        "JET_JER_EffectiveNP_6_PseudoData__1up",
        "JET_JER_EffectiveNP_7_PseudoData__1down",
        "JET_JER_EffectiveNP_7_PseudoData__1up",
        "JET_JER_EffectiveNP_8_PseudoData__1down",
        "JET_JER_EffectiveNP_8_PseudoData__1up",
        "JET_JER_EffectiveNP_9_PseudoData__1down",
        "JET_JER_EffectiveNP_9_PseudoData__1up",

        # suffix for weight_pileup
        # "weight_pileup_UP",
        # "weight_pileup_DOWN",

        # suffix for weight_trigger
        # "weight_trigger_EL_SF_Trigger_UP",
        # "weight_trigger_EL_SF_Trigger_DOWN",
        # "weight_trigger_MU_SF_STAT_UP",
        # "weight_trigger_MU_SF_STAT_DOWN",
        # "weight_trigger_MU_SF_SYST_UP",
        # "weight_trigger_MU_SF_SYST_DOWN",

        # suffix for weight_leptonSF_tight
        # "EL_SF_Reco_UP",
        # "EL_SF_Reco_DOWN",
        # "EL_SF_ID_UP",
        # "EL_SF_ID_DOWN",
        # "EL_SF_Isol_UP",
        # "EL_SF_Isol_DOWN",
        # "MU_SF_ID_STAT_UP",
        # "MU_SF_ID_STAT_DOWN",
        # "MU_SF_ID_SYST_UP",
        # "MU_SF_ID_SYST_DOWN",
        # "MU_SF_ID_STAT_LOWPT_UP",
        # "MU_SF_ID_STAT_LOWPT_DOWN",
        # "MU_SF_ID_SYST_LOWPT_UP",
        # "MU_SF_ID_SYST_LOWPT_DOWN",
        # "MU_SF_Isol_STAT_UP",
        # "MU_SF_Isol_STAT_DOWN",
        # "MU_SF_Isol_SYST_UP",
        # "MU_SF_Isol_SYST_DOWN",
        # "MU_SF_TTVA_STAT_UP",
        # "MU_SF_TTVA_STAT_DOWN",
        # "MU_SF_TTVA_SYST_UP",
        # "MU_SF_TTVA_SYST_DOWN",
    ]
systematics = [Systematic(syst, syst) for syst in systematics]


mc_weight_systematics = {
    'ttbar': [
        Systematic("GEN_isrmuRfac10_fsrmuRfac05", "ttbar_PhPy8_weight_mc_fsr_DOWN"),
        Systematic("GEN_isrmuRfac10_fsrmuRfac20", "ttbar_PhPy8_weight_mc_fsr_UP"),
        Systematic("GEN_MUR05_MUF1_PDF260000", "ttbar_weight_mc_MuR_DOWN"),
        Systematic("GEN_MUR2_MUF1_PDF260000", "ttbar_weight_mc_MuR_UP"),
        Systematic("GEN_MUR1_MUF05_PDF260000", "ttbar_MuF_DOWN"),
        Systematic("GEN_MUR1_MUF2_PDF260000", "ttbar_MuF_UP"),
        Systematic("GEN_Var3cDown", "ttbar_isr_DOWN"),
        Systematic("GEN_Var3cUp", "ttbar_isr_UP"),
    ] +
    [Systematic(f"GEN_MUR1_MUF1_PDF933{i*2:02d}", f"ttbar_PDF93300var{i}_UP") for i in range(1, 22)] +
    [Systematic(f"GEN_MUR1_MUF1_PDF933{i*2-1:02d}", f"ttbar_PDF93300var{i}_DOWN") for i in range(1, 22)], # LHCPDF systematics
    

    'Singletop_tW': [
        Systematic("GEN_isrmuRfac10_fsrmuRfac05", "Singletop_tW_weight_mc_fsr_DOWN"),
        Systematic("GEN_isrmuRfac10_fsrmuRfac20", "Singletop_tW_weight_mc_fsr_UP"),
        Systematic("GEN_Var3cDown", "Singletop_tW_isr_DOWN"),
        Systematic("GEN_Var3cUp", "Singletop_tW_isr_UP"),
    ] + 
    [Systematic(f"GEN_MUR1_MUF1_PDF933{i*2:02d}", f"Singletop_tW_PDF93300var{i}_UP") for i in range(1, 22)] +
    [Systematic(f"GEN_MUR1_MUF1_PDF933{i*2-1:02d}", f"Singletop_tW_PDF93300var{i}_DOWN") for i in range(1, 22)], # LHCPDF systematics
}



def get_systematics():
    return systematics

def get_mc_weight_systematics():
    return mc_weight_systematics


if __name__ == '__main__':
    # print('weight_systematics: ', get_weight_systematics())
    print('systematics: ', get_systematics())
    print('mc_weight_systematics:', get_mc_weight_systematics())
    # print('samples_pdf_systematics: ', samples_pdf_systematics)
    # print('samples_qcd_systematics: ', samples_qcd_systematics)
    # print('get_pdf_systematics("ttbar"): ', get_pdf_systematics("ttbar"))
    # print('get_qcd_systematics("ttbar"): ', get_qcd_systematics("ttbar"))
    # print('get_qcd_systematics("ZJets"): ', get_qcd_systematics("ZJets"))
    # print('get_qcd_systematics("singletop"): ', get_qcd_systematics("singletop"))

