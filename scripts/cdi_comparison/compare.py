import os
import math
import re
from collections import defaultdict, namedtuple
import numpy as np

def extract_sf_and_sys(flavour, wp, year, file_path):
    bin_reg, SF_reg, syst_reg = regex_expressions(wp)

    pt_bins = []
    scale_factors = []
    total_uncertainties = []
    stat_uncertainties = []
    uncert_breakdown = []
    detailed_systs = []

    SF_bins = get_bins_content(file_path, bin_reg)
    for SF_bins_i in SF_bins:
        pt_min = int(SF_bins_i[0])
        pt_max = int(SF_bins_i[1])
        bin_content = SF_bins_i[5]

        # Append pt range to pt_bins
        if pt_min not in pt_bins:
            pt_bins.append(pt_min)
        if pt_max not in pt_bins:
            pt_bins.append(pt_max)
        # Extract central_value for this bin
        central_value_match = SF_reg.search(bin_content)
        if central_value_match:
            central_value = float(central_value_match.group(1))
            stat_percent = central_value_match.group(2)
            scale_factors.append(central_value)
            # Extract all sys and usys values for this bin
            sys_dict = syst_reg.findall(bin_content)
            sys_dict.append(("stat", stat_percent))

            # Group items by systematics group
            grouped_systs = group_by_prefix(flavour, sys_dict)

            # Output the grouped list
            syst_breakdown = {}
            for prefix, items in grouped_systs.items():
                syst_breakdown[prefix] = [
                    central_value * float(p[1]) / 100 for p in items
                ]
                syst_breakdown[prefix] = sum_in_quadrature(syst_breakdown[prefix])

            # Calculate the effective scale factor error for this bin
            total_percentages = [p for p in syst_breakdown.values()]
            tot_SF_value = sum_in_quadrature(total_percentages)
            total_uncertainties.append(tot_SF_value)
            
            for prefix, items in syst_breakdown.items():
                absolute_sys = syst_breakdown[prefix]
                syst_breakdown[prefix] = 100 * absolute_sys / central_value

            syst_breakdown["Total Uncertainty"] = 100 * tot_SF_value / central_value
            
            stat_SF_value = central_value * syst_breakdown["Statistical uncertainty"] / 100
            stat_uncertainties.append(stat_SF_value)

            uncert_breakdown.append(syst_breakdown)
            sys_dict = {k: v for k, v in sys_dict}
            detailed_systs.append(sys_dict)

    # Sort pt_bins and remove duplicates
    pt_bins = sorted(set(pt_bins))

    # the uncertainties are relative to the central value
    return pt_bins, scale_factors, (total_uncertainties,stat_uncertainties), uncert_breakdown, detailed_systs


def regex_expressions(wp):
    # 1. look only for WP bin we want to plot
    PCBT_edges = re.compile(r"(\d+)-(\d+)")
    PCBT_bin = PCBT_edges.findall(wp)

    if PCBT_bin[0][0] == "100":
        wp_reg = rf"tagweight<FixedCutBEff_{PCBT_bin[0][1]}"
    elif PCBT_bin[0][1] == "0":
        wp_reg = rf"FixedCutBEff_{PCBT_bin[0][0]}<tagweight"
    else:
        wp_reg = (
            rf"FixedCutBEff_{PCBT_bin[0][0]}<tagweight<FixedCutBEff_{PCBT_bin[0][1]}"
        )

    # 2. extract pT and eta bin
    pt_reg = r"(\d+)<pt<(\d+)"
    eta_reg = r"([\d.]+)<abseta<([\d.]+)"

    # 3. dump bin content given pT, eta, WP bin
    bin_reg = re.compile(rf"bin\({pt_reg},\s*{eta_reg},\s*({wp_reg})\)\s*{{([^}}]*)}}")

    # 4. value of SF and stat uncertainty
    SF_reg = re.compile(r"central_value\(\s*([\d.]+)\s*,\s*([\d.]+)%?\s*\)")

    # 5. each systematic name and relative % value
    syst_reg = re.compile(r"u?sys\(([a-zA-Z0-9_-]+),-?(\d+.\d+)%\)")

    return bin_reg, SF_reg, syst_reg


def sum_in_quadrature(values):
    """calculate sum in quadrature"""
    return math.sqrt(sum(val**2 for val in values))


def get_bins_content(file_name, bin_reg):
    with open(file_name, "r") as file:
        content = file.read()
        return bin_reg.findall(content)


def group_by_prefix(flavour, data):
    """Function to group by prefixes"""
    prefix_groups = defaultdict(list)

    for item in data:
        name, value = item
        name = name.replace("FT_EFF_", "")

        # for c and b the stat uncertainty comes from cov
        # so it is a separate term
        if "stat" in name:
            prefix = "Statistical uncertainty"
        elif "JET" in name:
            prefix = "Jet systematics"
        elif "ttbar" in name:
            prefix = "ttbar modelling"
        elif "Singletop" in name:
            prefix = "Single top modelling"
        elif "MUON" in name or "EGAMMA" in name or "EG" in name:
            prefix = "Lepton systematics"
        #elif "correctFakes" in name:
        #    prefix = "Fake leptons modelling"
        else:
            prefix = "Other sources combined"

        prefix_groups[prefix].append(item)
    
    # sort to have Jet, Lepton, ttbar, Single top, other, stat
    sorted_keys = ["Jet systematics", "Lepton systematics", "ttbar modelling", "Single top modelling", "Other sources combined", "Statistical uncertainty"]
    prefix_groups = dict(sorted(prefix_groups.items(), key=lambda item: sorted_keys.index(item[0])))

    return dict(prefix_groups)

import ROOT as R
R.gROOT.SetBatch(True)
def compare_arrays(bin_edges, hist1, hist2, label1, label2, xlabel, var_name, save_path, 
                   y_label="Error [%]", ratio_range=(0.5, 1.5), y_round_error=0, difference=False
                   ):
    v_bin_edges = R.std.vector("double")()
    for edge in bin_edges:
        v_bin_edges.push_back(edge)
    h1 = R.TH1D("h1", "", len(bin_edges) - 1, v_bin_edges.data())
    h2 = R.TH1D("h2", "", len(bin_edges) - 1, v_bin_edges.data())
    h1_ratio = R.TH1D("h1_ratio", "h1_ratio", len(bin_edges) - 1, v_bin_edges.data())
    h2_ratio = R.TH1D("h2_ratio", "h2_ratio", len(bin_edges) - 1, v_bin_edges.data())
    for i, (val1, val2) in enumerate(zip(hist1, hist2), 1):
        # y values are rounded to 2 decimal places
        h1.SetBinContent(i, val1)
        h1.SetBinError(i, y_round_error)
        h2.SetBinContent(i, val2)
        h2.SetBinError(i, y_round_error)
        if difference:
            h1_ratio.SetBinContent(i, 0)
            h1_ratio.SetBinError(i, 0)
            h2_ratio.SetBinContent(i, val2 - val1)
            h2_ratio.SetBinError(i, y_round_error * 2**0.5)
        else:
            h1_ratio.SetBinContent(i, 1)
            ratio = val2 / val1 if val1 != 0 else 1
            error_ratio = ratio * math.sqrt((y_round_error / val1 if val1 != 0 else 0)**2 + (y_round_error / val2 if val2 != 0 else 0)**2)
            h2_ratio.SetBinContent(i, val2 / val1 if val1 != 0 else 1)
            h2_ratio.SetBinError(i, error_ratio)
    
    c = R.TCanvas("c", "c", 600, 600)

    P_1 = R.TPad("1", "1", 0, 0.25, 1, 1)
    P_2 = R.TPad("2", "2", 0, 0, 1, 0.25)
    P_1.SetMargin(0.1, 0.1, 0.01, 0.05)
    P_2.SetMargin(0.1, 0.1, 0.3, 0.1)
    P_1.Draw()
    P_2.Draw()

    # top pad: histograms
    P_1.cd()

    # draw the histograms
    h1.SetLineColor(R.kBlack)
    h1.SetLineWidth(3)
    h2.SetLineColor(R.kBlue)
    h2.SetLineWidth(3)
    h2.SetMinimum(0)
    h2.SetMaximum(1.75 * (max(h1.GetMaximum(),h2.GetMaximum())))
    h2.SetTitleSize(0.04)
    h2.Draw('h')
    h1.Draw('hsame')

    # draw the axes labels
    ay = h2.GetYaxis()
    ax = h2.GetXaxis()
    ax.SetLabelOffset(999)
    ax.SetLabelSize(0)
    ay.SetTitle(y_label)
    ax.SetTitle()
    ax.Draw()
    ay.Draw()

    # draw labels on the plot
    tsize = 0.04
    y_text_ATLAS = 0.85
    x_text_ATLAS = 0.5
    dy_text = 0.04
    do_text(x_text_ATLAS,y_text_ATLAS,"ATLAS",0.0631579,1,72)
    do_text(x_text_ATLAS + 0.16,y_text_ATLAS,"Internal",0.0631579)
    do_text(x_text_ATLAS - 0.05,y_text_ATLAS - 1.7 * dy_text,var_name,0.05,1)
    do_text(x_text_ATLAS,y_text_ATLAS - 3.5 * dy_text,label1,tsize, R.kBlack)
    do_text(x_text_ATLAS,y_text_ATLAS - 4.5 * dy_text,label2,tsize, R.kBlue)

    R.gStyle.SetOptStat(0)

    # bottom pad: ratio plot
    P_2.cd()

    # draw the histograms
    h1_ratio.SetLineColor(R.kBlack)
    h1_ratio.SetLineWidth(3)
    h2_ratio.SetLineColor(R.kBlue)
    h2_ratio.SetLineWidth(3)
    h1_ratio.SetTitle('')
    h1_ratio.Draw('h')
    h2_ratio.Draw('hsame')

    # axes cosmesis
    ay_ratio = h1_ratio.GetYaxis()
    ax_ratio = h1_ratio.GetXaxis()
    if difference:
        ay_ratio.SetTitle('Difference')
    else:
        ay_ratio.SetTitle('Ratio')
    ax_ratio.SetTitle(xlabel)
    ax_ratio.SetTitleSize(0.1)
    ax_ratio.SetLabelSize(0.1)
    ay_ratio.SetTitleOffset(0.4)
    ay_ratio.SetTitleSize(0.1)
    ay_ratio.SetLabelSize(0.1)
    ay_ratio.SetRangeUser(ratio_range[0],ratio_range[1])
    ay_ratio.SetNdivisions(5)

    ax_ratio.Draw()
    ay_ratio.Draw()

    P_2.SetGrid()

    # save the canvas
    c.SaveAs(save_path)


def do_text(x, y, tt="", tsize=0.04, color=1, font=42):
    l = R.TLatex()
    l.SetTextSize(tsize)
    l.SetNDC()
    l.SetTextColor(color)
    l.SetTextFont(font)
    l.DrawLatex(x,y,tt)





if __name__ == '__main__':
    wp = ["100-90", "90-85", "85-77", "77-70", "70-65", "65-0"]
    flavour = "bottom"
    year = "2022"

    file_name_a = "AnalysisTop_r25.2.6"
    file_name_b = "TopCPToolkit_r25.2.40"
    cdi_files = {
        file_name_a: 
            "/afs/cern.ch/user/c/cmo/private/workspace/qualification_task/bcalibration_tcp/likelihood-fit/results/ATresults/cdi-input-files-combined_fit/continuous/btag_ttbarPDF_v1.0_24-2-02_GN2v01_FixedCutBEff_Continuous.txt",
        file_name_b: 
            "/afs/cern.ch/user/c/cmo/private/workspace/qualification_task/bcalibration_tcp/likelihood-fit/results/cdi-input-files/data22/cdi-input-files-combined_fit/continuous/btag_ttbarPDF_v1.0_24-2-02_GN2v01_FixedCutBEff_Continuous.txt",
    }

    results = {key: {} for key in cdi_files.keys()}

    for file_name, file_path in cdi_files.items():
        print(f"File: {file_name}")
        for wp_i in wp:
            pt_bins, scale_factors, uncertainties, uncert_breakdown, detailed_systs = extract_sf_and_sys(flavour, wp_i, year, file_path)
            results[file_name][wp_i] = {
                "pt_bins": pt_bins,
                "scale_factors": scale_factors,
                "uncertainties": uncertainties,
                "uncert_breakdown": uncert_breakdown,
                "detailed_systs": detailed_systs
            }
            # print(f"WP: {wp_i}")
            # print(f"pt_bins: {pt_bins}")
            # print(f"scale_factors: {scale_factors}")
            # print(f"uncertainties: {uncertainties}")
            # print(f"uncert_breakdown: {uncert_breakdown}")
            # print(f"detailed_systs: {detailed_systs}")
            # print("\n")
    
    out_base_dir = "plots/"
    if not os.path.exists(out_base_dir):
        os.makedirs(out_base_dir)

    for wp_i in wp[1:]:
        text = "GN2v01,  " + wp_i.replace("-", "% < #epsilon_{b} < ") + "%"
        bin_edges = results[file_name_b][wp_i]["pt_bins"]
        sf2 = results[file_name_b][wp_i]["scale_factors"]
        sf1 = results[file_name_a][wp_i]["scale_factors"]

        # Compare the scale factors
        out_dir = f"{out_base_dir}/scale_factors/"
        if not os.path.exists(out_dir): os.makedirs(out_dir)
        compare_arrays(bin_edges, sf1, sf2, file_name_a,file_name_b, "p_{T} [GeV]", text, f"{out_dir}/sf_comparison_{wp_i}.png", y_label="Scale Factor", ratio_range=(0.9, 1.1))

        # Compare syst breakdown
        items = results[file_name_b][wp_i]['uncert_breakdown'][0].keys()
        for item in items:
            out_dir = f"{out_base_dir}/syst_breakdown/{item}/"
            if not os.path.exists(out_dir): os.makedirs(out_dir)
            syst1 = [x[item] for x in results[file_name_a][wp_i]["uncert_breakdown"]]
            syst2 = [x[item] for x in results[file_name_b][wp_i]["uncert_breakdown"]]
            compare_arrays(bin_edges, syst1, syst2, file_name_a,file_name_b, "p_{T} [GeV]", text, f"{out_dir}/syst_comparison_{wp_i}.png", 
                           difference=True, ratio_range=[-5,5]
                           )
        
        # Compare detailed systs
        keys_to_compare = list(set(results[file_name_a][wp_i]["detailed_systs"][0].keys()) | set(results[file_name_b][wp_i]["detailed_systs"][0].keys()))
        for key in keys_to_compare:
            # if 'stat' in key or (key not in results[file_name_a][wp_i]["detailed_systs"][0].keys()):
            #     continue
            if 'stat' in key: continue
            if key not in results[file_name_a][wp_i]["detailed_systs"][0].keys():
                syst1 = [0 for x in results[file_name_a][wp_i]["detailed_systs"]]
            else:
                syst1 = [abs(float(x[key])) for x in results[file_name_a][wp_i]["detailed_systs"]]

            if key not in results[file_name_b][wp_i]["detailed_systs"][0].keys(): 
                syst2 = [0 for x in results[file_name_b][wp_i]["detailed_systs"]]
            else:
                syst2 = [abs(float(x[key])) for x in results[file_name_b][wp_i]["detailed_systs"]]
            if np.linalg.norm(syst1) > 0.4 or np.linalg.norm(syst2) > 0.4:
                out_dir = f"{out_base_dir}/detailed_systs/{key}/"
                if not os.path.exists(out_dir): os.makedirs(out_dir)
                compare_arrays(bin_edges, syst1, syst2, file_name_a,file_name_b, "p_{T} [GeV]", text, f"{out_dir}/syst_comparison_{wp_i}.png", y_round_error=0.01,
                           difference=True, ratio_range=[-5,5]
                               )


