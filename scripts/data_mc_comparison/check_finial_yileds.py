import glob

histo_path = '/afs/cern.ch/user/c/cmo/private/workspace/qualification_task/FTAGResults/topcp_sample/archived-results/bcalibration_tcp_v1.0-furtherfix-nominal-only-20240922/histograms/'
histo_path = '/afs/cern.ch/user/c/cmo/private/workspace/qualification_task/FTAGResults/topcp_sample/r25.2.35_DiLep-v2.2_mc23a/histograms/'

data_list = ['data22']
sample_list = [
    'ttbar_PhPy8_nominal',
    'Diboson_Sherpa22',
    'Singletop_PowPy8',
    'Wjets_Sherpa22',
    'Zjets_Sherpa22'
]

import ROOT as R
def get_yields(sample_tag):
    f_path = glob.glob(histo_path + sample_tag + '/*nominal_combination*.root')[0]
    f = R.TFile(f_path)
    h = f.Get('emu_OS_J2/h_emu_OS_J2_CutFlow')
    yields = h.GetBinContent(6)
    return yields

data_yields = 0
for data_tag in data_list:
    data_yields += get_yields(data_tag)
    print(f'{data_tag}: {data_yields}')

sample_yields = 0
for sample_tag in sample_list:
    yields = get_yields(sample_tag)
    print(f'{sample_tag}: {yields}')
    sample_yields += yields

print(f'Total: {sample_yields}')
print(f'Data/MC: {data_yields/sample_yields}')