import ROOT
import os
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import gROOT, TCanvas, TPad, TLine, TH1, THStack, TLegend, TFile, TText, SetOwnership, gStyle, TH1D
import sys
sys.path.insert(0, '../../FinalSelection/')
from options_file import options

dataName=options.data_name
outdir=options.output_dir
ttb=options.nominal_samples['ttbar']
systematic_name="nominal" #"weight_pileup_DOWN"

ttb_sample = options.nominal_samples['ttbar']
ZJets_sample, Wjets_sample, Diboson_sample = options.nominal_samples['ZJets'], options.nominal_samples['Wjets'], options.nominal_samples['Diboson']
singleTop_sample = options.nominal_samples['Singletop']
other_samples = [ZJets_sample, Wjets_sample, Diboson_sample]
DoNorm=True

option="INT"
lumi=options.data_lumi
# Can reweight MC with the scale below:
scale = 1.0

legendLabels={
    "FTAG2_ttV_aMCnlo" : "ttV",
    "Zjets_Sherpa22" : "Z+jets",
    "Wjets_Sherpa22" : "W+jets",
    "Singletop_PowPy8" : "Single Top",
    "Diboson_Sherpa22" : "Diboson",
    "ttbar_PhPy8_nominal" : "t#bar{t}",
    "data1516" : "Data 2015 + 2016",
    "data17" : "Data 2017",
    "data18" : "Data 2018",
    "dataRun2" : "Data Run2",
    "data22" : "Data 2022",
    "data23" : "Data 2023",
    "FTAG2_ttbar_aMcPy8" : "t#bar{t} aMcPy8",
    "FTAG2_ttbar_Sherpa221" : "t#bar{t} Sherpa",
    "FTAG2_ttbar_PowHW7" : "t#bar{t} Powheg+Herwig7",
    "FTAG2_ttbar_PhPy8_AF2" : "t#bar{t} AF2"
}


class h_plot:
    _canvaslist=[]
    def __init__(self,name,data_h,outputFolder,xLabel="GeV",yLabel="Events",setYlim=0,ratioRange=[0.71,1.29]):
        self.name = name
        self.outputFolder = outputFolder
        self.xLabel = xLabel
        self.yLabel = yLabel
        self.setYlim = setYlim
        self._MC_hists=[]
        self._MC_legendlabs=[]
        self._data_h= data_h
        self._signalName="Signal"
        self._ratioRange=ratioRange
        #print("ratstart: " + str(ratioRange[0]))

        if(self.xLabel=="GeV"):
            if self.name[-3:]=="mll" :
                self.xLabel="MLL [GeV]"
            if self.name[-3:]=="met" :
                self.xLabel="MET [GeV]"
    def setplotOptions(self, stack):
        stack.GetXaxis().SetTitle(self.xLabel)
        stack.GetYaxis().SetTitle(self.yLabel)
        if self.setYlim :
            if self.setYlim[0] < 1:
                stack.SetMinimum(self.setYlim[0])
            else:
                stack.SetMinimum(0.01)
            stack.SetMaximum(self.setYlim[1])
    def addMC(self,legendLabel,hist):
        self._MC_hists.append(hist)
        self._MC_legendlabs.append(legendLabel)
    def setSignal(self,hist,name):
        self._signal=hist
        self._signalName=name
    def make_plot(self,draw_ratio=True,try_cut_ratio=0,outputfile=0,title=""):
        import atlas_labels as al
        # print "For plot: ", self.name
        AtlasStyle = al.atlasStyle()
        AtlasStyle.SetErrorX(0.5)
        AtlasStyle.SetEndErrorSize( 0 )
        AtlasStyle.SetPadTopMargin(    0.05)
        AtlasStyle.SetPadRightMargin(  0.05)
        AtlasStyle.SetPadBottomMargin( 0.20)
        AtlasStyle.SetPadLeftMargin(   0.15)
        gROOT.SetStyle("ATLAS")
        gROOT.ForceStyle()
        text_size = 30
        TH1.SetDefaultSumw2()

        ###-- Now Draw Everything --###
        height=600
        if draw_ratio:
            height=1000
        width  = 800
        #print('creating canvas:  c_'+self.name)
        c1 = TCanvas('c_'+self.name, title, width, height)
        h_plot._canvaslist.append(c1)
        if draw_ratio:
            mainPad  = TPad("mainPad", "top",    0.0, 0.4, 1.0, 1.00)
            ratioPad1 = TPad("ratioPad1","bottom", 0.0, 0.25, 1.0, 0.4)
            ratioPad2 = TPad("ratioPad2","bottom", 0.0, 0.00, 1.0, 0.25)
            mainPad.SetBottomMargin(0.01)
            ratioPad1.SetTopMargin(0.02)
            ratioPad1.SetBottomMargin(0.01)
            ratioPad2.SetTopMargin(0.02)
            ratioPad2.SetBottomMargin(0.4)
        else:
            mainPad  = TPad("mainPad",  "centre", 0.0, 0.0, 1.0, 1.0)
            ratioPad1 = TPad("ratioPad1", "null",   0.0, 0.0, 0.0, 0.0)
            ratioPad2 = TPad("ratioPad2", "null",   0.0, 0.0, 0.0, 0.0)
            mainPad.SetBottomMargin(0.15)
        ###--- Main Pad --###


        mainPad.Draw()
        mainPad.cd()

        mainPad.SetLogy(True)
        legend_x      = 0.55#0.65
        legend_y      = 0.90
        legend_width  = 0.28
        legend_height = 0.42#0.34
        legend = TLegend(legend_x, legend_y-legend_height, legend_x+legend_width, legend_y)
        legend.SetFillStyle(0)
        legend.SetLineColor(0)
        legend.SetTextFont(42)
        legend.SetNColumns(2)
        stack=THStack(self.name,'Title: '+self.name)

        integral_MC=0
        data=self._data_h
        integral_data=data.Integral(0,-1)
        for i in range(0,len(self._MC_hists)):
            #print(self._MC_hists[i].Integral(), integral_MC, integral_data)
            integral_MC+=self._MC_hists[i].Integral(0,-1) # get the integral of MC for normalisation

        for i in range(0,len(self._MC_hists)):
            legend.AddEntry(self._MC_hists[i],self._MC_legendlabs[i],"F")
            if DoNorm and integral_MC:
                self._MC_hists[i].Scale(integral_data/integral_MC) # normalise the MC to data, in order to better focus on the shape
                print("Normalisation factor = "+str(integral_data/integral_MC), integral_data, integral_MC)
            elif DoNorm:
                print("Integral of MC = 0!")
            stack.Add(self._MC_hists[i])
        data=self._data_h
        data.SetMarkerStyle(20)
        data.SetMarkerSize(1.2)
        data.Sumw2()
        data.SetName("h_data_"+self.name)
        data.SetTitle(title)
#        data.SetMaximum(data.GetMaximum()*100)
        data.Draw()
        self.setplotOptions(data)


        latex=ROOT.TLatex()
        latex.SetTextFont(42)
        latex.SetTextSize(0.04)
        mainPad.cd()
        legend.SetTextSize(text_size*0.8/(mainPad.GetWh()*mainPad.GetAbsHNDC()))
        data.GetXaxis().SetLabelSize(text_size/(mainPad.GetWh()*mainPad.GetAbsHNDC()))
        data.GetYaxis().SetLabelSize(text_size/(mainPad.GetWh()*mainPad.GetAbsHNDC()))
        data.GetXaxis().SetTitleSize(text_size/(mainPad.GetWh()*mainPad.GetAbsHNDC()))
        data.GetYaxis().SetTitleSize(text_size/(mainPad.GetWh()*mainPad.GetAbsHNDC()))
        data.GetYaxis().SetTitleOffset(1)
        data.Draw("lpe")
        stack.Draw("HIST SAME")
        #print "Data", data.Integral()
        legend.AddEntry(data,legendLabels[dataName])
        legend.SetBorderSize(0)  # no border
        legend.SetFillColor(0)   # Legend background should be white
        legend.SetTextSize(0.035) # Increase entry font size!
        #al.make_ATLAS_string( 0.20, 0.90, self.name, text_size/(mainPad.GetWh()*mainPad.GetAbsHNDC()))
        legend.SetNColumns(2)
        legend.Draw()
        latex.SetTextSize(0.04)
        if option =="INT":
            latex.DrawLatexNDC(0.22,0.85,"#bf{#it{ATLAS}} Internal")
        elif option =="SIMINT":
            latex.DrawLatexNDC(0.18,0.85,"#bf{#it{ATLAS}} Simulation Internal")
        elif option =="SIM":
            latex.DrawLatexNDC(0.22,0.85,"#bf{#it{ATLAS}} Simulation")
        elif option =="WIP":
            latex.DrawLatexNDC(0.18,0.85,"#bf{#it{ATLAS}} Work in progress")
        elif option =="OFFICIAL":
            latex.DrawLatexNDC(0.3,0.85,"#bf{#it{ATLAS}}")
        elif option == "PREL":
            latex.DrawLatexNDC(0.22,0.85,"#bf{#it{ATLAS}} Preliminary")
        latex.SetTextSize(0.035)
        latex.DrawLatexNDC(0.18,0.75,"#sqrt{s} = 13.6 TeV, #int Ldt = %.2f fb^{-1}"%(lumi))
        data.Draw("lpe same")
        c1.cd()
        if draw_ratio:
            ratioPad1.Draw()
            ratioPad1.cd()
            #gROOT.cd()
            data.GetXaxis().SetLabelOffset(0.08)
            ratio_title = "Data / MC "
            data_clone = data.Clone("data_temp_clone" + self.name)
            SetOwnership(data_clone,0)
            sumHist=0
            for hist in self._MC_hists:
                # Reweight MC here (if need)
                hist.Scale(scale)
                if not sumHist:
                    sumHist=hist.Clone("sum_temp_clone" + self.name)
                else:
                    sumHist.Add(hist)
            for ibin in range(0,sumHist.GetNbinsX()):
                sumHist.SetBinError(ibin,0)
            data_clone.Divide(sumHist)
            data_clone.SetLineColor(1)
            data_clone.SetMarkerStyle(20)
            data_clone.SetMarkerSize(1.2)
            data_clone.SetTitle("")

            #data_clone.GetYaxis().SetRangeUser(0.71,1.29)
            #still have to change the errors.
            data_clone.Draw("LPE")

            data_clone.GetYaxis().SetTitle(ratio_title)
            data_clone.GetYaxis().SetLabelSize(text_size/(ratioPad1.GetWh()*ratioPad1.GetAbsHNDC()))
            data_clone.GetYaxis().SetTitleSize(text_size/(ratioPad1.GetWh()*ratioPad1.GetAbsHNDC()))
            data_clone.GetYaxis().CenterTitle(1)
            data_clone.GetYaxis().SetTitleOffset(0.25)
            data_clone.GetYaxis().SetTitleColor(1)
            data_clone.GetYaxis().CenterLabels(False)
            data_clone.GetYaxis().SetNdivisions(3)

            data_clone.GetXaxis().SetLabelSize(text_size/(ratioPad1.GetWh()*ratioPad1.GetAbsHNDC()))
            data_clone.GetXaxis().SetLabelSize(text_size/(ratioPad1.GetWh()*ratioPad1.GetAbsHNDC()))
            data_clone.GetYaxis().SetRangeUser(self._ratioRange[0],self._ratioRange[1])
            line1 = TLine(data_clone.GetXaxis().GetXmin(),1,data_clone.GetXaxis().GetXmax(),1)
            line1.SetLineWidth(2)
            line1.SetLineStyle(2)
            line1.Draw("SAME")
            data_clone.SetLineWidth(3)
            data_clone.SetLineColor(1)
            c1.cd()


            ratioPad2.Draw()
            ratioPad2.cd()
            data.GetXaxis().SetLabelOffset(0.08)
            ratio_title = self._signalName+" / all   "
            signal_clone = self._signal.Clone("signal_temp_clone_" + self.name)
            SetOwnership(signal_clone,0)
            signal_clone.Divide(sumHist)
            signal_clone.SetLineColor(1)
            signal_clone.SetMarkerStyle(20)
            signal_clone.SetMarkerSize(1.2)
            signal_clone.SetTitle("")
            signal_clone.Draw("LPE")
            signal_clone.GetYaxis().SetTitle(ratio_title)
            signal_clone.GetYaxis().SetLabelSize(text_size/(ratioPad2.GetWh()*ratioPad2.GetAbsHNDC()))
            signal_clone.GetYaxis().SetTitleSize(text_size/(ratioPad2.GetWh()*ratioPad2.GetAbsHNDC()))
            signal_clone.GetYaxis().CenterTitle(1)
            signal_clone.GetYaxis().SetTitleOffset(0.4)
            signal_clone.GetYaxis().SetTitleColor(1)
            signal_clone.GetYaxis().CenterLabels(False)
            signal_clone.GetYaxis().SetNdivisions(3)

            signal_clone.GetXaxis().SetLabelSize(text_size/(ratioPad2.GetWh()*ratioPad2.GetAbsHNDC()))
            signal_clone.GetXaxis().SetLabelSize(text_size/(ratioPad2.GetWh()*ratioPad2.GetAbsHNDC()))
            signal_clone.GetXaxis().SetTitleSize(0.16)
            signal_clone.GetXaxis().SetTitle(data_clone.GetXaxis().GetTitle())
            signal_clone.GetXaxis().SetTitleOffset(1.1)
            signal_clone.GetYaxis().SetRangeUser(0,1)
            #lets try a cut
            if try_cut_ratio:
                cutratio=try_cut_ratio
                line2 = TLine(signal_clone.GetXaxis().GetXmin(),cutratio,signal_clone.GetXaxis().GetXmax(),cutratio)
                line2.SetLineWidth(2)
                line2.SetLineStyle(2)
                line2.Draw("SAME")
                signal_clone.SetLineWidth(3)
                signal_clone.SetLineColor(1)
                #calculate cut efficiency
                events_cutted=0
                purity_bCut=self._signal.Integral()/sumHist.Integral()
                signal_events_aCut=0
                all_events_aCut=0
                cutval=0
                signalFound=0
                for ibin in range(0,sumHist.GetNbinsX()):
                    sumHist.SetBinError(ibin,0)
                    if (signal_clone.GetBinContent(ibin)<cutratio):
                        events_cutted=events_cutted+sumHist.GetBinContent(ibin)
                        if signalFound:
                            if not cutval:
                                cutval=signal_clone.GetXaxis().GetBinLowEdge(ibin)
                                print("cutval: "+str(cutval)+" "+str(ibin))

                    else:
                        signalFound=1
                        all_events_aCut=all_events_aCut+sumHist.GetBinContent(ibin)
                        signal_events_aCut=signal_events_aCut+self._signal.GetBinContent(ibin)
                purity_aCut=signal_events_aCut/all_events_aCut
                print("events thrown away: "+str(events_cutted))
                print("purity before cut: "+str(purity_bCut))
                print("purity after cut: "+str(purity_aCut))
                cut_eff_label=TText()
                cut_eff_label.SetTextSize(0.1)
                cut_eff_label.DrawText(self._signal.GetXaxis().GetXmin(), 0.8, "Purity before Cut: "+str(round(purity_bCut*100,2))+"% after Cut: "+str(round(purity_aCut*100,2))+"% Cutvalue: "+str(cutval))
            c1.cd()


        mainPad.RedrawAxis()
        c1.cd()
        if outputfile==0:
            if not os.path.exists(self.outputFolder):
                os.makedirs(self.outputFolder)
            c1.SaveAs(self.outputFolder+self.name+".pdf")
            # c1.SaveAs(self.outputFolder+self.name+".png")
        else:
            outputfile.cd()
            c1.Write()





class channel_plot:
    def __init__(self,samples,name="ee_OS_J2",n_jets=2,n_el=2,n_mu=0):
        self.name = name
        self.n_jets = n_jets
        self.n_el = n_el
        self.n_mu = n_mu
        self.plotdir = options.plot_dir+name+"_"+systematic_name +"/"
        self.samples = samples
    def makePlots(self):
        if not os.path.exists(self.plotdir):
            os.makedirs(self.plotdir)
        self._plot_h(self.name+'_met',"MET [GeV]")
        self._plot_h(self.name+'_mu',"mu")
        self._plot_h_mu_shifted(self.name+'_mu_shifted',"mu shifted")
        if "SS_NPel" not in self.name:
            self._plot_h(self.name+'_nPV',"nPV")
            self._plot_h_pt(self.name+'_m_jl_max',1, "max m_jl [GeV]")
        self._plot_h_pt(self.name+'_m_jl_max',2, "max m_jl [GeV]")


        self._plot_xx_con(self.name+"_ll")
        for jet in range(1,self.n_jets+1):
            self._plot_h(self.name+'_jet'+str(jet)+'_GN2',"GN2 discriminant of "+str(jet)+". jet" ,"Events")
            
            self._plot_h(self.name+'_jet'+str(jet)+'_eta',"eta of "+str(jet)+". jet" ,"Events")
            self._plot_h(self.name+'_jet'+str(jet)+'_phi',"phi of "+str(jet)+". jet" ,"Events")
            self._plot_h(self.name+'_jet'+str(jet)+'_pt',"pt of "+str(jet)+". jet [GeV]",setYlim=0)
            #self._plot_h(self.name+'_jet'+str(jet)+'_HadronConeExclExtendedTruthLabelID',"HadronTruthLabelID for "+str(jet)+". jet" ,"Events")
            if self.n_jets==2:
                self._plot_h(self.name+'_jet'+str(jet)+'_m_jl',"invariant mass of lepton and "+str(jet)+". jet [GeV]","Events")
        for el in range(1,self.n_el+1):
            self._plot_h(self.name+'_el'+str(el)+'_pt'+str(el),"pt of "+str(el)+". el [GeV]",setYlim=0)
            self._plot_h(self.name+'_el'+str(el)+'_eta'+str(el),"eta of "+str(el)+". el" ,"Events")
            self._plot_h(self.name+'_el'+str(el)+'_cl_eta'+str(el),"calo eta of "+str(el)+". el" ,"Events")
            self._plot_h(self.name+'_el'+str(el)+'_phi'+str(el),"phi of "+str(el)+'. el' ,"Events")
        for mu in range(1,self.n_mu+1):
            self._plot_h(self.name+'_mu'+str(mu)+'_pt'+str(mu),"pt of "+str(mu)+". mu [GeV]",setYlim=0)
            self._plot_h(self.name+'_mu'+str(mu)+'_eta'+str(mu),"eta of "+str(mu)+". mu" ,"Events")
            self._plot_h(self.name+'_mu'+str(mu)+'_phi'+str(mu),"phi of "+str(mu)+'. mu' ,"Events")
    def _plot_xx_con(self,con_name,subFold=""):
            cut_ratio=0
            self._plot_h(con_name+"_m","invariant Mass [GeV]",subFold=subFold,try_cut_ratio=cut_ratio, setYlim=0)
            self._plot_h(con_name+"_mT","transverse Mass [GeV]", subFold=subFold,try_cut_ratio=cut_ratio, setYlim=0)
            self._plot_h(con_name+"_pT","pt [GeV]", subFold=subFold,try_cut_ratio=cut_ratio,setYlim=0)
            self._plot_h(con_name+"_d_R","delta R", subFold=subFold,try_cut_ratio=cut_ratio)
            self._plot_h(con_name+"_d_eta","delta eta" ,"Events", subFold=subFold,try_cut_ratio=cut_ratio)
            self._plot_h(con_name+"_d_phi","delta phi" ,"Events", subFold=subFold,try_cut_ratio=cut_ratio)

    def _plot_h_mu_shifted(self,plot_name,xLabel="GeV",yLabel="Events",draw_ratio=True,try_cut_ratio=0,subFold="",setYlim=0):
        flavours={'bb':0,'bc':0,'bl':0,'cb':0,'cc':0,'cl':0,'lb':0,'lc':0,'ll':0}
        flavourskeys=flavours.keys()
        #plots devided by sample
        if "SS_NPel" in self.name:
            infile= ROOT.TFile(outdir+dataName+"/"+dataName+"_data_modified_"+ systematic_name +"_combination.root", "read")
        else:
            infile= ROOT.TFile(outdir+dataName+"/"+dataName+"_nominal_combination.root", "read")
        data_h=infile.Get(self.name+"/h_"+plot_name+"_data").Clone()
        data_h.SetDirectory(0)
        infile.Close()
        data_h.SetName("h_data_"+plot_name+"_data")
        data_h.Rebin(3)
        data_h.SetMaximum(data_h.GetMaximum()*100)
        if "SS" in self.name:
            if "SS_NPel" in self.name:
                pl=h_plot(plot_name,data_h,self.plotdir+subFold,xLabel=xLabel,yLabel=yLabel,setYlim=setYlim,ratioRange=[0.49,5.01])
            else:
                pl=h_plot(plot_name,data_h,self.plotdir+subFold,xLabel=xLabel,yLabel=yLabel,setYlim=setYlim,ratioRange=[0.49,2.01])
        else:
            pl=h_plot(plot_name,data_h,self.plotdir+subFold,xLabel=xLabel,yLabel=yLabel,setYlim=setYlim)
        hists=[]
        for sample in self.samples:
            infile= ROOT.TFile(outdir+sample.name+"/"+sample.name+"_" + systematic_name + "_combination.root", "read")
            hist=infile.Get(self.name+"/h_"+plot_name+"_"+list(flavourskeys)[0]).Clone()
            hist.SetDirectory(0)
            hist.SetName("h_"+sample.name+"_"+plot_name)
            hist.Reset()
            hist.SetStats(0)
            for flav in flavourskeys:
                hist.Add(infile.Get(self.name+"/h_"+plot_name+"_"+flav))
            sample.setHistOptions(hist)
            hist.Rebin(3)
            if sample==ttb:
                pl.setSignal(hist,"ttbar")
            pl.addMC(legendLabels[sample.name],hist)
            hists.append(hist)
            infile.Close()
        pl.make_plot()


    def _plot_h(self,plot_name,xLabel="GeV",yLabel="Events",draw_ratio=True,try_cut_ratio=0,subFold="",setYlim=0):
        flavours={'bb':0,'bc':0,'bl':0,'cb':0,'cc':0,'cl':0,'lb':0,'lc':0,'ll':0}
        flavourskeys=flavours.keys()
        #plots devided by sample
        if "SS_NPel" in self.name:
            infile= ROOT.TFile(outdir+dataName+"/"+dataName+"_data_modified_"+ systematic_name +"_combination.root", "read")
        else:
            infile= ROOT.TFile(outdir+dataName+"/"+dataName+"_nominal_combination.root", "read")
        print(self.name+"/h_"+plot_name+"_data")
        data_h=infile.Get(self.name+"/h_"+plot_name+"_data").Clone()
        data_h.SetDirectory(0)
        data_h.SetMaximum(data_h.GetMaximum()*100)
        infile.Close()
        data_h.SetName("h_data_"+plot_name)
        if "SS" in self.name:
            if "SS_NPel" in self.name:
                pl=h_plot(plot_name,data_h,self.plotdir+subFold,xLabel=xLabel,yLabel=yLabel,setYlim=setYlim,ratioRange=[0.49,5.01])
            else:
                pl=h_plot(plot_name,data_h,self.plotdir+subFold,xLabel=xLabel,yLabel=yLabel,setYlim=setYlim,ratioRange=[0.49,2.01])
        else:
            pl=h_plot(plot_name,data_h,self.plotdir+subFold,xLabel=xLabel,yLabel=yLabel,setYlim=setYlim)
        hists=[]
        for sample in self.samples:
            infile= ROOT.TFile(outdir+sample.name+"/"+sample.name+"_" + systematic_name + "_combination.root", "read")
            hist=infile.Get(self.name+"/h_"+plot_name+"_"+list(flavourskeys)[0]).Clone()
            #print "starting with falvour", flavourskeys[0], "for sample:", sample.name
            hist.SetDirectory(0)
            hist.SetName("h_"+sample.name+"_"+plot_name)
            hist.Reset()
            hist.SetStats(0)
            for flav in flavourskeys:
                hist.Add(infile.Get(self.name+"/h_"+plot_name+"_"+flav))
                #print "adding flavour", flav
            sample.setHistOptions(hist)
            if sample==ttb:
                pl.setSignal(hist,"ttbar")
            pl.addMC(legendLabels[sample.name],hist)
            hists.append(hist)
            infile.Close()
        pl.make_plot()

        # jet plot devided by flavours:
        if "SS" in self.name:
            if "SS_NPel" in self.name:
                flavpl=h_plot(plot_name+'_flavpl',data_h,self.plotdir+subFold,xLabel=xLabel,yLabel=yLabel,setYlim=setYlim,ratioRange=[0.49,5.01])
            else:
                flavpl=h_plot(plot_name+'_flavpl',data_h,self.plotdir+subFold,xLabel=xLabel,yLabel=yLabel,setYlim=setYlim,ratioRange=[0.49,2.01])
        else:
            flavpl=h_plot(plot_name+'_flavpl',data_h,self.plotdir+subFold,xLabel=xLabel,yLabel=yLabel,setYlim=setYlim)
        singletopflavs=dict(flavours)
        ttbarflavs=dict(flavours)

        infile= ROOT.TFile(outdir+self.samples[0].name+"/"+self.samples[0].name+"_" + systematic_name + "_combination.root", "read")
        for flav in flavours.keys():
            flavours[flav]=infile.Get(self.name+"/h_"+plot_name+"_"+flav).Clone()
            flavours[flav].SetDirectory(0)
            flavours[flav].SetName("h_"+"_"+plot_name+'_flav'+flav)
            flavours[flav].Reset()
            flavours[flav].SetStats(0)
        infile.Close()

        for sample in other_samples:
            # print(sample.name)
            # print("looking for: "+outdir+sample.name + " h_"+plot_name+"_"+flavourskeys[0])
            infile= ROOT.TFile(outdir+sample.name+"/"+sample.name+"_" + systematic_name + "_combination.root", "read")
            for flav in flavours.keys():
                flavours[flav].Add(infile.Get(self.name+"/h_"+plot_name+"_"+flav))
            infile.Close()
        infile= ROOT.TFile(outdir+singleTop_sample.name+"/"+singleTop_sample.name+"_" + systematic_name + "_combination.root", "read")
        for flav in singletopflavs.keys():
            singletopflavs[flav]=infile.Get(self.name+"/h_"+plot_name+"_"+flav).Clone()
            singletopflavs[flav].SetDirectory(0)
            singletopflavs[flav].SetName("h_"+singleTop_sample.name+"_"+plot_name+'_flav'+flav)
        infile.Close()
        infile= ROOT.TFile(outdir+ttb_sample.name+"/"+ttb_sample.name+"_" + systematic_name + "_combination.root", "read")
        for flav in ttbarflavs.keys():
            ttbarflavs[flav]=infile.Get(self.name+"/h_"+plot_name+"_"+flav).Clone()
            ttbarflavs[flav].SetDirectory(0)
            ttbarflavs[flav].SetName("h_"+ttb_sample.name+"_"+plot_name+'_flav'+flav)
        infile.Close()

        flavours['ll'].SetFillColor(ROOT.kRed+1)
        flavours['ll'].Add(flavours['lc'])
        flavours['ll'].Add(flavours['lb'])
        flavours['ll'].Add(singletopflavs['ll'])
        flavours['ll'].Add(singletopflavs['lc'])
        flavours['ll'].Add(singletopflavs['lb'])
        flavours['ll'].Add(ttbarflavs['ll'])
        flavours['ll'].Add(ttbarflavs['lc'])
        flavours['ll'].Add(ttbarflavs['lb'])
        flavpl.addMC("l+(l,c,b)", flavours['ll'])

        flavours['cl'].SetFillColor(ROOT.kGreen-8)
        flavours['cl'].Add(flavours['cc'])
        flavours['cl'].Add(flavours['cb'])
        flavours['cl'].Add(singletopflavs['cl'])
        flavours['cl'].Add(singletopflavs['cc'])
        flavours['cl'].Add(singletopflavs['cb'])
        flavours['cl'].Add(ttbarflavs['cl'])
        flavours['cl'].Add(ttbarflavs['cc'])
        flavours['cl'].Add(ttbarflavs['cb'])
        flavpl.addMC("c+(l,c,b)", flavours['cl'])

        flavours['bl'].SetFillColor(ROOT.kBlue+1)
        flavours['bl'].Add(flavours['bc'])
        flavours['bl'].Add(ttbarflavs['bl'])
        flavours['bl'].Add(ttbarflavs['bc'])
        flavours['bl'].Add(singletopflavs['bl'])
        flavours['bl'].Add(singletopflavs['bc'])
        flavpl.addMC("b+(l,c) ", flavours['bl'])

        flavours['bb'].SetFillColor(7)
        flavours['bb'].Add(ttbarflavs['bb'])
        flavours['bb'].Add(singletopflavs['bb'])
        flavpl.addMC("bb", flavours['bb'])
        flavpl.setSignal(flavours['bb'],"bb")

        flavpl.make_plot(try_cut_ratio=try_cut_ratio)
    def _plot_h_pt(self,plot_name,jet,xLabel="GeV",yLabel="Events",draw_ratio=True,try_cut_ratio=0,subFold="",setYlim=0):
        output_filename=self.plotdir+subFold+plot_name+"_jet"+str(jet)+".root"
        outputfile= ROOT.TFile(output_filename, "RECREATE")
        infile= ROOT.TFile(outdir+dataName+"/"+dataName+"_nominal_combination.root", "read")
        nYbins=infile.Get(self.name+"/h_pt_"+plot_name+"_data").GetYaxis().GetNbins()
        infile.Close()
        for ybin in range(1,nYbins+1):
            for zbin in range(1,nYbins+1):
                if ybin >= zbin:
                    binName='_pt1_'+str(ybin)+'_pt2_'+str(zbin)
                    flavours={'bb':0,'bc':0,'bl':0,'cb':0,'cc':0,'cl':0,'lb':0,'lc':0,'ll':0}
                    flavourskeys=flavours.keys()
                    #plots devided by sample
                    infile= ROOT.TFile(outdir+dataName+"/"+dataName+"_nominal_combination.root", "read")
                    data_h=infile.Get(self.name+"/h_pt_"+plot_name+"_data").ProjectionX('_px',ybin,ybin,zbin,zbin)
                    data_h.SetDirectory(0)
                    data_h.SetMaximum(data_h.GetMaximum()*100)
                    yAxis=infile.Get(self.name+"/h_pt_"+plot_name+"_data").GetYaxis()
                    binLab='pT1: '+str(yAxis.GetBinLowEdge(ybin))+" - "+str(yAxis.GetBinUpEdge(ybin)) +' pT2: ' + str(yAxis.GetBinLowEdge(zbin)) + " - " + str(yAxis.GetBinUpEdge(zbin))
                    infile.Close()
                    data_h.SetName("h_data_"+plot_name)
                if "SS" in self.name:
                    if "SS_NPel" in self.name:
                        pl=h_plot(plot_name+binName,data_h,self.plotdir+subFold,xLabel=xLabel,yLabel=yLabel,setYlim=setYlim,ratioRange=[0.41,5.01])
                    else:
                        pl=h_plot(plot_name+binName,data_h,self.plotdir+subFold,xLabel=xLabel,yLabel=yLabel,setYlim=setYlim,ratioRange=[0.41,2.01])
                else:
                    pl=h_plot(plot_name+binName,data_h,self.plotdir+subFold,xLabel=xLabel,yLabel=yLabel,setYlim=setYlim,ratioRange=[0.41,1.39])
                    hists=[]
                for sample in self.samples:
                    infile= ROOT.TFile(outdir+sample.name+"/"+sample.name+"_" + systematic_name + "_combination.root", "read")
                    hist=infile.Get(self.name+"/h_pt_"+plot_name+"_"+list(flavourskeys)[0]).ProjectionX('_px',ybin,ybin,zbin,zbin).Clone()
                    hist.SetDirectory(0)
                    hist.SetName("h_"+sample.name+"_"+plot_name)
                    hist.Reset()
                    hist.SetStats(0)
                    for flav in flavourskeys:
                        hist.Add(infile.Get(self.name+"/h_pt_"+plot_name+"_"+flav).ProjectionX('_px',ybin,ybin,zbin,zbin))
                    sample.setHistOptions(hist)
                    if sample==ttb:
                        pl.setSignal(hist,"ttbar")
                    pl.addMC(legendLabels[sample.name],hist)
                    hists.append(hist)
                    infile.Close()
                pl.make_plot(outputfile=outputfile)

                    # jet plot devided by flavours:
                if "SS" in self.name:
                    if "SS_NPel" in self.name:
                        flavpl=h_plot(plot_name+binName+'_flavpl',data_h,self.plotdir+subFold,xLabel=xLabel,yLabel=yLabel,setYlim=setYlim,ratioRange=[0.41,5.01])
                    else:	                    
                        flavpl=h_plot(plot_name+binName+'_flavpl',data_h,self.plotdir+subFold,xLabel=xLabel,yLabel=yLabel,setYlim=setYlim,ratioRange=[0.41,2.01])
                else:
                    flavpl=h_plot(plot_name+binName+'_flavpl',data_h,self.plotdir+subFold,xLabel=xLabel,yLabel=yLabel,setYlim=setYlim,ratioRange=[0.41,1.39])
                singletopflavs=dict(flavours)
                ttbarflavs=dict(flavours)
                infile= ROOT.TFile(outdir+samples[0].name+"/"+samples[0].name+"_" + systematic_name + "_combination.root", "read")
                for flav in flavours.keys():
                    flavours[flav]=infile.Get(self.name+"/h_pt_"+plot_name+"_"+flav).ProjectionX('_px',ybin,ybin,zbin,zbin).Clone()
                    flavours[flav].SetDirectory(0)
                    flavours[flav].SetName("h_"+"_"+plot_name+'_flav'+flav)
                    flavours[flav].Reset()
                    flavours[flav].SetStats(0)
                infile.Close()
                for sample in other_samples:
                    #print sample.name
                    #print "looking for: ",outdir+sample.name,"h_"+plot_name+"_"+flavourskeys[0]
                    infile= ROOT.TFile(outdir+sample.name+"/"+sample.name+"_" + systematic_name + "_combination.root", "read")
                    for flav in flavours.keys():
                        flavours[flav].Add(infile.Get(self.name+"/h_pt_"+plot_name+"_"+flav).ProjectionX('_px',ybin,ybin,zbin,zbin))
                    infile.Close()
                infile= ROOT.TFile(outdir+singleTop_sample.name+"/"+singleTop_sample.name+"_" + systematic_name + "_combination.root", "read")
                for flav in singletopflavs.keys():
                    singletopflavs[flav]=infile.Get(self.name+"/h_pt_"+plot_name+"_"+flav).ProjectionX('_px',ybin,ybin,zbin,zbin).Clone()
                    singletopflavs[flav].SetDirectory(0)
                    singletopflavs[flav].SetName("h_"+singleTop_sample.name+"_"+plot_name+'_flav'+flav)
                infile.Close()
                infile= ROOT.TFile(outdir+ttb_sample.name+"/"+ttb_sample.name+"_" + systematic_name + "_combination.root", "read")
                for flav in ttbarflavs.keys():
                    ttbarflavs[flav]=infile.Get(self.name+"/h_pt_"+plot_name+"_"+flav).ProjectionX('_px',ybin,ybin,zbin,zbin).Clone()
                    ttbarflavs[flav].SetDirectory(0)
                    ttbarflavs[flav].SetName("h_"+ttb_sample.name+"_"+plot_name+'_flav'+flav)
                infile.Close()
                if jet==1:
                    flavours['ll'].SetFillColor(ROOT.kRed+1)
                    flavours['ll'].Add(flavours['lc'])
                    flavours['ll'].Add(flavours['lb'])
                    flavours['ll'].Add(singletopflavs['ll'])
                    flavours['ll'].Add(singletopflavs['lc'])
                    flavours['ll'].Add(singletopflavs['lb'])
                    flavours['ll'].Add(ttbarflavs['ll'])
                    flavours['ll'].Add(ttbarflavs['lc'])
                    flavours['ll'].Add(ttbarflavs['lb'])
                    flavpl.addMC("l+(l,c,b)", flavours['ll'])

                    flavours['cl'].SetFillColor(ROOT.kGreen-8)
                    flavours['cl'].Add(flavours['cc'])
                    flavours['cl'].Add(flavours['cb'])
                    flavours['cl'].Add(singletopflavs['cl'])
                    flavours['cl'].Add(singletopflavs['cc'])
                    flavours['cl'].Add(singletopflavs['cb'])
                    flavours['cl'].Add(ttbarflavs['cl'])
                    flavours['cl'].Add(ttbarflavs['cc'])
                    flavours['cl'].Add(ttbarflavs['cb'])
                    flavpl.addMC("c+(l,c,b)", flavours['cl'])

                    flavours['bl'].SetFillColor(ROOT.kBlue+1)
                    flavours['bl'].Add(flavours['bc'])
                    flavours['bl'].Add(ttbarflavs['bl'])
                    flavours['bl'].Add(ttbarflavs['bc'])
                    flavours['bl'].Add(singletopflavs['bl'])
                    flavours['bl'].Add(singletopflavs['bc'])
                    flavpl.addMC("b+(l,c) ", flavours['bl'])


                    flavours['bb'].SetFillColor(7)
                    flavours['bb'].Add(ttbarflavs['bb'])
                    flavours['bb'].Add(singletopflavs['bb'])
                    flavpl.addMC("bb", flavours['bb'])
                    flavpl.setSignal(flavours['bb'],"bb")
                if jet==2:
                    flavours['ll'].SetFillColor(ROOT.kRed+1)
                    flavours['ll'].Add(flavours['cl'])
                    flavours['ll'].Add(flavours['bc'])
                    flavours['ll'].Add(singletopflavs['ll'])
                    flavours['ll'].Add(singletopflavs['cl'])
                    flavours['ll'].Add(singletopflavs['bl'])
                    flavours['ll'].Add(ttbarflavs['ll'])
                    flavours['ll'].Add(ttbarflavs['cl'])
                    flavours['ll'].Add(ttbarflavs['bl'])
                    flavpl.addMC("(l,c,b)+l", flavours['ll'])

                    flavours['cc'].SetFillColor(ROOT.kGreen-8)
                    flavours['cc'].Add(flavours['cb'])
                    flavours['cc'].Add(flavours['cb'])
                    flavours['cc'].Add(singletopflavs['lc'])
                    flavours['cc'].Add(singletopflavs['cc'])
                    flavours['cc'].Add(singletopflavs['bc'])
                    flavours['cc'].Add(ttbarflavs['lc'])
                    flavours['cc'].Add(ttbarflavs['cc'])
                    flavours['cc'].Add(ttbarflavs['bc'])
                    flavpl.addMC("(l,c,b)+c", flavours['cc'])

                    flavours['lb'].SetFillColor(ROOT.kBlue+1)
                    flavours['lb'].Add(flavours['cb'])
                    flavours['lb'].Add(ttbarflavs['lb'])
                    flavours['lb'].Add(ttbarflavs['cb'])
                    flavours['lb'].Add(singletopflavs['lb'])
                    flavours['lb'].Add(singletopflavs['cb'])
                    flavpl.addMC("(l,c)+b ", flavours['lb'])

                    flavours['bb'].SetFillColor(7)
                    flavours['bb'].Add(ttbarflavs['bb'])
                    flavours['bb'].Add(singletopflavs['bb'])
                    flavpl.addMC("bb", flavours['bb'])
                    flavpl.setSignal(flavours['bb'],"bb")

                flavpl.make_plot(try_cut_ratio=try_cut_ratio,outputfile=outputfile,title="iMass of bin "+binLab)
            if jet==1:
                binName='_pt1_'+str(ybin)+'_pt2_integrated'
                y1=ybin
                y2=ybin
                z1=1
                z2=nYbins+1
            if jet==2:
                binName='_pt1_integrated'+'_pt2_'+str(ybin)
                z1=ybin
                z2=ybin
                y1=1
                y2=nYbins+1
            # print "jet",binName
            flavours={'bb':0,'bc':0,'bl':0,'cb':0,'cc':0,'cl':0,'lb':0,'lc':0,'ll':0}
            flavourskeys=flavours.keys()
            #plots devided by sample
            infile= ROOT.TFile(outdir+dataName+"/"+dataName+"_nominal_combination.root", "read")
            data_h=infile.Get(self.name+"/h_pt_"+plot_name+"_data").ProjectionX('_px',y1,y2,z1,z2)
            data_h.SetDirectory(0)
            data_h.SetMaximum(data_h.GetMaximum()*100)
            yAxis=infile.Get(self.name+"/h_pt_"+plot_name+"_data").GetYaxis()
            if jet==1:
                binLab='pT1: '+str(yAxis.GetBinLowEdge(ybin))+" - "+str(yAxis.GetBinUpEdge(ybin)) +' pT2: integrated'
            if jet==2:
                binLab='pT1: integrated'+' pT2: '+str(yAxis.GetBinLowEdge(ybin))+" - "+str(yAxis.GetBinUpEdge(ybin))
            infile.Close()
            data_h.SetName("h_data_"+plot_name)
        if "SS" in self.name:
            if "SS_NPel" in self.name:
                pl=h_plot(plot_name+binName,data_h,self.plotdir+subFold,xLabel=xLabel,yLabel=yLabel,setYlim=setYlim,ratioRange=[0.31,5.01])
            else:
                pl=h_plot(plot_name+binName,data_h,self.plotdir+subFold,xLabel=xLabel,yLabel=yLabel,setYlim=setYlim,ratioRange=[0.31,2.01])
        else:
            pl=h_plot(plot_name+binName,data_h,self.plotdir+subFold,xLabel=xLabel,yLabel=yLabel,setYlim=setYlim,ratioRange=[0.41,1.39])
        hists=[]
        for sample in self.samples:
            infile= ROOT.TFile(outdir+sample.name+"/"+sample.name+"_" + systematic_name + "_combination.root", "read")
            hist=infile.Get(self.name+"/h_pt_"+plot_name+"_"+list(flavourskeys)[0]).ProjectionX('_px',y1,y2,z1,z2).Clone()
            hist.SetDirectory(0)
            hist.SetName("h_"+sample.name+"_"+plot_name)
            for flav in list(flavourskeys)[1:]:
                hist.Add(infile.Get(self.name+"/h_pt_"+plot_name+"_"+flav).ProjectionX('_px',y1,y2,z1,z2))
            sample.setHistOptions(hist)
            if sample==ttb:
                pl.setSignal(hist,"ttbar")
            pl.addMC(legendLabels[sample.name],hist)
            hists.append(hist)
            infile.Close()
        pl.make_plot(outputfile=outputfile)

            # jet plot devided by flavours:
        if "SS" in self.name:
            if "SS_NPel" in self.name:
                flavpl=h_plot(plot_name+binName+'_flavpl',data_h,self.plotdir+subFold,xLabel=xLabel,yLabel=yLabel,setYlim=setYlim,ratioRange=[0.31,5.01])
            else:
                flavpl=h_plot(plot_name+binName+'_flavpl',data_h,self.plotdir+subFold,xLabel=xLabel,yLabel=yLabel,setYlim=setYlim,ratioRange=[0.31,2.01])
        else:
            flavpl=h_plot(plot_name+binName+'_flavpl',data_h,self.plotdir+subFold,xLabel=xLabel,yLabel=yLabel,setYlim=setYlim,ratioRange=[0.41,1.39])
        singletopflavs=dict(flavours)
        ttbarflavs=dict(flavours)
        infile= ROOT.TFile(outdir+samples[0].name+"/"+samples[0].name+"_" + systematic_name + "_combination.root", "read")
        for flav in flavours.keys():
            flavours[flav]=infile.Get(self.name+"/h_pt_"+plot_name+"_"+flav).ProjectionX('_px',y1,y2,z1,z2).Clone()
            flavours[flav].SetDirectory(0)
            flavours[flav].SetName("h_"+"_"+plot_name+'_flav'+flav)
        infile.Close()
        for sample in other_samples:
            #print sample.name
            #print "looking for: ",outdir+sample.name,"h_"+plot_name+"_"+flavourskeys[0]
            infile= ROOT.TFile(outdir+sample.name+"/"+sample.name+"_" + systematic_name + "_combination.root", "read")
            for flav in flavours.keys():
                flavours[flav].Add(infile.Get(self.name+"/h_pt_"+plot_name+"_"+flav).ProjectionX('_px',y1,y2,z1,z2))
            infile.Close()
        infile= ROOT.TFile(outdir+singleTop_sample.name+"/"+singleTop_sample.name+"_" + systematic_name + "_combination.root", "read")
        for flav in singletopflavs.keys():
            singletopflavs[flav]=infile.Get(self.name+"/h_pt_"+plot_name+"_"+flav).ProjectionX('_px',y1,y2,z1,z2).Clone()
            singletopflavs[flav].SetDirectory(0)
            singletopflavs[flav].SetName("h_"+singleTop_sample.name+"_"+plot_name+'_flav'+flav)
        infile.Close()
        infile= ROOT.TFile(outdir+ttb_sample.name+"/"+ttb_sample.name+"_" + systematic_name + "_combination.root", "read")
        for flav in ttbarflavs.keys():
            ttbarflavs[flav]=infile.Get(self.name+"/h_pt_"+plot_name+"_"+flav).ProjectionX('_px',y1,y2,z1,z2).Clone()
            ttbarflavs[flav].SetDirectory(0)
            ttbarflavs[flav].SetName("h_"+ttb_sample.name+"_"+plot_name+'_flav'+flav)
        infile.Close()
        if jet==1:
            flavours['ll'].SetFillColor(ROOT.kRed+1)
            flavours['ll'].Add(flavours['lc'])
            flavours['ll'].Add(flavours['lb'])
            flavours['ll'].Add(singletopflavs['ll'])
            flavours['ll'].Add(singletopflavs['lc'])
            flavours['ll'].Add(singletopflavs['lb'])
            flavours['ll'].Add(ttbarflavs['ll'])
            flavours['ll'].Add(ttbarflavs['lc'])
            flavours['ll'].Add(ttbarflavs['lb'])
            flavpl.addMC("l+(l,c,b)", flavours['ll'])

            flavours['cl'].SetFillColor(ROOT.kGreen-8)
            flavours['cl'].Add(flavours['cc'])
            flavours['cl'].Add(flavours['cb'])
            flavours['cl'].Add(singletopflavs['cl'])
            flavours['cl'].Add(singletopflavs['cc'])
            flavours['cl'].Add(singletopflavs['cb'])
            flavours['cl'].Add(ttbarflavs['cl'])
            flavours['cl'].Add(ttbarflavs['cc'])
            flavours['cl'].Add(ttbarflavs['cb'])
            flavpl.addMC("c+(l,c,b)", flavours['cl'])

            flavours['bl'].SetFillColor(ROOT.kBlue+1)
            flavours['bl'].Add(flavours['bc'])
            flavours['bl'].Add(ttbarflavs['bl'])
            flavours['bl'].Add(ttbarflavs['bc'])
            flavours['bl'].Add(singletopflavs['bl'])
            flavours['bl'].Add(singletopflavs['bc'])
            flavpl.addMC("b+(l,c) ", flavours['bl'])


            flavours['bb'].Add(ttbarflavs['bb'])
            flavours['bb'].Add(singletopflavs['bb'])
            flavpl.addMC("bb", flavours['bb'])
            flavpl.setSignal(flavours['bb'],"bb")
        if jet==2:
            flavours['ll'].SetFillColor(ROOT.kRed+1)
            flavours['ll'].Add(flavours['cl'])
            flavours['ll'].Add(flavours['bc'])
            flavours['ll'].Add(singletopflavs['ll'])
            flavours['ll'].Add(singletopflavs['cl'])
            flavours['ll'].Add(singletopflavs['bl'])
            flavours['ll'].Add(ttbarflavs['ll'])
            flavours['ll'].Add(ttbarflavs['cl'])
            flavours['ll'].Add(ttbarflavs['bl'])
            flavpl.addMC("(l,c,b)+l", flavours['ll'])

            flavours['cc'].SetFillColor(ROOT.kGreen-8)
            flavours['cc'].Add(flavours['cb'])
            flavours['cc'].Add(flavours['cb'])
            flavours['cc'].Add(singletopflavs['lc'])
            flavours['cc'].Add(singletopflavs['cc'])
            flavours['cc'].Add(singletopflavs['bc'])
            flavours['cc'].Add(ttbarflavs['lc'])
            flavours['cc'].Add(ttbarflavs['cc'])
            flavours['cc'].Add(ttbarflavs['bc'])
            flavpl.addMC("(l,c,b)+c", flavours['cc'])

            flavours['lb'].SetFillColor(ROOT.kBlue+1)
            flavours['lb'].Add(flavours['cb'])
            flavours['lb'].Add(ttbarflavs['lb'])
            flavours['lb'].Add(ttbarflavs['cb'])
            flavours['lb'].Add(singletopflavs['lb'])
            flavours['lb'].Add(singletopflavs['cb'])
            flavpl.addMC("(l,c)+b ", flavours['lb'])


            flavours['bb'].Add(ttbarflavs['bb'])
            flavours['bb'].Add(singletopflavs['bb'])
            flavpl.addMC("bb", flavours['bb'])
            flavpl.setSignal(flavours['bb'],"bb")
        flavpl.make_plot(try_cut_ratio=try_cut_ratio,outputfile=outputfile,title="iMass of bin "+binLab)
        outputfile.Close()
        print('ouputfile '+output_filename+' closed.')

def sr_cr_plot(samples,channel_name,hist_name,draw_ratio=False):
    legend_x      = 0.55#0.65
    legend_y      = 0.90
    legend_width  = 0.28
    legend_height = 0.42#0.34
    #for the canvas
    height=600
    if draw_ratio:
        height=1000
    width  = 800
    flavours={'bb':0,'bc':0,'bl':0,'cb':0,'cc':0,'cl':0,'lb':0,'lc':0,'ll':0}
    flavourskeys=flavours.keys()
    gStyle.SetPaintTextFormat("1.3f")
    gStyle.SetNumberContours(12)
    outFile= ROOT.TFile(channel_name+"_"+hist_name+".root", "RECREATE")
    if "SS_NPel" in self.name:
        infile= ROOT.TFile(outdir+dataName+"/"+dataName+"_data_modified_"+ systematic_name +"_combination.root", "read")
        if infile:
            print("open: "+outdir+dataName+"/"+dataName+"_data_modified_"+ systematic_name +"_combination.root")
            outFile.cd()
            h_data=infile.Get(channel_name+"/"+hist_name+"_data").Clone()
            infile.Close()
        else:
            print("could not open: "+outdir+dataName+"/"+dataName+"_data_modified_"+ systematic_name +"_combination.root")
            return 1
    else:
        infile= ROOT.TFile(outdir+dataName+"/"+dataName+"_nominal_combination.root", "read")
        if infile:
            print("open: "+outdir+dataName+"/"+dataName+"_nominal_combination.root")
            outFile.cd()
            h_data=infile.Get(channel_name+"/"+hist_name+"_data").Clone()
            infile.Close()
        else:
            print("could not open: "+outdir+dataName+"/"+dataName+"_nominal_combination.root")
            return 1
    infile= ROOT.TFile(outdir+samples[0].name+"/"+samples[0].name+"_" + systematic_name + "_combination.root", "read")
    for flav in flavours.keys():
        outFile.cd()
        flavours[flav]=infile.Get(channel_name+"/"+hist_name+"_"+flav).Clone()
    infile.Close()
    for sample in samples[1:]:
        infile= ROOT.TFile(outdir+sample.name+"/"+sample.name+"_" + systematic_name + "_combination.root", "read")
        outFile.cd()
        for flav in flavours.keys():
#            print(outdir+sample.name+"/"+sample.name+"_" + systematic_name + "_combination.root")
#            print(channel_name+"/"+hist_name+"_"+flav)
            flavours[flav].Add(infile.Get(channel_name+"/"+hist_name+"_"+flav))
        infile.Close()
    #now lets get rid of c  ROOT.kRed+1, ROOT.kGreen-8, ROOT.kBlue+1, ROOT.kGray+1
    outFile.cd()
    flavours['ll'].Add(flavours['lc'])
    flavours['ll'].Add(flavours['cl'])
    flavours['ll'].Add(flavours['cc'])

    flavours['bl'].Add(flavours['bc'])


    flavours['lb'].Add(flavours['cb'])


    flavours_reduced={'ll':0,'lb':0,'bl':0,'bb':0}
    flavour_fill = {'bb':ROOT.kGreen-8,'bl':ROOT.kGray+1,'lb':ROOT.kBlue+1,'ll':ROOT.kRed+1,"data":0}
    flavours_reduced_keys=flavours_reduced.keys()
    for flav in flavours_reduced_keys:
        flavours_reduced[flav]=flavours[flav]
    for flav in flavours_reduced_keys:
        c1 = TCanvas('c_'+channel_name+"_"+hist_name+"_inclusive_"+flav, hist_name+"_inclusive_"+flav, width, height)
        c1.cd()
        h_proj=flavours_reduced[flav].Projection(0,1)
        #h_proj.SetFillColor(flavour_fill[flav])
        h_proj.Draw("Colztexte")
        outFile.cd()
        c1.Write()
    #get n tp nYbins
    n_pt=flavours_reduced["bb"].GetAxis(2).GetNbins()
    dir_n_pt1_pt2_flav=outFile.mkdir("n_pt1_pt2_flav")
    dir_n_pt1_pt2_flav.cd()
    for pt1 in range(1,n_pt+1):
        for pt2 in range(1,n_pt+1):
            if pt1 >= pt2:
                legend = TLegend(legend_x, legend_y-legend_height, legend_x+legend_width, legend_y)
                legend.SetFillStyle(0)
                legend.SetLineColor(0)
                legend.SetTextFont(42)
                legend.SetNColumns(2)
                pt_label = "_pt_"+str(pt1)+"_"+str(pt2)
                print("processing n: " + pt_label)
                #now we have to do stuff like:
                #data_input_hist_this->GetAxis(2)->SetRange(pt_jet1, pt_jet1)
                h_1d_n_sum=TH1D("h_1d_n"+pt_label+"_"+"flav_sum","number of event in signal and contro regions",4,0,4)
                h_1d_n_sum.GetXaxis().SetBinLabel(1,"SR")
                h_1d_n_sum.GetXaxis().SetBinLabel(2,"CR_HL")
                h_1d_n_sum.GetXaxis().SetBinLabel(3,"CR_LH")
                h_1d_n_sum.GetXaxis().SetBinLabel(4,"CR_HH")
                n_stack=THStack("s_N"+pt_label,"flavour fractions stacked")
                for flav in flavours_reduced_keys+["data"]:
                    c1 = TCanvas("c_N_2D"+pt_label+"_"+flav, hist_name+pt_label+"_"+flav, width, height)
                    c1.cd()
                    if flav=="data":
                        full_hist=h_data
                    else:
                        full_hist=flavours_reduced[flav]
                    full_hist.GetAxis(1).SetRange(pt1, pt1)
                    full_hist.GetAxis(2).SetRange(pt2, pt2)

                    h_proj=full_hist.Projection(0,1)
                    h_proj.SetName("h_N"+pt_label+"_"+flav)
                    #h_proj.SetFillColor(flavour_fill[flav])
                    h_proj.Draw("Colztexte")
                    dir_n_pt1_pt2_flav.cd()
                    c1.Write()
                    h_proj.Write()
                    #now lets get it to 1d
                    h_1d_n_flav=h_1d_n_sum.Clone("h_1d_n"+pt_label+"_"+flav)
                    h_1d_n_flav.SetStats(0)
                    h_1d_n_flav.Reset()
                    h_1d_n_flav.SetFillColor(flavour_fill[flav])
                    h_1d_n_flav.SetBinContent(1, h_proj.GetBinContent(1,1))
                    h_1d_n_flav.SetBinError(1, h_proj.GetBinError(1,1))
                    h_1d_n_flav.SetBinContent(2, h_proj.GetBinContent(2,1))
                    h_1d_n_flav.SetBinError(2, h_proj.GetBinError(2,1))
                    h_1d_n_flav.SetBinContent(3, h_proj.GetBinContent(1,2))
                    h_1d_n_flav.SetBinError(3, h_proj.GetBinError(1,2))
                    h_1d_n_flav.SetBinContent(4, h_proj.GetBinContent(2,2))
                    h_1d_n_flav.SetBinError(4, h_proj.GetBinError(2,2))
                    h_1d_n_flav.Write()
                    if flav=="data":
                        h_1d_n_data=h_1d_n_flav
                        legend.AddEntry(h_1d_n_data,"data","LPE")
                    else:
                        h_1d_n_sum.Add(h_1d_n_flav)
                        n_stack.Add(h_1d_n_flav)
                        legend.AddEntry(h_1d_n_flav,flav,"F")
                h_1d_n_sum.Write()
                c2 = TCanvas("c_n_stack"+pt_label, "c with event per flav"+pt_label, width, height)
                c2.SetLogy()
                h_1d_n_data.SetMarkerStyle(20)
                h_1d_n_data.SetMarkerSize(1.2)
                h_1d_n_data.Draw("LPE")
                n_stack.Draw("HISTSAME")
                legend.Draw()
                h_1d_n_data.Draw("LPESame")
                c2.Write()
    outFile.cd()
    dir_f_pt1_pt2=outFile.mkdir("f_pt1_pt2")
    dir_f_pt1_pt2.cd()
    for pt1 in range(1,n_pt+1):
        for pt2 in range(1,n_pt+1):
            if pt1 >= pt2:
                pt_label = "_pt_"+str(pt1)+"_"+str(pt2)
                print("processing f: "+ pt_label)
                dir_f_pt1_pt2.cd()
                legend = TLegend(legend_x, legend_y-legend_height, legend_x+legend_width, legend_y)
                legend.SetFillStyle(0)
                legend.SetLineColor(0)
                legend.SetTextFont(42)
                legend.SetNColumns(2)
                f_stack=THStack("s_f"+pt_label,"flavour fractions stacked "+pt_label)
                h_1d_n_sum = outFile.Get("n_pt1_pt2_flav/"+ "h_1d_n"+pt_label+"_"+"flav_sum")
                for flav in flavours_reduced_keys:
                    h_1d_f_flav=outFile.Get("n_pt1_pt2_flav/"+ "h_1d_n"+pt_label+"_"+flav).Clone("h_1d_f"+pt_label+"_"+flav)
                    h_1d_f_flav.Divide(h_1d_n_sum)
                    h_1d_f_flav.Write()
                    legend.AddEntry(h_1d_f_flav,flav,"F")
                    f_stack.Add(h_1d_f_flav)
                dir_f_pt1_pt2.cd()
                f_stack.Write()
                c1 = TCanvas("c_f_stack"+pt_label, "c with flav fractions", width, height)
                f_stack.Draw("HISTTEXT")
                legend.Draw()
                c1.Write()

#ROOT.gROOT.LoadMacro("AtlasUtils.C")
#ROOT.gROOT.LoadMacro("AtlasStyle.C")
#SetAtlasStyle()

samples = options.nominal_samples
samples = list(samples.values())
print(samples)

#if get plots of offline
channel_plot(samples,name="emu_OS_J2",n_jets=2,n_el=1,n_mu=1).makePlots()
channel_plot(samples,name="emu_OS_J2_m_lj_cut",n_jets=2,n_el=1,n_mu=1).makePlots()
#if get plots of online
# channel_plot(samples,name="emu_OS_J2_online_nominal",n_jets=2,n_el=1,n_mu=1).makePlots()
# channel_plot(samples,name="emu_OS_J2_online_emutrig",n_jets=2,n_el=1,n_mu=1).makePlots()
# channel_plot(samples,name="emu_OS_J2_online_match",n_jets=2,n_el=1,n_mu=1).makePlots()
# channel_plot(samples,name="emu_OS_J2_online_tagoff60",n_jets=2,n_el=1,n_mu=1).makePlots()
# channel_plot(samples,name="emu_OS_J2_online_tagoff70",n_jets=2,n_el=1,n_mu=1).makePlots()
# channel_plot(samples,name="emu_OS_J2_online_tagoff77",n_jets=2,n_el=1,n_mu=1).makePlots()
# channel_plot(samples,name="emu_OS_J2_online_tagoff85",n_jets=2,n_el=1,n_mu=1).makePlots()

