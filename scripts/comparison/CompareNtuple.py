import os
import argparse
import numpy as np
import ROOT as R
from ROOT import RDataFrame as RDF
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser()
# parser.add_argument('--list1', '-l1', type=str, default='/afs/cern.ch/user/c/cmo/private/workspace/qualification_task/FTAGResults/topcp_sample/archived-results/bcalibration_tcp_v1.0-furtherfix-nominal-only-20240922/input_files/ttbar_PhPy8_nominal/user.cmo.601229.PhPy8EG.DAOD_FTAG2.e8514_s4162_r15540_p6285.FTAG-DiLep-v2_output.txt', help='sample list 1 to be compared in txt format')
# parser.add_argument('--list2', '-l2', type=str, default='/afs/cern.ch/user/c/cmo/private/workspace/qualification_task/FTAGResults/topcp_sample/r25.2.35_DiLep-v2.2_mc23a/input_files/ttbar_PhPy8_nominal/user.cmo.601229.PhPy8EG.DAOD_FTAG2.e8514_s4162_r15540_p6285.Samples_Dilep-v2.2-metsys_output.txt', help='sample list 2 to be compared in txt format')
parser.add_argument('--old', '-l1', type=str, default='/afs/cern.ch/user/c/cmo/private/workspace/qualification_task/FTAGResults/topcp_sample/archived-results/bcalibration_tcp_v1.0-furtherfix-nominal-only-20240922/input_files/ttbar_PhPy8_nominal/user.cmo.601230.PhPy8EG.DAOD_FTAG2.e8514_s4162_r15540_p6285.FTAG-DiLep-v2_output.txt', help='sample list 1 to be compared in txt format')
# parser.add_argument('--list1', '-l1', type=str, default='/eos/home-c/cmo/project/Calibration/FTAGResults/topcp_sample/archived-results/bcalibration_tcp_v1.0-furtherfix-nominal-only-20240922/mysample/user.cmo.601230.PhPy8EG.DAOD_FTAG2.e8514_s4162_r15540_p6285.Dilep-frameworkv1.0fix_output.txt', help='sample list 1 to be compared in txt format')
parser.add_argument('--new', '-l2', type=str, default='/afs/cern.ch/user/c/cmo/private/workspace/qualification_task/FTAGResults/topcp_sample/r25.2.35_DiLep-v2.2_mc23a/input_files/ttbar_PhPy8_nominal/user.cmo.601230.PhPy8EG.DAOD_FTAG2.e8514_s4162_r15540_p6285.Samples_Dilep-v2.2-metsys_output.txt', help='sample list 2 to be compared in txt format')
args = parser.parse_args()


ary_dict = {
    'jet0_pt': [],
    'jet1_pt': [],
    'jet_sum_pt': [],
    'jet0_eta': [],
    'jet1_eta': [],
    'el_pt': [],
    'mu_pt': [],
}


for file_list in [args.old, args.new]:
    print(f'Processing {file_list}')
    with open(file_list) as f:
        file_vec = R.vector('string')()
        for line in f:
            file_vec.push_back(line.strip())
        rdf = RDF("reco", file_vec)
        rdf = rdf \
                .Define("is_good_jet", "jet_pt_NOSYS>20000 && abs(jet_eta)<2.5") \
                .Define("good_jet_pt", "jet_pt_NOSYS[is_good_jet]") \
                .Define("good_jet_eta", "jet_eta[is_good_jet]") \
                .Filter("good_jet_pt.size()>=2 && el_pt_NOSYS.size()==1 && mu_pt_NOSYS.size()==1") \
                .Define("jet_idx", """
                            std::vector<int> jet_idx_unsorted(good_jet_pt.size());
                            std::iota(jet_idx_unsorted.begin(), jet_idx_unsorted.end(), 0);
                            std::sort(jet_idx_unsorted.begin(), jet_idx_unsorted.end(), [&](int i, int j) { return good_jet_pt.at(i) > good_jet_pt.at(j); });
                            return jet_idx_unsorted;
                        """) \
                .Define("jet0_pt", "good_jet_pt.at(jet_idx[0]);") \
                .Define("jet1_pt", "good_jet_pt.at(jet_idx[1]);") \
                .Define("jet_sum_pt", "jet0_pt + jet1_pt") \
                .Define("jet0_eta", "good_jet_eta.at(jet_idx[0]);") \
                .Define("jet1_eta", "good_jet_eta.at(jet_idx[1]);") \
                .Define("el_pt", "el_pt_NOSYS[0]>27000? el_pt_NOSYS[0]:0") \
                .Define("mu_pt", "mu_pt_NOSYS[0]>27000? mu_pt_NOSYS[0]:0")

        arys = rdf.AsNumpy(columns=ary_dict.keys())
        for key in ary_dict.keys():
            ary_dict[key].append(arys[key])



histograms = []
bin_dict = {
    'pt': np.linspace(20000, 200000, 31),
    'eta': np.linspace(-3, 3, 31),
}
for key, arys in ary_dict.items():
    bins = bin_dict['pt'] if 'pt' in key else bin_dict['eta']
    # compare each pair of arys. Upper: 2 histograms; lower: ratio plot
    fig, axs = plt.subplots(2, 1, figsize=(6, 8), sharex=True, gridspec_kw={'height_ratios': [3, 1]})
    hist0, _, _ = axs[0].hist(arys[0], bins, label='old', color='b', histtype='step')
    hist1, _, _ = axs[0].hist(arys[1], bins, label='new', color='r', histtype='step')
    axs[0].set_ylabel('Entries')
    axs[0].legend(loc='upper right')
    axs[0].set_title(key)
    ratio = np.divide(hist0, hist1, out=np.zeros_like(hist0.astype(float)), where=hist1!=0)
    rel_err0 = 1/np.sqrt(hist0)
    rel_err1 = 1/np.sqrt(hist1)
    rel_err = np.sqrt(np.square(rel_err0) + np.square(rel_err1))
    err = np.multiply(ratio, rel_err)

    x = (bins[1:] + bins[:-1]) / 2
    axs[1].errorbar(x, ratio, yerr=err, fmt='o', color='b')
    axs[1].step(x, ratio, color='b')
    # axs[1].step(bins, np.ones_like(bins), color='b', linestyle='--')
    axs[1].axhline(1, color='black', linestyle='--')
    axs[1].set_xlabel(key)
    axs[1].set_ylabel('Ratio')
    axs[1].set_ylim(0.7, 1.3)
    if not os.path.exists('save'):
        os.makedirs('save')
    fig.savefig(f'save/{key}.png')
