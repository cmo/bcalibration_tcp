'''
########################################################################################################################
# A script to produce a set of comparison plots between histograms stored in two given .root files 
########################################################################################################################

usage: CompareHistograms [-h] [-f1 FILE1] [-f2 FILE2] [-l1 LABEL1] [-l2 LABEL2] 

Arguments:
	-h, --help            show this help message and exit
	-f1 FILE1, --file1 FILE1
                    Directory for production 1
	-f2 FILE2, --file2 FILE2
                    Directory for production 2
	-l1 LABEL1, --label1 LABEL1
                    Labels 1
	-l2 LABEL2, --label2 LABEL2
                    Labels 2
	-o OUT_DIR, --out_dir OUT_DIR
                    Where to save the plots
    -r REGIONS [REGIONS ...], --regions REGIONS [REGIONS ...]
                        The regions (directories) to be considered (for all the regions use "all")
    -pdf, --pdf           Add it to arrange the plots in several pdfs
	-n, --normalize       Add it to normalize the plots
	-L, --logscale        Use the log scale on the y axis
'''
import ROOT
import argparse
import os
import numpy as np
from array import array
import re

class HistComparison:
    def __init__(self, ifilename1, ifilename2):
        self.fname1 = ifilename1
        self.fname2 = ifilename2
        self.colors = {
            'data':(ROOT.kRed + 3),
        }


    def histogram_maker(self, hist1, hist2, normalize):
        self.hist_1 = hist1
        self.hist_2 = hist2

        bin_edges1 = []
        bin_edges2 = []
        for i in range(1, hist1.GetNbinsX() + 2):
            bin_edges1.append(hist1.GetXaxis().GetBinLowEdge(i))
        bin_edges1.append(hist1.GetXaxis().GetBinUpEdge(hist1.GetNbinsX()))
        for i in range(1, hist1.GetNbinsX() + 2):
            bin_edges2.append(hist2.GetXaxis().GetBinLowEdge(i))
        bin_edges2.append(hist2.GetXaxis().GetBinUpEdge(hist2.GetNbinsX()))

        hist_ratio1 = ROOT.TH1D(hist1.GetName()+"ratio1", hist1.GetName()+"ratio1", len(bin_edges1) -1, array('d', bin_edges1))
        hist_ratio2 = ROOT.TH1D(hist1.GetName()+"ratio2", hist2.GetName()+"ratio2", len(bin_edges2) -1, array('d', bin_edges2))

        # overflow bin treatment
        self.hist_1.SetBinContent(self.hist_1.GetNbinsX(), self.hist_1.GetBinContent(self.hist_1.GetNbinsX()) + self.hist_1.GetBinContent(self.hist_1.GetNbinsX() + 1))
        self.hist_1.SetBinContent(self.hist_1.GetNbinsX() + 1, 0)  
        self.hist_2.SetBinContent(self.hist_2.GetNbinsX(), self.hist_2.GetBinContent(self.hist_2.GetNbinsX()) + self.hist_2.GetBinContent(self.hist_2.GetNbinsX() + 1))
        self.hist_2.SetBinContent(self.hist_2.GetNbinsX() + 1, 0)

        # underflow bin treatment
        self.hist_1.SetBinContent(1, self.hist_1.GetBinContent(0) + self.hist_1.GetBinContent(1))
        self.hist_1.SetBinContent(0, 0)  
        self.hist_2.SetBinContent(1, self.hist_2.GetBinContent(0) + self.hist_2.GetBinContent(1))
        self.hist_2.SetBinContent(0, 0)

        if self.hist_1.Integral() > 0 and self.hist_2.Integral():
            self.yield_diff = (1 - (self.hist_1.Integral()/self.hist_2.Integral()))*100
        else:
            self.yield_diff = 0
        
        self.normalize = False
        if normalize:
            self.normalize = True
            scale1 = 1 / self.hist_1.Integral() if self.hist_1.Integral() > 0 else 1
            self.hist_1.Scale(scale1)
            scale2 = 1 / self.hist_2.Integral() if self.hist_2.Integral() > 0 else 1
            self.hist_2.Scale(scale2)
        
        # ratio histograms
        for i in range (0, self.hist_1.GetNbinsX()+1, 1):
            old = self.hist_1.GetBinContent(i+1)
            new = self.hist_2.GetBinContent(i+1)
            e_old =self.hist_1.GetBinError(i+1)
            e_new = self.hist_2.GetBinError(i+1)
            if old != 0:
                r = new/old
            else:
                old = 1e-12 # if old = 0, set it !=0
                if new != 0:
                    r = 1e12
                else:
                    r = 1
            e_ratio_new = np.sqrt(e_new*e_new + r*r*e_old*e_old)/old
            e_ratio_old = np.sqrt(e_old*e_old + e_old*e_old)/old
            hist_ratio1.SetBinContent(i+1,1)
            hist_ratio2.SetBinContent(i+1,r)
            hist_ratio1.SetBinError(i+1,e_ratio_old)
            hist_ratio2.SetBinError(i+1,e_ratio_new)

        self.hist_ratio_1 = hist_ratio1
        self.hist_ratio_2 = hist_ratio2

    def do_text(self, x , y, tt="", tsize=0.04, color=1, font=42):
        l=ROOT.TLatex() 
        l.SetTextSize(tsize)
        l.SetNDC()
        l.SetTextColor(color)
        l.SetTextFont(font)
        l.DrawLatex(x,y,tt)

    def ratio_plot_maker(self, sample, variable, oname, label1, label2, additional_info, outdir, drawtitle, logscale = False):
        self.canvas = ROOT.TCanvas(oname, oname, 600, 600)
        self.canvas.cd()

        h1 = self.hist_1
        h2 = self.hist_2
        
        h1_ratio = self.hist_ratio_1
        h2_ratio = self.hist_ratio_2
        
        P_1 = ROOT.TPad("1", "1", 0, 0.25, 1, 1)
        P_2 = ROOT.TPad("2", "2", 0, 0, 1, 0.25)
        P_1.SetMargin(0.1, 0.1, 0.01, 0.05)
        P_2.SetMargin(0.1, 0.1, 0.3, 0.1)
        P_1.Draw()
        P_2.Draw()

        # top pad: histograms
        P_1.cd()
        
        # draw the histograms
        h1.SetLineColor(ROOT.kBlack)
        h1.SetLineWidth(3)
        h2.SetLineColor(self.colors.get(sample, ROOT.kBlue))
        h2.SetLineWidth(3)
        h2.SetMinimum(0)
        h2.SetMaximum(1.75*(max(h1.GetMaximum(),h2.GetMaximum())))
        h2.SetTitleSize(0.04)
        h2.Draw('h')
        h1.Draw('hsame')
        
        # draw the axes labels
        ay = h2.GetYaxis()
        ax = h2.GetXaxis()
        ax.SetLabelOffset(999)
        ax.SetLabelSize(0)
        ay.SetTitle('Entries')
        if (self.normalize): ay.SetTitle('Normalized Entries')
        ax.SetTitle()
        ax.Draw()
        ay.Draw()
        
        # draw labels on the plot
        tsize = 0.04
        y_text_ATLAS = 0.85
        x_text_ATLAS = 0.5
        dy_text = 0.04
        self.do_text(x_text_ATLAS,y_text_ATLAS,"ATLAS",tsize,1,72)    
        self.do_text(x_text_ATLAS+0.1,y_text_ATLAS,"work in progress",tsize) 
        self.do_text(x_text_ATLAS,y_text_ATLAS-2*dy_text,"%s"%(additional_info),tsize)
        self.do_text(x_text_ATLAS,y_text_ATLAS-3.5*dy_text,label1,tsize, ROOT.kBlack)
        self.do_text(x_text_ATLAS,y_text_ATLAS-4.5*dy_text,label2,tsize, self.colors.get(sample, ROOT.kBlue))
        self.do_text(0.15,y_text_ATLAS-1.7*dy_text,variable,0.03,1)
        if (self.yield_diff < 0):
            self.do_text(0.15,0.85,"1-Ratio(Yields) = %.1f%%"%self.yield_diff, tsize)
        else: 
            self.do_text(0.15,0.85,"1-Ratio(Yields) = +%.1f%%"%self.yield_diff, tsize)
        
        self.do_text(0.15,0.85+dy_text, sample, tsize, self.colors.get(sample, ROOT.kBlue))
        
        # general top pad cosmetic
        def find_min_nonzero(hist):
            min_val = float('inf')
            for bin in range(1, hist.GetNbinsX() + 1):
                bin_content = hist.GetBinContent(bin)
                if bin_content > 0 and bin_content < min_val:
                    min_val = bin_content
            return min_val  
        
        if logscale: 
            h2.SetMinimum(min(find_min_nonzero(h1), find_min_nonzero(h2)))
            h2.SetMaximum(20*(max(h1.GetMaximum(),h2.GetMaximum())))
            P_1.SetLogy()
        ROOT.gStyle.SetOptStat(0)

        # bottom pad: ratio plot
        P_2.cd()
        
        # draw the histograms
        h1_ratio.SetLineColor(ROOT.kBlack)
        h1_ratio.SetLineWidth(3)
        h2_ratio.SetLineColor(self.colors.get(sample, ROOT.kBlue))
        h2_ratio.SetLineWidth(3)    
        h1_ratio.SetTitle('')
        h1_ratio.Draw('h')
        h2_ratio.Draw('hsame')

        # axes cosmesis
        ay_ratio = h1_ratio.GetYaxis()
        ax_ratio = h1_ratio.GetXaxis()
        ay_ratio.SetTitle('Ratio')
        ax_ratio.SetTitle(variable)
        ax_ratio.SetTitleSize(0.1)
        ax_ratio.SetLabelSize(0.1)
        ay_ratio.SetTitleOffset(0.4)
        ay_ratio.SetTitleSize(0.1)
        ay_ratio.SetLabelSize(0.1)
        ay_ratio.SetRangeUser(0.7,1.3)
        # ay_ratio.SetRangeUser(0.8,1.2)
        ay_ratio.SetNdivisions(5)
        
        ax_ratio.Draw()
        ay_ratio.Draw()

        P_2.SetGrid()

        # save the canvas
        plotname = os.path.join(outdir, oname)
        if self.normalize:
            plotname += '_norm'
        if logscale:
            plotname += '_log'
        self.canvas.SaveAs(plotname + '.png')
        return plotname + '.png'


from PIL import Image
class PlotArranger:
    @classmethod
    def create_image_grid(cls, images, max_images_per_row=4):
        # Determine image sizes (assuming all images are the same size)
        width, height = images[0].size

        # Calculate the number of rows needed for this page
        num_rows = (len(images) + max_images_per_row - 1) // max_images_per_row

        # Create a new blank image to hold the grid
        grid_image = Image.new('RGB', (width * max_images_per_row, height * num_rows))

        # Paste images into the grid image
        for index, image in enumerate(images):
            x_offset = (index % max_images_per_row) * width
            y_offset = (index // max_images_per_row) * height
            grid_image.paste(image, (x_offset, y_offset))
        return grid_image

    @classmethod
    def arrange_plots(cls, plots, outname):
        # Dimensions and counters
        images_per_page = 12  # nx4 grid
        images_per_row = 4

        # Ensure all images are converted to the same mode and size if necessary
        images = [Image.open(x).convert('RGB') for x in plots]

        # List to hold each grid image
        images_to_draw = []
        grid_images = []
        image_group_name = None
        for i, image in enumerate(images):
            cur_image_group_name = plots[i].split('/')[:-1]
            cur_image_group_name = '/'.join(cur_image_group_name)

            # If we have enough images for a page, or if we're switching to a new group, create a grid image
            if (
                len(images_to_draw) == images_per_page or 
                (image_group_name != None and image_group_name != cur_image_group_name) or 
                i == len(images) - 1
            ):
                grid = cls.create_image_grid(images_to_draw, images_per_row)
                grid_images.append(grid)
                images_to_draw = []
            images_to_draw.append(image)
            image_group_name = cur_image_group_name

        print(f"Saving PDF file: {outname}")
        grid_images[0].save(outname, save_all=True, append_images=grid_images[1:], quality=600)



def main():
    # Parse the arguments
    parser = argparse.ArgumentParser(prog = 'CompareHistograms',
                                    description = 'A simple plotting tool for a comparison of distributions between two different productions.')
    parser.add_argument('-f1', '--file1', help='ROOT file 1')
    parser.add_argument('-f2', '--file2', help='ROOT file 2')
    parser.add_argument('-l1', '--label1', default = 'Version 1', help='Labels 1')
    parser.add_argument('-l2', '--label2', default = 'Version 2', help='Labels 2')
    parser.add_argument('-o', '--out_dir', default = 'Plots/', help='Where to save the plots')
    parser.add_argument('-r', '--regions', nargs ='+', default =['all'], help='The regions (directories) to be considered (for all the regions use "all")')
    parser.add_argument('-pdf', '--pdf', default = False, action = 'store_true', help='Add it to arrange the plots in several pdfs')
    parser.add_argument('-n', '--normalize', default = False, action = 'store_true', help='Add it to normalize the plots')
    parser.add_argument('-L', '--logscale', default = False, action='store_true', help='Use the log scale on the y axis')
    args = parser.parse_args()  

    filename1 = args.file1
    filename2 = args.file2

    comp = HistComparison(filename1, filename2)
    file1 = ROOT.TFile(comp.fname1, 'read')
    file2 = ROOT.TFile(comp.fname2, 'read')

    out_dir = args.out_dir
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    # create the list of histograms to be compared
    hists1 = [] # histograms in file 1
    hists2 = [] # histograms in file 2

    keys1 = file1.GetListOfKeys()
    keys2 = file2.GetListOfKeys()

    # retrieve the list of histograms in file1
    for key in keys1:
        if "TH1" in key.GetClassName():
            hists1.append(key.GetName())
        elif "TDirectory" in key.GetClassName():
            dir_name = key.GetName()
            # if the regions are specified, only consider the specified regions
            if args.regions != ['all'] and dir_name not in args.regions:
                continue
            d = file1.Get(dir_name)
            subkeys = d.GetListOfKeys()
            for subkey in subkeys:
                if "TH1" in subkey.GetClassName():
                    hist_path = dir_name + "/" + subkey.GetName()
                    hists1.append(hist_path)
    
    # retrieve the list of histograms in file2
    for key in keys2:
        if "TH1" in key.GetClassName():
            hists2.append(key.GetName())
        elif "TDirectory" in key.GetClassName():
            d = file2.Get(key.GetName())
            subkeys = d.GetListOfKeys()
            for subkey in subkeys:
                if "TH1" in subkey.GetClassName():
                    hist_path = key.GetName() + "/" + subkey.GetName()
                    hists2.append(hist_path)

    # assert set(hists1) == set(hists2), f"The two files do not contain the same histograms.\n Only in file 1: {set(hists1) - set(hists2)}\n Only in file 2: {set(hists2) - set(hists1)}"
    if set(hists1) != set(hists2):
        print("Warning: The two files do not contain the same histograms.")
        # print(f"Only in file 1: {set(hists1) - set(hists2)}")
        # print(f"Only in file 2: {set(hists2) - set(hists1)}")
    common_hists = set(hists1).intersection(set(hists2))
    hist_path_list = list(common_hists)

    for item in hist_path_list:
        print(item)

    # now, do the plots for each pair of histograms
    saved_plots = []
    for histpath in hist_path_list:
        keywords = histpath.split('/')
        if len(keywords) == 1:
            histname = keywords[0]
            cur_out_dir = out_dir
        elif len(keywords) == 2:
            dirname = keywords[0]
            histname = keywords[1]
            cur_out_dir = os.path.join(out_dir, dirname)
            if not os.path.exists(cur_out_dir):
                os.makedirs(cur_out_dir)

        hist1 = file1.Get(histpath)
        hist2 = file2.Get(histpath)
        comp.histogram_maker(hist1, hist2, args.normalize)
        plot = comp.ratio_plot_maker("", histname, histname, args.label1, args.label2, '', cur_out_dir, True, False) # no log
        saved_plots.append(plot)
        # comp.ratio_plot_maker("", histname, histpath, args.label1, args.label2, '', cur_out_dir, True, True) # log
    
    if args.pdf:
        saved_plots.sort()
        outname = os.path.join(out_dir, "comparison_plots.pdf")
        PlotArranger.arrange_plots(saved_plots, outname)


if __name__ == "__main__":
    print ('Starting CompareHistograms')
    main()
    print ('The end')
