#!/usr/bin/env python

# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
# script modified from ../toptoolkit-ntuples/Samples/submitToGrid.py 
# Also see https://gitlab.cern.ch/atlas-ftag-calibration/toptoolkit-ntuples/-/blob/main/Samples/submitToGrid.py?ref_type=heads

import GridSubmission.grid
from Samples_Dilep import SamplesDilep
import os
import argparse

file_dir = os.path.dirname(os.path.realpath(__file__))

# Define the arguments when calling this script.
def get_args():
    cmdLineArgs = argparse.ArgumentParser(description='Submit TopCPTookit jobs to the grid given a set of samples and a channel.')
    cmdLineArgs.add_argument('tag', type=str, default='FTAG', help='Tag to be used for the output. Used to indentify the samples on the grid.')
    cmdLineArgs.add_argument('account', type=str, help='Account to use for the grid submission.')
    cmdLineArgs.add_argument('--only-check', action='store_true', help='Only check the status of the samples on the grid.')
    cmdLineArgs.add_argument('--dry-run', action='store_true', help='Do not submit the jobs to the grid, just test.')
    cmdLineArgs = cmdLineArgs.parse_args()
    return cmdLineArgs

cmdLineArgs = get_args()

def copyConfigsToTCTPath():
    pathToConfig = f'{file_dir}/../configs/DiLepton/'
    pathToTCTConfigs = f'{file_dir}/../toptoolkit-ntuples/TopCPToolkit/source/TopCPToolkit/share/configs/'
    os.system('cp -r '+pathToConfig+' '+pathToTCTConfigs)

samplesToUse = SamplesDilep
copyConfigsToTCTPath()

config = GridSubmission.grid.Config()
config.code          = 'runTop_el.py -t DiLepton ' # --no-systematics'  # run reco only and systematics defined in the YML Config.
config.outputName    = 'output' # output directory from EventLoop
config.gridUsername  = cmdLineArgs.account # use e.g. phys-top or phys-higgs for group production
config.suffix        = cmdLineArgs.tag
config.excludedSites = ''
config.noSubmit      = cmdLineArgs.dry_run # set to True if you just want to test the submission
config.mergeType     = 'None' #'None', 'Default' or 'xAOD'
config.destSE        = '' #This is the default (anywhere), or try e.g. 'UKI-SOUTHGRID-BHAM-HEP_LOCALGROUPDISK'
config.maxNFilesPerJob = '5'
# by default the requested memory is set to 2GB, if you need to increase this, see the option below
# config.memory = '4000' # NOTE grid sites with 4GB for single-core user jobs are rare
# config.otherOptions = '' # use this to pass any other arguments to prun

datasets = {}
names = samplesToUse.GetListOfKeys()
for iname in names:
    GridSubmission.grid.Add(iname).datasets = samplesToUse.datasetsDic[iname].datasets

samples = GridSubmission.grid.Samples(names)
GridSubmission.ami.check_sample_status(samples)  # Call with (samples, True) to halt on error
if not cmdLineArgs.only_check:
    GridSubmission.grid.submit(config, samples)
