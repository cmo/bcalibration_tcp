class Sample:
    def __init__(self, name):
        self.name = name
        self.datasets = []

class GridDatasets:
    def __init__(self, derivationType : str = 'AOD') -> None:
        self.datasetsDic = {}
        self.datasetsType = derivationType

    def Add(self, key: str) -> dict:
        self.datasetsDic[key] = Sample(key)
        return self.datasetsDic[key]
    def Print(self):
        for key in self.datasetsDic:
            print(key)
            for dataset in self.datasetsDic[key].datasets:
                print(dataset)
            print('\n')
    def GetListOfKeys(self):
        listOfKeys = []
        for key in self.datasetsDic:
            listOfKeys.append(key)
        return listOfKeys

SamplesDilep = GridDatasets('DAOD_FTAG2')

# Samples not found are marked with a comment

##################################
##########     Data     ##########
##################################

SamplesDilep.Add("data_run3").datasets = [
#     "data22_13p6TeV:data22_13p6TeV.00428648.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00428700.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00428747.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00428759.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00428770.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00428776.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00428777.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00428855.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00429027.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00429142.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00429452.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00429470.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00429603.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00429606.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00429697.physics_Main.deriv.DAOD_FTAG2.r15158_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00429929.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00429940.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00430036.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00430178.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00430336.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00430488.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00430490.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00430526.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00430542.physics_Main.deriv.DAOD_FTAG2.r15052_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00430580.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00430644.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00430648.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00430896.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00431037.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00431178.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00431241.physics_Main.deriv.DAOD_FTAG2.r15052_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00431341.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00431493.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00431810.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00429018.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00429716.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00431179.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00429137.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00429469.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00429612.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00429658.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00429782.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00429993.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00430183.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00430341.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00430536.physics_Main.deriv.DAOD_FTAG2.r15052_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00430647.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00430702.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00430897.physics_Main.deriv.DAOD_FTAG2.r15052_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00431215.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00431228.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00431850.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00431885.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00431894.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00431914.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00432180.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00435816.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00435831.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00435931.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00436169.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00436182.physics_Main.deriv.DAOD_FTAG2.r15052_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00436195.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00436354.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00436550.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00436582.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00436584.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00436602.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00436656.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00436799.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00436863.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00436983.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437010.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437034.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437062.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437065.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437079.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437124.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437484.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437522.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437548.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437580.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437623.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437692.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437711.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437744.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437756.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437778.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437834.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437837.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437881.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437898.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437944.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437971.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437991.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00438181.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00438219.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00438234.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00438252.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00438266.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00438277.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00438298.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00438323.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00438364.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00438411.physics_Main.deriv.DAOD_FTAG2.r15052_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00438446.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00438481.physics_Main.deriv.DAOD_FTAG2.r15052_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00438502.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00438638.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00438737.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00439519.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00439529.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00439676.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00439798.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00439830.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00439911.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00439927.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00440199.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00440407.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00440485.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00440570.physics_Main.deriv.DAOD_FTAG2.r15052_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00439607.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00431812.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00438532.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00431906.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00435946.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00435854.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00437780.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00436041.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00440499.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00440543.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00436377.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00436422.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00439859.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00436496.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
#     "data22_13p6TeV:data22_13p6TeV.00438786.physics_Main.deriv.DAOD_FTAG2.r14755_p5604_p6324",
    "data23_13p6TeV.periodAllYear.physics_Main.PhysCont.DAOD_FTAG2.grp23_v01_p6288"
]

# ##################################
# ##########     MC23a     ##########
# ##################################

# # Use r15540 for ttbar (due to muon trigger bug. refer to: https://its.cern.ch/jira/browse/ATLMCPROD-11215)
# SamplesDilep.Add("ttbar_mc23a").datasets = [
#     "mc23_13p6TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.deriv.DAOD_FTAG2.e8514_s4162_r15540_p6285",
#     "mc23_13p6TeV.601230.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_FTAG2.e8514_s4162_r15540_p6285",
# ]

# SamplesDilep.Add("ttbar_syst_mc23a").datasets = [
#     "mc23_13p6TeV.601415.PhH7EG_A14_ttbar_hdamp258p75_Dilep.deriv.DAOD_FTAG2.e8514_s4162_r15540_p6285",
#     "mc23_13p6TeV.601399.PhPy8EG_A14_ttbar_hdamp517p5_dil.deriv.DAOD_FTAG2.e8514_s4162_r15540_p6285",
# 	"mc23_13p6TeV.601491.PhPy8EG_A14_ttbar_pThard1_dil.deriv.DAOD_FTAG2.e8549_s4162_r14622_p6285"
# ]


# SamplesDilep.Add("Singletop_PowPy8_mc23a").datasets = [
# 	"mc23_13p6TeV.601348.PhPy8EG_tb_lep_antitop.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285",
# 	"mc23_13p6TeV.601349.PhPy8EG_tb_lep_top.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285",
# 	"mc23_13p6TeV.601350.PhPy8EG_tqb_lep_antitop.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285",
# 	"mc23_13p6TeV.601351.PhPy8EG_tqb_lep_top.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285",
# 	"mc23_13p6TeV.601353.PhPy8EG_tW_dyn_DR_dil_antitop.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285",
# 	"mc23_13p6TeV.601354.PhPy8EG_tW_dyn_DR_dil_top.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285"
# ]

# SamplesDilep.Add("Singletop_PowPy8_DS_mc23a").datasets = [
# 	"mc23_13p6TeV.601627.PhPy8EG_tW_DS_dyn_incl_antitop.deriv.DAOD_FTAG2.e8549_s4162_r14622_p6285",
# 	"mc23_13p6TeV.601631.PhPy8EG_tW_DS_dyn_incl_top.deriv.DAOD_FTAG2.e8549_s4162_r14622_p6285"
# ]

# SamplesDilep.Add("Wjets_mc23a").datasets = [
# 	"mc23_13p6TeV.700777.Sh_2214_Wenu_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285",
# 	"mc23_13p6TeV.700778.Sh_2214_Wenu_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285",
# 	# "mc23_13p6TeV.700779.Sh_2214_Wenu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285",
# 	"mc23_13p6TeV.700780.Sh_2214_Wmunu_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285",
# 	"mc23_13p6TeV.700781.Sh_2214_Wmunu_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285",
# 	# "mc23_13p6TeV.700782.Sh_2214_Wmunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285",
# 	"mc23_13p6TeV.700783.Sh_2214_Wtaunu_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285",
# 	"mc23_13p6TeV.700784.Sh_2214_Wtaunu_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285",
# 	# "mc23_13p6TeV.700785.Sh_2214_Wtaunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285",

#     # Use r15540 for CVetoBVeto (due to muon trigger bug. refer to: https://its.cern.ch/jira/browse/ATLMCPROD-11215)
#     "mc23_13p6TeV.700779.Sh_2214_Wenu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4162_r15540_p6285",
#     "mc23_13p6TeV.700782.Sh_2214_Wmunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4162_r15540_p6285",
#     "mc23_13p6TeV.700785.Sh_2214_Wtaunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4162_r15540_p6285",
# ]

# SamplesDilep.Add("Zjets_mc23a").datasets = [
# 	"mc23_13p6TeV.700786.Sh_2214_Zee_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285",
# 	"mc23_13p6TeV.700787.Sh_2214_Zee_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285",
# 	"mc23_13p6TeV.700789.Sh_2214_Zmumu_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285",
# 	"mc23_13p6TeV.700790.Sh_2214_Zmumu_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285",
# 	"mc23_13p6TeV.700792.Sh_2214_Ztautau_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285",
# 	"mc23_13p6TeV.700793.Sh_2214_Ztautau_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285",
    
#     # Use r15540 for CVetoBVeto (due to muon trigger bug. refer to: https://its.cern.ch/jira/browse/ATLMCPROD-11215)
#     "mc23_13p6TeV.700788.Sh_2214_Zee_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4162_r15540_p6285",
#     "mc23_13p6TeV.700791.Sh_2214_Zmumu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4162_r15540_p6285",
#     "mc23_13p6TeV.700794.Sh_2214_Ztautau_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4162_r15540_p6285",

#     # # Znunu sample is missing in Jackson's list
# 	# "mc23_13p6TeV.700795.Sh_2214_Znunu_pTV2_BFilter.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285",
# 	# "mc23_13p6TeV.700796.Sh_2214_Znunu_pTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285",
# 	# "mc23_13p6TeV.700797.Sh_2214_Znunu_pTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6285",

# 	#"mc23_13p6TeV.700795.Sh_2214_Znunu_pTV2_BFilter.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6026",
# 	#"mc23_13p6TeV.700796.Sh_2214_Znunu_pTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6026",
# 	#"mc23_13p6TeV.700797.Sh_2214_Znunu_pTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6026",
# 	#"mc23_13p6TeV.700895.Sh_2214_Zee_maxHTpTV2_Mll10_40_BFilter.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6026",
# 	#"mc23_13p6TeV.700896.Sh_2214_Zee_maxHTpTV2_Mll10_40_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6026",
# 	#"mc23_13p6TeV.700897.Sh_2214_Zee_maxHTpTV2_Mll10_40_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6026",
# 	#"mc23_13p6TeV.700898.Sh_2214_Zmumu_maxHTpTV2_Mll10_40_BFilter.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6026",
# 	#"mc23_13p6TeV.700899.Sh_2214_Zmumu_maxHTpTV2_Mll10_40_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6026",
# 	#"mc23_13p6TeV.700900.Sh_2214_Zmumu_maxHTpTV2_Mll10_40_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6026",
# 	#"mc23_13p6TeV.700901.Sh_2214_Ztt_maxHTpTV2_Mll10_40_BFilter.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6026",
# 	#"mc23_13p6TeV.700902.Sh_2214_Ztt_maxHTpTV2_Mll10_40_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6026",
# 	#"mc23_13p6TeV.700903.Sh_2214_Ztt_maxHTpTV2_Mll10_40_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p6026",
# ]

# SamplesDilep.Add("Diboson_mc23a").datasets = [
# 	"mc23_13p6TeV.701125.Sh_2214_WlvWqq.deriv.DAOD_FTAG2.e8543_s4162_r14622_p6285",
# 	"mc23_13p6TeV.701085.Sh_2214_ZqqZll.deriv.DAOD_FTAG2.e8543_s4162_r14622_p6285",
# 	"mc23_13p6TeV.701055.Sh_2214_llvv_ss.deriv.DAOD_FTAG2.e8543_s4162_r14622_p6285",
# 	"mc23_13p6TeV.701115.Sh_2214_WlvZqq.deriv.DAOD_FTAG2.e8543_s4162_r14622_p6285",
# 	"mc23_13p6TeV.701040.Sh_2214_llll.deriv.DAOD_FTAG2.e8543_s4162_r14622_p6285",
# 	"mc23_13p6TeV.701060.Sh_2214_lvvv.deriv.DAOD_FTAG2.e8543_s4162_r14622_p6285",
# 	"mc23_13p6TeV.701090.Sh_2214_ZbbZll.deriv.DAOD_FTAG2.e8543_s4162_r14622_p6285",
# 	"mc23_13p6TeV.701105.Sh_2214_WqqZll.deriv.DAOD_FTAG2.e8543_s4162_r14622_p6285",
# 	"mc23_13p6TeV.701120.Sh_2214_WlvZbb.deriv.DAOD_FTAG2.e8543_s4162_r14622_p6285",
# 	"mc23_13p6TeV.701045.Sh_2214_lllv.deriv.DAOD_FTAG2.e8543_s4162_r14622_p6285",
# 	"mc23_13p6TeV.701050.Sh_2214_llvv_os.deriv.DAOD_FTAG2.e8543_s4162_r14622_p6285",
# ]

##################################
##########     MC23d     ##########
##################################

SamplesDilep.Add("ttbar_mc23d").datasets = [
	#add single lepton ttbar
    "mc23_13p6TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.deriv.DAOD_FTAG2.e8514_s4159_r15530_p6285",
    "mc23_13p6TeV.601230.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_FTAG2.e8514_s4159_r15530_p6285",
]

SamplesDilep.Add("ttbar_syst_mc23d").datasets = [
    "mc23_13p6TeV.601415.PhH7EG_A14_ttbar_hdamp258p75_Dilep.deriv.DAOD_FTAG2.e8514_s4159_r15530_p6285",
    "mc23_13p6TeV.601399.PhPy8EG_A14_ttbar_hdamp517p5_dil.deriv.DAOD_FTAG2.e8514_s4159_r15530_p6285",
	"mc23_13p6TeV.601491.PhPy8EG_A14_ttbar_pThard1_dil.deriv.DAOD_FTAG2.e8549_s4159_r15224_p6285"
]

SamplesDilep.Add("Singletop_PowPy8_mc23d").datasets = [
	"mc23_13p6TeV.601348.PhPy8EG_tb_lep_antitop.deriv.DAOD_FTAG2.e8514_s4159_r15224_p6285",
	"mc23_13p6TeV.601349.PhPy8EG_tb_lep_top.deriv.DAOD_FTAG2.e8514_s4159_r15224_p6285",
	"mc23_13p6TeV.601350.PhPy8EG_tqb_lep_antitop.deriv.DAOD_FTAG2.e8514_s4159_r15224_p6285",
	"mc23_13p6TeV.601351.PhPy8EG_tqb_lep_top.deriv.DAOD_FTAG2.e8514_s4159_r15224_p6285",
    "mc23_13p6TeV.601353.PhPy8EG_tW_dyn_DR_dil_antitop.deriv.DAOD_FTAG2.e8551_s4159_r15224_p6285",
    "mc23_13p6TeV.601354.PhPy8EG_tW_dyn_DR_dil_top.deriv.DAOD_FTAG2.e8551_s4159_r15224_p6285"
]

SamplesDilep.Add("Wjets_mc23d").datasets = [
	"mc23_13p6TeV.700777.Sh_2214_Wenu_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8514_s4159_r15224_p6285",
	"mc23_13p6TeV.700778.Sh_2214_Wenu_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4159_r15224_p6285",
	"mc23_13p6TeV.700780.Sh_2214_Wmunu_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8514_s4159_r15224_p6285",
	"mc23_13p6TeV.700781.Sh_2214_Wmunu_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4159_r15224_p6285",
	"mc23_13p6TeV.700783.Sh_2214_Wtaunu_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8514_s4159_r15224_p6285",
	"mc23_13p6TeV.700784.Sh_2214_Wtaunu_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4159_r15224_p6285",

    "mc23_13p6TeV.700779.Sh_2214_Wenu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4159_r15530_p6285",
    "mc23_13p6TeV.700782.Sh_2214_Wmunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4159_r15530_p6285",
    "mc23_13p6TeV.700785.Sh_2214_Wtaunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4159_r15530_p6285",
]

SamplesDilep.Add("Zjets_mc23d").datasets = [
	"mc23_13p6TeV.700786.Sh_2214_Zee_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8514_s4159_r15224_p6285",
	"mc23_13p6TeV.700787.Sh_2214_Zee_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4159_r15224_p6285",
	"mc23_13p6TeV.700789.Sh_2214_Zmumu_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8514_s4159_r15224_p6285",
	"mc23_13p6TeV.700790.Sh_2214_Zmumu_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4159_r15224_p6285",
	"mc23_13p6TeV.700792.Sh_2214_Ztautau_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8514_s4159_r15224_p6285",
	"mc23_13p6TeV.700793.Sh_2214_Ztautau_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4159_r15224_p6285",
    
    "mc23_13p6TeV.700788.Sh_2214_Zee_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4159_r15530_p6285",
    "mc23_13p6TeV.700791.Sh_2214_Zmumu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4159_r15530_p6285",
    "mc23_13p6TeV.700794.Sh_2214_Ztautau_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4159_r15530_p6285",
]

SamplesDilep.Add("Diboson_mc23d").datasets = [
	"mc23_13p6TeV.701125.Sh_2214_WlvWqq.deriv.DAOD_FTAG2.e8543_s4159_r15224_p6285",
	"mc23_13p6TeV.701085.Sh_2214_ZqqZll.deriv.DAOD_FTAG2.e8543_s4159_r15224_p6285",
	"mc23_13p6TeV.701055.Sh_2214_llvv_ss.deriv.DAOD_FTAG2.e8543_s4159_r15224_p6285",
	"mc23_13p6TeV.701115.Sh_2214_WlvZqq.deriv.DAOD_FTAG2.e8543_s4159_r15224_p6285",
	# "mc23_13p6TeV.701040.Sh_2214_llll.deriv.DAOD_FTAG2.e8543_s4159_r15224_p6285",
	# "mc23_13p6TeV.701060.Sh_2214_lvvv.deriv.DAOD_FTAG2.e8543_s4159_r15224_p6285",
	"mc23_13p6TeV.701090.Sh_2214_ZbbZll.deriv.DAOD_FTAG2.e8543_s4159_r15224_p6285",
	"mc23_13p6TeV.701105.Sh_2214_WqqZll.deriv.DAOD_FTAG2.e8543_s4159_r15224_p6285",
	"mc23_13p6TeV.701120.Sh_2214_WlvZbb.deriv.DAOD_FTAG2.e8543_s4159_r15224_p6285",
	# "mc23_13p6TeV.701045.Sh_2214_lllv.deriv.DAOD_FTAG2.e8543_s4159_r15224_p6285",
	# "mc23_13p6TeV.701050.Sh_2214_llvv_os.deriv.DAOD_FTAG2.e8543_s4159_r15224_p6285",
]