# TopCPToolkit nTuple Production

This folder contains the code needed to produce ntuples for b-ject calibration using the TopCPToolkit framework.

# Installation
To install `TopCPToolkit`:
```bash
cd toptoolkit-ntuples/TopCPToolkit/
mkdir -p build run
setupATLAS
cd build
asetup AthAnalysis,25.2.40
cmake ../source/
make -j4
source */setup.sh
```

# Updating
You may occasionally have to resync the submodules after an update. You can run
```
git submodule update --init --recursive
```
When changing branches with `git checkout` or `git switch`, also be sure to use the --recurse-submodule option to keep the submodules current.

# Produce nTuples using TopCPToolkit
* Setup environment using `source setup.sh`.
* Modify the samples to submit which is listed in `samples/Samples_Dilep.py`.
* Submit with `python samples/submitToGrid.py`


