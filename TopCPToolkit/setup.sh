#!/bin/bash
CUR_PATH=$PWD
SCRIPT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
setupATLAS -q

# setup TopCPToolkit
cd $SCRIPT_PATH/toptoolkit-ntuples/TopCPToolkit/build/
asetup --restore
source */setup.sh

# setup grid related
voms-proxy-init -voms atlas
localSetupPandaClient --noAthenaCheck
lsetup pyami

cd $CUR_PATH