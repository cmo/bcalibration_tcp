import ROOT
import os
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
from array import array
import sys
import subprocess
import argparse
import shutil
from mergeJERNP import merge_JERNP_syst_files
from multiprocessing.pool import ThreadPool
sys.path.insert(0, '../FinalSelection/')
from options_file import options
from utils import SystematicsManager


# options.addJERUncertainties(final_fit=False)
outdir=options.output_dir


#do_bootstrap_global=True
do_bootstrap_global=False


def combine_sample_files(data_file,ttbar_sf,ttbar_file,single_top_schan_file,single_top_tchan_file,single_top_tW_file,diboson_file,Zjets_file,Wjets_file,output_file,do_bootstrap=False,do_bootstrap_ttbar_only=False,save_histos_bin_mean=True):
    print("combination for outputfile:",  output_file)
    additional_options = []
    if do_bootstrap:
        if do_bootstrap_ttbar_only:
            additional_options.append("--do_bootstrap_ttbar_only")
        else:
            additional_options.append("--do_bootstrap")
    if save_histos_bin_mean:
        print( "go save_histos_bin_mean")
        additional_options.append("--save_bin_mean")
    if do_bootstrap:
        command=" ".join(["./build/Combine_histograms_for_fit", "--data_file", data_file, "--ttbar_sf", ttbar_sf, "--ttbar_file", ttbar_file, "--single_top_schan_file", single_top_schan_file,"--single_top_tchan_file", single_top_tchan_file,"--single_top_tW_file", single_top_tW_file, "--diboson_file", diboson_file, "--Zjets_file", Zjets_file, "--Wjets_file", Wjets_file, "--output_file", output_file] + additional_options )
        print( "command",command)
    command=" ".join(["./build/Combine_histograms_for_fit", "--data_file", data_file, "--ttbar_sf", ttbar_sf, "--ttbar_file", ttbar_file, "--single_top_schan_file", single_top_schan_file,"--single_top_tchan_file", single_top_tchan_file,"--single_top_tW_file", single_top_tW_file, "--diboson_file", diboson_file, "--Zjets_file", Zjets_file, "--Wjets_file", Wjets_file, "--output_file", output_file] + additional_options )
    print( "command",command)
    p=subprocess.Popen(["./build/Combine_histograms_for_fit", "--data_file", data_file, "--ttbar_sf", ttbar_sf, "--ttbar_file", ttbar_file, "--single_top_schan_file", single_top_schan_file,"--single_top_tchan_file", single_top_tchan_file,"--single_top_tW_file", single_top_tW_file, "--diboson_file", diboson_file, "--Zjets_file", Zjets_file, "--Wjets_file", Wjets_file, "--output_file", output_file] + additional_options) #,stdout=log
    p.wait()

output_path = options.plot_dir+"combination_sys_"+options.nominal_samples['ttbar'].name+"/"
if not os.path.exists(output_path):
    os.makedirs(output_path)

sample_hist_path_format = outdir+"{sample_name}/{sample_name}_{syst}_combination.root"
data_name = sample_hist_path_format.format(sample_name=options.data_name, syst="nominal")
ttb_sf_name = sample_hist_path_format.format(sample_name=options.nominal_samples['ttbar'].name, syst="nominal")


num_of_cores_to_use=1
tp = ThreadPool(num_of_cores_to_use)

syst="nominal"
ouputfile_name=options.plot_dir+"combination_sys_"+options.nominal_samples['ttbar'].name+"/"+"combination_for_fit_"+syst+".root"


print("FLAG #1: combine nominal samples")
tp.apply_async(
    combine_sample_files, (
        data_name,
        ttb_sf_name,
        sample_hist_path_format.format(sample_name=options.nominal_samples['ttbar'].name, syst=syst),
        sample_hist_path_format.format(sample_name=options.nominal_samples['Singletop_schan'].name, syst=syst),
        sample_hist_path_format.format(sample_name=options.nominal_samples['Singletop_tchan'].name, syst=syst),
        sample_hist_path_format.format(sample_name=options.nominal_samples['Singletop_tW'].name, syst=syst),
        sample_hist_path_format.format(sample_name=options.nominal_samples['Diboson'].name, syst=syst),
        sample_hist_path_format.format(sample_name=options.nominal_samples['ZJets'].name, syst=syst),
        sample_hist_path_format.format(sample_name=options.nominal_samples['Wjets'].name, syst=syst),
        ouputfile_name
    )
)


print("FLAG #2")
print("combine branch systematics")
syst_to_run_over = [syst.name_for_histos for syst in SystematicsManager.get_systematics()]

for syst in syst_to_run_over:
    ouputfile_name=options.plot_dir+"combination_sys_"+options.nominal_samples['ttbar'].name+"/"+"combination_for_fit_"+syst+".root"
    tp.apply_async(
        combine_sample_files, (
            data_name,
            ttb_sf_name,
            sample_hist_path_format.format(sample_name=options.nominal_samples['ttbar'].name, syst=syst),
            sample_hist_path_format.format(sample_name=options.nominal_samples['Singletop_schan'].name, syst=syst),
            sample_hist_path_format.format(sample_name=options.nominal_samples['Singletop_tchan'].name, syst=syst),
            sample_hist_path_format.format(sample_name=options.nominal_samples['Singletop_tW'].name, syst=syst),
            sample_hist_path_format.format(sample_name=options.nominal_samples['Diboson'].name, syst=syst),
            sample_hist_path_format.format(sample_name=options.nominal_samples['ZJets'].name, syst=syst),
            sample_hist_path_format.format(sample_name=options.nominal_samples['Wjets'].name, syst=syst),
            ouputfile_name
        )
    )

print("combine alternative ttbar and Singletop_tW samples")
for key, sample in options.alternative_samples.items():
    # decide which sample is systematics aware
    if "ttbar" in sample.name:
        ttbar_sample_name = sample.name
        Singletop_tW_sample_name = options.nominal_samples['Singletop_tW'].name
    elif "Singletop_tW" in sample.name:
        ttbar_sample_name = options.nominal_samples['ttbar'].name
        Singletop_tW_sample_name = sample.name
    output_path = options.plot_dir+"combination_sys_"+options.nominal_samples['ttbar'].name+"/"+"combination_for_fit_"+sample.name+".root"
    tp.apply_async(
        combine_sample_files, (
            data_name,
            ttb_sf_name,
            sample_hist_path_format.format(sample_name=ttbar_sample_name, syst="nominal"),
            sample_hist_path_format.format(sample_name=options.nominal_samples['Singletop_schan'].name, syst="nominal"),
            sample_hist_path_format.format(sample_name=options.nominal_samples['Singletop_tchan'].name, syst="nominal"),
            sample_hist_path_format.format(sample_name=Singletop_tW_sample_name, syst="nominal"),
            sample_hist_path_format.format(sample_name=options.nominal_samples['Diboson'].name, syst="nominal"),
            sample_hist_path_format.format(sample_name=options.nominal_samples['ZJets'].name, syst="nominal"),
            sample_hist_path_format.format(sample_name=options.nominal_samples['Wjets'].name, syst="nominal"),
            output_path
        )
    )
    # data_file,ttbar_sf,ttbar_file,single_top_schan_file,single_top_tchan_file,single_top_tW_file,diboson_file,Zjets_file,Wjets_file,output_file

print("combine weight systematics")
for sample_name, systs in SystematicsManager.get_mc_weight_systematics().items():
    print(f"running over {sample_name}")
    is_ttbar_syst = "ttbar" in sample_name
    is_Singletop_tW_syst = "Singletop_tW" in sample_name
    if not is_ttbar_syst and not is_Singletop_tW_syst:
        print(f"Erorr: sample {sample_name} is not ttbar or Singletop_tW")
        continue

    for syst in systs:
        ouputfile_name=options.plot_dir+"combination_sys_"+options.nominal_samples['ttbar'].name+"/"+"combination_for_fit_"+syst.name_for_histos+".root"
        tp.apply_async(
            combine_sample_files, (
                data_name,
                ttb_sf_name,
                sample_hist_path_format.format(sample_name=options.nominal_samples['ttbar'].name, syst=syst.name_for_histos if is_ttbar_syst else "nominal"),
                sample_hist_path_format.format(sample_name=options.nominal_samples['Singletop_schan'].name, syst="nominal"),
                sample_hist_path_format.format(sample_name=options.nominal_samples['Singletop_tchan'].name, syst="nominal"),
                sample_hist_path_format.format(sample_name=options.nominal_samples['Singletop_tW'].name, syst=syst.name_for_histos if is_Singletop_tW_syst else "nominal"),
                sample_hist_path_format.format(sample_name=options.nominal_samples['Diboson'].name, syst="nominal"),
                sample_hist_path_format.format(sample_name=options.nominal_samples['ZJets'].name, syst="nominal"),
                sample_hist_path_format.format(sample_name=options.nominal_samples['Wjets'].name, syst="nominal"),
                ouputfile_name
            )
        )


print("FLAG #3")
tp.close()
print("FLAG #4")
tp.join()
print("FLAG #5")
print("Merge JER NP syst files")
list_jernp = [
    syst.replace("_PseudoData", "") for syst in syst_to_run_over if ("JET_JER" in syst and "PseudoData" in syst)
]
merge_JERNP_syst_files(options.plot_dir+"combination_sys_"+options.nominal_samples['ttbar'].name+"/", list_jernp)
print("FLAG #6")
print("Ende gut alles gut! ")
