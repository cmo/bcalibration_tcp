#! /usr/bin/python

from ROOT import TFile, THnSparseD, TH2D
import sys
sys.path.insert(0, '../FinalSelection/')
from options_file import options
from utils import SystematicsManager


# hist_NP1_up = hist_nominal + (hist_NP1_up_MC - hist_NP1_up_PseudoData)
#
# for each up and down variation and all NP that exist. 

# path to the folder where all your input for the fitting part lives


def merge_JERNP_syst_files(FolderPath, ListJERNP):
    for JERNP in ListJERNP:
        input_nom = FolderPath+"/combination_for_fit_nominal.root"
        input_MC  = FolderPath+"/combination_for_fit_"+JERNP+".root"
        input_PD = input_MC.replace("__1down", "_PseudoData__1down").replace("__1up", "_PseudoData__1up")
        # input_PD  = FolderPath+"/combination_for_fit_"+JERNP+"_PseudoData.root"
        output    = FolderPath+"/combination_for_fit_"+JERNP+"_final.root"
        
        file_nom  = TFile(input_nom, "READ")
        file_MC   = TFile(input_MC,  "READ")
        file_PD   = TFile(input_PD,  "READ")
        file_out  = TFile(output,    "RECREATE")
        print(f"Combining {input_nom} and {input_MC} and {input_PD} into {output}")

        for obj in file_nom.GetListOfKeys():
            if "THnSparse" in obj.GetClassName():
                obj_nom    = THnSparseD()
                obj_MC     = THnSparseD()
                obj_PD     = THnSparseD()
                obj_interm = THnSparseD()
                
                obj_nom.Write()
            elif "TH2D" in obj.GetClassName():
                obj_nom = TH2D()
                obj_MC  = TH2D()
                obj_PD  = TH2D()
                obj_interm = TH2D()
            else:
                obj_nom.Write()
                continue

            obj_nom = file_nom.Get(obj.GetName())
            obj_MC  = file_MC.Get(obj.GetName())
            obj_PD  = file_PD.Get(obj.GetName())

            obj_interm = obj_MC.Clone()
            obj_interm.Add(obj_PD, -1.0)
            obj_nom.Add(obj_interm, 1.0)

            obj_nom.Write()

        file_out.Close()


if __name__=='__main__':
    FolderPath=options.plot_dir+"combination_sys_"+options.nominal_samples['ttbar'].name+"/"

    full_syst_list = [syst.name_for_histos for syst in SystematicsManager.get_systematics()]
    ListJERNP = [
        syst.replace("_PseudoData", "") for syst in full_syst_list if ("JET_JER" in syst and "_PseudoData" in syst)
    ]

    merge_JERNP_syst_files(FolderPath, ListJERNP)
