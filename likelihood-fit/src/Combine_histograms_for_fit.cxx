#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <iomanip>
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TList.h"
#include <THnSparse.h>
#include "TFile.h"
#include <string>


#include "cxxopts.hpp"


using namespace std;

std::vector<std::string>  taggers={"GN2v01"};
std::vector<std::string>  workingpoints={"FixedCutBEff"}; //,"HybBEff"};
//std::vector<std::string>  channels={"emu_OS_J2"};
std::vector<std::string>  channels={"emu_OS_J2","emu_OS_J2_m_lj_cut"};
std::vector<std::string>  flavours={"bb","bc","bl","cb","cc","cl","lb","lc", "ll"};

int combine_files_fo_fits(std::string s_data, std::string s_ttbar_sf, std::string s_ttbar, std::string s_single_top_schan, std::string s_single_top_tchan, std::string s_single_top_tW, std::string s_diboson, std::string s_Zjets, std::string s_Wjets, std::string s_output, bool do_bootstrap=false,bool do_bootstrap_ttbar_only=false, bool save_bin_mean=false){
    // creatin outpufile:
    TFile *f_output = new TFile(s_output.c_str(), "RECREATE");
    std::cerr << "Output file: " << s_output << std::endl;

    TFile *f_data = new TFile((s_data).c_str(), "READ");
    if (f_data->IsZombie()) {
        std::cerr << "Input file: " << s_data << " not found ... exiting." << std::endl;
        return -1;
    }
    TFile *f_ttbar_sf = new TFile((s_ttbar_sf).c_str(), "READ");
    if (f_ttbar_sf->IsZombie()) {
        std::cerr << "Input file: " << s_ttbar_sf << " not found ... exiting." << std::endl;
        return -1;
    }
    TFile *f_ttbar = new TFile((s_ttbar).c_str(), "READ");
    if (f_ttbar->IsZombie()) {
        std::cerr << "Input file: " << s_ttbar << " not found ... exiting." << std::endl;
        return -1;
    }
    std::vector<TFile *> v_f_samples_to_combine={f_ttbar};
    std::vector<std::string> v_str_other_to_combine= {
                                                        s_single_top_schan,
                                                        s_single_top_tchan,
                                                        s_single_top_tW,
                                                        s_diboson,
                                                        s_Zjets,
                                                        s_Wjets,
                                                    };
    for (const auto &s_name : v_str_other_to_combine){
        TFile *f_this = new TFile((s_name).c_str(), "READ");
        std::cout << "loading: "<<s_name<<std::endl;
        if (f_this->IsZombie()) {
            std::cerr << "Input file: " << s_name << " not found ... exiting." << std::endl;
            // return -1;
            continue;
        }
        v_f_samples_to_combine.push_back(f_this);
    }
    
    // just no bootstrap name for the nominal version
    std::vector<std::string> v_boot_strap_name= {""};
    if (do_bootstrap){
        std::cout << "combining with bootstrapweights - this will take a while."<< std::endl;
        for (int i_boot=0;i_boot<100;i_boot++){
            v_boot_strap_name.push_back("_boot_strap_"+to_string(i_boot));
        }
        if (do_bootstrap_ttbar_only) std::cerr << "combining with bootstrapweights ttbar-only!!- not correct, but better then nothing!"<< std::endl;
    }
    for (const auto &channel : channels){
        bool save_ncr=false;
        if (channel=="emu_OS_J2")
            save_ncr=true;
        for (const auto &tagger : taggers){
            for (const auto &workingpoint : workingpoints){
                std::string combined_tagger_name=channel+"_hist_for_fit_"+tagger+"_"+workingpoint;
                std::cerr << "combining "<<combined_tagger_name<< std::endl;
                THnSparseD* data_sr_hist= (THnSparseD*)f_data->Get((channel+"/hf4_"+combined_tagger_name+"_data").c_str())->Clone((std::string("hf4_")+"data_"+combined_tagger_name).c_str());
                f_output->cd();
                data_sr_hist->Write();
                if (save_ncr){
                    THnSparseD* data_Ncr_hist= (THnSparseD*)f_data->Get((channel+"/hf4_Ncr_"+combined_tagger_name+"_data").c_str())->Clone((std::string("hf4_Ncr_")+"data_"+combined_tagger_name).c_str());
                    f_output->cd();
                    data_Ncr_hist->Write();
                }
                std::cerr << "saved data. "<< std::endl;
                TH2D* hff_l=0;
                TH2D* hff_b=0;
                for (const auto &f_sample : v_f_samples_to_combine){
                    if (!hff_l){
                        hff_l=(TH2D*)((TH2D*)f_sample->Get((channel+"/hff_"+combined_tagger_name+"_light").c_str()))->Clone((string("hff_")+"MC_combined_"+combined_tagger_name+"_light").c_str());
                        hff_b=(TH2D*)((TH2D*)f_sample->Get((channel+"/hff_"+combined_tagger_name+"_b").c_str()))->Clone((string("hff_")+"MC_combined_"+combined_tagger_name+"_b").c_str());
                        std::cerr << "loaded  hffl. "<< std::endl;
                    }
                    else{
                        hff_l->Add((TH2D*)f_sample->Get((channel+"/hff_"+combined_tagger_name+"_light").c_str()));
                        hff_b->Add((TH2D*)f_sample->Get((channel+"/hff_"+combined_tagger_name+"_b").c_str()));
                    }
                }
                f_output->cd();
                hff_l->Write();
                hff_b->Write();
                TH2D* hff_l_ttb_nominal=(TH2D*)((TH2D*)f_ttbar_sf->Get((channel+"/hff_"+combined_tagger_name+"_light").c_str()))->Clone((string("hff_")+"MC_ttb_nominal_"+combined_tagger_name+"_light").c_str());
                TH2D* hff_b_ttb_nominal=(TH2D*)((TH2D*)f_ttbar_sf->Get((channel+"/hff_"+combined_tagger_name+"_b").c_str()))->Clone((string("hff_")+"MC_ttb_nominal_"+combined_tagger_name+"_b").c_str());
                TH2D* hff_l_ttb=(TH2D*)((TH2D*)f_ttbar->Get((channel+"/hff_"+combined_tagger_name+"_light").c_str()))->Clone((string("hff_")+"MC_ttb_"+combined_tagger_name+"_light").c_str());
                TH2D* hff_b_ttb=(TH2D*)((TH2D*)f_ttbar->Get((channel+"/hff_"+combined_tagger_name+"_b").c_str()))->Clone((string("hff_")+"MC_ttb_"+combined_tagger_name+"_b").c_str());
                f_output->cd();
                hff_l_ttb_nominal->Write();
                hff_b_ttb_nominal->Write();
                hff_l_ttb->Write();
                hff_b_ttb->Write();
                if (save_bin_mean)
                {
                    cout<< "saving bin mean:"<<endl;
                    TDirectory* bin_mean_dir=f_output->mkdir((combined_tagger_name + "_bin_mean").c_str());
                    string path= channel+"/"+combined_tagger_name +"_bin_mean";
                    std::cout << path.c_str() << std::endl;
                    TDirectory* dir =(TDirectory*) f_ttbar->Get(path.c_str());
                    //dir->ls();
                    TList* keys =  dir->GetListOfKeys();
                    for(const auto&& obj: *keys)
                    {
                        std::cout << (path+"/"+obj->GetName()).c_str() << std::endl;
                        bin_mean_dir->cd();
                        THnSparseD* h_pt_sum = (THnSparseD*) f_ttbar->Get((path+"/"+obj->GetName()).c_str())->Clone(obj->GetName());
                        h_pt_sum->Reset();
                        for (const auto &f_sample : v_f_samples_to_combine)
                        {
                            h_pt_sum->Add( (THnSparseD*) f_sample->Get((path+"/"+obj->GetName()).c_str())); 
                        }
                        h_pt_sum->Write();
                    }
                }
                f_output->cd();
                for (const auto &boot_strap_name : v_boot_strap_name){
                    std::map< std::string, THnSparseD* > mc_sr_h_per_flav;
                    std::map< std::string, THnSparseD* > mc_Ncr_h_per_flav;
                    for (const auto &flav : flavours){
                        mc_sr_h_per_flav[flav]=0;
                        mc_Ncr_h_per_flav[flav]=0;
                    }
                    for (const auto &f_sample : v_f_samples_to_combine){
                        std::string boot_name_help=boot_strap_name;
                        if (do_bootstrap_ttbar_only && (f_sample !=f_ttbar)){
                            boot_name_help="";
                        }
                        for (const auto &flav : flavours){
                            if (mc_sr_h_per_flav[flav]==0){
                                mc_sr_h_per_flav[flav]= (THnSparseD*)f_sample->Get((channel+"/hf4_"+combined_tagger_name+"_"+flav+boot_name_help).c_str())->Clone((std::string("hf4_")+"MC_combined_"+combined_tagger_name+"_"+flav+boot_strap_name).c_str());
                                if(save_ncr){
                                    mc_Ncr_h_per_flav[flav]= (THnSparseD*)f_sample->Get((channel+"/hf4_Ncr_"+combined_tagger_name+"_"+flav+boot_name_help).c_str())->Clone((std::string("hf4_Ncr_")+"MC_combined_"+combined_tagger_name+"_"+flav+boot_strap_name).c_str());
                                }
                            }
                            else{
                                mc_sr_h_per_flav[flav]->Add((THnSparseD*) f_sample->Get((channel+"/hf4_"+combined_tagger_name+"_"+flav+boot_name_help).c_str()));
                                if(save_ncr){
                                    mc_Ncr_h_per_flav[flav]->Add((THnSparseD*) f_sample->Get((channel+"/hf4_Ncr_"+combined_tagger_name+"_"+flav+boot_name_help).c_str()));
                                }
                            }
                        }
                    }
                    //get rid of c
                    f_output->cd();
                    std::vector< std::map < std::string, THnSparseD* > > v_maps_to_get_rid_of_c{mc_sr_h_per_flav};
                    std::map < std::string, THnSparseD* > h_map= mc_sr_h_per_flav;

                    h_map["bb"]->Write();
                    h_map["bl"]->Add(h_map["bc"]);
                    h_map["bl"]->Write();
                    h_map["ll"]->Add(h_map["cc"]);
                    h_map["ll"]->Add(h_map["cl"]);
                    h_map["ll"]->Add(h_map["lc"]);
                    h_map["ll"]->Write();
                    h_map["lb"]->Add(h_map["cb"]);
                    h_map["lb"]->Write();
                    if (save_ncr){
                        h_map=mc_Ncr_h_per_flav;
                        h_map["bb"]->Write();
                        h_map["bl"]->Add(h_map["bc"]);
                        h_map["bl"]->Write();
                        h_map["ll"]->Add(h_map["cc"]);
                        h_map["ll"]->Add(h_map["cl"]);
                        h_map["ll"]->Add(h_map["lc"]);
                        h_map["ll"]->Write();
                        h_map["lb"]->Add(h_map["cb"]);
                        h_map["lb"]->Write();
                    }

                }
            }
        }
    }
    std::cerr << "closing files: "<< std::endl;
    for (const auto &f_sample : v_f_samples_to_combine){
        f_sample->Close();
    }
    f_ttbar_sf->Close();
    f_data->Close();
    f_output->Close();
    std::cerr << "all closed.: "<< std::endl;

    return 0;
}

int main (int argc, char *argv[])
{
        std::string  s_data, s_ttbar_sf, s_ttbar, s_single_top_schan, s_single_top_tchan, s_single_top_tW, s_diboson, s_Zjets, s_Wjets, s_output ;
        bool do_bootstrap=false;
        bool do_bootstrap_ttbar_only=false;
        bool save_bin_mean = false;
        try
        {
                cxxopts::Options options(argv[0], " - example command line options");
                options.positional_help("[input output]");
                options.add_options()
                        ("ttbar_sf", "Nominal ttbar file for sf computation", cxxopts::value<std::string>(),"file")
                        ("ttbar_file", "ttbar MC file used for this variation", cxxopts::value<std::string>(),"file")
                        ("single_top_schan_file", "single_top MC file used for this variation", cxxopts::value<std::string>(),"file")
                        ("single_top_tchan_file", "single_top MC file used for this variation", cxxopts::value<std::string>(),"file")
                        ("single_top_tW_file", "single_top MC file used for this variation", cxxopts::value<std::string>(),"file")
                        ("diboson_file", "diboson MC file used for this variation", cxxopts::value<std::string>(),"file")
                        ("Zjets_file", "Zjets MC file used for this variation", cxxopts::value<std::string>(),"file")
                        ("Wjets_file", "Wjets MC file used for this variation", cxxopts::value<std::string>(),"file")
                        ("output_file", "output_file name", cxxopts::value<std::string>(),"file")
                        ("data_file", "data_file", cxxopts::value<std::string>(),"file")
                        ("save_bin_mean", "save histos for bin mean calculation", cxxopts::value<bool>(save_bin_mean))
                        ("do_bootstrap", "combine with bootstrap weights", cxxopts::value<bool>(do_bootstrap))
                        ("do_bootstrap_ttbar_only", "combine with bootstrap weights - user only bootstrap from ttbar - not correct, but better then nothing!", cxxopts::value<bool>(do_bootstrap_ttbar_only))
                        ("h, help", "Print help")
                        ;

                        options.parse(argc, argv);

                if (options.count("help"))
                {
                        std::cout << options.help({""}) << std::endl;
                        exit(0);
                }
                s_ttbar_sf=options["ttbar_sf"].as<std::string>();
                s_ttbar=options["ttbar_file"].as<std::string>();
                s_single_top_schan=options["single_top_schan_file"].as<std::string>();
                s_single_top_tchan=options["single_top_tchan_file"].as<std::string>();
                s_single_top_tW=options["single_top_tW_file"].as<std::string>();
                s_diboson=options["diboson_file"].as<std::string>();
                s_Zjets=options["Zjets_file"].as<std::string>();
                s_Wjets=options["Wjets_file"].as<std::string>();
                s_output=options["output_file"].as<std::string>();
                s_data=options["data_file"].as<std::string>();

        } catch (const cxxopts::OptionException& e)
        {
                std::cout << "error parsing options: " << e.what() << std::endl;
                exit(1);
        }
        if (do_bootstrap_ttbar_only)
            do_bootstrap=true;
        combine_files_fo_fits(s_data, s_ttbar_sf, s_ttbar, s_single_top_schan, s_single_top_tchan, s_single_top_tW, s_diboson, s_Zjets, s_Wjets, s_output, do_bootstrap, do_bootstrap_ttbar_only, save_bin_mean);

}
