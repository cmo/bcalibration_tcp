#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <iomanip>
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include <THnSparse.h>
#include "TFile.h"
#include "TChain.h"
#include <string>
#include "TString.h"
#include "TObjString.h"
#include "RooGlobalFunc.h"
#include "RooGaussian.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooSimultaneous.h"
#include "RooParametricStepFunction.h"
#include "RooConstVar.h"
#include "RooWorkspace.h"
#include "RooCategory.h"
#include "RooUniform.h"
#include "RooExtendPdf.h"
#include "RooAddPdf.h"
#include "RooBinning.h"
#include "RooProdPdf.h"
#include "TRandom3.h"
#include "RooFit.h"
#include "RooArgList.h"
#include "RooFitResult.h"
#include "cxxopts.hpp"
#include "Workspace_plottingMacro_3SB.h"
#include "RooFormulaVar.h"
using namespace std;
using namespace RooFit;
TH3D* sparse_to_3d_sr_cr(std::string name, THnSparseD* h_sparse){
    double N_reg, unc_Nreg;
    int n_pt_bins=h_sparse->GetAxis(2)->GetNbins();
    const Double_t* pt_bins=h_sparse->GetAxis(2)->GetXbins()->GetArray ();
    const Double_t sr_cr_bins[5] = { 0., 1., 2., 3., 4.};
    typedef std::tuple<int,int> i2tuple;
    std::vector<i2tuple> reg_index;
    reg_index.push_back(i2tuple(1,1)); //sr
    reg_index.push_back(i2tuple(2,1)); //hl
    reg_index.push_back(i2tuple(1,2)); //lh
    reg_index.push_back(i2tuple(2,2)); //hh
    std::vector<std::string> cr_names={"SR","CR_HL","CR_LH","CR_HH"};
    TH3D* h_result = new TH3D(name.c_str(),name.c_str(),n_pt_bins,pt_bins,n_pt_bins,pt_bins,4,sr_cr_bins);
    TAxis * z_a_res= h_result ->GetZaxis ();
    for (int i=0;i<cr_names.size();i++ ){
        z_a_res->SetBinLabel(i+1,cr_names.at(i).c_str());
    }
    for (int pt_jet1=1; pt_jet1<=n_pt_bins; pt_jet1++) {
        for (int pt_jet2=1; pt_jet2<=n_pt_bins; pt_jet2++) {
            if (pt_jet2>pt_jet1)
                continue;
            for (int cr_i=0;cr_i<4;cr_i++){
                int cr_reg_bin[4] = {std::get<0>(reg_index.at(cr_i)),std::get<1>(reg_index.at(cr_i)),pt_jet1,pt_jet2};
                N_reg=h_sparse->GetBinContent(cr_reg_bin);
                if (N_reg>=0){
                    unc_Nreg=h_sparse->GetBinError(cr_reg_bin);
                }
                else{
                    std::cout<<"WARNING!!: NEGATIVE N_events IN "<< name << " p1 "<<pt_jet1<<" p2 "<<pt_jet2<<" "<< cr_names.at(cr_i)<< " !!! Nevents: "<<N_reg<<" ; setting to 0!"<<std::endl;
                    N_reg=0;
                    unc_Nreg=0;
                }
                h_result->SetBinContent(pt_jet1,pt_jet2,cr_i+1,N_reg);
                h_result->SetBinError(pt_jet1,pt_jet2,cr_i+1,unc_Nreg);
            }
        }
    }
    return h_result;
}
void custom_print_p_xx_matrix(RooWorkspace* w,int n_pt_bins,string name="p_bb",RooFitResult* fitResult=NULL){
        const int numWidth      = 12;
        const char separator    = ' ';
        if (fitResult) {
            cout<<name<<"(pt1,pt2) uncertanty postfit:"<<endl;
        }else{
            cout<<name<<"(pt1,pt2) used in fit:"<<endl;
        }
        cout << left << setw(numWidth) << setfill(separator) << "pt"<<',';
        for (int pt_jet1=1; pt_jet1<=n_pt_bins; pt_jet1++) {
                cout << left << setw(numWidth) << setfill(separator) << pt_jet1<<',';
        }
        cout << endl;
        for (int pt_jet1=1; pt_jet1<=n_pt_bins; pt_jet1++) {
                cout << left << setw(numWidth) << setfill(separator) << pt_jet1<<',';
                for (int pt_jet2=1; pt_jet2<=n_pt_bins; pt_jet2++) {
                        //be carefull with this! we only estimate the half of the matrix, since jets are pt ordered!
                        if (pt_jet2>pt_jet1) {
                                cout << left << setw(numWidth) << setfill(separator) << "-" <<',';
                                continue;
                        }
                        string suffix_pt12 = "pt_";
                        suffix_pt12 = suffix_pt12 + to_string(pt_jet1)+"_"+to_string(pt_jet2);
                        RooAbsReal * p_bb_bin = w->function((name+"_"+suffix_pt12).c_str());
                        if (fitResult) {
                            cout << left << setw(numWidth) << setfill(separator) << p_bb_bin->getPropagatedError(*fitResult) <<',';
                        }else{
                            cout << left << setw(numWidth) << setfill(separator) << p_bb_bin->getValV() <<',';
                        }
                }
                cout << endl;
        }
        cout<<"**************************************************************************************************************************"<<endl;
}
void custom_print_workspace(RooWorkspace* w,int n_pt_bins,int n_tagger_bins,THnSparseD* hf4_SR_data_tag,RooFitResult* fitResult=NULL){
        const int numWidth      = 12;
        const char separator    = ' ';
        std::vector<std::string> flavours={"bb","bl","lb","ll"};
        std::vector<std::string> cr_names={"SR","CR_HL","CR_LH","CR_HH"};
        for (int tagger_bin=1; tagger_bin<=n_tagger_bins; tagger_bin++) {
                double lower_tagger_bound,upper_tagger_bound,lower_pt_bound,upper_pt_bound;
                cout << left << setw(numWidth) << setfill(separator) << "tagger_bin"<<',';
                cout << left << setw(numWidth) << setfill(separator) << "pt_bin"<<',';
                cout << left << setw(numWidth) << setfill(separator) << "lower_pt"<<',';
                cout << left << setw(numWidth) << setfill(separator) << "upper_pt"<<',';
                cout << left << setw(numWidth) << setfill(separator) << "p_b"<<',';
                cout << left << setw(numWidth) << setfill(separator) << "p_b_error"<<',';
                cout << left << setw(numWidth) << setfill(separator) << "e_b"<<',';
                cout << left << setw(numWidth) << setfill(separator) << "e_b_error"<<',';
                cout << left << setw(numWidth) << setfill(separator) << "sf_b"<<',';
                cout << left << setw(numWidth) << setfill(separator) << "sf_b_error"<<',';
                cout << left << setw(numWidth) << setfill(separator) << "p_l"<<',';
                cout << endl;
                // Loop on tagger bins
                for (int pt_bin=1; pt_bin<=n_pt_bins; pt_bin++) {
                        string suffix_pt = "pt_";
                        suffix_pt = suffix_pt + to_string(pt_bin);
                        string suffix_mv = suffix_pt+"_tagger_";
                        suffix_mv = suffix_mv + to_string(tagger_bin);
                        lower_pt_bound=hf4_SR_data_tag->GetAxis(3)->GetBinLowEdge ( pt_bin);
                        upper_pt_bound=hf4_SR_data_tag->GetAxis(3)->GetBinUpEdge ( pt_bin);
                        cout << left << setw(numWidth) << setfill(separator) << tagger_bin <<',';
                        cout << left << setw(numWidth) << setfill(separator) << pt_bin<<',';
                        cout << left << setw(numWidth) << setfill(separator) << lower_pt_bound<<',';
                        cout << left << setw(numWidth) << setfill(separator) << upper_pt_bound<<',';
                        RooAbsReal * p_b_bin = w->function(("p_b_"+suffix_mv).c_str());
                        cout << left << setw(numWidth) << setfill(separator) << p_b_bin->getValV() <<',';
                        if (fitResult) {cout << left << setw(numWidth) << setfill(separator) << w->function(("p_b_"+suffix_mv).c_str())->getPropagatedError(*fitResult)<<','; }
                        else {cout << left << setw(numWidth) << setfill(separator) << "-" <<','; }
                        cout << left << setw(numWidth) << setfill(separator) <<  w->function(("e_b_"+suffix_mv).c_str())->getValV() <<',';
                        if (fitResult) {cout << left << setw(numWidth) << setfill(separator) << w->function(("e_b_"+suffix_mv).c_str())->getPropagatedError(*fitResult)<<','; }
                        else {cout << left << setw(numWidth) << setfill(separator) << "-" <<','; }
                        cout << left << setw(numWidth) << setfill(separator) <<  w->function(("sf_b_"+suffix_mv).c_str())->getValV() <<',';
                        if (fitResult) {cout << left << setw(numWidth) << setfill(separator) << w->function(("sf_b_"+suffix_mv).c_str())->getPropagatedError(*fitResult)<<','; }
                        else {cout << left << setw(numWidth) << setfill(separator) << "-" <<','; }
                        cout << left << setw(numWidth) << setfill(separator) <<  w->function(("p_l_"+suffix_mv).c_str())->getValV() <<',';
                        cout << endl;
                }
                cout << endl;
        }
        cout<<"**************************************************************************************************************************"<<endl;
        cout<<"templates:"<<endl;
        cout << left << setw(numWidth) << setfill(separator) << "pt_1_bin"<<',';
        cout << left << setw(numWidth) << setfill(separator) << "pt_2_bin"<<',';
        cout << left << setw(numWidth) << setfill(separator) << "flavor"<<',';
        for (const auto &reg : cr_names) {
            cout << left << setw(numWidth) << setfill(separator) << reg<<',';
        }
        cout << endl;
        for (int pt_jet1=1; pt_jet1<=n_pt_bins; pt_jet1++) {

                for (int pt_jet2=1; pt_jet2<=n_pt_bins; pt_jet2++) {
                        if (pt_jet2>pt_jet1) {
                                continue;
                        }
                        string suffix_pt12 = "pt_";
                        suffix_pt12 = suffix_pt12 + to_string(pt_jet1)+"_"+to_string(pt_jet2);
                        for (const auto &flav : flavours) {
                            cout << left << setw(numWidth) << setfill(separator) << pt_jet1<<',';
                            cout << left << setw(numWidth) << setfill(separator) << pt_jet2<<',';
                            cout << left << setw(numWidth) << setfill(separator) << flav<<',';
                            for (const auto &reg : cr_names) {
                                //p_lb_pt_2_1_CR_HH
                                RooAbsReal * p_flav_pt_reg = w->function(("N_MC_"+flav+"_"+reg+"_"+suffix_pt12).c_str());
                                cout << left << setw(numWidth) << setfill(separator) << p_flav_pt_reg->getValV() <<',';
                            }
                            cout << endl;
                        }
                }

        }
        cout<<"**************************************************************************************************************************"<<endl;
        for (const auto &flav : flavours) {
            custom_print_p_xx_matrix(w,n_pt_bins,"sf_"+flav);
            if (fitResult){custom_print_p_xx_matrix(w,n_pt_bins,"sf_"+flav,fitResult);}
        }
}

int main (int argc, char *argv[])
{
        string inputfile_name, out_dir,outputfile_name, config_string_name, dataName, rlTag, taggerName, workingPoint, systName, channelName, fitConfig, boot_strap_name, lumi_label, MC_label;
        bool closure_test = false;
        bool do_plots=true;
        int closure_test_seed = -1;
        bool varry_fs=false; //set to true if you want to let f_bl,f_bb,f_ll and f_lb float.
        bool NConst=false;
	bool pl_up=false;
        bool fix_p_l=true;
        bool use_Minos=false;
        bool fit_CR_only=false;
        bool fit_SR_as_CR=false;
        bool allow_neg_sf_xx=false;
        int reduce_N_pt_bins=0;
        int boot_strap_weight=-1;
        double sf_xx_min_val=0;
        try
        {
                cxxopts::Options options(argv[0], " - example command line options");
                options.positional_help("[input output]");
                options.add_options()
                        ("i,input", "Input", cxxopts::value<std::string>(),"input_file")
                        ("o,output", "Final Outputfile_name will be added together: out_dir/Workspaces/Workspace _ rlTag _ dataName _ taggerName _ workingPoint _ channelName  _ fitConfig _ systName ", cxxopts::value<std::string>()
                        ->default_value("FitResults"), "out_dir") //name
                        ("r,rlTag", "rlTag", cxxopts::value<std::string>()->default_value("r21"), "rlTag")
                        ("d,dataName", "dataName", cxxopts::value<std::string>()->default_value("d1516"), "dataName")
                        ("t,tagger", "taggerName", cxxopts::value<std::string>()->default_value("GN2v01"), "taggerName")
                        ("w,workingPoint", "Name of the Working point: FixedCutBEff, HybBEff", cxxopts::value<std::string>()->default_value("FixedCutBEff"),"name" ) //
                        ("s,syst", "name of systematics", cxxopts::value<std::string>()->default_value("nominal"), "systName")
                        ("l,lumi_label", "lumi_label", cxxopts::value<std::string>(lumi_label)->default_value("36.1"), "lumi_label")
                        ("m, MC_label", "MC_label", cxxopts::value<std::string>(MC_label)->default_value("mc16a"), "MC_label")
                        ("c,channel", "name of channel to fit", cxxopts::value<std::string>()->default_value("emu_OS_J2"), "channelName")
                        ("closure_test", "take mc as data", cxxopts::value<bool>(closure_test))
                        ("closure_test_seed", "set seed for closure_test random generator or -2 for nstat", cxxopts::value<int>(), "seed")
                        ("boot_strap", "run over a boot strap weight", cxxopts::value<int>(), "weight")
                        ("reduce_N_pt_bins", "set number of pT bins to reduce ", cxxopts::value<int>(), "n_pt")
                                ("use_Minos", "use Minos to scan the likelihood. Takes ages!", cxxopts::value<bool>(use_Minos))
                                ("pl_up", "simulate mistagrate of light up by factor 2", cxxopts::value<bool>(pl_up))
                                ("varry_fs", "varry the f_xx fractions", cxxopts::value<bool>(varry_fs))
                                ("fit_CR_only", "fit only conrtol regions", cxxopts::value<bool>(fit_CR_only))
                                ("fit_SR_as_CR", "fit SR with same dump model as the CR regions. Should give same results as 3_sideband fit.", cxxopts::value<bool>(fit_SR_as_CR))
                                ("NConst", "differnet normalisation of f_ll(p1,p2) fractions. no N(p1,p2) but one single N_tot for all pt Bins.", cxxopts::value<bool>(NConst))
                                ("allow_neg_sf_xx", "allow negative values for sf_xx. sometimes needed for correct uncertanty estimation.", cxxopts::value<bool>(allow_neg_sf_xx))
                                ("h, help", "Print help")
                                ;

                                options.parse(argc, argv);

                if (options.count("help"))
                {
                        std::cout << options.help({""}) << std::endl;
                        exit(0);
                }
                if (options.count("input"))
                {
                        std::cout << "Input = " << options["input"].as<std::string>()<< std::endl;
                        inputfile_name=options["input"].as<std::string>();
                }
                else {
                        std::cout << "please give input file!"<< std::endl;
                        std::cout << options.help({""}) << std::endl;
                        exit(0);
                }
                std::cout << "Output = " << options["output"].as<std::string>()<< std::endl;
                out_dir=options["output"].as<std::string>();
                std::cout << "tagger_name = " << options["tagger"].as<std::string>()<< std::endl;
                taggerName=options["tagger"].as<std::string>();
                std::cout << "workingPoint = " << options["workingPoint"].as<std::string>()<< std::endl;
                workingPoint=options["workingPoint"].as<std::string>();
                rlTag=options["rlTag"].as<std::string>();
                dataName=options["dataName"].as<std::string>();
                systName=options["syst"].as<std::string>();
                channelName=options["channel"].as<std::string>();
                if (options.count("reduce_N_pt_bins"))
                {

                        reduce_N_pt_bins=options["reduce_N_pt_bins"].as<int>();
                        cout<< "got n_pt seed "<<reduce_N_pt_bins <<endl;
                }
                if (options.count("closure_test_seed"))
                {

                        closure_test_seed=options["closure_test_seed"].as<int>();
                        cout<< "got cl seed "<<closure_test_seed <<endl;
                        closure_test=true;
                }
                if (options.count("boot_strap"))
                {

                        boot_strap_weight=options["boot_strap"].as<int>();
                        cout<< "got boot_strap"<<boot_strap_weight <<endl;
                }
        } catch (const cxxopts::OptionException& e)
        {
                std::cout << "error parsing options: " << e.what() << std::endl;
                exit(1);
        }
        fitConfig="";
        if (use_Minos) {
                fitConfig=fitConfig+"_Minos";
        }
        if (reduce_N_pt_bins) {
                fitConfig=fitConfig+"_n_pT_"+to_string(reduce_N_pt_bins);
        }
        if (fit_CR_only) {
                fitConfig=fitConfig+"fit_CR_only";
        }
        if (fit_SR_as_CR) {
                fitConfig=fitConfig+"fit_SR_as_CR";
        }
        boot_strap_name="";
        if (boot_strap_weight>=0){
            boot_strap_name="_boot_strap_"+to_string(boot_strap_weight);
            cout<<"running on: "<< boot_strap_name<<endl;
        }
        if (allow_neg_sf_xx){
            sf_xx_min_val=-10;
            std::cout << "allowed falvour scale factor to go negtaive (-10)! Check results!!"<< std::endl;
        }
        string combined_tagger_name=channelName+"_hist_for_fit_"+taggerName+"_"+workingPoint;
        string combined_tagger_name_with_cut=channelName+"_m_lj_cut_hist_for_fit_"+taggerName+"_"+workingPoint; //for SR we take the values from the file with m_lj_cut. -saves space! but keep cut at the same place!



        // // RooWorkspace, RooCategory, RooSimultaneous building
        // //idea to use tagger as variable and pt1pt2 as category
        RooWorkspace* w = new RooWorkspace("w","w");
        RooCategory cat_reg_pt_jet1_jet2("cat_reg_pt_jet1_jet2","cat_reg_pt_jet1_jet2");
        RooSimultaneous full_model("full_model","full_model",cat_reg_pt_jet1_jet2);
        //set to put constrains into: not needed atm
        RooArgSet* ex_constraints=new RooArgSet("ex_constraints");
        RooArgSet* parameters_of_interest=new RooArgSet("parameters_of_interest");
        //datalist to insert categorys

        //maps to stroe input histograms:
        std::vector<std::string> flavours={"bb","bl","lb","ll"};
        std::vector<std::string> cr_names={"SR","CR_HL","CR_LH","CR_HH"};
        std::map<std::string, THnSparseD*> hf4_Ncr_MC_sparse;
        std::map<std::string, THnSparseD*> hf4_SR_MC_tag;
        std::map<std::string, TH3D*> hf3_Nreg_MC;
        //opening input file:
        cout<<"opening input File ... "<<endl;
        TFile *input_file = new TFile(inputfile_name.c_str(),"read");
        //Loading MC:
        cout<<"loading MC from File ... "<<endl;
        for (const auto &flav : flavours) {
            //loading the events in sr and cr:
	  std::cout <<"Trying to load: " << ("hf4_Ncr_MC_combined_"+combined_tagger_name+"_"+flav+boot_strap_name).c_str() << std::endl;
	  THnSparseD* hf4_Ncr_MC_falv=(THnSparseD*)input_file->Get(("hf4_Ncr_MC_combined_"+combined_tagger_name+"_"+flav+boot_strap_name).c_str());
	  hf3_Nreg_MC[flav]=sparse_to_3d_sr_cr("hf3_Ncr_MC_"+flav,hf4_Ncr_MC_falv);
	  w->import(*hf3_Nreg_MC[flav]);
	  //loading events in the signal region with tagger weights -the ones we want to fit:
	  std::cout <<"Trying to load: " << ("hf4_MC_combined_"+combined_tagger_name_with_cut+"_"+flav+boot_strap_name).c_str()<< std::endl;
	  hf4_SR_MC_tag[flav]=(THnSparseD*)((THnSparseD*)input_file->Get(("hf4_MC_combined_"+combined_tagger_name_with_cut+"_"+flav+boot_strap_name).c_str()))->Clone(("hf4_MC_combined_SR_"+flav).c_str());
	  w->import(*hf4_SR_MC_tag[flav]);
        }
        // TH2D*       h_mc_input_hist_l_n= (TH2D*) input_file->Get(("hff_MC_combined_"+combined_tagger_name_with_cut+"_light").c_str());
        // h_mc_input_hist_l_n=(TH2D*) h_mc_input_hist_l_n->Clone("hff_MC_combined_light");
        TH2D* h_mc_input_hist_l_n=(TH2D*) hf4_SR_MC_tag["ll"]->Projection(2,0)->Clone("hff_MC_combined_light");
        h_mc_input_hist_l_n->Add( hf4_SR_MC_tag["ll"]->Projection(3,1));
        h_mc_input_hist_l_n->Add( hf4_SR_MC_tag["bl"]->Projection(3,1));
        h_mc_input_hist_l_n->Add( hf4_SR_MC_tag["lb"]->Projection(2,0));
        // TH2D*       h_mc_input_hist_b_n= (TH2D*)input_file->Get(("hff_MC_combined_"+combined_tagger_name_with_cut+"_b").c_str());
        // h_mc_input_hist_b_n=(TH2D*) h_mc_input_hist_b_n->Clone("hff_MC_combined_b");
        TH2D* h_mc_input_hist_b_n=(TH2D*) hf4_SR_MC_tag["bb"]->Projection(2,0)->Clone("hff_MC_combined_b");
        h_mc_input_hist_b_n->Add( hf4_SR_MC_tag["bb"]->Projection(3,1));
        h_mc_input_hist_b_n->Add( hf4_SR_MC_tag["lb"]->Projection(3,1));
        h_mc_input_hist_b_n->Add( hf4_SR_MC_tag["bl"]->Projection(2,0));
        TH2D*       h_mc_input_hist_b_ttb= (TH2D*)input_file->Get(("hff_MC_ttb_"+combined_tagger_name_with_cut+"_b").c_str());
        h_mc_input_hist_b_ttb=(TH2D*) h_mc_input_hist_b_ttb->Clone("hff_MC_ttb_b");

        TH3D* hf3_Nreg_MC_tot=(TH3D*)hf3_Nreg_MC["ll"]->Clone("hf3_Ncr_MC_tot");
        hf3_Nreg_MC_tot->Reset();
        THnSparseD* hf4_SR_MC_tot=(THnSparseD*) hf4_SR_MC_tag["ll"]->Clone("hf4_MC_combined_SR_tot");
        hf4_SR_MC_tot->Reset();
        for (const auto &flav : flavours) {
            hf3_Nreg_MC_tot->Add(hf3_Nreg_MC[flav]);
            hf4_SR_MC_tot->Add(hf4_SR_MC_tag[flav]);
        }
        w->import(*hf4_SR_MC_tot);
        w->import(*hf3_Nreg_MC_tot);
        //Loading Data"
	std::cout << "Loading the data " << std::endl;
        TH3D* hf3_Nreg_data;
        THnSparseD* hf4_SR_data_tag;
        if (!closure_test) {
                cout<<"loading Data from File ... "<<"hf4_data_"+combined_tagger_name<<" and "<<combined_tagger_name_with_cut <<endl;
                THnSparseD* hf4_Ncr_data_sparse=(THnSparseD*)input_file->Get(("hf4_Ncr_data_"+combined_tagger_name).c_str());
                hf3_Nreg_data =sparse_to_3d_sr_cr("hf3_Ncr_data", hf4_Ncr_data_sparse);
                hf4_SR_data_tag= (THnSparseD*)((THnSparseD*)input_file->Get(("hf4_data_"+combined_tagger_name_with_cut).c_str()))->Clone("hf4_data_SR");
        }
        else{
                cout<<"closure_test ...!!!!!!! Data from MC!! "<<endl;
                if (systName=="nominal"||systName=="clTestMoMc") {
                        systName="clTestMoMc";
                }
                else if (systName!="clTestMoMo") systName="clTestMoMc";
                hf3_Nreg_data=(TH3D*)hf3_Nreg_MC_tot->Clone("hf3_Ncr_data");
                hf4_SR_data_tag=(THnSparseD*) hf4_SR_MC_tot->Clone("hf4_data_SR");
                if (closure_test_seed==-1)
                {
                        closure_test_seed=101;
                }
                else if (closure_test_seed==-2) {
                        cout<<"closure_test ...!!!!!!! Data from MC withou stat!! "<<endl;
                        if (systName!="clTestMoMc_nstat") systName=systName+"_nstat";
                }
                else{
                        //we are doing cl tests for
                        systName=systName+"S"+to_string(closure_test_seed);
                        //do_plots=false;
                }
                if (closure_test_seed>=0)
                {
                        TRandom3* rGenerator=new TRandom3(closure_test_seed);
                        for(int pt1=1; pt1 <= hf3_Nreg_data->GetXaxis()->GetNbins(); pt1++) {
                            for(int pt2=1; pt2 <= hf3_Nreg_data->GetYaxis()->GetNbins(); pt2++) {
                                for(int cbin=1; cbin <= hf3_Nreg_data->GetZaxis()->GetNbins(); cbin++) {
                                        double reg_old_value =hf3_Nreg_data->GetBinContent(pt1,pt2,cbin);
                                        int reg_bin_value=0;
                                        if (cbin==1){
                                            for(int w1=1; w1 <= hf4_SR_data_tag->GetAxis(0)->GetNbins(); w1++) {
                                                for(int w2=1; w2 <= hf4_SR_data_tag->GetAxis(1)->GetNbins(); w2++) {
                                                    int where[4]={w1,w2,pt1,pt2};
                                                    double sr_old_value =hf4_SR_data_tag->GetBinContent(where);
                                                    int sr_bin_value=0;
                                                    if (sr_old_value > 0){
                                                        sr_bin_value=rGenerator->Poisson(sr_old_value );
                                                    }
                                                    hf4_SR_data_tag->SetBinContent(where, sr_bin_value);
                                                    hf4_SR_data_tag->SetBinError(where,sqrt(sr_bin_value));
                                                    reg_bin_value+=sr_bin_value;
                                                    cout<<"setted bin "<<pt1<<pt2<<w1<<w2<< " Old value: "<< sr_old_value << " new value "<< sr_bin_value<< " error "<< hf4_SR_data_tag->GetBinError(where) <<endl;
                                                }
                                            }
                                        }//shitty case we are in signal region and have to make sure that those two histrogramms fit together, so bin value=sum of N in taggers!                                        }
                                        else{
                                            if (reg_old_value > 0){
                                                reg_bin_value=rGenerator->Poisson(reg_old_value );
                                            }
                                        }
                                        hf3_Nreg_data->SetBinContent(pt1,pt2,cbin, reg_bin_value);
                                        hf3_Nreg_data->SetBinError(pt1,pt2,cbin,sqrt(reg_bin_value));
                                        cout<<"setted bin "<<pt1<<pt2<<cbin<< " Old value: "<< reg_old_value << " new value "<< reg_bin_value<< " error "<< hf3_Nreg_data->GetBinError(pt1,pt2,cbin) <<endl;
                                }
                            }
                        }
                }
	}

        //setting the Observables:
        int n_pt_bins=hf3_Nreg_data->GetXaxis()->GetNbins();
        int n_cr_bins=hf3_Nreg_data->GetZaxis()->GetNbins();
        int n_tagger_bins=hf4_SR_data_tag->GetAxis(0)->GetNbins();

        if (pl_up) {
	  double factor=2.0;
                cout<<"Simulating pl up by factor "<< factor <<" !!!!!!! "<<endl;
                if (systName=="nominal"||systName=="misstagLight_up"){systName="misstagLight_up";}
                else {systName=systName+"_misstagLight_up";}
                for (int pt_bin=1; pt_bin<=n_pt_bins; pt_bin++)
                {
                        cout<<"pt_bin: "<<pt_bin<<endl;
                        double untagged = h_mc_input_hist_l_n->GetBinContent(1,pt_bin);
                        double tagged = h_mc_input_hist_l_n->Integral(2,n_tagger_bins,pt_bin,pt_bin);
                        cout<<"untagged: "<<untagged<<" tagged: "<<tagged<<endl;
                        h_mc_input_hist_l_n->SetBinContent(1,pt_bin,(untagged - tagged*(factor-1.)));
                        for (int tagger_bin=2; tagger_bin<=n_tagger_bins; tagger_bin++)
                        {
                                h_mc_input_hist_l_n->SetBinContent(tagger_bin,pt_bin,h_mc_input_hist_l_n->GetBinContent(tagger_bin,pt_bin)*factor);
                        }
                        untagged = h_mc_input_hist_l_n->GetBinContent(1,pt_bin);
                        tagged = h_mc_input_hist_l_n->Integral(2,n_tagger_bins,pt_bin,pt_bin);
                        cout<<"later: untagged: "<<untagged<<" tagged: "<<tagged<<endl;
                }
        }

        w->import(*hf3_Nreg_data);
        w->import(*hf4_SR_data_tag);
        config_string_name=rlTag+"_"+dataName+"_"+taggerName+"_"+workingPoint+"_"+channelName+fitConfig+"_"+systName+boot_strap_name;
        outputfile_name=out_dir+"/Workspaces_3SB"+"/Workspace_"+config_string_name;
        outputfile_name=outputfile_name+".root";
        std::cout<<"Tryinig to create: "<< outputfile_name<<std::endl;
        TFile *output_file = new TFile((outputfile_name).c_str(),"recreate");
        output_file->cd();
        hf3_Nreg_data->Write();
        hf4_SR_data_tag->Write();
        hf3_Nreg_MC_tot->Write();
        hf4_SR_MC_tot->Write();
        w->import(*h_mc_input_hist_l_n);
        w->import(*h_mc_input_hist_b_n);
        w->import(*h_mc_input_hist_b_ttb);

        //importing TStings to workspace in order to have them as labels on the plots :)
        TObjString* dataName_help=new TObjString(dataName.c_str());
        w->import(*dataName_help,"dataName");
        TObjString* rlTag_help=new TObjString(rlTag.c_str());
        w->import(*rlTag_help,"rlTag");
        TObjString* taggerName_help=new TObjString(taggerName.c_str());
        w->import(*taggerName_help,"taggerName");
        TObjString* workingPoint_help=new TObjString(workingPoint.c_str());
        w->import(*workingPoint_help,"workingPoint");
        TObjString* systName_help=new TObjString(systName.c_str());
        w->import(*systName_help,"systName");
        TObjString* channelName_help=new TObjString(channelName.c_str());
        w->import(*channelName_help,"channelName");
        TObjString* fitConfig_help=new TObjString(fitConfig.c_str());
        w->import(*fitConfig_help,"fitConfig");
        TObjString* boot_strap_help=new TObjString(boot_strap_name.c_str());
        w->import(*boot_strap_help,"boot_strap_name");
        TObjString* lumi_label_help=new TObjString(lumi_label.c_str());
        w->import(*lumi_label_help,"lumi_label");
        TObjString* MC_label_help=new TObjString(MC_label.c_str());
        w->import(*MC_label_help,"MC_label");        
        



        for (const auto &flav : flavours) {
            hf3_Nreg_MC[flav]->Write();
            hf4_SR_MC_tag[flav]->Write();
        }

        if (reduce_N_pt_bins){
                n_pt_bins=reduce_N_pt_bins;
        }
        cout<<"Data loaded: n_pt_bins: "<<n_pt_bins<<" n_tagger_bins: "<<n_tagger_bins<<" n_cr_bins: "<<n_cr_bins<<endl;
        //use constant binning for tagger bins. bins 0,1,2...
        RooBinning* r_tagger_bins= new RooBinning (n_tagger_bins,  0., n_tagger_bins,"r_tagger_bins");
        //2-jets->2 w variables
        w->import(RooRealVar("w_jet1", "w_jet1", 0.5,0,n_tagger_bins));
        RooRealVar* w_jet1=w->var("w_jet1");
        w_jet1->setBinning(*r_tagger_bins);
        w->import(RooRealVar("w_jet2", "w_jet2", 0.5,0,n_tagger_bins));
        RooRealVar* w_jet2=w->var("w_jet2");
        w_jet2->setBinning(*r_tagger_bins);


//first loop over pt bins to define p_b_pt and p_l_pt for jet1 and jet2 which are used to define pdf_b_for a given pt in SR-region

        TArrayD* limits=new TArrayD (n_tagger_bins+1,r_tagger_bins->array()); //limits = [0,1,2,...] which are the bin boundrys...needed for parametric step functions
        for (int pt_bin=1; pt_bin<=n_pt_bins; pt_bin++) {
                string suffix_pt = "pt_";
                int tagger_bin;
                double lower_tagger_bound,upper_tagger_bound,lower_pt_bound,upper_pt_bound;
                lower_pt_bound=hf4_SR_data_tag->GetAxis(3)->GetBinLowEdge ( pt_bin);
                upper_pt_bound=hf4_SR_data_tag->GetAxis(3)->GetBinUpEdge ( pt_bin);
                suffix_pt = suffix_pt + to_string(pt_bin);
                RooArgSet* p_b_all_tagger=new RooArgSet("p_b_all_tagger");
                RooArgSet* p_l_all_tagger=new RooArgSet("p_l_all_tagger");
                double pt_bin_integral_b=h_mc_input_hist_b_n->Integral(0,n_tagger_bins,pt_bin,pt_bin); //how many b jets in given pt bin?
                double pt_bin_integral_b_ttb=h_mc_input_hist_b_ttb->Integral(0,n_tagger_bins,pt_bin,pt_bin); //how many b jets in given pt bin?
                double pt_bin_integral_l=h_mc_input_hist_l_n->Integral(0,n_tagger_bins,pt_bin,pt_bin); //how many l jets in given pt bin?
                cout<<"pt_Bin: "<<pt_bin<<" from: "<<lower_pt_bound<<" to: "<<upper_pt_bound<<" Suffix: "<< suffix_pt<<endl;
                // Loop on tagger bins
                for (tagger_bin=1; tagger_bin<n_tagger_bins; tagger_bin++) {
                        lower_tagger_bound=hf4_SR_data_tag->GetAxis(0)->GetBinLowEdge ( tagger_bin);
                        upper_tagger_bound=hf4_SR_data_tag->GetAxis(0)->GetBinUpEdge ( tagger_bin);
                        // suffix used for categories
                        string suffix_mv = suffix_pt+"_tagger_";
                        suffix_mv = suffix_mv + to_string(tagger_bin);
                        w->import( RooRealVar(("p_b_"+suffix_mv).c_str(),("p_b_"+suffix_mv).c_str(),0.2,0.,1.));
                        RooRealVar * p_b_bin = w->var(("p_b_"+suffix_mv).c_str());
                        p_b_bin->setVal((h_mc_input_hist_b_n->GetBinContent(tagger_bin,pt_bin))/pt_bin_integral_b);
                        //  p_b_bin ->	setConstant(kTRUE);
                        p_b_all_tagger->add(*p_b_bin);
                        parameters_of_interest->add(* p_b_bin);
                        w->import( RooRealVar(("p_l_"+suffix_mv).c_str(),("p_l_"+suffix_mv).c_str(),0.2,0.,1.));
                        RooRealVar * p_l_bin = w->var(("p_l_"+suffix_mv).c_str());
                        double p_l_val=(h_mc_input_hist_l_n->GetBinContent(tagger_bin,pt_bin))/pt_bin_integral_l;
                        p_l_bin->setVal(p_l_val);
                        if (fix_p_l) {
                                //if (tagger_bin<4){p_l_bin ->	setConstant(kTRUE);}
                                //if (pt_bin<3){p_l_bin ->	setConstant(kTRUE);}
                                p_l_bin->setConstant(kTRUE);
                        }
                        else{
                                RooGaussian* p_l_constraint = new RooGaussian(("p_l_constraint_"+suffix_mv).c_str(),"p_l constraint per bin",*p_l_bin, RooConst(p_l_val),RooConst(p_l_val/100 ) );
                                w->import(*p_l_constraint);
                                ex_constraints->add(*w->arg(("p_l_constraint_"+suffix_mv).c_str()));
                                parameters_of_interest->add(* p_l_bin);
                        }
                        p_l_all_tagger->add(*p_l_bin);
                        cout<<"Bin: "<<tagger_bin<<" from: "<<lower_tagger_bound<<" to: "<<upper_tagger_bound<<" Suffix: "<< suffix_mv<<endl;
                        cout<<"p_b_bin: "<<tagger_bin<<endl;
                }
                //for each pt bin we define RooParametricStepFunctions  as pdfs
                //RooParametricStepFunction * pdf_b=new RooParametricStepFunction ("pdf_b", "pdf_b", w_je1, const RooArgList &coefList, TArrayD &limits, Int_t nBins=1)
                w->import(RooParametricStepFunction (("pdf_b_jet1_"+suffix_pt).c_str(), ("pdf_b_jet1_"+suffix_pt).c_str(), *w_jet1, *p_b_all_tagger,*limits, n_tagger_bins ));
                w->import(RooParametricStepFunction (("pdf_b_jet2_"+suffix_pt).c_str(), ("pdf_b_jet2_"+suffix_pt).c_str(), *w_jet2, *p_b_all_tagger,*limits, n_tagger_bins ));
                w->import(RooParametricStepFunction (("pdf_l_jet1_"+suffix_pt).c_str(), ("pdf_l_jet1_"+suffix_pt).c_str(), *w_jet1, *p_l_all_tagger,*limits, n_tagger_bins ));
                w->import(RooParametricStepFunction (("pdf_l_jet2_"+suffix_pt).c_str(), ("pdf_l_jet2_"+suffix_pt).c_str(), *w_jet2, *p_l_all_tagger,*limits, n_tagger_bins ));
                // last tagger bin seperatly:, Just for printout!!!
                lower_tagger_bound=hf4_SR_data_tag->GetAxis(0)->GetBinLowEdge ( tagger_bin);
                upper_tagger_bound=hf4_SR_data_tag->GetAxis(0)->GetBinUpEdge ( tagger_bin);
                string suffix_mv = suffix_pt+"_tagger_";
                suffix_mv = suffix_mv + to_string(tagger_bin);
                cout<<"Last Bin: "<<tagger_bin<<" from: "<<lower_tagger_bound<<" to: "<<upper_tagger_bound<<" Suffix: "<< suffix_mv<<endl;
		TString import_formula="1-(";
		for(int i_tagger=0; i_tagger<n_tagger_bins-1;i_tagger++){
		  if(i_tagger) import_formula+="+";
		  import_formula+="@"+TString::Itoa(i_tagger,10);
		}
		import_formula+=")"; // With 5 WPs, import_formula = "1-(@0+@1+@2+@3)"
                w->import( RooFormulaVar(("p_b_"+suffix_mv).c_str(),("p_b_"+suffix_mv).c_str(),import_formula.Data(),*p_b_all_tagger));
                w->import( RooFormulaVar(("p_l_"+suffix_mv).c_str(),("p_l_"+suffix_mv).c_str(),import_formula.Data(),*p_l_all_tagger));
                p_b_all_tagger->add(*w->function(("p_b_"+suffix_mv).c_str()));
                p_l_all_tagger->add(*w->function(("p_l_"+suffix_mv).c_str()));
                if (!(fix_p_l)) {
                        double p_l_val=(h_mc_input_hist_l_n->GetBinContent(tagger_bin,pt_bin))/pt_bin_integral_l;
                        RooGaussian* p_l_constraint = new RooGaussian(("p_l_constraint_"+suffix_mv).c_str(),"p_l constraint per bin",*w->function(("p_l_"+suffix_mv).c_str()), RooConst(p_l_val),RooConst(p_l_val/100 ) );
                        w->import(*p_l_constraint);
                        ex_constraints->add(*w->arg(("p_l_constraint_"+suffix_mv).c_str()));
                }
                //e_bs (also just for print out!):
		for(int i_tagger=0;i_tagger<n_tagger_bins;i_tagger++){
		  TString varname="e_b_"+TString(suffix_pt)+"_tagger_"+TString::Itoa(i_tagger+1,10);
		  TString p_b_formula="(";
		  for(int j_tagger=i_tagger;j_tagger<n_tagger_bins;j_tagger++){
		    if(j_tagger-i_tagger) p_b_formula+="+";
		    p_b_formula+="@"+TString::Itoa(j_tagger,10);
		  }
		  p_b_formula+=")";
		  // e.g. RooFormulaVar("e_b_pt_2_tagger_3","e_b_pt_2_tagger_3","(@2+@3+@4)",*p_b_all_tagger)
		  // RooFormulaVar("e_b_pt_2_tagger_1","e_b_pt_2_tagger_1","(@0+@1+@2+@3+@4)",*p_b_all_tagger)
                  w->import( RooFormulaVar(varname.Data(),varname.Data(),p_b_formula,*p_b_all_tagger) ); 
		}
                //scalefactors:
                for (tagger_bin=1; tagger_bin<=n_tagger_bins; tagger_bin++) {
                        double e_b_pt_mv;
                        e_b_pt_mv=(h_mc_input_hist_b_n->Integral(tagger_bin,n_tagger_bins,pt_bin,pt_bin))/pt_bin_integral_b; // the denominator of the SF is the b-tagging efficiency of total MC
                        w->import( RooFormulaVar(("sf_b_"+suffix_pt+"_tagger_"+to_string(tagger_bin)).c_str(),("sf_b_"+suffix_pt+"_tagger_"+to_string(tagger_bin)).c_str(),"(@0/@1)",RooArgSet(*w->function(("e_b_"+suffix_pt+"_tagger_"+to_string(tagger_bin)).c_str()),RooConst(e_b_pt_mv))));
                }
        }
//end loop oder pt bin
//for the control regions we need a dummy step funtion:
        w->import( RooRealVar("c_x_dummy_1","c_x_dummy_1",1.));
        RooRealVar * c_x_dummy_1 = w->var("c_x_dummy_1");
        w->import( RooRealVar("c_x_dummy_0","c_x_dummy_0",1.));
        RooRealVar * c_x_dummy_0 = w->var("c_x_dummy_0");

        RooArgList* c_x_dummy_all=new RooArgList("c_x_dummy_all");
        c_x_dummy_all->add(*c_x_dummy_1);
        for (int tagger_bin=2; tagger_bin<n_tagger_bins; tagger_bin++) {
            c_x_dummy_all->add(*c_x_dummy_0);
        }
        w->import(RooParametricStepFunction ("pdf_x_dummy_jet1", "pdf_x_dummy_jet1", *w_jet1, *c_x_dummy_all,*limits, n_tagger_bins ));
        w->import(RooParametricStepFunction ("pdf_x_dummy_jet2", "pdf_x_dummy_jet2", *w_jet2, *c_x_dummy_all,*limits, n_tagger_bins ));
        w->import( RooProdPdf("P_xx_dummy","P_xx_dummy",*w->pdf("pdf_x_dummy_jet1"),*w->pdf("pdf_x_dummy_jet2")));
        RooAbsPdf* P_xx_dummy = w->pdf("P_xx_dummy");
//we have the dummy stuff.



//now we need to define N_roocat_flav depending on sf_pt1pt2_flav
        for (int pt_jet1=1; pt_jet1<=n_pt_bins; pt_jet1++) {
                for (int pt_jet2=1; pt_jet2<=n_pt_bins; pt_jet2++) {
                        //be carefull with this! we only estimate the half of the matrix, since jets are pt ordered!
                        if (pt_jet2>pt_jet1) {continue; }
                        string suffix_pt12 = "pt_";
                        suffix_pt12 = suffix_pt12 + to_string(pt_jet1)+"_"+to_string(pt_jet2);

                        for (const auto &flav : flavours) {
                                string s_flav_pt=flav+"_"+suffix_pt12;
                                TH1D* pt_slice=hf3_Nreg_MC[flav]->ProjectionZ(("hff_Ncr_MC_"+s_flav_pt).c_str(),pt_jet1,pt_jet1,pt_jet2,pt_jet2);
                                w->import( RooRealVar(("sf_"+s_flav_pt).c_str(),("sf_"+s_flav_pt).c_str(),1.,sf_xx_min_val,10));
                                RooRealVar * sf_flav = w->var(("sf_"+s_flav_pt).c_str());
                                parameters_of_interest->add(* sf_flav);
                                if (fit_CR_only && (flav=="bb")) {sf_flav->setConstant(kTRUE);}
                                if (NConst){
                                    sf_flav->setConstant(kTRUE);
                                }
                                //lets define the templates. with N_reg_pt and p_reg_pt
                                for (int cr_bin=1;cr_bin<=n_cr_bins;cr_bin++){
                                        string s_N_name="N_"+flav+"_"+cr_names.at(cr_bin-1)+"_"+suffix_pt12;
                                        string s_N_MC_name="N_MC_"+flav+"_"+cr_names.at(cr_bin-1)+"_"+suffix_pt12;
                                        const char * c_N_MC_name = s_N_MC_name.c_str();
                                        w->import( RooRealVar(c_N_MC_name,c_N_MC_name,pt_slice->GetBinContent(cr_bin)));
                                        RooRealVar * N_MC_cr_bin = w->var(c_N_MC_name);
                                        N_MC_cr_bin ->	setConstant(kTRUE);
                                        w->import( RooFormulaVar((s_N_name).c_str(),(s_N_name).c_str(),"(@0*@1)",RooArgSet(*N_MC_cr_bin,*sf_flav)));
                                }
                        }

                        //now lets build models!
                        for (int cr_bin=1;cr_bin<=n_cr_bins;cr_bin++){
                                //in eacht category:
                                string s_reg=cr_names.at(cr_bin-1);
                                string suffix_roocat=s_reg+"_"+suffix_pt12;
                                if ((s_reg == "SR" ) && !fit_SR_as_CR){
                                        if (fit_CR_only) {continue; }
                                        //we are in the signal region, we need more compicated model!
                                        // Defining model:
                                        //getting pdf to use:
                                        string suffix_jet1 = "pt_"+ to_string(pt_jet1);
                                        string suffix_jet2 = "pt_"+ to_string(pt_jet2);
                                        RooAbsPdf* pdf_b_jet1=w->pdf(("pdf_b_jet1_"+suffix_jet1).c_str());
                                        RooAbsPdf* pdf_b_jet2=w->pdf(("pdf_b_jet2_"+suffix_jet2).c_str());
                                        RooAbsPdf* pdf_l_jet1=w->pdf(("pdf_l_jet1_"+suffix_jet1).c_str());
                                        RooAbsPdf* pdf_l_jet2=w->pdf(("pdf_l_jet2_"+suffix_jet2).c_str());
                                        w->import( RooProdPdf(("P_bb_"+suffix_roocat).c_str(),("P_bb_"+suffix_roocat).c_str(),*pdf_b_jet1,*pdf_b_jet2));
                                        w->import( RooProdPdf(("P_bl_"+suffix_roocat).c_str(),("P_bl_"+suffix_roocat).c_str(),*pdf_b_jet1,*pdf_l_jet2));
                                        w->import( RooProdPdf(("P_lb_"+suffix_roocat).c_str(),("P_lb_"+suffix_roocat).c_str(),*pdf_l_jet1,*pdf_b_jet2));
                                        w->import( RooProdPdf(("P_ll_"+suffix_roocat).c_str(),("P_ll_"+suffix_roocat).c_str(),*pdf_l_jet1,*pdf_l_jet2));
                                        //lets implement the sum in the likelihood
                                        // RooArgList* P_list=new RooArgList(("l_P_"+suffix_roocat).c_str());

                                        RooArgList* P_N_list=new RooArgList(("l_P_N_"+suffix_roocat).c_str());
                                        for (const auto &flav : flavours) {
                                                string s_suf=flav+"_"+suffix_roocat;
                                                RooAbsPdf* pdf_flav = w->pdf(("P_"+s_suf).c_str());
                                                // P_list->add(*pdf_flav);
                                                RooAbsReal * N_flav = w->function(("N_"+s_suf).c_str());
                                                w->import(RooExtendPdf(("P_N_"+flav+"_"+suffix_roocat).c_str(),("P_N_"+flav+"_"+suffix_roocat).c_str(), *pdf_flav, *N_flav));
                                                RooAbsPdf* P_N_flav=w->pdf(("P_N_"+flav+"_"+suffix_roocat).c_str());
                                                P_N_list->add(*P_N_flav);
                                        }
                                        w->import(RooAddPdf(("P_N_sum_"+suffix_roocat).c_str(),("P_N_sum_"+suffix_roocat).c_str(), *P_N_list));
                                }
                                else{
                                        //we are in a Conrtol region, we have to work with dummys:
                                        // RooArgList* P_list=new RooArgList(("l_P_"+suffix_roocat).c_str());
                                        // RooArgList* N_list=new RooArgList(("l_N_"+suffix_roocat).c_str());
                                        RooArgList* P_N_list=new RooArgList(("l_P_N_"+suffix_roocat).c_str());
                                        for (const auto &flav : flavours) {
                                                string s_suf=flav+"_"+suffix_roocat;
                                                // P_list->add(*P_xx_dummy);
                                                RooAbsReal * N_flav = w->function(("N_"+s_suf).c_str());
                                                //N_list->add(*N_flav);
                                                w->import(RooExtendPdf(("P_N_"+flav+"_"+suffix_roocat).c_str(),("P_N_"+flav+"_"+suffix_roocat).c_str(), *P_xx_dummy, *N_flav));
                                                RooAbsPdf* P_N_flav=w->pdf(("P_N_"+flav+"_"+suffix_roocat).c_str());
                                                P_N_list->add(*P_N_flav);
                                        }
                                        w->import(RooAddPdf(("P_N_sum_"+suffix_roocat).c_str(),("P_N_sum_"+suffix_roocat).c_str(), *P_N_list));
                                }
                                //just for fits and printouts
                                RooArgList* N_list=new RooArgList(("l_N_"+suffix_roocat).c_str());
                                for (const auto &flav : flavours) {
                                    string s_suf=flav+"_"+suffix_roocat;
                                    RooAbsReal * N_flav = w->function(("N_"+s_suf).c_str());
                                    N_list->add(*N_flav);
                                }
                                w->import(RooFormulaVar(("N_tot_"+suffix_roocat).c_str(),("N_tot_"+suffix_roocat).c_str(), "(@0+@1+@2+@3)",*N_list));
                                RooAbsReal * N_tot = w->function(("N_tot_"+suffix_roocat).c_str());
                                for (const auto &flav : flavours) {
                                        string s_suf=flav+"_"+suffix_roocat;
                                        RooAbsReal * N_flav = w->function(("N_"+s_suf).c_str());
                                        w->import(RooFormulaVar(("f_"+s_suf).c_str(),("f_"+s_suf).c_str(), "(@0/@1)",RooArgSet(*N_flav,*N_tot)));
                                }
                                //back to usefull stuff again

                                RooAbsPdf* P_N_sum=w->pdf(("P_N_sum_"+suffix_roocat).c_str());
                                full_model.addPdf(*P_N_sum,suffix_roocat.c_str());
                                cat_reg_pt_jet1_jet2.defineType(suffix_roocat.c_str());
                        }
                }
        }
        w->import(full_model);


        //create dummy histogram for importing data in cr regions:
        TH2D* hf2_dummy_cr=(TH2D*)hf4_SR_data_tag->Projection(1,0)->Clone("hf2_dummy_cr");
        hf2_dummy_cr->GetXaxis()->Set(n_tagger_bins,0,n_tagger_bins);
        hf2_dummy_cr->GetYaxis()->Set(n_tagger_bins,0,n_tagger_bins);
        hf2_dummy_cr->Reset();
        hf2_dummy_cr->SetStats(0);
        std::map<std::string, RooDataHist*> datalist;
        datalist.clear();
        //new loop over both pt bins and regions to import data.
        for (int pt_jet1=1; pt_jet1<=n_pt_bins; pt_jet1++) {
                for (int pt_jet2=1; pt_jet2<=n_pt_bins; pt_jet2++) {
                        if (pt_jet2>pt_jet1) {continue; }
                        string suffix_pt12 = "pt_";
                        suffix_pt12 = suffix_pt12 + to_string(pt_jet1)+"_"+to_string(pt_jet2);
                        TH1D* hf1_reg_pt_slice=hf3_Nreg_data->ProjectionZ(("hf1_Ncr_data_"+suffix_pt12).c_str(),pt_jet1,pt_jet1,pt_jet2,pt_jet2);
                        //hf1_reg_pt_slice->Write();
                        for (int cr_bin=1;cr_bin<=n_cr_bins;cr_bin++){
                                //hn->GetAxis(12)->SetRange(from_bin, to_bin);
                                string s_reg=cr_names.at(cr_bin-1);
                                string suffix_roocat=s_reg+"_"+suffix_pt12;
                                TH2D* hf2_data_tag_weights_pt12;
                                if ((s_reg=="SR" ) && !fit_SR_as_CR){
                                        if (fit_CR_only) {continue; }
                                        THnSparseD* hf4_SR_data_tag_this=(THnSparseD*) hf4_SR_data_tag->Clone(("hf4_SR_data_tag_"+suffix_roocat).c_str());
                                        hf4_SR_data_tag_this->GetAxis(2)->SetRange(pt_jet1, pt_jet1);
                                        hf4_SR_data_tag_this->GetAxis(3)->SetRange(pt_jet2, pt_jet2);
                                        hf2_data_tag_weights_pt12=hf4_SR_data_tag_this->Projection(1,0);
                                        hf2_data_tag_weights_pt12->GetXaxis()->Set(n_tagger_bins,0,n_tagger_bins);
                                        hf2_data_tag_weights_pt12->GetYaxis()->Set(n_tagger_bins,0,n_tagger_bins);
                                        //control printouts:
                                        int where[4]={2,3,pt_jet1,pt_jet2};
                                        if (abs(hf2_data_tag_weights_pt12->Integral()-hf1_reg_pt_slice->GetBinContent(cr_bin))>0.00001){
                                                //what happend? they should be equal!

                                                cout<<"error while importing data in SR for bin: "<<suffix_roocat<<" integral3 : "<< hf2_data_tag_weights_pt12->Integral()<<" != "<<hf1_reg_pt_slice->GetBinContent(cr_bin)<<"!!!!!"<<endl;
                                                cout<<"importing data in SR for bin: "<<suffix_roocat<<" value in bin 2,3 : "<< hf2_data_tag_weights_pt12->GetBinContent(2,3)<<" == "<<hf4_SR_data_tag_this->GetBinContent(where)<<endl;
                                                exit(1);
                                        }
                                        cout<<"importing data in SR for bin: "<<suffix_pt12<<" value in bin 2,3 : "<< hf2_data_tag_weights_pt12->GetBinContent(2,3)<<" == "<<hf4_SR_data_tag_this->GetBinContent(where)<<endl;
                                }
                                else{  //we are in a control region, so lets fill only the first bin
                                        hf2_data_tag_weights_pt12=(TH2D*)hf2_dummy_cr->Clone(("hf4_CR_data_tag_"+suffix_roocat+"_proj10").c_str());
                                        double n_events_CR=hf1_reg_pt_slice->GetBinContent(cr_bin);
                                        double err=hf1_reg_pt_slice->GetBinError(cr_bin);
                                        // double flattened_val=n_events_CR/n_tagger_bins/n_tagger_bins;
                                        // for (int t_bin1=1;t_bin1<=n_tagger_bins;t_bin1++){
                                        //     for (int t_bin2=1;t_bin2<=n_tagger_bins;t_bin2++){
                                        //         hf2_data_tag_weights_pt12->SetBinContent(t_bin1,t_bin2,flattened_val);
                                        //         hf2_data_tag_weights_pt12->SetBinError(t_bin1,t_bin2,sqrt(flattened_val));
                                        //     }
                                        // }
                                        hf2_data_tag_weights_pt12->SetBinContent(1,1,n_events_CR);
                                        hf2_data_tag_weights_pt12->SetBinError(1,1,err);
                                        cout<<"importing data in CR for bin: "<<suffix_roocat<<" value : "<< hf2_data_tag_weights_pt12->GetBinContent(1,1)<<" (1,2) "<<hf2_data_tag_weights_pt12->GetBinContent(1,2)<<endl;
                                }
                                RooDataHist* dh_data =new RooDataHist (("dh_data_"+suffix_roocat).c_str(),("dh_data"+suffix_roocat).c_str(), RooArgList(*w_jet1,*w_jet2), hf2_data_tag_weights_pt12);
                                w->import(*dh_data);
                                //hf2_data_tag_weights_pt12->Write();
                                datalist[suffix_roocat] =  (RooDataHist*)w->data(("dh_data_"+suffix_roocat).c_str());
                                delete dh_data;
                                dh_data=0;
                        }
                }
        }
        cout<<"importing datalist:"<<endl;
        RooDataHist* combined_data = new RooDataHist("combined_data", "combined_data", RooArgList(*w_jet1,*w_jet2), cat_reg_pt_jet1_jet2, datalist);
        w->import(*combined_data);
        w->Print();
        custom_print_workspace(w,n_pt_bins,n_tagger_bins,hf4_SR_data_tag);

        w->Clone("Workspace_before_fit")->Write();
        RooFitResult* fitResult; //,Extended(kTRUE),ExternalConstraints(*ex_constraints)
        if (use_Minos){
                fitResult = full_model.fitTo(*w->data("combined_data"),ExternalConstraints(*ex_constraints),RooFit::Save(),RooFit::Minos(*parameters_of_interest),SumW2Error(kFALSE), PrintLevel(-1));
                if (fitResult->status()!= 0)
                {
                        fitResult = full_model.fitTo(*w->data("combined_data"),ExternalConstraints(*ex_constraints),RooFit::Save(),RooFit::Minimizer("Minuit2","Migrad"), RooFit::Minos(*parameters_of_interest),SumW2Error(kFALSE), PrintLevel(-1));
                }
        }
        else{
                fitResult = full_model.fitTo(*w->data("combined_data"),ExternalConstraints(*ex_constraints),RooFit::Save(),SumW2Error(kFALSE),PrintLevel(-1), Warnings(kTRUE),Timer(kTRUE));// //,Extended(kTRUE),PrintLevel(-1)
                if (fitResult->status()!= 0 ||fitResult->covQual() != 3)
                {
                        std::cerr << "Warning:: Fitstatus = " << fitResult->status() << " ; covQual = "<< fitResult->covQual() <<" ; now trying running with Minuit2."<<std::endl;
                        std::cout << "Warning:: Fitstatus = " << fitResult->status() << " ; covQual = "<< fitResult->covQual() <<" ; now trying running with Minuit2."<<std::endl;
                        fitResult = full_model.fitTo(*w->data("combined_data"),ExternalConstraints(*ex_constraints),RooFit::Save(),RooFit::Minimizer("Minuit2","Migrad"),SumW2Error(kFALSE),PrintLevel(-1), Warnings(kTRUE),Timer(kTRUE));// //,Extended(kTRUE),PrintLevel(-1)
                        std::cout << "Fitstatus now = " << fitResult->status() << " ; covQual = "<< fitResult->covQual() <<" ;"<<std::endl;
                        if (fitResult->status()!= 0 ||fitResult->covQual() != 3)
                        {
                                std::cout << "Warning:: Fitstatus still = " << fitResult->status() << " ; covQual = "<< fitResult->covQual() << " ; now trying running with Minuit again."<<std::endl;
                                std::cerr << "Warning:: Fitstatus still = " << fitResult->status() << " ; covQual = "<< fitResult->covQual() << " ; now trying running with Minuit again."<<std::endl;
                                fitResult = full_model.fitTo(*w->data("combined_data"),ExternalConstraints(*ex_constraints),RooFit::Save(),SumW2Error(kFALSE),PrintLevel(-1), Warnings(kTRUE),Timer(kTRUE));// //,Extended(kTRUE),PrintLevel(-1)
                                std::cout << "Fitstatus now (final)= " << fitResult->status() << " ; covQual = "<< fitResult->covQual() << " ."<<std::endl;
                                if (fitResult->status()!= 0 ||fitResult->covQual() != 3)
                                        std::cerr << "Warning:: Final Fitstatus still = " << fitResult->status() << " ; covQual = "<< fitResult->covQual() << " !!!!!!!!!!!!!!!!!!"<<std::endl;
                        }
                }
        }


        w->Clone("Workspace_after_fit")->Write();
        fitResult->SetName("fitResult");
        fitResult->Write();
        fitResult->Print();
        //
        custom_print_workspace(w,n_pt_bins,n_tagger_bins,hf4_SR_data_tag,fitResult);
        output_file ->Close();
        input_file -> Close();
        cout<<"Outputfile Closed: "<<outputfile_name<<endl;
        cout<<"Data loaded: n_pt_bins: "<<n_pt_bins<<" n_tagger_bins: "<<n_tagger_bins<<" n_cr_bins: "<<n_cr_bins<<endl;
        // custom_print_p_xx_matrix(w,n_pt_bins,"sf_bb");
        // custom_print_p_xx_matrix(w,n_pt_bins,"sf_bl");
        // custom_print_p_xx_matrix(w,n_pt_bins,"sf_lb");
        // custom_print_p_xx_matrix(w,n_pt_bins,"sf_ll");
        if (do_plots) Workspace_plottingMacro_3SB(outputfile_name, out_dir+"/FitPlots_3SB/FitPlot_"+config_string_name+".root");

}
