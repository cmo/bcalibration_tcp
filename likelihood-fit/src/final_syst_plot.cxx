#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <iomanip>
#include "TH1.h"
#include "TH2.h"
#include <THnSparse.h>
#include "TFile.h"
#include "TCanvas.h"
#include "TChain.h"
#include <string>
#include "TObjString.h"
#include "RooGlobalFunc.h"
#include "RooGaussian.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooSimultaneous.h"
#include "RooParametricStepFunction.h"
#include "RooConstVar.h"
#include "RooWorkspace.h"
#include "RooCategory.h"
#include "RooUniform.h"
#include "RooExtendPdf.h"
#include "RooAddPdf.h"
#include "RooBinning.h"
#include "RooProdPdf.h"
#include "RooFit.h"
#include "RooArgList.h"
#include "RooFitResult.h"
#include "cxxopts.hpp"
#include "SystError_plottingMacro.h"

using namespace std;
using namespace RooFit;

int main (int argc, char *argv[])
{
        string inputfile_folder, outputfile_name, dataName, rlTag, taggerName, workingPoint, channelName,fit_config, dataLumi, CME, nominal_fit_combination_for_bin_mean;
        bool closure_test = false;
        bool varry_fs=false; //set to true if you want to let f_bl,f_bb,f_ll and f_lb float.
        std::vector<std::string>systNames;
        try
        {
                cxxopts::Options options(argv[0], " - example command line options");
                options.positional_help("[input output]");
                options.add_options()
                        ("i,input", "inputfile_folder", cxxopts::value<std::string>(),"inputfile_folder")
                        ("r,rlTag", "rlTag", cxxopts::value<std::string>()->default_value("r21"), "rlTag")
                        ("d,dataName", "dataName", cxxopts::value<std::string>()->default_value("d1516"), "dataName")
                        ("l,dataLumi", "Luminosity of the data sample to pirnt on Plots.[fb-1]", cxxopts::value<std::string>()->default_value("36.1"), "lumi")
                        ("t,tagger", "taggerName", cxxopts::value<std::string>()->default_value("MV2c10"), "taggerName")
                        ("e,CME", "CME", cxxopts::value<std::string>()->default_value("13"), "CME")
                        ("w,workingPoint", "Name of the Working point: FixedCutBEff, HybBEff", cxxopts::value<std::string>()->default_value("FixedCutBEff"),"name" ) //
                        ("c,channel", "name of channel to fit", cxxopts::value<std::string>()->default_value("emu_OS_J2"), "channelName")
                        ("f,fit_config", "fit_config", cxxopts::value<std::string>()->default_value(""), "fit_config")
                        ("n,nomFit", "nominal_fit_combination_for_bin_mean", cxxopts::value<std::string>()->default_value(""), "path")
                        
                        ("h, help", "Print help")
                        ("closure_test", "take mc as data", cxxopts::value<bool>(closure_test))
                        ("varry_fs", "varry the f_xx fractions", cxxopts::value<bool>(varry_fs))
                        ;


                        options.parse(argc, argv);

                if (options.count("help"))
                {
                        std::cout << options.help({""}) << std::endl;
                        exit(0);
                }
                if (options.count("input"))
                {
                        std::cout << "Input = " << options["input"].as<std::string>()<< std::endl;
                        inputfile_folder=options["input"].as<std::string>();
                }

                std::cout << "tagger_name = " << options["tagger"].as<std::string>()<< std::endl;
                taggerName=options["tagger"].as<std::string>();
                std::cout << "workingPoint = " << options["workingPoint"].as<std::string>()<< std::endl;
                workingPoint=options["workingPoint"].as<std::string>();
                rlTag=options["rlTag"].as<std::string>();
                dataName=options["dataName"].as<std::string>();
                dataLumi=options["dataLumi"].as<std::string>();
                CME=options["CME"].as<std::string>();
                channelName=options["channel"].as<std::string>();
                fit_config = options["fit_config"].as<std::string>();
                nominal_fit_combination_for_bin_mean = options["nomFit"].as<std::string>();
                std::cout << "Arguments remain = " << argc << std::endl;
                for (int i=1; i<argc; i++)
                {
                        std::cout <<"Lets put "<<argv[i] << " into the syst vector."<<std::endl;
                        systNames.push_back(argv[i]);
                }

        } catch (const cxxopts::OptionException& e)
        {
                std::cout << "error parsing options: " << e.what() << std::endl;
                exit(1);
        }
        //do the inputfile coorectly!!
        //which systeamtic plots to do,  dont make this huge mess!
        std::cout << "calling plotting macro: " << std::endl;
        std::cout << systNames.at(0) << " size "<< systNames.size()<<endl;
        SystError_plottingMacro(rlTag, taggerName, workingPoint, channelName, dataName, dataLumi, CME, fit_config, systNames, inputfile_folder, nominal_fit_combination_for_bin_mean);


}
