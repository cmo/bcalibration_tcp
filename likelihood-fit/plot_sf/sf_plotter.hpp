void plot_sf(vector<double> nominal_sfs, vector<double> stats_uncertainty, vector<double> total_uncertainty, 
        vector<double> x_bin_edges={20., 40., 60., 140., 250., 400.}, string x_label="Jet p_{T} [GeV]", 
        string text="GN2v01,  85% < #epsilon_{b} < 90%", string output_name="sf_pt.png"){

    TCanvas *c = new TCanvas("c", "",462,283,700,500);
    c->SetHighLightColor(2);
    c->Range(-56.96202,0.1798734,424.0506,1.243165);
    c->SetFillColor(0);
    c->SetBorderMode(0);
    c->SetBorderSize(2);
    c->SetTickx(1);
    c->SetTicky(1);
    c->SetLeftMargin(0.16);
    c->SetRightMargin(0.05);
    c->SetTopMargin(0.05);
    c->SetBottomMargin(0.16);
    c->SetFrameBorderMode(0);
    c->SetFrameBorderMode(0);

    double* x_bins = x_bin_edges.data();
    int num_bins = x_bin_edges.size()-1;
    TH1D *h_postfit_1 = new TH1D("h","", num_bins, x_bins);

    for (int i = 0; i < num_bins; i++){
        h_postfit_1->SetBinContent(i+1, nominal_sfs[i]);
        h_postfit_1->SetBinError(i+1, stats_uncertainty[i]);
    }

    h_postfit_1->SetMinimum(0.5+1e-4); // Y-axis
    h_postfit_1->SetMaximum(1.5-1e-4); // Y-axis
    h_postfit_1->SetEntries(num_bins);
    h_postfit_1->SetDirectory(nullptr);
    h_postfit_1->SetStats(0);
    h_postfit_1->SetLineWidth(2);
    h_postfit_1->SetMarkerStyle(20);
    h_postfit_1->SetMarkerSize(1.2);
    h_postfit_1->GetXaxis()->SetTitle(x_label.c_str());
    h_postfit_1->GetXaxis()->SetRange(1,num_bins);
    h_postfit_1->GetXaxis()->SetMoreLogLabels();
    h_postfit_1->GetXaxis()->SetLabelFont(42);
    h_postfit_1->GetXaxis()->SetLabelSize(0.06);    
    h_postfit_1->GetXaxis()->SetTitleSize(0.06);
    h_postfit_1->GetXaxis()->SetTitleOffset(1.2);
    h_postfit_1->GetXaxis()->SetTitleFont(42);
    h_postfit_1->GetYaxis()->SetTitle("b-jet tagging efficiency SF");
    h_postfit_1->GetYaxis()->SetNdivisions(507);
    h_postfit_1->GetYaxis()->SetLabelFont(42);
    h_postfit_1->GetYaxis()->SetLabelSize(0.06);
    h_postfit_1->GetYaxis()->SetTitleSize(0.06);
    h_postfit_1->GetYaxis()->SetTitleOffset(1.17);
    h_postfit_1->GetYaxis()->SetTitleFont(42);
    h_postfit_1->GetZaxis()->SetLabelFont(42);
    h_postfit_1->GetZaxis()->SetLabelSize(0.05);
    h_postfit_1->GetZaxis()->SetTitleSize(0.05);
    h_postfit_1->GetZaxis()->SetTitleOffset(1);
    h_postfit_1->GetZaxis()->SetTitleFont(42);
    h_postfit_1->Draw("AXIS");

    double* x_bin_centers = new double[num_bins];
    double* x_error_h = new double[num_bins];
    double* x_error_l = new double[num_bins];
    for (int i = 0; i < num_bins; i++){
        x_bin_centers[i] = (x_bin_edges[i] + x_bin_edges[i+1])/2;
        x_error_h[i] = x_bin_edges[i+1] - x_bin_centers[i];
        x_error_l[i] = x_bin_centers[i] - x_bin_edges[i];
    }

    double* scale_factors = nominal_sfs.data();
    double* y_stat_error = stats_uncertainty.data();
    double* y_sys_error = total_uncertainty.data();

    TGraphAsymmErrors *grae = new TGraphAsymmErrors(num_bins, x_bin_centers, scale_factors, x_error_l, x_error_h, y_sys_error, y_sys_error);
    grae->SetName("error_band");
    grae->SetTitle("");

    Int_t ci;      // for color index setting
    TColor *color; // for color definition with alpha
    ci = TColor::GetColor("#99cc99");
    grae->SetFillColor(ci);
    grae->SetFillStyle(1000);


    ci = TColor::GetColor("#99cc99");
    grae->SetLineColor(ci);
    grae->SetMarkerStyle(20);
    grae->SetMarkerSize(1.2);

    TH1F* h_error_band = new TH1F("graph_error_band", "", num_bins, x_bin_edges[0], x_bin_edges[num_bins]);
    h_error_band->SetMinimum(-1);
    h_error_band->SetMaximum(-1);
    h_error_band->SetDirectory(nullptr);
    h_error_band->SetStats(0);

    ci = TColor::GetColor("#000099");
    h_error_band->SetLineColor(ci);
    h_error_band->GetXaxis()->SetLabelFont(42);
    h_error_band->GetXaxis()->SetTitleOffset(1);
    h_error_band->GetXaxis()->SetTitleFont(42);
    h_error_band->GetYaxis()->SetLabelFont(42);
    h_error_band->GetYaxis()->SetTitleFont(42);
    h_error_band->GetZaxis()->SetLabelFont(42);
    h_error_band->GetZaxis()->SetTitleOffset(1);
    h_error_band->GetZaxis()->SetTitleFont(42);
    grae->SetHistogram(h_error_band);
    grae->Draw("2");

    TLine* line = new TLine(x_bin_edges[0], 1, x_bin_edges[num_bins], 1);
    line->SetLineColor(1);
    line->SetLineStyle(2);
    line->Draw();

    grae = new TGraphAsymmErrors(num_bins, x_bin_centers, scale_factors, x_error_l, x_error_h, y_stat_error, y_stat_error);
    grae->SetName("nominal_values");
    grae->SetTitle("");
    grae->SetFillStyle(1000);
    grae->SetLineWidth(2);
    grae->SetMarkerStyle(20);
    grae->SetMarkerSize(1.2);

    TH1F *h_nominal_values = new TH1F("h_nominal_values", "", num_bins, x_bin_edges[0], x_bin_edges[num_bins]);
    h_nominal_values->SetMinimum(-1);
    h_nominal_values->SetMaximum(-1);
    h_nominal_values->SetDirectory(nullptr);
    h_nominal_values->SetStats(0);

    ci = TColor::GetColor("#000099");


    h_nominal_values->SetLineColor(ci);
    h_nominal_values->GetXaxis()->SetLabelFont(42);
    h_nominal_values->GetXaxis()->SetTitleOffset(1);
    h_nominal_values->GetXaxis()->SetTitleFont(42);
    h_nominal_values->GetYaxis()->SetLabelFont(42);
    h_nominal_values->GetYaxis()->SetTitleFont(42);
    h_nominal_values->GetZaxis()->SetLabelFont(42);
    h_nominal_values->GetZaxis()->SetTitleOffset(1);
    h_nominal_values->GetZaxis()->SetTitleFont(42);
    grae->SetHistogram(h_nominal_values);
    grae->Draw("pz");

    TH1D *h_postfit_3 = new TH1D("h_postfit_3","", num_bins, x_bins);
    for (int i = 0; i < num_bins; i++){
        h_postfit_3->SetBinContent(i+1, 1);
        h_postfit_3->SetBinError(i+1, 0);
    }

    h_postfit_3->SetMinimum(0.35);
    h_postfit_3->SetMaximum(1.19);
    h_postfit_3->SetEntries(num_bins);
    h_postfit_3->SetDirectory(nullptr);
    h_postfit_3->SetStats(0);
    h_postfit_3->SetLineWidth(2);
    h_postfit_3->SetMarkerStyle(20);
    h_postfit_3->SetMarkerSize(1.2);
    h_postfit_3->GetXaxis()->SetTitle(x_label.c_str());
    h_postfit_3->GetXaxis()->SetRange(1,num_bins);
    h_postfit_3->GetXaxis()->SetMoreLogLabels();
    h_postfit_3->GetXaxis()->SetLabelFont(42);
    h_postfit_3->GetXaxis()->SetLabelSize(0.06);
    h_postfit_3->GetXaxis()->SetTitleSize(0.06);
    h_postfit_3->GetXaxis()->SetTitleOffset(1.2);
    h_postfit_3->GetXaxis()->SetTitleFont(42);
    h_postfit_3->GetYaxis()->SetTitle("b-jet tagging efficiency");
    h_postfit_3->GetYaxis()->SetNdivisions(507);
    h_postfit_3->GetYaxis()->SetLabelFont(42);
    h_postfit_3->GetYaxis()->SetLabelSize(0.06);
    h_postfit_3->GetYaxis()->SetTitleSize(0.06);
    h_postfit_3->GetYaxis()->SetTitleOffset(1.17);
    h_postfit_3->GetYaxis()->SetTitleFont(42);
    h_postfit_3->GetZaxis()->SetLabelFont(42);
    h_postfit_3->GetZaxis()->SetLabelSize(0.05);
    h_postfit_3->GetZaxis()->SetTitleSize(0.05);
    h_postfit_3->GetZaxis()->SetTitleOffset(1);
    h_postfit_3->GetZaxis()->SetTitleFont(42);
    h_postfit_3->Draw("AXISSAME");
    
    TPaveText *pt = new TPaveText(0.210602,0.846316,0.330946,0.913684,"brNDC");
    pt->SetBorderSize(0);
    pt->SetFillColor(0);
    pt->SetTextFont(72);
    pt->SetTextSize(0.0631579);
    TText *pt_LaTex = pt->AddText("ATLAS");
    pt->Draw();

    pt = new TPaveText(0.363897,0.848421,0.484241,0.915789,"brNDC");
    pt->SetBorderSize(0);
    pt->SetFillColor(0);
    pt->SetTextFont(42);
    pt->SetTextSize(0.0631579);
    pt_LaTex = pt->AddText("Internal");
    pt->Draw();

    pt = new TPaveText(0.663324,0.848421,0.769341,0.915789,"brNDC");
    pt->SetBorderSize(0);
    pt->SetFillColor(0);
    pt->SetTextFont(42);
    pt->SetTextSize(0.0631579);
    pt_LaTex = pt->AddText("#sqrt{s} = 13.6 TeV, 29.0 fb^{-1}");
    pt->Draw();

    pt = new TPaveText(0.494986,0.724211,0.613897,0.848421,"brNDC");
    pt->SetBorderSize(0);
    pt->SetFillColor(0);
    pt->SetTextFont(42);
    pt->SetTextSize(0.0631579);
    pt_LaTex = pt->AddText(text.c_str());
    pt->Draw();

    TLegend *leg = new TLegend(0.352436,0.208861,0.62894,0.333333,NULL,"brNDC");
    leg->SetBorderSize(0);
    leg->SetTextSize(0.0547368);
    leg->SetLineColor(1);
    leg->SetLineStyle(1);
    leg->SetLineWidth(1);
    leg->SetFillColor(0);
    leg->SetFillStyle(1001);

    TLegendEntry *entry=leg->AddEntry("nominal_values","Scale Factor (stat. unc.)","LPE");
    entry->SetLineColor(1);
    entry->SetLineStyle(1);
    entry->SetLineWidth(2);
    entry->SetMarkerColor(1);
    entry->SetMarkerStyle(20);
    entry->SetMarkerSize(1.2);
    entry->SetTextFont(42);
    entry=leg->AddEntry("error_band","Scale Factor (total unc.)","F");

    ci = TColor::GetColor("#99cc99");
    entry->SetFillColor(ci);
    entry->SetFillStyle(1000);

    ci = TColor::GetColor("#cc0000");
    entry->SetLineColor(ci);
    entry->SetLineStyle(1);
    entry->SetLineWidth(2);
    entry->SetMarkerColor(1);
    entry->SetMarkerStyle(21);
    entry->SetMarkerSize(1);
    entry->SetTextFont(42);
    leg->Draw();
    c->Modified();
    c->cd();
    c->SetSelected(c);

    c->SaveAs(output_name.c_str());
}
