import argparse
import os
import ROOT as R
R.gROOT.SetBatch(True)

R.gInterpreter.ProcessLine('#include "sf_plotter.hpp"')

parser = argparse.ArgumentParser()
parser.add_argument('--final_fit_plots', '-f', type=str, default='/eos/home-c/cmo/project/Calibration/FTAGResults/topcp_sample/r25.2.40_emujet-v3.0_mc23a/plots/FitPlots_3SB/FinalFitPlots_r24_data22_GN2v01_FixedCutBEff_emu_OS_J2.root', help='Path to the final fit plots')
arg = parser.parse_args()

f = R.TFile(arg.final_fit_plots)
d_results_continuous = f.Get('results_continuous')

gn2_wp_bins = [r"100%-90%", r"90%-85%", r"85%-77%", r"77%-70%", r"70%-65%", r"65%-0%"]
pt_bin_edges = [20., 40., 60., 140., 250., 400.]
# pt_bin_edges = [20., 40., 60., 140., 400.]
pt_bins = [(pt_bin_edges[i], pt_bin_edges[i + 1]) for i in range(len(pt_bin_edges) - 1)]

print("pt_bins: ", pt_bins)
for i_wp, wp in enumerate(gn2_wp_bins, 1):
    nominal_values = []
    stats_errors = []
    syst_errors = []
    total_errors = []
    for i_pt, pt in enumerate(pt_bins, 1):
        nominal_hist = d_results_continuous.Get(f"p_b_sf_pt_{i_pt}_nominal")
        rel_syst_hist = d_results_continuous.Get(f"p_b_sf_pt_{i_pt}_syst_Error_combined_rel")

        nominal = nominal_hist.GetBinContent(i_wp)
        stats = nominal_hist.GetBinError(i_wp)
        syst = rel_syst_hist.GetBinContent(i_wp) * nominal
        total_error = (stats**2 + syst**2)**0.5

        nominal_values.append(nominal)
        stats_errors.append(stats)
        syst_errors.append(syst)
        total_errors.append(total_error)


    print('-' * 80)
    print(f"WP: {wp}")
    print("Nominal values:     " + ', '.join([f"{v:.5f}" for v in nominal_values]))
    print("Statistical errors: " + ', '.join([f"{v:.5f}" for v in stats_errors]))
    print("Systematic errors:  " + ', '.join([f"{v:.5f}" for v in syst_errors]))
    print("Total errors:       " + ', '.join([f"{v:.5f}" for v in total_errors]))
    print('-' * 80)

    wp_high, wp_low = wp.split('-')
    R.gInterpreter.ProcessLine(f"""
            vector<double> nominal_values_{i_wp} = {{ {', '.join([str(v) for v in nominal_values])} }};
            vector<double> stats_errors_{i_wp} = {{ {', '.join([str(v) for v in stats_errors])} }};
            vector<double> total_errors_{i_wp} = {{ {', '.join([str(v) for v in total_errors])} }};
            vector<double> pt_bin_edges_{i_wp} = {{ {', '.join([f"{pt}" for pt in pt_bin_edges])} }};
            string x_label_{i_wp} = "p_{{T}} [GeV]";
            string text = "GN2v01,  {wp_low} < #epsilon_{{b}} < {wp_high}";
            string output_name = "sf_plots/sf_{wp.replace('%', '')}.png";
        """)

    if not os.path.exists('sf_plots'):
        os.makedirs('sf_plots')

    R.gInterpreter.ProcessLine(f"""
            plot_sf(nominal_values_{i_wp}, stats_errors_{i_wp}, total_errors_{i_wp}, pt_bin_edges_{i_wp}, x_label_{i_wp}, text, output_name);
        """)


