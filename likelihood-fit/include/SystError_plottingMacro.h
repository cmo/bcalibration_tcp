#ifndef SYSTERROR_PLOTTINGMACRO_H
#define SYSTERROR_PLOTTINGMACRO_H
int SystError_plottingMacro(std::string releaseName, std::string taggerName, std::string WPName, std::string ChannelName, std::string dataName, std::string dataLumi, std::string dataCME, std::string fitConfigName, std::vector<std::string> systNames, std::string inputfile_folder, std::string nominal_fit_combination_for_bin_mean);
#endif
