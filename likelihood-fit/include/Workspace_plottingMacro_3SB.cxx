// Plotting macro for the b-calibration analysis based on ttbar emu channel (PDF-method) //
// -------------- Written by Matthias Saimpert (matthias.saimpert@cern.ch) ------------- //
// ------------------------------------------------------------------------------------- //

// get latest AtlasStyle package and include it in the repo
// can be downloaded here: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PubComPlotStyle
#include "../atlasstyle-00-03-05/AtlasStyle.C"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <iomanip>
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TLine.h"
#include "THStack.h"
#include <THnSparse.h>
#include "TFile.h"
#include "TPaveText.h"
#include "TLegend.h"
#include "TCanvas.h"
#include <string>
#include <sstream>
//#include "RooGlobalFunc.h"
#include "RooGaussian.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooSimultaneous.h"
#include "RooParametricStepFunction.h"
#include "RooConstVar.h"
#include "RooWorkspace.h"
#include "RooCategory.h"
#include "RooUniform.h"
#include "RooExtendPdf.h"
#include "RooAddPdf.h"
#include "RooBinning.h"
#include "RooProdPdf.h"
#include "RooFit.h"
#include "TObjString.h"
#include "RooArgList.h"
#include "RooFitResult.h"
#include "RooFormulaVar.h"

using namespace std;
int Workspace_plottingMacro_3SB(std::string fileName, std::string outputFile_str)
{

  // ************************************************************************************
  // Load Workspaces and related tags, output ROOT directories and files ****************
  // ************************************************************************************

  // List of names
  std::vector<std::string> w_names = {"Prefit", "Postfit"};
  std::vector<std::string> wp_names = {"90", "85", "77","70","65"};

  // Load workspace and RooFitResult
  TFile *f_input = new TFile(fileName.c_str(),"READ");
  if(!f_input)
  {
    std::cerr << "Input file: " << fileName << " not found ... exiting." << std::endl;
    return -1;
  }

  RooFitResult *fitResult = (RooFitResult*)f_input->Get("fitResult");
  RooWorkspace *w_prefit = (RooWorkspace*)f_input->Get("Workspace_before_fit");
  RooWorkspace *w_postfit = (RooWorkspace*)f_input->Get("Workspace_after_fit");
  std::vector<RooWorkspace*> w = {w_prefit,w_postfit};

  // load strings from Workspace, i.e. for labels
  std::string dataName=((TObjString*)w_prefit->obj("dataName"))->GetString().Data();
  std::string releaseName=((TObjString*)w_prefit->obj("rlTag"))->GetString().Data();
  std::string taggerName=((TObjString*)w_prefit->obj("taggerName"))->GetString().Data();
  std::string WPName=((TObjString*)w_prefit->obj("workingPoint"))->GetString().Data();
  std::string systName=((TObjString*)w_prefit->obj("systName"))->GetString().Data();
  std::string ChannelName=((TObjString*)w_prefit->obj("channelName"))->GetString().Data();
  std::string lumi_label=((TObjString*)w_prefit->obj("lumi_label"))->GetString().Data();
  std::string MC_label=((TObjString*)w_prefit->obj("MC_label"))->GetString().Data();

  // should contain nothing out of very peculiar test
  std::string fitConfigName2=((TObjString*)w_prefit->obj("fitConfig"))->GetString().Data();
  if (!fitConfigName2.empty()) std::cout << "**WARNING** SPECIAL RUN OPTIONS DETECTED: " << fitConfigName2 << std::endl;

  // Create output file
  //std::string outputFile_str = "FitPlots_" + releaseName + "_" + taggerName + "_" + WPName + "_" + ChannelName + "_" + dataName + "_" + systName + ".root";
  TFile *f_output = new TFile(outputFile_str.c_str(),"RECREATE");
  // Create directories
  TDirectory *d_MCeff = f_output->mkdir("MCeff");

  TDirectory *d_yield = f_output->mkdir("yields");
  TDirectory *d_yield_CR = d_yield->mkdir("CR");
  TDirectory *d_yield_SF = d_yield->mkdir("SF");

  TDirectory *d_fraction = f_output->mkdir("fractions");
  TDirectory *d_fraction_CR = d_fraction->mkdir("CR");

  TDirectory *d_sf_and_beff = f_output->mkdir("sf_and_beff");

  TDirectory *d_pdf = f_output->mkdir("pdf");
  TDirectory *d_pdf_region = f_output->mkdir("pdf_region");

  TDirectory *d_data_MC = f_output->mkdir("data_MC");
  TDirectory *d_data_MC_region = f_output->mkdir("data_MC_region");

  // ************************************************************************************
  // Create TPaveText and Legends with right style **************************************
  // ************************************************************************************

  SetAtlasStyle();
  gStyle->SetPaintTextFormat("1.3f");
  gStyle->SetNumberContours(12);

  // ATLAS label, sqrt(s), lumi
  TPaveText *pt = new TPaveText(0.228354,0.841772,0.348735,0.909283,"brNDC");
  pt->SetBorderSize(0);
  pt->SetFillColor(0);
  pt->SetTextSize(0.05);
  pt->SetTextFont(72);
  pt->AddText("ATLAS");

  TPaveText *pt2 = new TPaveText(0.34384,0.840717,0.464183,0.910338,"brNDC");
  pt2->SetBorderSize(0);
  pt2->SetFillColor(0);
  pt2->SetTextSize(0.05);
  pt2->SetTextFont(42);
  pt2->AddText("Internal");

  TPaveText *pt3 = new TPaveText(0.56447,0.840717,0.683381,0.910338,"brNDC");
  pt3->SetBorderSize(0);
  pt3->SetFillColor(0);
  pt3->SetTextSize(0.05);
  pt3->SetTextFont(42);
  pt3->AddText(("#sqrt{s} = 13 TeV, "+ lumi_label +" fb^{-1}").c_str());

  TPaveText *pt4 = new TPaveText(0.570201,0.704641,0.689112,0.829114,"brNDC");
  pt4->SetBorderSize(0);
  pt4->SetFillColor(0);
  pt4->SetTextSize(0.04);
  pt4->SetTextFont(42);

  TPaveText *pt5 = new TPaveText(0.501433,0.71308,0.620344,0.820675,"brNDC");
  pt5->SetBorderSize(0);
  pt5->SetFillColor(0);
  pt5->SetTextSize(0.04);
  pt5->SetTextFont(42);

  TPaveText *pt_2d = new TPaveText(0.200401,0.843882,0.360745,0.913502,"brNDC");
  pt_2d->SetBorderSize(0);
  pt_2d->SetFillColor(0);
  pt_2d->SetTextSize(0.05);
  pt_2d->SetTextFont(72);
  pt_2d->AddText("ATLAS");
  TPaveText *pt_2d_MC = new TPaveText(0.176218,0.843158,0.336676,0.912632,"brNDC");
  pt_2d_MC->SetBorderSize(0);
  pt_2d_MC->SetFillColor(0);
  pt_2d_MC->SetTextSize(0.05);
  pt_2d_MC->SetTextFont(72);
  pt_2d_MC->AddText("ATLAS");


  TPaveText *pt2_2d = new TPaveText(0.340974,0.844937,0.461318,0.914557,"brNDC");
  pt2_2d->SetBorderSize(0);
  pt2_2d->SetFillColor(0);
  pt2_2d->SetTextSize(0.05);
  pt2_2d->SetTextFont(42);
  pt2_2d->AddText("Internal");


  TPaveText *pt2_2d_MC = new TPaveText(0.389685,0.843158,0.511461,0.912632,"brNDC");
  pt2_2d_MC->SetBorderSize(0);
  pt2_2d_MC->SetFillColor(0);
  pt2_2d_MC->SetTextSize(0.05);
  pt2_2d_MC->SetTextFont(42);
  pt2_2d_MC->AddText("Simulation Internal");

  TPaveText *pt3_2d = new TPaveText(0.548711,0.847046,0.667622,0.916667,"brNDC");
  pt3_2d->SetBorderSize(0);
  pt3_2d->SetFillColor(0);
  pt3_2d->SetTextSize(0.04);
  pt3_2d->SetTextFont(42);
  pt3_2d->AddText(("#sqrt{s} = 13 TeV, "+ lumi_label +" fb^{-1}").c_str());

  TPaveText *pt3_2d_no_lumi = new TPaveText(0.595989,0.843158,0.7149,0.912632,"brNDC");
  pt3_2d_no_lumi->SetBorderSize(0);
  pt3_2d_no_lumi->SetFillColor(0);
  pt3_2d_no_lumi->SetTextSize(0.04);
  pt3_2d_no_lumi->SetTextFont(42);
  pt3_2d_no_lumi->AddText("#sqrt{s} = 13 TeV");  

  TPaveText *pt4_2d = new TPaveText(0.342407,0.695148,0.461318,0.81962,"brNDC");
  pt4_2d->SetBorderSize(0);
  pt4_2d->SetFillColor(0);
  pt4_2d->SetTextSize(0.04);
  pt4_2d->SetTextFont(42);

  // Legend - pT plots (prefit/postfit)
  TLegend* legend_fit = new TLegend(0.206304,0.214135,0.482808,0.351266);
  legend_fit->SetTextFont(42);
  legend_fit->SetBorderSize(0);  // no border
  legend_fit->SetFillColor(0);   // Legend background should be white
  legend_fit->SetTextSize(0.04); // Increase entry font size!

  TLegend* legend_fit_bb = new TLegend(0.206304,0.214135,0.482808,0.351266);
  legend_fit_bb->SetTextFont(42);
  legend_fit_bb->SetBorderSize(0);  // no border
  legend_fit_bb->SetFillColor(0);   // Legend background should be white
  legend_fit_bb->SetTextSize(0.04); // Increase entry font size!

  TLegend* legend_fit_2d = new TLegend(0.19914,0.508439,0.434097,0.632911);
  legend_fit_2d->SetTextFont(42);
  legend_fit_2d->SetBorderSize(0);  // no border
  legend_fit_2d->SetFillColor(0);   // Legend background should be white
  legend_fit_2d->SetTextSize(0.04); // Increase entry font size!

  // Legend - tagger output plots (b, l)
  TLegend* legend_data = new TLegend(0.206304,0.214135,0.482808,0.451266);
  legend_data->SetTextFont(42);
  legend_data->SetBorderSize(0);  // no border
  legend_data->SetFillColor(0);   // Legend background should be white
  legend_data->SetTextSize(0.04); // Increase entry font size!

  // Legend - region plots
  TLegend* legend_region = new TLegend(0.22063,0.293249,0.375358,0.563291);
  legend_region->SetTextFont(42);
  legend_region->SetBorderSize(0);  // no border
  legend_region->SetFillColor(0);   // Legend background should be white
  legend_region->SetTextSize(0.04); // Increase entry font size!

  // Legend - region data raio plots
  TLegend* legend_region_ratio = new TLegend(0.88,0.2,0.95,0.4);
  legend_region_ratio->SetTextFont(42);
  legend_region_ratio->SetBorderSize(0);  // no border
  legend_region_ratio->SetFillColor(0);   // Legend background should be white
  legend_region_ratio->SetTextSize(0.04); // Increase entry font size!


  // ************************************************************************************
  // Getting number of tagger and pT bins. Create 1D and 2D template histograms *********
  // ************************************************************************************

  // [tagger vs pT] for light, b, b from ttbar
  TH2D *h_combinedMC_tagger_pt_l = (TH2D*)w_prefit->obj("hff_MC_combined_light");
  TH2D *h_combinedMC_tagger_pt_b = (TH2D*)w_prefit->obj("hff_MC_combined_b");
  TH2D *h_ttbarMC_tagger_pt_b = (TH2D*)w_prefit->obj("hff_MC_ttb_b");
  // [tagger vs pT] for b from ttbar [bb]
  TH2D *h_combinedMC_tagger_pt_bb = ((THnSparseT<TArrayD>*)w_prefit->obj("hf4_MC_combined_SR_bb"))->Projection(3,1,"E");
  h_combinedMC_tagger_pt_bb->Add(((THnSparseT<TArrayD>*)w_prefit->obj("hf4_MC_combined_SR_bb"))->Projection(2,0,"E"));
  // [tagger vs pT] for b from ttbar [b+l or l+b]
  TH2D *h_combinedMC_tagger_pt_singleb = ((THnSparseT<TArrayD>*)w_prefit->obj("hf4_MC_combined_SR_bl"))->Projection(2,0,"E");
  h_combinedMC_tagger_pt_singleb->Add(((THnSparseT<TArrayD>*)w_prefit->obj("hf4_MC_combined_SR_lb"))->Projection(3,1,"E"));

  int Nbin_tagger = h_combinedMC_tagger_pt_l->GetNbinsX();
  int Nbin_pt     = h_combinedMC_tagger_pt_l->GetNbinsY();
  int Nwp = Nbin_tagger - 1;

  // template 1D pT histogram
  TH1D *h_tmp_pt =  h_combinedMC_tagger_pt_l->ProjectionY();
  double pt_binning[Nbin_pt+1];
  for (int i_pt=0; i_pt<=Nbin_pt; i_pt++) pt_binning[i_pt] = h_tmp_pt->GetBinLowEdge(i_pt+1);

  //do we use the pt bins as eta bins?
  bool pt_binning_as_eta= (pt_binning[0]<0);
  std::string pt_label_jet1 = "p_{T,1} [GeV]";
  std::string pt_label_jet2 = "p_{T,2} [GeV]";
  std::string pt_label_single = "p_{T} [GeV]";
  if (pt_binning_as_eta){
      pt_label_jet1="#eta_{1}";
      pt_label_jet2="#eta_{2}";
      pt_label_single="#eta";
  }

  // template 2D pT1,pT2 histogram
  TH2D *h_tmp_2d_pt =  new TH2D("h_tmp_2d_pt","",Nbin_pt, 0., Nbin_pt, Nbin_pt, 0, Nbin_pt);
  for (int i_pt=0; i_pt < Nbin_pt; i_pt++)
  {
    h_tmp_2d_pt->GetXaxis()->SetBinLabel(i_pt + 1, ( std::to_string((int)pt_binning[i_pt]) + "-" + std::to_string((int)pt_binning[i_pt+1]) ).c_str());
    h_tmp_2d_pt->GetYaxis()->SetBinLabel(i_pt + 1, ( std::to_string((int)pt_binning[i_pt]) + "-" + std::to_string((int)pt_binning[i_pt+1]) ).c_str());
  }

  // template 1D tagger histogram
  double tagger_binning[10] = {100, 90, 85, 77, 70, 65, 0};
  TH1D *h_tmp_tagger = new TH1D("h_tmp_tagger","",Nbin_tagger, 0., Nbin_tagger);
  for (int i_tagger=0; i_tagger < Nbin_tagger; i_tagger++)
  {
    h_tmp_tagger->GetXaxis()->SetBinLabel(i_tagger + 1, ( std::to_string((int)tagger_binning[i_tagger]) + "-" + std::to_string((int)tagger_binning[i_tagger+1]) + "%" ).c_str());
  }

  // template 1D region histogram
  std::vector<std::string> cr_binning={"SR","CR_HL","CR_LH","CR_HH"};
  int Nregion = cr_binning.size();

  TH3D *h_ll = (TH3D*)w_prefit->obj("hf3_Ncr_MC_ll");
  TH3D *h_bb = (TH3D*)w_prefit->obj("hf3_Ncr_MC_bb");
  TH3D *h_bl = (TH3D*)w_prefit->obj("hf3_Ncr_MC_bl");
  TH3D *h_lb = (TH3D*)w_prefit->obj("hf3_Ncr_MC_lb");
  TH3D *h_tot = (TH3D*)w_prefit->obj("hf3_Ncr_MC_tot");
  TH3D *h_data = (TH3D*)w_prefit->obj("hf3_Ncr_data");

  TH1D *h_tmp_region = (TH1D*)h_ll->ProjectionZ("h_tmp_region");
  h_tmp_region->GetXaxis()->SetTitle("Region");
  h_tmp_region->GetXaxis()->SetRangeUser(0,Nregion);

  // needs to be defined everywhere
  std::vector<std::string> obs_2d_pt_names = {"f_bb", "f_bl", "f_lb", "f_ll"};
  std::vector<std::string> obs_2d_pt_labels = {"bb event fraction", "bl + bc event fraction",
                                               "lb + cb event fraction", "ll + cc fraction"};

  THnSparseT<TArrayD> *h_bb_4d = (THnSparseT<TArrayD>*)w_prefit->obj("hf4_MC_combined_SR_bb");
  THnSparseT<TArrayD> *h_bl_4d = (THnSparseT<TArrayD>*)w_prefit->obj("hf4_MC_combined_SR_bl");
  THnSparseT<TArrayD> *h_lb_4d = (THnSparseT<TArrayD>*)w_prefit->obj("hf4_MC_combined_SR_lb");
  THnSparseT<TArrayD> *h_ll_4d = (THnSparseT<TArrayD>*)w_prefit->obj("hf4_MC_combined_SR_ll");
  THnSparseT<TArrayD> *h_tot_4d = (THnSparseT<TArrayD>*)w_prefit->obj("hf4_MC_combined_SR_tot");
  THnSparseT<TArrayD> *h_data_4d = (THnSparseT<TArrayD>*)w_prefit->obj("hf4_data_SR");

  // ***************************************************************************
  // get prefit l, b, and b in ttbar efficiencies for each WP with MC stat. unc.
  // ----> MCeff directory *****************************************************
  // ***************************************************************************

  // vectors with each WP
  std::vector<TH1D*> hv_prefit_effMC_l;
  std::vector<TH1D*> hv_prefit_effMC_b;
  std::vector<TH1D*> hv_prefit_effMC_ttbar_b;

  // 1D pT histograms
  TH1D *h_tot_l = h_combinedMC_tagger_pt_l->ProjectionY("h_tot_l", 0, -1,"E" );
  TH1D *h_tot_b = h_combinedMC_tagger_pt_b->ProjectionY("h_tot_b", 0, -1,"E" );
  TH1D *h_tot_ttbar_b = h_ttbarMC_tagger_pt_b->ProjectionY("h_tot_ttbar_b", 0, -1,"E" );

  // loop on WPs
  for(int i_wp=0; i_wp < Nwp; i_wp++)
  {
    // tmp 2D tagger/pT histograms ...
    TH2D *h_combinedMC_tagger_pt_l_pass = (TH2D*)h_combinedMC_tagger_pt_l->Clone( ("h_combinedMC_tagger_pt_l_" + wp_names.at(i_wp) + "wp").c_str() );
    TH2D *h_combinedMC_tagger_pt_b_pass = (TH2D*)h_combinedMC_tagger_pt_b->Clone( ("h_combinedMC_tagger_pt_b_" + wp_names.at(i_wp) + "wp").c_str() );
    TH2D *h_ttbarMC_tagger_pt_b_pass = (TH2D*)h_ttbarMC_tagger_pt_b->Clone( ("h_ttbarMC_tagger_pt_b_" + wp_names.at(i_wp) + "wp").c_str() );

    // ... with bins out from WP definition set to 0
    for(int i_tagger=0; i_tagger < Nbin_tagger - (Nwp-i_wp); i_tagger++)
    {
      for(int i_pt=0; i_pt < Nbin_pt; i_pt++)
      {
        h_combinedMC_tagger_pt_l_pass->SetBinContent(i_tagger+1, i_pt+1, 0);
        h_combinedMC_tagger_pt_b_pass->SetBinContent(i_tagger+1, i_pt+1, 0);
        h_ttbarMC_tagger_pt_b_pass->SetBinContent(i_tagger+1, i_pt+1, 0);
        h_combinedMC_tagger_pt_l_pass->SetBinError(i_tagger+1, i_pt+1, 0);
        h_combinedMC_tagger_pt_b_pass->SetBinError(i_tagger+1, i_pt+1, 0);
        h_ttbarMC_tagger_pt_b_pass->SetBinError(i_tagger+1, i_pt+1, 0);
      }
    }

    // projection of these guys on pT
    TH1D *h_effMC_l = h_combinedMC_tagger_pt_l_pass->ProjectionY( ("h_effMC_l_" + wp_names.at(i_wp) + "wp").c_str(), 0, -1,"E" );
    TH1D *h_effMC_b = h_combinedMC_tagger_pt_b_pass->ProjectionY( ("h_effMC_b_" + wp_names.at(i_wp) + "wp").c_str(), 0, -1,"E" );
    TH1D *h_effMC_ttbar_b = h_ttbarMC_tagger_pt_b_pass->ProjectionY( ("h_effMC_ttbar_b_" + wp_names.at(i_wp) + "wp").c_str(), 0, -1,"E" );

    // get efficiencies, use of binomial errors
    h_effMC_l->Divide(h_effMC_l, h_tot_l, 1, 1, "B");
    h_effMC_b->Divide(h_effMC_b, h_tot_b, 1, 1, "B");
    h_effMC_ttbar_b->Divide(h_effMC_ttbar_b, h_tot_ttbar_b, 1, 1, "B");

    // push back and savings
    hv_prefit_effMC_l.push_back(h_effMC_l);
    hv_prefit_effMC_b.push_back(h_effMC_b);
    hv_prefit_effMC_ttbar_b.push_back(h_effMC_ttbar_b);

    f_output->cd();
    d_MCeff->cd();
    h_effMC_l->Write();
    h_effMC_b->Write();
    h_effMC_ttbar_b->Write();
  }


  // Loop on regions
  for (int i_region=0; i_region<Nregion; i_region++)
  {
    // ***************************************************************************
    // get total counts in SR and CR prefit and postfit **************************
    // ----> yield directory *****************************************************
    // ***************************************************************************

    std::cout << std::endl << "yield (pT1, pT2) plots for " << cr_binning.at(i_region) << " ...";

    // select region
    h_ll->GetZaxis()->SetRange(i_region+1, i_region+1);
    h_bb->GetZaxis()->SetRange(i_region+1, i_region+1);
    h_bl->GetZaxis()->SetRange(i_region+1, i_region+1);
    h_lb->GetZaxis()->SetRange(i_region+1, i_region+1);
    h_tot->GetZaxis()->SetRange(i_region+1, i_region+1);

    // get prefit for ll, bb, bl and lb fractions in pT1,pT2
    TH2D *h_ll_region = (TH2D*)h_ll->Project3D("yxe");
    TH2D *h_bb_region = (TH2D*)h_bb->Project3D("yxe");
    TH2D *h_bl_region = (TH2D*)h_bl->Project3D("yxe");
    TH2D *h_lb_region = (TH2D*)h_lb->Project3D("yxe");
    TH2D *h_mc_prefit_region_tmp = (TH2D*)h_tot->Project3D("yxe");

    h_ll_region->SetName( ("h_ll_N_" + cr_binning.at(i_region)).c_str() );
    h_bb_region->SetName( ("h_bb_N_" + cr_binning.at(i_region)).c_str() );
    h_bl_region->SetName( ("h_bl_N_" + cr_binning.at(i_region)).c_str() );
    h_lb_region->SetName( ("h_lb_N_" + cr_binning.at(i_region)).c_str() );
    h_mc_prefit_region_tmp->SetName( ("h_mc_prefit_region_" + cr_binning.at(i_region) + "_tmp").c_str() );

    // get data counts in pT1, pT2
    h_data->GetZaxis()->SetRange(i_region+1, i_region+1);
    TH2D *h_data_region_tmp = (TH2D*)h_data->Project3D("yxe");
    h_data_region_tmp->SetName( ("h_data_N_" + cr_binning.at(i_region) + "_tmp").c_str() );

    // New histogram for data
    TH2D *h_data_region = (TH2D*)h_tmp_2d_pt->Clone(("h_data_region_" + cr_binning.at(i_region)).c_str());
    h_data_region->SetStats(0);
    h_data_region->Reset();
    h_data_region->SetTitle("");
    h_data_region->SetXTitle(pt_label_jet1.c_str());
    h_data_region->SetYTitle(pt_label_jet2.c_str());
    h_data_region->SetZTitle( ("N_{data} in " + cr_binning.at(i_region)).c_str() );
    h_data_region->GetXaxis()->SetLabelSize(0.05);
    h_data_region->GetXaxis()->SetTitleSize(0.05);
    h_data_region->GetXaxis()->SetTitleOffset(1.55);
    h_data_region->GetXaxis()->SetLabelOffset(0.01);
    h_data_region->GetYaxis()->SetLabelSize(0.05);
    h_data_region->GetYaxis()->SetTitleSize(0.05);
    h_data_region->GetYaxis()->SetTitleOffset(1.55);
    h_data_region->GetZaxis()->SetTitleOffset(1.3);
    h_data_region->SetMarkerColor(kBlack);
    h_data_region->SetMarkerSize(1);
    h_data_region->SetFillColor(kBlack);
    h_data_region->SetLineColor(kBlack);
    h_data_region->SetBarOffset(+0.22);

    // New histograms for MC tot prefit/postfit
    TH2D *h_mc_prefit_region = (TH2D*)h_tmp_2d_pt->Clone( ("h_mc_prefit_region_" + cr_binning.at(i_region) ).c_str() );
    h_mc_prefit_region->SetStats(0);
    h_mc_prefit_region->Reset();
    h_mc_prefit_region->SetTitle("");
    h_mc_prefit_region->SetXTitle(pt_label_jet1.c_str());
    h_mc_prefit_region->SetYTitle(pt_label_jet2.c_str());
    h_mc_prefit_region->SetZTitle( ("N_{mc_prefit} in " + cr_binning.at(i_region)).c_str() );
    h_mc_prefit_region->GetXaxis()->SetLabelSize(0.05);
    h_mc_prefit_region->GetXaxis()->SetTitleSize(0.05);
    h_mc_prefit_region->GetXaxis()->SetTitleOffset(1.55);
    h_mc_prefit_region->GetXaxis()->SetLabelOffset(0.01);
    h_mc_prefit_region->GetYaxis()->SetLabelSize(0.05);
    h_mc_prefit_region->GetYaxis()->SetTitleSize(0.05);
    h_mc_prefit_region->GetYaxis()->SetTitleOffset(1.55);
    h_mc_prefit_region->GetZaxis()->SetTitleOffset(1.3);
    h_mc_prefit_region->SetMarkerColor(kBlack);
    h_mc_prefit_region->SetMarkerSize(1);
    h_mc_prefit_region->SetFillColor(kBlack);
    h_mc_prefit_region->SetLineColor(kBlack);
    h_mc_prefit_region->SetBarOffset(+0.22);

    TH2D *h_mc_postfit_region = (TH2D*)h_tmp_2d_pt->Clone( ("h_mc_postfit_region" + cr_binning.at(i_region)).c_str() );
    h_mc_postfit_region->SetStats(0);
    h_mc_postfit_region->Reset();
    h_mc_postfit_region->SetTitle("");
    h_mc_postfit_region->SetXTitle(pt_label_jet1.c_str());
    h_mc_postfit_region->SetYTitle(pt_label_jet2.c_str());
    h_mc_postfit_region->SetZTitle( ("N_{mc_postfit} in " + cr_binning.at(i_region)).c_str() );
    h_mc_postfit_region->GetXaxis()->SetLabelSize(0.05);
    h_mc_postfit_region->GetXaxis()->SetTitleSize(0.05);
    h_mc_postfit_region->GetXaxis()->SetTitleOffset(1.55);
    h_mc_postfit_region->GetXaxis()->SetLabelOffset(0.01);
    h_mc_postfit_region->GetYaxis()->SetLabelSize(0.05);
    h_mc_postfit_region->GetYaxis()->SetTitleSize(0.05);
    h_mc_postfit_region->GetYaxis()->SetTitleOffset(1.55);
    h_mc_postfit_region->GetZaxis()->SetTitleOffset(1.3);
    h_mc_postfit_region->SetMarkerColor(kRed+1);
    h_mc_postfit_region->SetMarkerSize(1);
    h_mc_postfit_region->SetFillColor(kRed+1);
    h_mc_postfit_region->SetLineColor(kRed+1);
    h_mc_postfit_region->SetBarOffset(-0.22);

    // filling final histo
    for(int i_pt1=0; i_pt1 < Nbin_pt; i_pt1++)
    {
      for(int i_pt2=0; i_pt2 <= i_pt1; i_pt2++)
      {
        h_data_region->SetBinContent(i_pt1+1, i_pt2+1, h_data_region_tmp->GetBinContent(i_pt1+1, i_pt2+1));
        h_data_region->SetBinError(i_pt1+1, i_pt2+1, h_data_region_tmp->GetBinError(i_pt1+1, i_pt2+1));

        h_mc_prefit_region->SetBinContent(i_pt1+1, i_pt2+1, h_mc_prefit_region_tmp->GetBinContent(i_pt1+1, i_pt2+1));
        h_mc_prefit_region->SetBinError(i_pt1+1, i_pt2+1, h_mc_prefit_region_tmp->GetBinError(i_pt1+1, i_pt2+1));

        h_mc_postfit_region->SetBinContent(i_pt1+1, i_pt2+1, w_postfit->function(("N_tot_" + cr_binning.at(i_region) +"_pt_"+to_string(i_pt1+1)+"_"+to_string(i_pt2+1)).c_str())->getValV());
        h_mc_postfit_region->SetBinError(i_pt1+1, i_pt2+1, w_postfit->function(("N_tot_" + cr_binning.at(i_region) + "_pt_"+to_string(i_pt1+1)+"_"+to_string(i_pt2+1)).c_str())->getPropagatedError(*fitResult));
      }
    }

    // record histogram
    f_output->cd();
    d_yield->cd();
    if(cr_binning.at(i_region).find("SR") ==std::string::npos) d_yield_CR->cd();

    h_data_region->Write();
    h_mc_prefit_region->Write();
    h_mc_postfit_region->Write();

    TCanvas *c_data_region = new TCanvas( ("c_data_region_" + cr_binning.at(i_region)).c_str() );
    c_data_region->SetLeftMargin(0.15);
    c_data_region->SetRightMargin(0.20);
    h_data_region->Draw("TEXTCOLZE");
    h_mc_postfit_region->Draw("TEXTESAME");
    h_data_region->Draw("AXISSAME");
    pt_2d->Draw();
    pt2_2d->Draw();
    pt3_2d->Draw();
    pt4_2d->Clear();
    pt4_2d->AddText( (releaseName + ", " + ChannelName + ", " + dataName).c_str() );
    pt4_2d->Draw();
    legend_fit_2d->Clear();
    legend_fit_2d->AddEntry(h_data_region,"Data", "F");
    legend_fit_2d->AddEntry(h_mc_postfit_region,"MC Postfit", "F");
    legend_fit_2d->Draw();
    c_data_region->Write();

    TCanvas *c_mc_region = new TCanvas( ("c_mc_region_" + cr_binning.at(i_region)).c_str() );
    c_mc_region->SetLeftMargin(0.15);
    c_mc_region->SetRightMargin(0.20);
    h_mc_prefit_region->Draw("TEXTCOLZE");
    h_mc_postfit_region->Draw("TEXTESAME");
    h_mc_prefit_region->Draw("AXISSAME");
    pt_2d->Draw();
    pt2_2d->Draw();
    pt3_2d->Draw();
    pt4_2d->Clear();
    pt4_2d->AddText( (releaseName + ", " + ChannelName + ", " + dataName).c_str() );
    pt4_2d->Draw();
    legend_fit_2d->Clear();
    legend_fit_2d->AddEntry(h_mc_prefit_region,"MC Prefit", "F");
    legend_fit_2d->AddEntry(h_mc_postfit_region,"MC Postfit", "F");
    legend_fit_2d->Draw();
    c_mc_region->Write();
    legend_fit_2d->Clear();

    std::cout << " done." << std::endl;


    // ***************************************************************************
    // get bb/bl/lb/ll fractions in SR and CR prefit and postfit *****************
    // ----> fractions directory *************************************************
    // ***************************************************************************

    std::cout << "fractions (pT1, pT2) plots in " << cr_binning.at(i_region) << " ...";

    // convert yields in fraction
    h_ll_region->Divide(h_ll_region,h_mc_prefit_region_tmp,1,1,"B");
    h_bb_region->Divide(h_bb_region,h_mc_prefit_region_tmp,1,1,"B");
    h_bl_region->Divide(h_bl_region,h_mc_prefit_region_tmp,1,1,"B");
    h_lb_region->Divide(h_lb_region,h_mc_prefit_region_tmp,1,1,"B");


    // loop on fraction type
    for(int i_2d_plots=0; i_2d_plots < obs_2d_pt_names.size(); i_2d_plots++)
    {
      // create new template 2D pT canvas (which will include prefit+postfit)
      std::string canvasname = "c_" + cr_binning.at(i_region) + "_" + obs_2d_pt_names.at(i_2d_plots);
      TCanvas *c_obs = new TCanvas(canvasname.c_str());
      c_obs->SetLeftMargin(0.15);
      c_obs->SetRightMargin(0.15);

      //TH2D *h_prefit for later:
      TH2D *h_prefit=0; 
      // prefit, postfit loop
      for (int i_w=0; i_w < w.size(); i_w++)
      {
        // create new template 2D pT histogram
        std::string histname = obs_2d_pt_names.at(i_2d_plots) + "_" + cr_binning.at(i_region) + "_" + w_names.at(i_w);
        TH2D *h_obs = (TH2D*)h_tmp_2d_pt->Clone(histname.c_str());
        h_obs->SetStats(0);
        h_obs->Reset();
        h_obs->SetTitle("");
        h_obs->SetXTitle(pt_label_jet1.c_str());
        h_obs->SetYTitle(pt_label_jet2.c_str());
        h_obs->SetZTitle( (obs_2d_pt_labels.at(i_2d_plots) + " in " + cr_binning.at(i_region)).c_str() );
        h_obs->GetXaxis()->SetLabelSize(0.05);
        h_obs->GetXaxis()->SetTitleSize(0.05);
        h_obs->GetXaxis()->SetTitleOffset(1.55);
        h_obs->GetXaxis()->SetLabelOffset(0.01);
        h_obs->GetYaxis()->SetLabelSize(0.05);
        h_obs->GetYaxis()->SetTitleSize(0.05);
        h_obs->GetYaxis()->SetTitleOffset(1.55);
        h_obs->GetZaxis()->SetTitleOffset(0.95);

        if(w_names.at(i_w)=="Prefit")
        {
          h_obs->SetMarkerColor(kBlack);
          h_obs->SetFillColor(kBlack);
          h_obs->SetLineColor(kBlack);
          h_obs->SetBarOffset(+0.22);
        }
        else
        {
          h_obs->SetMarkerColor(kRed+1);
          h_obs->SetFillColor(kRed+1);
          h_obs->SetLineColor(kRed+1);
          h_obs->SetBarOffset(-0.22);
        }

        if(i_w==0 && i_2d_plots==0) legend_fit_2d->AddEntry(h_obs,"Prefit", "F");
        else if(i_w==1 && i_2d_plots==0) legend_fit_2d->AddEntry(h_obs,"Postfit", "F");

        // fill histogram
        for(int i_pt1=0; i_pt1 < Nbin_pt; i_pt1++)
        {
          for(int i_pt2=0; i_pt2 <= i_pt1; i_pt2++)
          {
            // obs
            std::string obs_name = obs_2d_pt_names.at(i_2d_plots) + "_" + cr_binning.at(i_region) +  "_pt_" + std::to_string(i_pt1+1) + "_" + std::to_string(i_pt2+1);
            RooFormulaVar *f_xx = (RooFormulaVar*)w.at(i_w)->function(obs_name.c_str());

            double obs = f_xx->getValV();
            double obs_unc = 0.;

            if(i_w==0 || !(f_xx->getPropagatedError(*fitResult)))
            {
              if(obs_2d_pt_names.at(i_2d_plots).find("_ll") !=std::string::npos) obs_unc = h_ll_region->GetBinError(i_pt1+1, i_pt2+1);
              else if(obs_2d_pt_names.at(i_2d_plots).find("_bb") !=std::string::npos) obs_unc = h_bb_region->GetBinError(i_pt1+1, i_pt2+1);
              else if(obs_2d_pt_names.at(i_2d_plots).find("_bl") !=std::string::npos) obs_unc = h_bl_region->GetBinError(i_pt1+1, i_pt2+1);
              else if(obs_2d_pt_names.at(i_2d_plots).find("_lb") !=std::string::npos) obs_unc = h_lb_region->GetBinError(i_pt1+1, i_pt2+1);
            }
            else if(i_w==1) obs_unc = f_xx->getPropagatedError(*fitResult);

            h_obs->SetBinContent(i_pt1+1, i_pt2+1, obs);
            h_obs->SetBinError(i_pt1+1, i_pt2+1, obs_unc);
          }
        }

        // record histogram
        f_output->cd();
        d_fraction->cd();
        if(cr_binning.at(i_region).find("SR") ==std::string::npos) d_fraction_CR->cd();
        h_obs->Write();

        // draw on canvas
        c_obs->cd();
        if(w_names.at(i_w)=="Prefit") {
          h_obs->Draw("TEXTCOLZE");
          h_prefit = (TH2D*) h_obs->Clone((histname + "_prefit_only").c_str());
        }
        else h_obs->Draw("TEXTESAME");
        h_obs->Draw("AXISSAME");

      } // end prefit+postfit

      // tune and record canvas
      c_obs->cd();
      pt_2d->Draw();
      pt2_2d->Draw();
      pt3_2d->Draw();
      pt4_2d->Clear();
      pt4_2d->AddText( (releaseName + ", " + ChannelName + ", " + systName + ", " + dataName).c_str() );
      pt4_2d->AddText( (taggerName + ", " + WPName).c_str() );
      pt4_2d->Draw();
      legend_fit_2d->Draw();
      c_obs->Write();

      //create Prefit only plot:
      c_obs = new TCanvas((canvasname+"_prefit_only" ).c_str());
      c_obs->SetLeftMargin(0.15);
      c_obs->SetRightMargin(0.15);
      h_prefit->GetZaxis()->SetRangeUser(0. , 1.);
      h_prefit->SetBarOffset(0.);
      h_prefit->Draw("TEXTCOLZE");
      pt_2d_MC->Draw();
      pt2_2d_MC->Draw();
      pt3_2d_no_lumi->Draw();
      pt4_2d->Clear();
      pt4_2d->AddText( (releaseName + ", " + ChannelName + ", " + systName + ", " + MC_label).c_str() );
      pt4_2d->AddText( (taggerName + ", " + WPName).c_str() );
      pt4_2d->Draw();
      pt4_2d->Draw();
      c_obs->Write();



    } // end fraction plots

    std::cout << " done." << std::endl;

  } // end region loop

  // ***************************************************************************
  // get bb/bl/lb/ll SF postfit (prefit is 1 by def) ***************************
  // ----> yield/SF directory *************************************************
  // ***************************************************************************

  std::vector<std::string> obs_2d_pt_sf_names = {"sf_bb", "sf_bl", "sf_lb", "sf_ll"};
  std::vector<std::string> obs_2d_pt_sf_labels = {"bb yield SF", "bl + bc yield SF",
                                                  "lb + cb yield SF", "ll + cc yield SF"};

  // loop on fraction type
  for(int i_2d_plots=0; i_2d_plots < obs_2d_pt_sf_names.size(); i_2d_plots++)
  {
    // create new template 2D pT canvas (postfit only since prefit is 1 by def)
    std::string canvasname = "c_" + obs_2d_pt_sf_names.at(i_2d_plots);
    TCanvas *c_obs = new TCanvas(canvasname.c_str());
    c_obs->SetLeftMargin(0.15);
    c_obs->SetRightMargin(0.15);

    // create new template 2D pT histogram
    std::string histname = obs_2d_pt_sf_names.at(i_2d_plots);
    TH2D *h_obs = (TH2D*)h_tmp_2d_pt->Clone(histname.c_str());
    h_obs->SetStats(0);
    h_obs->Reset();
    h_obs->SetTitle("");
    h_obs->SetXTitle(pt_label_jet1.c_str());
    h_obs->SetYTitle(pt_label_jet2.c_str());
    h_obs->SetZTitle( obs_2d_pt_sf_labels.at(i_2d_plots).c_str() );
    h_obs->GetXaxis()->SetLabelSize(0.05);
    h_obs->GetXaxis()->SetTitleSize(0.05);
    h_obs->GetXaxis()->SetTitleOffset(1.55);
    h_obs->GetXaxis()->SetLabelOffset(0.01);
    h_obs->GetYaxis()->SetLabelSize(0.05);
    h_obs->GetYaxis()->SetTitleSize(0.05);
    h_obs->GetYaxis()->SetTitleOffset(1.55);
    h_obs->GetZaxis()->SetTitleOffset(0.95);
    h_obs->SetMarkerColor(kBlack);
    h_obs->SetFillColor(kBlack);
    h_obs->SetLineColor(kBlack);

    if(i_2d_plots==0)
    {
      legend_fit_2d->Clear();
      legend_fit_2d->AddEntry(h_obs,"Postfit", "F");
    }

    // fill histogram
    for(int i_pt1=0; i_pt1 < Nbin_pt; i_pt1++)
    {
      for(int i_pt2=0; i_pt2 <= i_pt1; i_pt2++)
      {
        // obs
        std::string obs_name = obs_2d_pt_sf_names.at(i_2d_plots) + "_pt_" + std::to_string(i_pt1+1) + "_" + std::to_string(i_pt2+1);
        RooFormulaVar *SF_xx = (RooFormulaVar*)w.at(1)->function(obs_name.c_str());

        double obs = SF_xx->getValV();
        double obs_unc = 0.;

        if(!(SF_xx->getPropagatedError(*fitResult))) std::cout << "WARNING: uncertainties on yield-SF missing, set to 0" << std::endl;
        else obs_unc = SF_xx->getPropagatedError(*fitResult);

        h_obs->SetBinContent(i_pt1+1, i_pt2+1, obs);
        h_obs->SetBinError(i_pt1+1, i_pt2+1, obs_unc);
      }
    }

    // record histogram
    f_output->cd();
    d_yield_SF->cd();
    h_obs->Write();

    // draw on canvas
    c_obs->cd();
    //h_obs->Draw("TEXTCOLZE");
    TH2D *h_obs_big_range = (TH2D*)h_obs->Clone((histname+"_big_range").c_str());
    h_obs_big_range->GetZaxis()->SetRangeUser(-10,10);
    h_obs->GetZaxis()->SetRangeUser(0,2);
    h_obs->Draw("COLZ");
    h_obs_big_range->Draw("TEXTESAME");
    h_obs->Draw("AXISSAME");


    // tune and record canvas
    c_obs->cd();
    pt_2d->Draw();
    pt2_2d->Draw();
    pt3_2d->Draw();
    pt4_2d->Clear();
    pt4_2d->AddText( (releaseName + ", " + ChannelName + ", " + systName + ", " + dataName).c_str() );
    pt4_2d->AddText( (taggerName + ", " + WPName).c_str() );
    pt4_2d->Draw();
    legend_fit_2d->Draw();
    c_obs->Write();

  } // end fraction plots


  // ***************************************************************************
  // get 1D plots vs pT in SR prefit and postfit *******************************
  // ----> sf_and_beff directory ***********************************************
  // ***************************************************************************

  // 1D plots vs pT (prefit, postfit): SF-b, eff-b
  std::cout << std::endl << "1D pT plots (b-SF, b-eff) ...";
  std::vector<std::string> obs_pt_names = {"sf_b", "e_b"};
  std::vector<std::string> obs_pt_labels = {"b-efficiency SF (data / MC ratio)", "b-jet identification efficiency"}; // SF = b-tagging efficiency in data / b-tagging efficiency in total MC

  // loop on WP
  for(int i_wp=0; i_wp < Nwp; i_wp++)
  {
    // loop on plots
    for(int i_plots=0; i_plots < obs_pt_names.size(); i_plots++)
    {
      // create new template pT canvas (combine prefit+postfit)
      std::string canvasname = "c_" + obs_pt_names.at(i_plots) + "_" + wp_names.at(i_wp);
      TCanvas *c_obs = new TCanvas(canvasname.c_str());
      if (h_tmp_pt->GetBinLowEdge(1)>0){
          c_obs->SetLogx();
      }

      for (int i_w=0; i_w < w.size(); i_w++) // prefit+postfit
      {
        // create new template 1D pT histogram
        std::string histname = obs_pt_names.at(i_plots) + "_" + wp_names.at(i_wp) + "_" + w_names.at(i_w);
        TH1D *h_obs = (TH1D*)h_tmp_pt->Clone(histname.c_str());
        h_obs->SetStats(0);
        h_obs->Reset();
        h_obs->SetTitle("");
        h_obs->SetXTitle(pt_label_single.c_str());
        h_obs->SetYTitle(obs_pt_labels.at(i_plots).c_str());
        h_obs->SetMaximum(1.5);
        h_obs->SetMinimum(0);
        h_obs->GetXaxis()->SetLabelSize(0.05);
        h_obs->GetXaxis()->SetTitleSize(0.05);
        h_obs->GetXaxis()->SetTitleOffset(1.4);
        h_obs->SetMarkerStyle(20);

        if(i_w==0 && i_plots==0 && i_wp==0)
        {
          legend_fit->AddEntry(h_obs,"Prefit", "LPE");
          legend_fit_bb->AddEntry(h_obs,"Prefit", "LPE");
        }
        else if(i_w==1 && i_plots==0 && i_wp==0)
        {
          legend_fit->AddEntry(h_obs,"Postfit", "LPE");
          legend_fit_bb->AddEntry(h_obs,"Postfit", "LPE");
        }

        if(w_names.at(i_w)=="Prefit")
        {
          h_obs->SetLineColor(kGreen-8);
          h_obs->SetMarkerColor(kGreen-8);
        }
        else
        {
          h_obs->SetLineColor(kRed+1);
          h_obs->SetMarkerColor(kRed+1);
        }

        // fill histogram
        for(int i_pt=0; i_pt < Nbin_pt; i_pt++)
        {
          std::string obs_name = obs_pt_names.at(i_plots) + "_pt_" + std::to_string(i_pt+1) + "_tagger_" + std::to_string(i_wp+2);
          double obs = ((RooFormulaVar*)w.at(i_w)->function(obs_name.c_str()))->getValV();

          double obs_unc = 0.;
          if(i_w==0 || !((RooFormulaVar*)w.at(i_w)->function(obs_name.c_str()))->getPropagatedError(*fitResult)) obs_unc = obs * hv_prefit_effMC_b.at(i_wp)->GetBinError(i_pt+1)/hv_prefit_effMC_b.at(i_wp)->GetBinContent(i_pt+1);
          else if(i_w==1) obs_unc = ((RooFormulaVar*)w.at(i_w)->function(obs_name.c_str()))->getPropagatedError(*fitResult);

          h_obs->SetBinContent(i_pt+1, obs);
          h_obs->SetBinError(i_pt+1, obs_unc);
        }

        // record histogram
        f_output->cd();
        d_sf_and_beff->cd();
        h_obs->Write();

        // draw on canvas
        c_obs->cd();
        h_obs->Draw("LPESAME");

      } // end prefit+postfit

      // tune and record canvas
      c_obs->cd();
      pt->Draw();
      pt2->Draw();
      pt3->Draw();
      pt4->Clear();
      pt4->AddText( (releaseName + ", " + ChannelName + ", " + systName + ", " + dataName).c_str() );
      pt4->AddText( (taggerName + ", " + WPName + ", #epsilon_{b}^{MC} = " + wp_names.at(i_wp) + "%").c_str() );
      pt4->Draw();
      legend_fit->Draw();
      f_output->cd();
      d_sf_and_beff->cd();
      c_obs->Write();
    } // end loop on plots
  } // end wp

  std::cout << " done." << std::endl;


  // ***************************************************************************
  // get 1D tagger output PDF for bb/bl/lb/ll/data in SR, pT1, pT2 prefit and postfit
  // ----> pdf directory *******************************************************
  // ***************************************************************************

  std::cout << "1D tagger output PDF plots ...";
  std::vector<std::string> obs_tagger_names = {"p_b", "p_l"};
  std::vector<std::string> obs_tagger_labels = {"b-jet PDF", "c- + light-jet PDF"};

  // loop on pT
  for(int i_pt=0; i_pt < Nbin_pt; i_pt++)
  {
    // average b-PDF from bb MC events for extra reference
    std::string histname_bb = "p_b_bb_pt_" + std::to_string(i_pt+1) + "_MC";
    TH1D *p_b_bb_tmp = h_combinedMC_tagger_pt_bb->ProjectionX( (histname_bb + "_tmp").c_str(), i_pt+1, i_pt+1, "E");

    TH1D *p_b_bb_tmp_all = (TH1D*)p_b_bb_tmp->Clone( (histname_bb + "_tmp_all").c_str() );

    // correct error computation for normalized distribution
    double error = 0;
    double integral = p_b_bb_tmp->IntegralAndError(1, p_b_bb_tmp->GetNbinsX(), error);
    for(int i_tagger=0; i_tagger < Nbin_tagger; i_tagger++)
    {
      p_b_bb_tmp_all->SetBinContent(i_tagger+1, integral);
      p_b_bb_tmp_all->SetBinError(i_tagger+1, error);
    }
    p_b_bb_tmp->Divide(p_b_bb_tmp, p_b_bb_tmp_all, 1., 1., "B");

    TH1D *p_b_bb = (TH1D*)h_tmp_tagger->Clone(histname_bb.c_str());
    p_b_bb->SetStats(0);
    p_b_bb->Reset();
    p_b_bb->SetLineStyle(2);
    p_b_bb->SetLineColor(kGray+2);
    p_b_bb->SetMarkerStyle(20);
    p_b_bb->SetMarkerSize(0);

    // average b-PDF from single b (bl+lb) MC events for extra reference
    std::string histname_singleb = "p_b_singleb_pt_" + std::to_string(i_pt+1) + "_MC";
    TH1D *p_b_singleb_tmp = h_combinedMC_tagger_pt_singleb->ProjectionX( (histname_singleb + "_tmp").c_str(), i_pt+1, i_pt+1, "E");

    TH1D *p_b_singleb_tmp_all = (TH1D*)p_b_singleb_tmp->Clone( (histname_singleb + "_tmp_all").c_str() );

    // correct error computation for normalized distribution
    error = 0;
    integral = p_b_singleb_tmp->IntegralAndError(1, p_b_singleb_tmp->GetNbinsX(), error);
    for(int i_tagger=0; i_tagger < Nbin_tagger; i_tagger++)
    {
      p_b_singleb_tmp_all->SetBinContent(i_tagger+1, integral);
      p_b_singleb_tmp_all->SetBinError(i_tagger+1, error);
    }
    p_b_singleb_tmp->Divide(p_b_singleb_tmp, p_b_singleb_tmp_all, 1., 1., "B");

    TH1D *p_b_singleb = (TH1D*)h_tmp_tagger->Clone(histname_singleb.c_str());
    p_b_singleb->SetStats(0);
    p_b_singleb->Reset();
    p_b_singleb->SetLineStyle(2);
    p_b_singleb->SetLineColor(kBlue+1);
    p_b_singleb->SetMarkerStyle(20);
    p_b_singleb->SetMarkerSize(0);

    // average b-PDF from ttbar MC events (flavor of accompanying jet does not matter)
    // for extra reference
    std::string histname_ttb = "p_b_ttb_pt_" + std::to_string(i_pt+1) + "_MC";
    TH1D *p_b_ttb_tmp = h_ttbarMC_tagger_pt_b->ProjectionX( (histname_ttb + "_tmp").c_str(), i_pt+1, i_pt+1, "E");
    TH1D *p_b_ttb_tmp_all = (TH1D*)p_b_ttb_tmp->Clone( (histname_ttb + "_tmp_all").c_str() );

    // correct error computation for normalized distribution
    error = 0;
    integral = p_b_ttb_tmp->IntegralAndError(1, p_b_ttb_tmp->GetNbinsX(), error);

    for(int i_tagger=0; i_tagger < Nbin_tagger; i_tagger++)
    {
      p_b_ttb_tmp_all->SetBinContent(i_tagger+1, integral);
      p_b_ttb_tmp_all->SetBinError(i_tagger+1, error);
    }
    p_b_ttb_tmp->Divide(p_b_ttb_tmp, p_b_ttb_tmp_all, 1., 1., "B");

    TH1D *p_b_ttb = (TH1D*)h_tmp_tagger->Clone(histname_ttb.c_str());
    p_b_ttb->SetStats(0);
    p_b_ttb->Reset();
    p_b_ttb->SetLineStyle(2);
    p_b_ttb->SetLineColor(kOrange-8);
    p_b_ttb->SetMarkerStyle(20);
    p_b_ttb->SetMarkerSize(0);

    // Fill of all 3 histograms
    for(int i_tagger=0; i_tagger < Nbin_tagger; i_tagger++)
    {
      p_b_bb->SetBinContent(i_tagger+1, p_b_bb_tmp->GetBinContent(i_tagger+1));
      p_b_bb->SetBinError(i_tagger+1, p_b_bb_tmp->GetBinError(i_tagger+1));

      p_b_singleb->SetBinContent(i_tagger+1, p_b_singleb_tmp->GetBinContent(i_tagger+1));
      p_b_singleb->SetBinError(i_tagger+1, p_b_singleb_tmp->GetBinError(i_tagger+1));

      p_b_ttb->SetBinContent(i_tagger+1, p_b_ttb_tmp->GetBinContent(i_tagger+1));
      p_b_ttb->SetBinError(i_tagger+1, p_b_ttb_tmp->GetBinError(i_tagger+1));
    }

    f_output->cd();
    d_pdf->cd();
    p_b_bb->Write();
    p_b_singleb->Write();
    p_b_ttb->Write();

    // loop on plots
    for(int i_plots=0; i_plots < obs_tagger_names.size(); i_plots++)
    {
      // create new template tagger canvas (combine prefit+postfit)
      std::string canvasname = "c_" + obs_tagger_names.at(i_plots) + "_pt_" + std::to_string(i_pt+1);
      TCanvas *c_obs = new TCanvas(canvasname.c_str());
      c_obs->SetLogy();

      for (int i_w=0; i_w < w.size(); i_w++) // prefit+postfit
      {
        // create new template tagger histogram
        std::string histname = obs_tagger_names.at(i_plots) + "_pt_" + std::to_string(i_pt+1) + "_" + w_names.at(i_w);
        TH1D *h_obs = (TH1D*)h_tmp_tagger->Clone(histname.c_str());
        h_obs->SetStats(0);
        h_obs->Reset();
        h_obs->SetTitle("");
        h_obs->SetXTitle( (taggerName + " output").c_str());
        h_obs->SetYTitle(obs_tagger_labels.at(i_plots).c_str());
        h_obs->SetMaximum(15);
        if(obs_tagger_names.at(i_plots)=="p_b") h_obs->SetMaximum(1.5);
        h_obs->GetXaxis()->SetLabelSize(0.05);
        h_obs->GetXaxis()->SetTitleSize(0.05);
        h_obs->GetXaxis()->SetTitleOffset(1.4);
        h_obs->SetMarkerStyle(20);

        if(i_pt==0 && i_w==0 && i_plots==0) legend_fit_bb->AddEntry(p_b_bb,"Prefit from bb events", "L");
        if(i_pt==0 && i_w==0 && i_plots==0) legend_fit_bb->AddEntry(p_b_singleb,"Prefit from single b events", "L");
        if(i_pt==0 && i_w==0 && i_plots==0) legend_fit_bb->AddEntry(p_b_ttb,"Prefit from t#bar{t} events", "L");

        if(w_names.at(i_w)=="Prefit")
        {
          h_obs->SetLineColor(kGreen-8);
          h_obs->SetMarkerColor(kGreen-8);
        }
        else
        {
          h_obs->SetLineColor(kRed+1);
          h_obs->SetMarkerColor(kRed+1);
        }

        // fill histogram
        for(int i_tagger=0; i_tagger < Nbin_tagger; i_tagger++)
        {
          std::string obs_name = obs_tagger_names.at(i_plots) + "_pt_" + std::to_string(i_pt+1) + "_tagger_" + std::to_string(i_tagger+1);
          double obs = ((RooFormulaVar*)w.at(i_w)->function(obs_name.c_str()))->getValV();

          double obs_unc = 0.;
          if(i_w==0 || !((RooFormulaVar*)w.at(i_w)->function(obs_name.c_str()))->getPropagatedError(*fitResult))
          {
	    if(obs_tagger_names.at(i_plots)=="p_b") obs_unc = obs * h_combinedMC_tagger_pt_b->GetBinError(i_tagger+1, i_pt+1) / h_combinedMC_tagger_pt_b->GetBinContent(i_tagger+1, i_pt+1);
            else if(obs_tagger_names.at(i_plots)=="p_l") obs_unc = obs * h_combinedMC_tagger_pt_l->GetBinError(i_tagger+1, i_pt+1) / h_combinedMC_tagger_pt_l->GetBinContent(i_tagger+1, i_pt+1);
          }
          else if(i_w==1) obs_unc = ((RooFormulaVar*)w.at(i_w)->function(obs_name.c_str()))->getPropagatedError(*fitResult);

          h_obs->SetBinContent(i_tagger+1, obs/h_obs->GetBinWidth(i_tagger+1)); // PDF
          h_obs->SetBinError(i_tagger+1, obs_unc/h_obs->GetBinWidth(i_tagger+1)); // PDF
        }

        // record histogram
        f_output->cd();
        d_pdf->cd();
        h_obs->Write();

        // draw on canvas
        c_obs->cd();
        h_obs->Draw("LPESAME");
        if(w_names.at(i_w)=="Prefit" && obs_tagger_names.at(i_plots)=="p_b")
        {
          p_b_bb->Draw("LESAME"); // b-PDF from bb event in MC as extra reference
          p_b_singleb->Draw("LESAME"); // b-PDF from single event in MC as extra reference
          p_b_ttb->Draw("LESAME"); // b-PDF from ttbar event in MC as extra reference
        }
        h_obs->Draw("LPESAME");

      } // end prefit+postfit

      // tune and record canvas
      c_obs->cd();
      pt->Draw();
      pt2->Draw();
      pt3->Draw();
      pt4->Clear();
      pt4->AddText( (releaseName + ", " + ChannelName + ", " + systName + ", " + dataName).c_str() );
      pt4->AddText( (taggerName + ", " + WPName + ", " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt+1)) + " GeV < p_{T}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt+2)) + " GeV" ).c_str() );
      pt4->Draw();
      if(obs_tagger_names.at(i_plots)=="p_b") legend_fit_bb->Draw();
      else legend_fit->Draw();
      f_output->cd();
      d_pdf->cd();
      c_obs->Write();

    } // end plots

  } // end pT

  std::cout << " done." << std::endl;


  // ***************************************************************************
  // get 1D region PDF for bb/bl/lb/ll/data in pT1, pT2 prefit (postfit identical)
  // ----> pdf_region directory ************************************************
  // ***************************************************************************

  std::cout << "1D region PDF plots ...";
  std::vector<std::string> obs_region_names = {"region_bb","region_bl","region_lb","region_ll","region_data"};
  std::vector<std::string> obs_region_labels = {"bb PDF", "b(l+c) PDF","(l+c)b PDF","(l+c)(l+c) PDF","Number of events"};

  // Loop on pT1/pT2
  for(int i_pt1=0; i_pt1 < Nbin_pt; i_pt1++)
  {
    for(int i_pt2=0; i_pt2 <= i_pt1; i_pt2++)
    {
      // one Canvas with all PDFs plotted
      std::string canvasname = "c_region_pdf_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) ;
      TCanvas *c_obs = new TCanvas(canvasname.c_str());
      c_obs->SetLogy();

      // run on bb/bl/lb/ll/data
      for(int i_plots=0; i_plots < obs_region_names.size(); i_plots++)
      {
        std::string histname = obs_region_names.at(i_plots) + "_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1);
        TH1D *h_obs;

        if(obs_region_names.at(i_plots).find("bb")!=std::string::npos)
        {
          h_bb->GetZaxis()->SetRange(1,4);
          h_obs = (TH1D*)h_bb->ProjectionZ(histname.c_str(),i_pt1+1, i_pt1+1, i_pt2+1, i_pt2+1);
          h_obs->SetLineColor(kGray+2);
          if(i_pt1==0 && i_pt2==0) legend_region->AddEntry(h_obs,"bb","L");
        }
        else if(obs_region_names.at(i_plots).find("bl")!=std::string::npos)
        {
          h_bl->GetZaxis()->SetRange(1,4);
          h_obs = (TH1D*)h_bl->ProjectionZ(histname.c_str(),i_pt1+1, i_pt1+1, i_pt2+1, i_pt2+1);
          h_obs->SetLineColor(kGreen-8);
          if(i_pt1==0 && i_pt2==0) legend_region->AddEntry(h_obs,"bl","L");
        }
        else if(obs_region_names.at(i_plots).find("lb")!=std::string::npos)
        {
          h_lb->GetZaxis()->SetRange(1,4);
          h_obs = (TH1D*)h_lb->ProjectionZ(histname.c_str(),i_pt1+1, i_pt1+1, i_pt2+1, i_pt2+1);
          h_obs->SetLineColor(kOrange-7);
          if(i_pt1==0 && i_pt2==0) legend_region->AddEntry(h_obs,"lb","L");
        }
        else if(obs_region_names.at(i_plots).find("ll")!=std::string::npos)
        {
          h_ll->GetZaxis()->SetRange(1,4);
          h_obs = (TH1D*)h_ll->ProjectionZ(histname.c_str(),i_pt1+1, i_pt1+1, i_pt2+1, i_pt2+1);
          h_obs->SetLineColor(kRed+1);
          if(i_pt1==0 && i_pt2==0) legend_region->AddEntry(h_obs,"ll","L");
        }
        else if(obs_region_names.at(i_plots).find("data")!=std::string::npos)
        {
          h_data->GetZaxis()->SetRange(1,4);
          h_obs = (TH1D*)h_data->ProjectionZ(histname.c_str(),i_pt1+1, i_pt1+1, i_pt2+1, i_pt2+1);
          h_obs->SetLineColor(kBlack);
          if(i_pt1==0 && i_pt2==0) legend_region->AddEntry(h_obs,"data","LPE");
        }

        // uncertainties WRONG because of this line (NON-NORMALIZED)
        // print with "HIST" option
        h_obs->Scale(1./h_obs->Integral());

        // set all errors to 0 to avoid confusion
        // set to 0.0000000001 bins at 0 for log scale
        for(int ibin=0; ibin<h_obs->GetNbinsX(); ibin++)
        {
          h_obs->SetBinError(ibin+1, 0.);
          if(h_obs->GetBinContent(ibin+1)==0) h_obs->SetBinContent(ibin+1,0.0000000001);
        }

        h_obs->SetMaximum(h_obs->GetMaximum()*8);
        h_obs->SetMinimum( std::max(0.001, h_obs->GetMinimum()*0.1) );
        h_obs->SetTitle("");
        h_obs->GetYaxis()->SetTitle("Probability density function");
        h_obs->GetXaxis()->SetTitle("Region");

        c_obs->cd();
        h_obs->Draw("HISTSAME");

        f_output->cd();
        d_pdf_region->cd();
        h_obs->GetYaxis()->SetTitle(obs_region_labels.at(i_plots).c_str());
        h_obs->Write();
      }

      pt->Draw();
      pt2->Draw();
      pt3->Draw();
      pt5->Clear();
      pt5->AddText( (releaseName + ", " + ChannelName + ", " + systName + ", " + dataName).c_str() );
      pt5->AddText( (std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt1+1)) + " GeV < p_{T,1}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt1+2)) + " GeV, " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt2+1)) + " GeV < p_{T,2}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt2+2)) + " GeV" ).c_str() );
      pt5->Draw();
      legend_region->Draw();
      c_obs->Write();

    } // end loop pT2
  } // end loop pT2

  std::cout << " done." << std::endl;


  // ***************************************************************************
  // get 1D data/MC plot vs tagger output in SR,pT1,pT2 ************************
  // ----> data_MC and data_MC_region directory ********************************
  // ***************************************************************************

  std::cout << std::endl << "data/MC plots (including chi2 computation) ..." << std::endl;

  // Plots vs tagger output (prefit, postfit) - data/MC (pT1, pT2 + inclusive)
  // for 1D pT
  std::vector<TH1D*> h1_bb_incl;
  std::vector<TH1D*> h2_bb_incl;
  std::vector<TH1D*> h1_bl_incl;
  std::vector<TH1D*> h2_bl_incl;
  std::vector<TH1D*> h1_lb_incl;
  std::vector<TH1D*> h2_lb_incl;
  std::vector<TH1D*> h1_ll_incl;
  std::vector<TH1D*> h2_ll_incl;

  std::vector<TH1D*> h1_tot_incl;
  std::vector<TH1D*> h2_tot_incl;

  std::vector<TH1D*> h1_data_incl;
  std::vector<TH1D*> h2_data_incl;

  // chi2 in (pT1, pT2, region)
  double chi2_Prefit = 0; // chi2 before fit
  double chi2_Postfit = 0; // chi2 after fit

  double Nbins_tot = (Nbin_tagger*Nbin_tagger + (Nregion-1) )*Nbin_pt*(Nbin_pt+1)/2.;

  // total number of bins -
  // 2*Number of N_pt_i_j [chi2 normalized in pT1/pT2 in each SR and in 3-bin CR histo]
  double ndf_Prefit = Nbins_tot - 2*Nbin_pt*(Nbin_pt+1)/2.;

  // total number of bins - N (p_b[pT]) - N ( fitted yields ) + [correction factor from sampling studies]
  double ndf_Postfit = ndf_Prefit - (Nbin_tagger-1)*Nbin_pt - 4*Nbin_pt*(Nbin_pt+1)/2. + (2*Nbin_pt*Nregion);

  // pT1, pT2, region (either SR or 3-bin CR)
  double chi2_pt_Prefit[Nbin_pt][Nbin_pt][2];
  double chi2_pt_Postfit[Nbin_pt][Nbin_pt][2];

  // loop on pT1, pT2
  for(int i_pt1=0; i_pt1 < Nbin_pt; i_pt1++)
  {
    for(int i_pt2=0; i_pt2 <= i_pt1; i_pt2++)
    {
      // selecting right pT1, pT2 regions
      h_data_4d->GetAxis(2)->SetRange(i_pt1+1, i_pt1+1); // pT1 axis
      h_data_4d->GetAxis(3)->SetRange(i_pt2+1, i_pt2+1); // pT2 axis
      h_data->GetXaxis()->SetRange(i_pt1+1, i_pt1+1); // pT1 axis
      h_data->GetYaxis()->SetRange(i_pt2+1, i_pt2+1); // pT2 axis

      h_bb_4d->GetAxis(2)->SetRange(i_pt1+1, i_pt1+1); // pT1 axis
      h_bb_4d->GetAxis(3)->SetRange(i_pt2+1, i_pt2+1); // pT2 axis

      h_bl_4d->GetAxis(2)->SetRange(i_pt1+1, i_pt1+1); // pT1 axis
      h_bl_4d->GetAxis(3)->SetRange(i_pt2+1, i_pt2+1); // pT2 axis

      h_lb_4d->GetAxis(2)->SetRange(i_pt1+1, i_pt1+1); // pT1 axis
      h_lb_4d->GetAxis(3)->SetRange(i_pt2+1, i_pt2+1); // pT2 axis

      h_ll_4d->GetAxis(2)->SetRange(i_pt1+1, i_pt1+1); // pT1 axis
      h_ll_4d->GetAxis(3)->SetRange(i_pt2+1, i_pt2+1); // pT2 axis

      h_tot_4d->GetAxis(2)->SetRange(i_pt1+1, i_pt1+1); // pT1 axis
      h_tot_4d->GetAxis(3)->SetRange(i_pt2+1, i_pt2+1); // pT2 axis

      // get 1D data yields
      std::string dataname_1 = "data_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_1";
      std::string dataname_2 = "data_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_2";
      std::string dataname_region = "data_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_region";

      // projecting
      TH1D *h1_data_tmp = (TH1D*)h_data_4d->Projection(0,"E"); // 1D tagger output
      TH1D *h2_data_tmp = (TH1D*)h_data_4d->Projection(1,"E"); // 1D tagger output
      TH1D *hregion_data_tmp = (TH1D*)h_data->Project3D("ze"); // 1D region type

      h1_data_tmp->SetName( (dataname_1 + "_tmp").c_str());
      h2_data_tmp->SetName( (dataname_2 + "_tmp").c_str());
      hregion_data_tmp->SetName( (dataname_region + "_tmp").c_str());

      TH1D *h1_data = (TH1D*)h_tmp_tagger->Clone(dataname_1.c_str());
      TH1D *h2_data = (TH1D*)h_tmp_tagger->Clone(dataname_2.c_str());
      TH1D *hregion_data = (TH1D*)h_tmp_tagger->Clone(dataname_region.c_str());

      for(int i_tagger=0; i_tagger < Nbin_tagger; i_tagger++)
      {
        h1_data->SetBinContent(i_tagger+1, h1_data_tmp->GetBinContent(i_tagger+1) );
        h2_data->SetBinContent(i_tagger+1, h2_data_tmp->GetBinContent(i_tagger+1) );
        hregion_data->SetBinContent(i_tagger+1, hregion_data_tmp->GetBinContent(i_tagger+1) );

        h1_data->SetBinError(i_tagger+1, h1_data_tmp->GetBinError(i_tagger+1) );
        h2_data->SetBinError(i_tagger+1, h2_data_tmp->GetBinError(i_tagger+1) );
        hregion_data->SetBinError(i_tagger+1, hregion_data_tmp->GetBinError(i_tagger+1) );
      }

      // get 1D MC yields
      for (int i_w=0; i_w < w.size(); i_w++) // prefit+postfit
      {
        // get bb/bl/lb/ll yields in the various regions
        std::vector<RooFormulaVar*> N_bb;
        std::vector<RooFormulaVar*> N_bl;
        std::vector<RooFormulaVar*> N_lb;
        std::vector<RooFormulaVar*> N_ll;
        std::vector<RooFormulaVar*> N_tot;

        for(int i_region=0; i_region < Nregion; i_region++)
        {
          std::string bb_obs_name = "N_bb_" + cr_binning.at(i_region) + "_pt_" + std::to_string(i_pt1+1) + "_" + std::to_string(i_pt2+1);
          std::string bl_obs_name = "N_bl_" + cr_binning.at(i_region) + "_pt_" + std::to_string(i_pt1+1) + "_" + std::to_string(i_pt2+1);
          std::string lb_obs_name = "N_lb_" + cr_binning.at(i_region) + "_pt_" + std::to_string(i_pt1+1) + "_" + std::to_string(i_pt2+1);
          std::string ll_obs_name = "N_ll_" + cr_binning.at(i_region) + "_pt_" + std::to_string(i_pt1+1) + "_" + std::to_string(i_pt2+1);
          std::string tot_obs_name = "N_tot_" + cr_binning.at(i_region) + "_pt_" + std::to_string(i_pt1+1) + "_" + std::to_string(i_pt2+1);

          N_bb.push_back( (RooFormulaVar*)w.at(i_w)->function(bb_obs_name.c_str()) );
          N_bl.push_back( (RooFormulaVar*)w.at(i_w)->function(bl_obs_name.c_str()) );
          N_lb.push_back( (RooFormulaVar*)w.at(i_w)->function(lb_obs_name.c_str()) );
          N_ll.push_back( (RooFormulaVar*)w.at(i_w)->function(ll_obs_name.c_str()) );
          N_tot.push_back( (RooFormulaVar*)w.at(i_w)->function(tot_obs_name.c_str()) );
        }

        // create MC 1D yield histograms vs tagger output in SR
        std::string histname_bb_yields_1 = "h_bb_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_1" + "_" + w_names.at(i_w);
        std::string histname_bl_yields_1 = "h_bl_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_1" + "_" + w_names.at(i_w);
        std::string histname_lb_yields_1 = "h_lb_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_1" + "_" + w_names.at(i_w);
        std::string histname_ll_yields_1 = "h_ll_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_1" + "_" + w_names.at(i_w);

        std::string histname_bb_yields_2 = "h_bb_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_2" + "_" + w_names.at(i_w);
        std::string histname_bl_yields_2 = "h_bl_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_2" + "_" + w_names.at(i_w);
        std::string histname_lb_yields_2 = "h_lb_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_2" + "_" + w_names.at(i_w);
        std::string histname_ll_yields_2 = "h_ll_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_2" + "_" + w_names.at(i_w);

        TH1D *h1_bb_yields = (TH1D*)h_tmp_tagger->Clone(histname_bb_yields_1.c_str());
        TH1D *h1_bl_yields = (TH1D*)h_tmp_tagger->Clone(histname_bl_yields_1.c_str());
        TH1D *h1_lb_yields = (TH1D*)h_tmp_tagger->Clone(histname_lb_yields_1.c_str());
        TH1D *h1_ll_yields = (TH1D*)h_tmp_tagger->Clone(histname_ll_yields_1.c_str());

        TH1D *h2_bb_yields = (TH1D*)h_tmp_tagger->Clone(histname_bb_yields_2.c_str());
        TH1D *h2_bl_yields = (TH1D*)h_tmp_tagger->Clone(histname_bl_yields_2.c_str());
        TH1D *h2_lb_yields = (TH1D*)h_tmp_tagger->Clone(histname_lb_yields_2.c_str());
        TH1D *h2_ll_yields = (TH1D*)h_tmp_tagger->Clone(histname_ll_yields_2.c_str());


        // projections for errors prefit
        TH1D *h_proj_tmp_bb_1 = (TH1D*) h_bb_4d->Projection(0,"E");
        TH1D *h_proj_tmp_bl_1 = (TH1D*) h_bl_4d->Projection(0,"E");
        TH1D *h_proj_tmp_lb_1 = (TH1D*) h_lb_4d->Projection(0,"E");
        TH1D *h_proj_tmp_ll_1 = (TH1D*) h_ll_4d->Projection(0,"E");
        TH1D *h_proj_tmp_tot_1 = (TH1D*) h_tot_4d->Projection(0,"E");

        TH1D *h_proj_tmp_bb_2 = (TH1D*) h_bb_4d->Projection(1,"E");
        TH1D *h_proj_tmp_bl_2 = (TH1D*) h_bl_4d->Projection(1,"E");
        TH1D *h_proj_tmp_lb_2 = (TH1D*) h_lb_4d->Projection(1,"E");
        TH1D *h_proj_tmp_ll_2 = (TH1D*) h_ll_4d->Projection(1,"E");
        TH1D *h_proj_tmp_tot_2 = (TH1D*) h_tot_4d->Projection(1,"E");

        h_proj_tmp_bb_1->SetName( ("h_proj_tmp_bb_1_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_" + w_names.at(i_w)).c_str() );
        h_proj_tmp_bl_1->SetName( ("h_proj_tmp_bl_1_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_" + w_names.at(i_w)).c_str() );
        h_proj_tmp_lb_1->SetName( ("h_proj_tmp_lb_1_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_" + w_names.at(i_w)).c_str() );
        h_proj_tmp_ll_1->SetName( ("h_proj_tmp_ll_1_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_" + w_names.at(i_w)).c_str() );
        h_proj_tmp_tot_1->SetName( ("h_proj_tmp_tot_1_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_" + w_names.at(i_w)).c_str() );

        h_proj_tmp_bb_2->SetName( ("h_proj_tmp_bb_2_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_" + w_names.at(i_w)).c_str() );
        h_proj_tmp_bl_2->SetName( ("h_proj_tmp_bl_2_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_" + w_names.at(i_w)).c_str() );
        h_proj_tmp_lb_2->SetName( ("h_proj_tmp_lb_2_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_" + w_names.at(i_w)).c_str() );
        h_proj_tmp_ll_2->SetName( ("h_proj_tmp_ll_2_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_" + w_names.at(i_w)).c_str() );
        h_proj_tmp_tot_2->SetName( ("h_proj_tmp_tot_2_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_" + w_names.at(i_w)).c_str() );

        // fill up these
        for(int i_tagger=0; i_tagger < Nbin_tagger; i_tagger++)
        {
          // p_b and p_l in this pT1, pT2, SR, tagger output bin required
          RooRealVar *p_b_1 = (RooRealVar*)w.at(i_w)->function( ("p_b_pt_" + std::to_string(i_pt1+1) + "_tagger_" + std::to_string(i_tagger+1)).c_str() );
          RooRealVar *p_l_1 = (RooRealVar*)w.at(i_w)->function( ("p_l_pt_" + std::to_string(i_pt1+1) + "_tagger_" + std::to_string(i_tagger+1)).c_str() );

          RooRealVar *p_b_2 = (RooRealVar*)w.at(i_w)->function( ("p_b_pt_" + std::to_string(i_pt2+1) + "_tagger_" + std::to_string(i_tagger+1)).c_str() );
          RooRealVar *p_l_2 = (RooRealVar*)w.at(i_w)->function( ("p_l_pt_" + std::to_string(i_pt2+1) + "_tagger_" + std::to_string(i_tagger+1)).c_str() );

          // uncertainty on MC set to 0 postfit because computation requires some work
          double obs_unc_bb_1 = 0.00000001;
          double obs_unc_bl_1 = 0.00000001;
          double obs_unc_lb_1 = 0.00000001;
          double obs_unc_ll_1 = 0.00000001;

          double obs_unc_bb_2 = 0.00000001;
          double obs_unc_bl_2 = 0.00000001;
          double obs_unc_lb_2 = 0.00000001;
          double obs_unc_ll_2 = 0.00000001;

          // prefit -> from 5D MC histo
          if(i_w==0)
          {
            obs_unc_bb_1 = h_proj_tmp_bb_1->GetBinError(i_tagger+1);
            obs_unc_bl_1 = h_proj_tmp_bl_1->GetBinError(i_tagger+1);
            obs_unc_lb_1 = h_proj_tmp_lb_1->GetBinError(i_tagger+1);
            obs_unc_ll_1 = h_proj_tmp_ll_1->GetBinError(i_tagger+1);

            obs_unc_bb_2 = h_proj_tmp_bb_2->GetBinError(i_tagger+1);
            obs_unc_bl_2 = h_proj_tmp_bl_2->GetBinError(i_tagger+1);
            obs_unc_lb_2 = h_proj_tmp_lb_2->GetBinError(i_tagger+1);
            obs_unc_ll_2 = h_proj_tmp_ll_2->GetBinError(i_tagger+1);
          }

          h1_bb_yields->SetBinContent(i_tagger+1, N_bb.at(0)->getValV()*p_b_1->getValV());
          h1_bl_yields->SetBinContent(i_tagger+1, N_bl.at(0)->getValV()*p_b_1->getValV());
          h1_lb_yields->SetBinContent(i_tagger+1, N_lb.at(0)->getValV()*p_l_1->getValV());
          h1_ll_yields->SetBinContent(i_tagger+1, N_ll.at(0)->getValV()*p_l_1->getValV());

          h2_bb_yields->SetBinContent(i_tagger+1, N_bb.at(0)->getValV()*p_b_2->getValV());
          h2_lb_yields->SetBinContent(i_tagger+1, N_lb.at(0)->getValV()*p_b_2->getValV());
          h2_bl_yields->SetBinContent(i_tagger+1, N_bl.at(0)->getValV()*p_l_2->getValV());
          h2_ll_yields->SetBinContent(i_tagger+1, N_ll.at(0)->getValV()*p_l_2->getValV());

          h1_bb_yields->SetBinError(i_tagger+1, obs_unc_bb_1);
          h1_bl_yields->SetBinError(i_tagger+1, obs_unc_bl_1);
          h1_lb_yields->SetBinError(i_tagger+1, obs_unc_lb_1);
          h1_ll_yields->SetBinError(i_tagger+1, obs_unc_ll_1);

          h2_bb_yields->SetBinError(i_tagger+1, obs_unc_bb_2);
          h2_lb_yields->SetBinError(i_tagger+1, obs_unc_bl_2);
          h2_bl_yields->SetBinError(i_tagger+1, obs_unc_lb_2);
          h2_ll_yields->SetBinError(i_tagger+1, obs_unc_ll_2);

        }

        // tot histograms
        std::string histname_tot_yields_1 = "h_tot_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_1" + "_" + w_names.at(i_w);
        std::string histname_tot_yields_2 = "h_tot_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_2" + "_" + w_names.at(i_w);

        TH1D *h1_tot_yields = (TH1D*)h1_bb_yields->Clone(histname_tot_yields_1.c_str());
        TH1D *h2_tot_yields = (TH1D*)h2_bb_yields->Clone(histname_tot_yields_2.c_str());

        h1_tot_yields->Add(h1_bl_yields);
        h1_tot_yields->Add(h1_lb_yields);
        h1_tot_yields->Add(h1_ll_yields);

        h2_tot_yields->Add(h2_bl_yields);
        h2_tot_yields->Add(h2_lb_yields);
        h2_tot_yields->Add(h2_ll_yields);

        for(int i_tagger=0; i_tagger < Nbin_tagger; i_tagger++)
        {
          // no uncertainties if postfit
          double obs_unc_tot_1 = 0.;
          double obs_unc_tot_2 = 0.;
          // MC uncertainties if prefit
          if(i_w==0)
          {
            obs_unc_tot_1 = h_proj_tmp_tot_1->GetBinError(i_tagger+1);
            obs_unc_tot_2 = h_proj_tmp_tot_2->GetBinError(i_tagger+1);
          }
            h1_tot_yields->SetBinError(i_tagger+1, obs_unc_tot_1);
            h2_tot_yields->SetBinError(i_tagger+1, obs_unc_tot_2);
        }

        // create MC 1D yield histograms vs region
        std::string histname_bb_yields_region = "h_bb_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_region" + "_" + w_names.at(i_w);
        std::string histname_bl_yields_region = "h_bl_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_region" + "_" + w_names.at(i_w);
        std::string histname_lb_yields_region = "h_lb_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_region" + "_" + w_names.at(i_w);
        std::string histname_ll_yields_region = "h_ll_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_region" + "_" + w_names.at(i_w);
        std::string histname_tot_yields_region = "h_tot_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_region" + "_" + w_names.at(i_w);

        TH1D *hregion_bb_yields = (TH1D*)h_tmp_region->Clone(histname_bb_yields_region.c_str());
        TH1D *hregion_bl_yields = (TH1D*)h_tmp_region->Clone(histname_bl_yields_region.c_str());
        TH1D *hregion_lb_yields = (TH1D*)h_tmp_region->Clone(histname_lb_yields_region.c_str());
        TH1D *hregion_ll_yields = (TH1D*)h_tmp_region->Clone(histname_ll_yields_region.c_str());
        TH1D *hregion_tot_yields = (TH1D*)h_tmp_region->Clone(histname_tot_yields_region.c_str());

        // fill up these
        for(int i_region=0; i_region < Nregion; i_region++)
        {
          RooFormulaVar *bb = N_bb.at(i_region);
          RooFormulaVar *bl = N_bl.at(i_region);
          RooFormulaVar *lb = N_lb.at(i_region);
          RooFormulaVar *ll = N_ll.at(i_region);
          RooFormulaVar *tot = N_tot.at(i_region);

          double obs_bb = bb->getValV();
          double obs_bl = bl->getValV();
          double obs_lb = lb->getValV();
          double obs_ll = ll->getValV();
          double obs_tot = tot->getValV();

          // here we get the unc out of the box
          double obs_unc_bb = 0.00000001;
          double obs_unc_bl = 0.00000001;
          double obs_unc_lb = 0.00000001;
          double obs_unc_ll = 0.00000001;
          double obs_unc_tot = 0.00000001;

          if(i_w==0 || !(bb->getPropagatedError(*fitResult)) ||
                       !(bl->getPropagatedError(*fitResult)) ||
                       !(lb->getPropagatedError(*fitResult)) ||
                       !(ll->getPropagatedError(*fitResult)) )
          {
            obs_unc_bb = h_bb->GetBinError(i_pt1+1, i_pt2+1, i_region+1);
            obs_unc_bl = h_bl->GetBinError(i_pt1+1, i_pt2+1, i_region+1);
            obs_unc_lb = h_lb->GetBinError(i_pt1+1, i_pt2+1, i_region+1);
            obs_unc_ll = h_ll->GetBinError(i_pt1+1, i_pt2+1, i_region+1);
            obs_unc_tot = h_tot->GetBinError(i_pt1+1, i_pt2+1, i_region+1);
          }
          else if(i_w==1)
          {
            obs_unc_bb = bb->getPropagatedError(*fitResult);
            obs_unc_bl = bl->getPropagatedError(*fitResult);
            obs_unc_lb = lb->getPropagatedError(*fitResult);
            obs_unc_ll = ll->getPropagatedError(*fitResult);
            obs_unc_tot = tot->getPropagatedError(*fitResult);
          }

          hregion_bb_yields->SetBinContent(i_region+1, obs_bb);
          hregion_bb_yields->SetBinError(i_region+1, obs_unc_bb);

          hregion_bl_yields->SetBinContent(i_region+1, obs_bl);
          hregion_bl_yields->SetBinError(i_region+1, obs_unc_bl);

          hregion_lb_yields->SetBinContent(i_region+1, obs_lb);
          hregion_lb_yields->SetBinError(i_region+1, obs_unc_lb);

          hregion_ll_yields->SetBinContent(i_region+1, obs_ll);
          hregion_ll_yields->SetBinError(i_region+1, obs_unc_ll);

          hregion_tot_yields->SetBinContent(i_region+1, obs_tot);
          hregion_tot_yields->SetBinError(i_region+1, obs_unc_tot);
        }

        // Cosmetics
        h1_tot_yields->SetYTitle("Number of events");
        h2_tot_yields->SetYTitle("Number of events");
        hregion_tot_yields->SetYTitle("Number of events");

        hregion_tot_yields->SetMaximum(hregion_tot_yields->GetMaximum()*50);
        hregion_tot_yields->SetMinimum(hregion_tot_yields->GetMinimum()*0.01);
        hregion_tot_yields->SetTitle("");
        hregion_tot_yields->GetXaxis()->SetLabelSize(0.06);
        hregion_tot_yields->GetXaxis()->SetTitleOffset(1.2);
        hregion_tot_yields->GetXaxis()->SetTitleSize(0.04);

        h1_bb_yields->SetLineColor(kGray+2);
        h1_bl_yields->SetLineColor(kGreen-8);
        h1_lb_yields->SetLineColor(kOrange-7);
        h1_ll_yields->SetLineColor(kRed+1);
        h1_tot_yields->SetLineColor(kBlue+1);

        h1_bb_yields->SetLineStyle(2);
        h1_bl_yields->SetLineStyle(2);
        h1_lb_yields->SetLineStyle(2);
        h1_ll_yields->SetLineStyle(2);

        h1_ll_yields->SetMaximum(h1_ll_yields->GetMaximum()*10);

        h2_bb_yields->SetLineColor(kGray+2);
        h2_bl_yields->SetLineColor(kGreen-8);
        h2_lb_yields->SetLineColor(kOrange-7);
        h2_ll_yields->SetLineColor(kRed+1);
        h2_tot_yields->SetLineColor(kBlue+1);

        h2_bb_yields->SetLineStyle(2);
        h2_bl_yields->SetLineStyle(2);
        h2_lb_yields->SetLineStyle(2);
        h2_ll_yields->SetLineStyle(2);

        h2_ll_yields->SetMaximum(h2_ll_yields->GetMaximum()*10);

        hregion_bb_yields->SetLineColor(kGray+2);
        hregion_bl_yields->SetLineColor(kGreen-8);
        hregion_lb_yields->SetLineColor(kOrange-7);
        hregion_ll_yields->SetLineColor(kRed+1);
        hregion_tot_yields->SetLineColor(kBlue+1);

        hregion_bb_yields->SetLineStyle(2);
        hregion_bl_yields->SetLineStyle(2);
        hregion_lb_yields->SetLineStyle(2);
        hregion_ll_yields->SetLineStyle(2);

        hregion_ll_yields->SetMaximum(hregion_ll_yields->GetMaximum()*10);

        h1_data->SetLineColor(kBlack);
        h2_data->SetLineColor(kBlack);
        hregion_data->SetLineColor(kBlack);

        h1_data->SetLineWidth(2);
        h2_data->SetLineWidth(2);
        hregion_data->SetLineWidth(2);

        h1_data->SetMarkerStyle(20);
        h2_data->SetMarkerStyle(20);
        hregion_data->SetMarkerStyle(20);

        h1_data->SetMarkerSize(1.2);
        h2_data->SetMarkerSize(1.2);
        hregion_data->SetMarkerSize(1.2);

        // for inclusive plots
        if(i_pt2==0)
        {
          std::string suffix = "_pt1_" + std::to_string(i_pt1+1) + "_" + w_names.at(i_w);

          h1_bb_incl.push_back((TH1D*)h1_bb_yields->Clone( ("h1_bb_incl" + suffix).c_str() ));
          h1_bl_incl.push_back((TH1D*)h1_bl_yields->Clone( ("h1_bl_incl" + suffix).c_str() ));
          h1_lb_incl.push_back((TH1D*)h1_lb_yields->Clone( ("h1_lb_incl" + suffix).c_str() ));
          h1_ll_incl.push_back((TH1D*)h1_ll_yields->Clone( ("h1_ll_incl" + suffix).c_str() ));
          h1_tot_incl.push_back((TH1D*)h1_tot_yields->Clone( ("h1_tot_incl" + suffix).c_str() ));

          if(i_w==0) h1_data_incl.push_back((TH1D*)h1_data->Clone( ("h1_data_incl_pt1_" + std::to_string(i_pt1+1)).c_str() ));
        }
        else
        {
          h1_bb_incl.at(w.size()*i_pt1+i_w)->Add(h1_bb_yields);
          h1_bl_incl.at(w.size()*i_pt1+i_w)->Add(h1_bl_yields);
          h1_lb_incl.at(w.size()*i_pt1+i_w)->Add(h1_lb_yields);
          h1_ll_incl.at(w.size()*i_pt1+i_w)->Add(h1_ll_yields);
          h1_tot_incl.at(w.size()*i_pt1+i_w)->Add(h1_tot_yields);
          if(i_w==0) h1_data_incl.at(i_pt1)->Add(h1_data);
        }

        if(i_pt1==i_pt2)
        {
          std::string suffix = "_pt2_" + std::to_string(i_pt2+1) + "_" + w_names.at(i_w);

          h2_bb_incl.push_back((TH1D*)h2_bb_yields->Clone( ("h2_bb_incl" + suffix).c_str() ));
          h2_bl_incl.push_back((TH1D*)h2_bl_yields->Clone( ("h2_bl_incl" + suffix).c_str() ));
          h2_lb_incl.push_back((TH1D*)h2_lb_yields->Clone( ("h2_lb_incl" + suffix).c_str() ));
          h2_ll_incl.push_back((TH1D*)h2_ll_yields->Clone( ("h2_ll_incl" + suffix).c_str() ));
          h2_tot_incl.push_back((TH1D*)h2_tot_yields->Clone( ("h2_tot_incl" + suffix).c_str() ));
          if(i_w==0) h2_data_incl.push_back((TH1D*)h2_data->Clone( ("h2_data_incl_pt2_" + std::to_string(i_pt2+1)).c_str() ));
        }
        else
        {
          h2_bb_incl.at(w.size()*i_pt2+i_w)->Add(h2_bb_yields);
          h2_bl_incl.at(w.size()*i_pt2+i_w)->Add(h2_bl_yields);
          h2_lb_incl.at(w.size()*i_pt2+i_w)->Add(h2_lb_yields);
          h2_ll_incl.at(w.size()*i_pt2+i_w)->Add(h2_ll_yields);
          h2_tot_incl.at(w.size()*i_pt2+i_w)->Add(h2_tot_yields);
          if(i_w==0) h2_data_incl.at(i_pt2)->Add(h2_data);
        }

        // create new template tagger canvas, pT1 and pT2
        std::string canvasname_1 = "c_data_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_" + w_names.at(i_w) + "_1";
        TCanvas *c_obs_1 = new TCanvas(canvasname_1.c_str());
        c_obs_1->SetLogy();

        std::string canvasname_2 = "c_data_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_" + w_names.at(i_w) + "_2";
        TCanvas *c_obs_2 = new TCanvas(canvasname_2.c_str());
        c_obs_2->SetLogy();

        std::string canvasname_region = "c_data_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_" + w_names.at(i_w) + "_region";
        TCanvas *c_obs_region = new TCanvas(canvasname_region.c_str());
        c_obs_region->SetLogy();

        std::string canvasname_region_div = "c_data_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_" + w_names.at(i_w) + "_region_div";
        TCanvas *c_obs_region_div = new TCanvas(canvasname_region_div.c_str());

        if(i_w==0 && i_pt1==0 && i_pt2==0)
        {
          legend_data->AddEntry(h1_data,"Data + stat. unc.", "LPE");
          legend_data->AddEntry(h1_tot_yields,"Full model", "L");
          legend_data->AddEntry(h1_bb_yields,"bb", "L");
          legend_data->AddEntry(h1_bl_yields,"bl", "L");
          legend_data->AddEntry(h1_lb_yields,"lb", "L");
          legend_data->AddEntry(h1_ll_yields,"ll", "L");

          legend_region->Clear();
          legend_region->AddEntry(hregion_data,"Data + stat. unc.", "LPE");
          legend_region->AddEntry(hregion_tot_yields,"Full model", "L");
          legend_region->AddEntry(hregion_bb_yields,"bb", "L");
          legend_region->AddEntry(hregion_bl_yields,"bl", "L");
          legend_region->AddEntry(hregion_lb_yields,"lb", "L");
          legend_region->AddEntry(hregion_ll_yields,"ll", "L");
        }

        // Chi2 computation
        // 2D tagger1-tagger2 histograms in SR
        // MC
        std::string histoname_2d_mc = "h_2d_mc_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_" + w_names.at(i_w);
        TH2D *h_2d_mc = new TH2D(histoname_2d_mc.c_str(),"",Nbin_tagger, 0, Nbin_tagger, Nbin_tagger, 0, Nbin_tagger);

        // data
        h_data_4d->GetAxis(2)->SetRange(i_pt1+1, i_pt1+1); // pT1 axis
        h_data_4d->GetAxis(3)->SetRange(i_pt2+1, i_pt2+1); // pT2 axis

        std::string histoname_2d_data = "h_2d_data_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_" + w_names.at(i_w);
        TH2D *h_2d_data = (TH2D*)h_data_4d->Projection(1,0,"E");
        h_2d_data->SetName(histoname_2d_data.c_str());

        // b- and l+c-PDF
        std::string histname_bPDF_1 = std::string(d_pdf->GetName()) + "/p_b_pt_" + std::to_string(i_pt1+1) + "_" + w_names.at(i_w);
        TH1D *h_bPDF_1 = (TH1D*)f_output->Get(histname_bPDF_1.c_str());

        std::string histname_bPDF_2 = std::string(d_pdf->GetName()) + "/p_b_pt_" + std::to_string(i_pt2+1) + "_" + w_names.at(i_w);
        TH1D *h_bPDF_2 = (TH1D*)f_output->Get(histname_bPDF_2.c_str());

        std::string histname_lPDF_1 = std::string(d_pdf->GetName()) + "/p_l_pt_" + std::to_string(i_pt1+1) + "_" + w_names.at(i_w);
        TH1D *h_lPDF_1 = (TH1D*)f_output->Get(histname_lPDF_1.c_str());

        std::string histname_lPDF_2 = std::string(d_pdf->GetName()) + "/p_l_pt_" + std::to_string(i_pt2+1) + "_" + w_names.at(i_w);
        TH1D *h_lPDF_2 = (TH1D*)f_output->Get(histname_lPDF_2.c_str());

        // MC in SR
        for(int i_tagger1=0; i_tagger1<Nbin_tagger; i_tagger1++)
        {
          for(int i_tagger2=0; i_tagger2<Nbin_tagger; i_tagger2++)
          {
            double N_tagger1_tagger2 =
                   N_bb.at(0)->getValV()*h_bPDF_1->GetBinContent(i_tagger1+1)*
                                         h_bPDF_2->GetBinContent(i_tagger2+1) +
                   N_bl.at(0)->getValV()*h_bPDF_1->GetBinContent(i_tagger1+1)*
                                         h_lPDF_2->GetBinContent(i_tagger2+1) +
                   N_lb.at(0)->getValV()*h_lPDF_1->GetBinContent(i_tagger1+1)*
                                         h_bPDF_2->GetBinContent(i_tagger2+1) +
                   N_ll.at(0)->getValV()*h_lPDF_1->GetBinContent(i_tagger1+1)*
                                         h_lPDF_2->GetBinContent(i_tagger2+1) ;

            if(N_tagger1_tagger2>0) h_2d_mc->SetBinContent(i_tagger1+1,i_tagger2+1,N_tagger1_tagger2);
            else
            {
              std::cout << "WARNING: some " << w_names.at(i_w) << " MC predictions are <= 0, filling with 0 ... but check your workspace! " << std::endl;
              h_2d_mc->SetBinContent(i_tagger1+1,i_tagger2+1,0.);
            }

            h_2d_mc->SetBinError(i_tagger1+1,i_tagger2+1,0.); // error of model set to 0 for chi2

            //std::cout << std::endl;
            //std::cout << " {pT1, pT2, Tagger1, Tagger2, data, MC} : " << i_pt1+1 << " " << i_pt2+1 << " " << i_tagger1+1 << " " << i_tagger2+1 << " " << h_2d_data->GetBinContent(i_tagger1+1,i_tagger2+1) << " " << h_2d_mc->GetBinContent(i_tagger1+1,i_tagger2+1) << " " << h_2d_data->GetBinContent(i_tagger1+1,i_tagger2+1)/h_2d_mc->GetBinContent(i_tagger1+1,i_tagger2+1) << std::endl;
           }
        }

        double chi2_pt1_pt2 = h_2d_data->Chi2Test(h_2d_mc,"CHI2UWP");
        if(i_w==0)
        {
          chi2_Prefit += chi2_pt1_pt2;
          chi2_pt_Prefit[i_pt1][i_pt2][0] = chi2_pt1_pt2;
        }
        else if (i_w==1)
        {
          chi2_Postfit += chi2_pt1_pt2;
          chi2_pt_Postfit[i_pt1][i_pt2][0] = chi2_pt1_pt2;
        }

        // 1D CR-only region histograms
        // MC
        std::string histoname_1d_CR_mc = "h_1d_CR_mc_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_" + w_names.at(i_w);
        TH1D *h_1d_CR_mc = new TH1D(histoname_1d_CR_mc.c_str(),"",Nregion-1, 0, Nregion-1);

        // data
        h_data->GetXaxis()->SetRange(i_pt1+1, i_pt1+1); // pT1 axis
        h_data->GetYaxis()->SetRange(i_pt2+1, i_pt2+1); // pT2 axis
        h_data->GetZaxis()->SetRange(1, Nregion); // CR region axis

        std::string histoname_1d_CR_data = "h_1d_CR_data_pt1_" + std::to_string(i_pt1+1) + "_pt2_" + std::to_string(i_pt2+1) + "_" + w_names.at(i_w);
        TH1D *h_1d_CR_data_tmp = (TH1D*)h_data->Project3D("ze"); // 1D region type
        h_1d_CR_data_tmp->SetName( (histoname_1d_CR_data + "_tmp").c_str());
        TH1D *h_1d_CR_data = new TH1D(histoname_1d_CR_data.c_str(),"",Nregion-1, 0, Nregion-1);

        // filling in CR
        for(int i_region=0; i_region<Nregion; i_region++)
        {
          // skip SR
          if(i_region==0) continue;

          h_1d_CR_data->SetBinContent(i_region,h_1d_CR_data_tmp->GetBinContent(i_region+1));
          h_1d_CR_data->SetBinError(i_region,h_1d_CR_data_tmp->GetBinError(i_region+1));

          double N_CR = N_tot.at(i_region)->getValV();

          if(N_CR>0) h_1d_CR_mc->SetBinContent(i_region,N_CR);
          else
          {
            std::cout << "WARNING: some " << w_names.at(i_w) << " MC predictions are <= 0, filling with 0 ... but check your workspace! " << std::endl;
            h_1d_CR_mc->SetBinContent(i_region,0.);
          }

          h_1d_CR_mc->SetBinError(i_region,0.); // error of model set to 0 for chi2

        }

        double chi2_pt1_pt2_CR = h_1d_CR_data->Chi2Test(h_1d_CR_mc,"CHI2UWP");
        if(i_w==0)
        {
          chi2_Prefit += chi2_pt1_pt2_CR;
          chi2_pt_Prefit[i_pt1][i_pt2][1] = chi2_pt1_pt2_CR;
        }
        else if (i_w==1)
        {
          chi2_Postfit += chi2_pt1_pt2_CR;
          chi2_pt_Postfit[i_pt1][i_pt2][1] = chi2_pt1_pt2_CR;
        }

        // tune and record canvas
        c_obs_1->cd();
        h1_tot_yields->Draw("HIST");
        h1_ll_yields->Draw("HISTSAME");
        h1_bl_yields->Draw("HISTSAME");
        h1_lb_yields->Draw("HISTSAME");
        h1_bb_yields->Draw("HISTSAME");
        h1_tot_yields->Draw("HISTSAME");
        h1_tot_yields->Draw("AXISSAME");
        h1_data->Draw("LPESAME");

        pt->Draw();
        pt2->Draw();
        pt3->Draw();
        pt5->Clear();
        pt5->AddText( (releaseName + ", " + systName + ", " + dataName).c_str() );
        pt5->AddText( (taggerName + ", " + WPName + ", " + w_names.at(i_w) + ", " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt1+1)) + " GeV < p_{T,1}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt1+2)) + " GeV, " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt2+1)) + " GeV < p_{T,2}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt2+2)) + " GeV" ).c_str() );
        pt5->Draw();
        legend_data->Draw();

        f_output->cd();
        d_data_MC->cd();
        c_obs_1->Write();

        c_obs_2->cd();
        h2_tot_yields->Draw("HIST");
        h2_ll_yields->Draw("HISTSAME");
        h2_bl_yields->Draw("HISTSAME");
        h2_lb_yields->Draw("HISTSAME");
        h2_bb_yields->Draw("HISTSAME");
        h2_tot_yields->Draw("HISTSAME");
        h2_tot_yields->Draw("AXISSAME");
        h2_data->Draw("LPESAME");

        pt->Draw();
        pt2->Draw();
        pt3->Draw();
        pt5->Clear();
        pt5->AddText( (releaseName + ", " + systName + ", " + dataName).c_str() );
        pt5->AddText( (taggerName + ", " + WPName + ", " + w_names.at(i_w) + ", " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt1+1)) + " GeV < p_{T,1}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt1+2)) + " GeV, " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt2+1)) + " GeV < p_{T,2}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt2+2)) + " GeV" ).c_str() );
        pt5->Draw();
        legend_data->Draw();

        f_output->cd();
        d_data_MC->cd();
        c_obs_2->Write();

        c_obs_region->cd();
        hregion_tot_yields->Draw("HIST");
        hregion_ll_yields->Draw("HISTSAME");
        hregion_lb_yields->Draw("HISTSAME");
        hregion_bl_yields->Draw("HISTSAME");
        hregion_bb_yields->Draw("HISTSAME");
        hregion_tot_yields->Draw("HISTSAME");
        hregion_tot_yields->Draw("AXISSAME");
        hregion_data->Draw("LPESAME");

        pt->Draw();
        pt2->Draw();
        pt3->Draw();
        pt5->Clear();
        pt5->AddText( (releaseName + ", " + systName + ", " + dataName).c_str() );
        pt5->AddText( (w_names.at(i_w) + ", " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt1+1)) + " GeV < p_{T,1}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt1+2)) + " GeV, " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt2+1)) + " GeV < p_{T,2}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt2+2)) + " GeV" ).c_str() );
        pt5->Draw();
        legend_region->Draw();

        f_output->cd();
        d_data_MC_region->cd();
        c_obs_region->Write();

        std::vector<TH1D*> v_hist_in_stack = {
          hregion_ll_yields,
          hregion_lb_yields,
          hregion_bl_yields,
          hregion_bb_yields,
        };
        std::vector<string> v_name_in_stack = {
          "ll",
          "lb",
          "bl",
          "bb",
        };
        d_data_MC_region->cd();
        THStack* s_region_ratio=new THStack("s_region_ratio","");
        legend_region_ratio->Clear();
        int i=0;
        for (const auto &hregion : v_hist_in_stack){
          TH1D* hregion_ratio=(TH1D*) hregion_tot_yields->Clone((string(hregion->GetName())+"_ratio").c_str());
          hregion_ratio->Reset();
          for (int reg_bin=1;reg_bin<=Nregion;reg_bin++){
            hregion_ratio->SetBinContent(reg_bin, hregion->GetBinContent(reg_bin)/hregion_data->GetBinContent(reg_bin));
            hregion_ratio->SetBinError(reg_bin, 0);
          }
          hregion_ratio->SetFillColor(hregion->GetLineColor());
          hregion_ratio->GetYaxis()->SetRangeUser(0,2);
          hregion_ratio->GetYaxis()->SetTitle("MC/data yield");
          hregion_ratio->GetXaxis()->SetRange(1,Nregion);
          hregion_ratio->SetLineColor(kBlack);
          legend_region_ratio->AddEntry(hregion_ratio, v_name_in_stack.at(i).c_str(), "f");
          i++;
          s_region_ratio->Add(hregion_ratio);
        }
        c_obs_region_div->cd();
        //s_region_ratio->GetYaxis()->SetRangeUser(0.,2.);
        s_region_ratio->Draw();
        s_region_ratio->SetMinimum(0.);
        s_region_ratio->SetMaximum(2.);
        s_region_ratio->GetXaxis()->SetRange(1,Nregion);
        s_region_ratio->GetXaxis()->SetTitle("Region");
        s_region_ratio->GetYaxis()->SetTitle((string("MC_{") + w_names.at(i_w) + "} / data yield").c_str());
        s_region_ratio->Draw();
        TLine* h_line=new TLine(0., 1. , 4., 1.);
        h_line->Draw("SAME");
        pt->Draw();
        pt2->Draw();
        pt3->Draw();
        pt5->Clear();
        pt5->AddText( (releaseName + ", " + systName + ", " + dataName).c_str() );
        pt5->AddText( (w_names.at(i_w) + ", " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt1+1)) + " GeV < p_{T,1}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt1+2)) + " GeV, " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt2+1)) + " GeV < p_{T,2}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt2+2)) + " GeV" ).c_str() );
        pt5->Draw();
        legend_region_ratio->Draw();

        f_output->cd();
        d_data_MC_region->cd();
        c_obs_region_div->Write();

      } // prefit+postfit
    } // pT2 loop
  } // pT1 loop
/*
  // inclusive
  for(int i_pt=0; i_pt<Nbin_pt; i_pt++)
  {
    // data rescaled to 1
    TH1D *h1_data_incl_tmp_all = (TH1D*)h1_data_incl.at(i_pt)->Clone( ("h1_data_incl_" + to_string(i_pt) + "_tmp_all").c_str() );
    TH1D *h2_data_incl_tmp_all = (TH1D*)h2_data_incl.at(i_pt)->Clone( ("h2_data_incl_" + to_string(i_pt) + "_tmp_all").c_str() );

    double error1 = 0;
    double integral1 = h1_data_incl_tmp_all->IntegralAndError(1, h1_data_incl_tmp_all->GetNbinsX(),
                                                                 error1);

    double error2 = 0;
    double integral2 = h2_data_incl_tmp_all->IntegralAndError(1, h2_data_incl_tmp_all->GetNbinsX(),
                                                                 error2);

    for(int i_tagger=0; i_tagger < Nbin_tagger; i_tagger++)
    {
      h1_data_incl_tmp_all->SetBinContent(i_tagger+1, integral1);
      h1_data_incl_tmp_all->SetBinError(i_tagger+1, error1);

      h2_data_incl_tmp_all->SetBinContent(i_tagger+1, integral2);
      h2_data_incl_tmp_all->SetBinError(i_tagger+1, error2);
    }
    h1_data_incl.at(i_pt)->Divide(h1_data_incl.at(i_pt), h1_data_incl_tmp_all, 1., 1., "B");
    h2_data_incl.at(i_pt)->Divide(h2_data_incl.at(i_pt), h2_data_incl_tmp_all, 1., 1., "B");

    for(int i_w=0; i_w<w.size(); i_w++)
    {
      // model rescaled to 1
      TH1D *h1_tot_incl_tmp_all = (TH1D*)h1_tot_incl.at(w.size()*i_pt+i_w)->Clone( ("h1_tot_incl_" + to_string(i_pt) + "_" + w_names.at(i_w) + "_tmp_all").c_str() );
      TH1D *h2_tot_incl_tmp_all = (TH1D*)h2_tot_incl.at(w.size()*i_pt+i_w)->Clone( ("h2_tot_incl_" + to_string(i_pt) + "_" + w_names.at(i_w) + "_tmp_all").c_str() );

      double error1_model = 0;
      double integral1_model = h1_tot_incl_tmp_all->IntegralAndError(1,
                                                       h1_tot_incl_tmp_all->GetNbinsX(),
                                                       error1_model);

      double error2_model = 0;
      double integral2_model = h2_tot_incl_tmp_all->IntegralAndError(1,
                                                       h2_tot_incl_tmp_all->GetNbinsX(),
                                                       error2_model);

      for(int i_tagger=0; i_tagger < Nbin_tagger; i_tagger++)
      {
        h1_tot_incl_tmp_all->SetBinContent(i_tagger+1, integral1_model);
        h1_tot_incl_tmp_all->SetBinError(i_tagger+1, error1_model);

        h2_tot_incl_tmp_all->SetBinContent(i_tagger+1, integral2_model);
        h2_tot_incl_tmp_all->SetBinError(i_tagger+1, error2_model);
      }
      h1_tot_incl.at(w.size()*i_pt+i_w)->Divide(h1_tot_incl.at(w.size()*i_pt+i_w),
                                                h1_tot_incl_tmp_all, 1., 1., "B");

      h2_tot_incl.at(w.size()*i_pt+i_w)->Divide(h2_tot_incl.at(w.size()*i_pt+i_w),
                                                h2_tot_incl_tmp_all, 1., 1., "B");

      double norm1_b = ( h1_tot_incl.at(w.size()*i_pt+i_w)->GetBinContent(1) -
                            h1_tot_incl.at(w.size()*i_pt+i_w)->GetBinContent(2)*
                            h1_l_incl.at(w.size()*i_pt+i_w)->GetBinContent(1)/
                            h1_l_incl.at(w.size()*i_pt+i_w)->GetBinContent(2) ) /
                         ( h1_b_incl.at(w.size()*i_pt+i_w)->GetBinContent(1) -
                              h1_b_incl.at(w.size()*i_pt+i_w)->GetBinContent(2)*
                              h1_l_incl.at(w.size()*i_pt+i_w)->GetBinContent(1)/
                              h1_l_incl.at(w.size()*i_pt+i_w)->GetBinContent(2) );

      double norm1_l = ( h1_tot_incl.at(w.size()*i_pt+i_w)->GetBinContent(2) -
                         norm1_b*h1_b_incl.at(w.size()*i_pt+i_w)->GetBinContent(2) ) /
                         h1_l_incl.at(w.size()*i_pt+i_w)->GetBinContent(2);

      double norm2_b = ( h2_tot_incl.at(w.size()*i_pt+i_w)->GetBinContent(1) -
                            h2_tot_incl.at(w.size()*i_pt+i_w)->GetBinContent(2)*
                            h2_l_incl.at(w.size()*i_pt+i_w)->GetBinContent(1)/
                            h2_l_incl.at(w.size()*i_pt+i_w)->GetBinContent(2) ) /
                         ( h2_b_incl.at(w.size()*i_pt+i_w)->GetBinContent(1) -
                              h2_b_incl.at(w.size()*i_pt+i_w)->GetBinContent(2)*
                              h2_l_incl.at(w.size()*i_pt+i_w)->GetBinContent(1)/
                              h2_l_incl.at(w.size()*i_pt+i_w)->GetBinContent(2) );

      double norm2_l = ( h2_tot_incl.at(w.size()*i_pt+i_w)->GetBinContent(2) -
                         norm2_b*h2_b_incl.at(w.size()*i_pt+i_w)->GetBinContent(2) ) /
                         h2_l_incl.at(w.size()*i_pt+i_w)->GetBinContent(2);

      // ERROR NORMALIZATION FOR INDIVIDUAL B AND L COMPONENT NOT 100% ACCURATE (error overstimated)
      h1_b_incl.at(w.size()*i_pt+i_w)->Scale(norm1_b);
      h1_l_incl.at(w.size()*i_pt+i_w)->Scale(norm1_l);

      h2_b_incl.at(w.size()*i_pt+i_w)->Scale(norm2_b);
      h2_l_incl.at(w.size()*i_pt+i_w)->Scale(norm2_l);

      h1_b_incl.at(w.size()*i_pt+i_w)->SetMaximum(10);
      h1_l_incl.at(w.size()*i_pt+i_w)->SetMaximum(10);

      h2_b_incl.at(w.size()*i_pt+i_w)->SetMaximum(10);
      h2_l_incl.at(w.size()*i_pt+i_w)->SetMaximum(10);

      // canvas
      std::string suffix = "_pt1_" + std::to_string(i_pt+1) + "_" + w_names.at(i_w);
      TCanvas *c1_incl = new TCanvas( ("c_incl"+suffix).c_str() );
      c1_incl->SetLogy();

      h1_tot_incl.at(w.size()*i_pt+i_w)->Draw("HIST");
      h1_l_incl.at(w.size()*i_pt+i_w)->Draw("HISTSAME");
      h1_b_incl.at(w.size()*i_pt+i_w)->Draw("HISTSAME");
      h1_tot_incl.at(w.size()*i_pt+i_w)->Draw("HISTSAME");
      h1_data_incl.at(i_pt)->Draw("LPESAME");
      h1_tot_incl.at(w.size()*i_pt+i_w)->Draw("AXISSAME");

      pt->Draw();
      pt2->Draw();
      pt3->Draw();
      pt5->Clear();
      pt5->SetTextSize(0.04);
      pt5->AddText( (releaseName + ", " + systName + ", " + dataName).c_str() );
      pt5->AddText( (taggerName + ", " + WPName + ", " + w_names.at(i_w) + ", " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt+1)) + " GeV < p_{T,1}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt+2)) + " GeV").c_str() );
      pt5->Draw();
      legend_data->Draw();

      f_output->cd();
      d_data_MC->cd();
      c1_incl->Write();

      suffix = "_pt2_" + std::to_string(i_pt+1) + "_" + w_names.at(i_w);
      TCanvas *c2_incl = new TCanvas( ("c_incl"+suffix).c_str() );
      c2_incl->SetLogy();

      h2_tot_incl.at(w.size()*i_pt+i_w)->Draw("HIST");
      h2_l_incl.at(w.size()*i_pt+i_w)->Draw("HISTSAME");
      h2_b_incl.at(w.size()*i_pt+i_w)->Draw("HISTSAME");
      h2_tot_incl.at(w.size()*i_pt+i_w)->Draw("HISTSAME");
      h2_data_incl.at(i_pt)->Draw("LPESAME");
      h2_tot_incl.at(w.size()*i_pt+i_w)->Draw("AXISSAME");

      pt->Draw();
      pt2->Draw();
      pt3->Draw();
      pt5->Clear();
      pt5->SetTextSize(0.04);
      pt5->AddText( (taggerName + ", " + WPName + ", " + w_names.at(i_w) + ", " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt+1)) + " GeV < p_{T,2}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt+2)) + " GeV").c_str() );
      pt5->Draw();
      legend_data->Draw();

      f_output->cd();
      d_data_MC->cd();
      c2_incl->Write();

    } // end prefit+postfit

  } // end pT loop
*/
  std::cout << " done." << std::endl;

  // ---------------chi2 ----------
  // global
  std::cout << "########## global chi2 ##########" << std::endl;
  std::cout << "PreFit: chi2, " << chi2_Prefit << ", ndf, " << ndf_Prefit << ", chi2/ndf, " << chi2_Prefit/ndf_Prefit << std::endl;
  std::cout << "PostFit: chi2, " << chi2_Postfit << ", ndf, " << ndf_Postfit << ", chi2/ndf, " << chi2_Postfit/ndf_Postfit << std::endl;

  // plot
  h_tmp_2d_pt->SetStats(0);
  h_tmp_2d_pt->Reset();
  h_tmp_2d_pt->SetTitle("");
  h_tmp_2d_pt->SetXTitle(pt_label_jet1.c_str());
  h_tmp_2d_pt->SetYTitle(pt_label_jet2.c_str());
  h_tmp_2d_pt->SetZTitle("% of #chi^{2}");
  h_tmp_2d_pt->GetXaxis()->SetLabelSize(0.05);
  h_tmp_2d_pt->GetXaxis()->SetTitleSize(0.05);
  h_tmp_2d_pt->GetXaxis()->SetTitleOffset(1.55);
  h_tmp_2d_pt->GetXaxis()->SetLabelOffset(0.01);
  h_tmp_2d_pt->GetYaxis()->SetLabelSize(0.05);
  h_tmp_2d_pt->GetYaxis()->SetTitleSize(0.05);
  h_tmp_2d_pt->GetYaxis()->SetTitleOffset(1.55);
  h_tmp_2d_pt->GetZaxis()->SetTitleOffset(1.05);

  std::string histname_chi2Frac_Prefit = "chi2Frac_Prefit";
  TH2D *h_chi2Frac_Prefit = (TH2D*)h_tmp_2d_pt->Clone(histname_chi2Frac_Prefit.c_str());

  std::string histname_chi2Frac_Postfit = "chi2Frac_Postfit";
  TH2D *h_chi2Frac_Postfit = (TH2D*)h_tmp_2d_pt->Clone(histname_chi2Frac_Postfit.c_str());

  h_chi2Frac_Prefit->SetMarkerColor(kBlack);
  h_chi2Frac_Prefit->SetFillColor(kBlack);
  h_chi2Frac_Prefit->SetLineColor(kBlack);
  h_chi2Frac_Prefit->SetBarOffset(+0.22);

  h_chi2Frac_Postfit->SetMarkerColor(kRed+1);
  h_chi2Frac_Postfit->SetFillColor(kRed+1);
  h_chi2Frac_Postfit->SetLineColor(kRed+1);
  h_chi2Frac_Postfit->SetBarOffset(-0.22);

  for(int i_pt1=0; i_pt1 < Nbin_pt; i_pt1++)
  {
    for(int i_pt2=0; i_pt2 <= i_pt1; i_pt2++)
    {
      h_chi2Frac_Prefit->SetBinContent(i_pt1+1, i_pt2+1, (chi2_pt_Prefit[i_pt1][i_pt2][0]+chi2_pt_Prefit[i_pt1][i_pt2][1])/chi2_Prefit);
      h_chi2Frac_Postfit->SetBinContent(i_pt1+1, i_pt2+1, (chi2_pt_Postfit[i_pt1][i_pt2][0]+chi2_pt_Postfit[i_pt1][i_pt2][1])/chi2_Postfit);
      h_chi2Frac_Prefit->SetBinError(i_pt1+1, i_pt2+1, 0.);
      h_chi2Frac_Postfit->SetBinError(i_pt1+1, i_pt2+1, 0.);
    }
  }

  std::string canvasname_chi2 = "c_chi2";
  TCanvas *c_chi2 = new TCanvas(canvasname_chi2.c_str());
  c_chi2->SetLeftMargin(0.15);
  c_chi2->SetRightMargin(0.15);

  c_chi2->cd();
  h_chi2Frac_Prefit->Draw("TEXTCOLZ");
  h_chi2Frac_Postfit->Draw("TEXTSAME");
  h_chi2Frac_Prefit->Draw("AXISSAME");

  c_chi2->cd();
  pt_2d->Draw();
  pt2_2d->Draw();
  pt3_2d->Draw();
  pt4_2d->Clear();
  pt4_2d->AddText( (releaseName + ", " + ChannelName + ", " + systName + ", " + dataName).c_str() );

  std::stringstream chi2_Prefit_string;
  std::stringstream chi2_Postfit_string;
  chi2_Prefit_string << std::setprecision(3) << chi2_Prefit/ndf_Prefit;
  chi2_Postfit_string << std::setprecision(3) << chi2_Postfit/ndf_Postfit;
  pt4_2d->AddText( (taggerName + ", " + WPName + ", " + ", #chi^{2}/ndf = " + chi2_Prefit_string.str() + " (Postfit, " + chi2_Postfit_string.str() + ")").c_str() );
  pt4_2d->Draw();
  legend_fit_2d->Clear();
  legend_fit_2d->AddEntry(h_chi2Frac_Prefit, "Prefit", "F");
  legend_fit_2d->AddEntry(h_chi2Frac_Postfit, "Postfit", "F");
  legend_fit_2d->Draw();
  f_output->cd();
  c_chi2->Write();
  fitResult->Write();

  //

  return 0;
}
