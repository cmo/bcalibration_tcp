#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <iomanip>
#include <sys/stat.h>
#include <unistd.h>
#include "TH1.h"
#include "TH2.h"
#include "TMatrixDSymEigen.h"
#include "TMatrixDEigen.h"
#include <THnSparse.h>
#include "TFile.h"
#include "TPaveText.h"
#include "TLegend.h"
#include "TLine.h"
#include "TCanvas.h"
#include <string>
#include "RooGlobalFunc.h"
#include "RooGaussian.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooSimultaneous.h"
#include "RooParametricStepFunction.h"
#include "RooConstVar.h"
#include "RooWorkspace.h"
#include "RooCategory.h"
#include "RooUniform.h"
#include "RooExtendPdf.h"
#include "RooAddPdf.h"
#include "RooBinning.h"
#include "RooProdPdf.h"
#include "RooFit.h"
#include "TObjString.h"
#include "TVectorD.h"
#include "RooArgList.h"
#include "RooFitResult.h"
#include "TGraphAsymmErrors.h"
#include "../smoothing/Smoothing.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <iomanip>
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include <THnSparse.h>
#include "TFile.h"
#include <string>
#include "TCanvas.h"
#include "TSystem.h"
#include "Check_fit.h"

#include "../atlasstyle-00-03-05/AtlasStyle.C"
using namespace std;

std::vector<std::string> wp_names = {"90", "85", "77", "70", "65"}; //
const std::vector<int> nice_colours = {(kRed + 1), (kOrange - 1), (kBlue + 1), (kGray + 1), (kBlack),  (kGreen - 8), (kCyan + 1)};
std::string lumi_global = "";
std::string CME_global="";
bool pt_binning_as_eta = false;

void SmoothSyst(std::string syst_name, std::string wp_name, TDirectory *input_dir, TH1D *up_input, TH1D *down_input, bool write_smoothed_in_input_dir = true, bool smooth_up_only = true)
{
	TH1::AddDirectory(kFALSE);
	//gROOT->LoadMacro("MyHelpers.C");
	// SetAtlasStyle();

	// Set the x-axis range, 1 bin below and above, where possible.
	Double_t minval = up_input->GetXaxis()->GetXmin(), maxval = up_input->GetXaxis()->GetXmax(); // pT_jet [GeV]
	cout << "minval " << minval << " maxval " << maxval << endl;
	string up_name, down_name, up_title, down_title;
	up_name = up_input->GetName();
	up_title = up_input->GetTitle();

	down_name = down_input->GetName();
	down_title = down_input->GetTitle();

	TDirectory *smoothing_dir = 0;
	// Location of output file and plots
	smoothing_dir = input_dir->GetDirectory(("smoothing_" + syst_name).c_str());
	if (smoothing_dir == 0)
	{
		smoothing_dir = input_dir->mkdir((("smoothing_" + syst_name).c_str()));
		std::cout << "Made directory: " << ("smoothing_" + syst_name).c_str() << std::endl;
	}
	smoothing_dir->cd();

	// Various useful variables
	Double_t maxup = 0.0, maxdown = 0.0;

	TH1D *temp = NULL;
	TH1D *up = NULL, *down = NULL;
	TH1D *upsig = NULL, *downsig = NULL;
	TH1D *upfine = NULL, *downfine = NULL;
	TH1D *upsmooth = NULL, *downsmooth = NULL;

	// Set the unused bins to 0.0
	up = new TH1D(*up_input);
	smooth::RemoveUnused(up, minval, maxval, maxup);

	// Get the negative portion of current uncertainty

	// Set the unused bins to 0.0
	down = new TH1D(*down_input);
	smooth::RemoveUnused(down, minval, maxval, maxdown);

	// If it's just floating point percision jitter from a component which is 0,
	// set it to zero, write, and continue.
	if (fabs(maxup) < 1e-6 && fabs(maxdown) < 1e-6)
	{
		Printf("exiting, too small");
		smooth::SetZero(up);
		smooth::SetZero(down);
		up->Write();
		down->Write();
		input_dir->cd();
		up_input->Write();
		down_input->Write();
		return;
	}

	// Rebin the histogram by combining neighbors until all bins
	// are significant from 0.
	upsig = smooth::RebinUntilSignificant(*up, minval, maxval);
	downsig = smooth::RebinUntilSignificant(*down, minval, maxval);

	// Return to fine binning, although with rebinned values.
	upfine = smooth::RebinToFineTemplate(*upsig, *up);
	downfine = smooth::RebinToFineTemplate(*downsig, *down);

	// Perform Gaussian-kernel smoothing, if necessary

	// If not simply rebin into original bins.
	if (smooth_up_only)
	{
		upsmooth = smooth::GaussianKernelSmooth(*upfine, minval, maxval, 2);
		downsmooth = down;
	}
	else
	{
		upsmooth = smooth::GaussianKernelSmooth(*upfine, minval, maxval, 2);
		downsmooth = smooth::GaussianKernelSmooth(*downfine, minval, maxval, 2);
	}
	smoothing_dir->cd();
	// Plot the original, rebinned, and smoothed spectra.
	smooth::PlotSyst(up, upsig, upsmooth, down, downsig, downsmooth, wp_name, minval, maxval, " p_{T}^{jet} [GeV]", syst_name.c_str());

	up_input->SetName((up_name + "_unsmoothed").c_str());
	up_input->SetTitle((up_title + "_unsmoothed").c_str());
	up_input->Write();

	down_input->SetName((down_name + "_unsmoothed").c_str());
	down_input->SetTitle((down_title + "_unsmoothed").c_str());
	down_input->Write();
	upsmooth->SetName(up_name.c_str());
	upsmooth->SetTitle(up_title.c_str());
	upsmooth->Write();

	downsmooth->SetName(down_name.c_str());
	downsmooth->SetTitle(down_title.c_str());
	downsmooth->Write();

	// Write the smoothed spectra to output file.
	// input_dir->cd();
	// if (write_smoothed_in_input_dir){
	//     upsmooth->SetName(up_name.c_str());
	//     upsmooth->SetTitle(up_title.c_str());
	//     upsmooth->Write();
	//
	//     downsmooth->SetName(down_name.c_str());
	//     downsmooth->SetTitle(down_title.c_str());
	//     downsmooth->Write();
	// }
}

int exist(const char *name)
{
	struct stat buffer;
	return (stat(name, &buffer) == 0);
}

class Systematics_Container
{
	string name;
	map<string, TH1D *> e_b_syst_up_Error;   //= (TH1D*) e_b_Postfit_up->Clone(("e_b_"+wp_name+"_"+systName_short+"_syst_up_Error_rel").c_str());
	map<string, TH1D *> e_b_syst_down_Error; //= (TH1D*) e_b_Postfit_down->Clone(("e_b_"+wp_name+"_"+systName_short+"_syst_down_Error_rel").c_str());
	map<string, TH1D *> e_b_syst_high_Error; //= (TH1D*) e_b_Postfit_up->Clone(("e_b_"+wp_name+"_"+systName_short+"_syst_up_Error_rel").c_str());
	map<string, TH1D *> e_b_syst_low_Error;  //= (TH1D*) e_b_Postfit_down->Clone(("e_b_"+wp_name+"_"+systName_short+"_syst_down_Error_rel").c_str());
	map<string, TH1D *> e_b_syst_Error;		 //= (TH1D*) e_b_Postfit_down->Clone(("e_b_"+wp_name+"_"+systName_short+"_syst_Error_rel").c_str());
	map<string, TH1D *> p_b_syst_up_Error;   //= (TH1D*) e_b_Postfit_up->Clone(("e_b_"+wp_name+"_"+systName_short+"_syst_up_Error_rel").c_str());
	map<string, TH1D *> p_b_syst_down_Error; //= (TH1D*) e_b_Postfit_down->Clone(("e_b_"+wp_name+"_"+systName_short+"_syst_down_Error_rel").c_str());
	map<string, TH1D *> p_b_syst_high_Error; //= (TH1D*) e_b_Postfit_up->Clone(("e_b_"+wp_name+"_"+systName_short+"_syst_up_Error_rel").c_str());
	map<string, TH1D *> p_b_syst_low_Error;  //= (TH1D*) e_b_Postfit_down->Clone(("e_b_"+wp_name+"_"+systName_short+"_syst_down_Error_rel").c_str());
	map<string, TH1D *> p_b_syst_Error;		 //= (TH1D*) e_b_Postfit_down->Clone(("e_b_"+wp_name+"_"+systName_short+"_syst_Error_rel").c_str());
	std::vector<std::string> pt_names;
	TFile *f_out;
	TDirectory* out_dir;
	TFile *f_bootstrap_up = 0;
	TFile *f_bootstrap_down = 0;
	bool symmetrize = false;

  public:
	//conrtuctor for two sided systematics
	Systematics_Container(string systematic_name, std::string folder_name, TFile *f_out, TFile *f_nominal_up, string filename_up, string filename_down, TFile *f_nominal_down = 0)
	{
		this->name = systematic_name;
		this->f_out = f_out;
		this->out_dir = f_out->GetDirectory(( folder_name + "_systematics").c_str());
		if (this->out_dir == 0)
			this->out_dir = f_out->mkdir(((folder_name + "_systematics").c_str()));
		this->out_dir->cd();
		if (f_nominal_down == 0)
		{
			f_nominal_down = f_nominal_up;
		}
		else
		{
			cout << "Processing systematic with differnet up and down nominal sample " << systematic_name << " in " << name << endl;
		}
		TFile *f_up = new TFile(filename_up.c_str(), "READ");
		if (check_fit( f_up, filename_up)) {
  			std::cerr << " ... exiting." << std::endl;
  			//exit(-1);
  		}
		std::string boot_file_name = filename_up;
		boot_file_name.erase(boot_file_name.end() - 5, boot_file_name.end());
		boot_file_name = boot_file_name + to_string("_bootStrap_combination.root");
		if (exist(boot_file_name.c_str()))
		{
			cout << "opening :" << boot_file_name << endl;
			f_bootstrap_up = new TFile(boot_file_name.c_str(), "READ");
			f_out->cd();
			if (f_bootstrap_up->IsZombie())
			{
				cout << "Error while opening :" << boot_file_name << endl;
				f_bootstrap_up = 0;
			}
		}
		TFile *f_down = new TFile(filename_down.c_str(), "READ");
		if (check_fit( f_down, filename_down)) {
  			std::cerr << " ... exiting." << std::endl;
  			//exit(-1);
  		}
		boot_file_name = filename_down;
		boot_file_name.erase(boot_file_name.end() - 5, boot_file_name.end());
		boot_file_name = boot_file_name + to_string("_bootStrap_combination.root");
		if (exist(boot_file_name.c_str()))
		{
			cout << "opening :" << boot_file_name << endl;
			f_bootstrap_down = new TFile(boot_file_name.c_str(), "READ");
			f_out->cd();
			if (f_bootstrap_down->IsZombie())
			{
				cout << "Error while opening :" << boot_file_name << endl;
				f_bootstrap_down = 0;
			}
		}

		for (const auto &wp_name : wp_names)
		{
			//cout<<"initializing "<<wp_name<<" in "<< name<<endl;
			TH1D *e_b_Postfit_nominal_down = (TH1D *)f_nominal_down->Get(("sf_and_beff/e_b_" + wp_name + "_Postfit").c_str());
			TH1D *e_b_Postfit_nominal_up = (TH1D *)f_nominal_up->Get(("sf_and_beff/e_b_" + wp_name + "_Postfit").c_str());
			TH1D *e_b_Postfit_up = (TH1D *)f_up->Get(("sf_and_beff/e_b_" + wp_name + "_Postfit").c_str());
			TH1D *e_b_Postfit_down = (TH1D *)f_down->Get(("sf_and_beff/e_b_" + wp_name + "_Postfit").c_str());
			f_out->cd();
			e_b_syst_up_Error[wp_name] = (TH1D *)e_b_Postfit_up->Clone(("e_b_" + wp_name + "_" + systematic_name + "_syst_up_Error_rel").c_str());
			e_b_syst_up_Error[wp_name]->Add(e_b_Postfit_nominal_up, -1);
			e_b_syst_up_Error[wp_name]->Divide(e_b_Postfit_nominal_up);
			e_b_syst_up_Error[wp_name]->GetYaxis()->SetTitle("relative uncertainty");

			e_b_syst_down_Error[wp_name] = (TH1D *)e_b_Postfit_down->Clone(("e_b_" + wp_name + "_" + systematic_name + "_syst_down_Error_rel").c_str());
			e_b_syst_down_Error[wp_name]->Add(e_b_Postfit_nominal_down, -1);
			e_b_syst_down_Error[wp_name]->Divide(e_b_Postfit_nominal_down);
			e_b_syst_down_Error[wp_name]->GetYaxis()->SetTitle("relative uncertainty");
			this->initialize(wp_name);
		}
		TH1D *e_b_Postfit_nominal = (TH1D *)f_nominal_down->Get(("sf_and_beff/e_b_" + wp_names.at(0) + "_Postfit").c_str());
		int n_pt_bins = e_b_Postfit_nominal->GetNbinsX();
		for (int bin = 1; bin <= n_pt_bins; bin++)
		{
			//cout<<"initializing "<<wp_name<<" in "<< name<<endl;
			string pt_name = "pt_" + to_string(bin);
			pt_names.push_back(pt_name);
			TH1D *p_b_pt_Postfit_nominal_down = (TH1D *)f_nominal_down->Get(("pdf/p_b_" + pt_name + "_Postfit").c_str());
			TH1D *p_b_pt_Postfit_nominal_up = (TH1D *)f_nominal_up->Get(("pdf/p_b_" + pt_name + "_Postfit").c_str());
			TH1D *p_b_pt_Postfit_up = (TH1D *)f_up->Get(("pdf/p_b_" + pt_name + "_Postfit").c_str());
			TH1D *p_b_pt_Postfit_down = (TH1D *)f_down->Get(("pdf/p_b_" + pt_name + "_Postfit").c_str());
			f_out->cd();
			p_b_syst_up_Error[pt_name] = (TH1D *)p_b_pt_Postfit_up->Clone(("p_b_" + pt_name + "_" + systematic_name + "_syst_up_Error_rel").c_str());
			p_b_syst_up_Error[pt_name]->Add(p_b_pt_Postfit_nominal_up, -1);
			p_b_syst_up_Error[pt_name]->Divide(p_b_pt_Postfit_nominal_up);
			p_b_syst_up_Error[pt_name]->GetYaxis()->SetTitle("relative uncertainty");

			p_b_syst_down_Error[pt_name] = (TH1D *)p_b_pt_Postfit_down->Clone(("p_b_" + pt_name + "_" + systematic_name + "_syst_down_Error_rel").c_str());
			p_b_syst_down_Error[pt_name]->Add(p_b_pt_Postfit_nominal_down, -1);
			p_b_syst_down_Error[pt_name]->Divide(p_b_pt_Postfit_nominal_down);
			p_b_syst_down_Error[pt_name]->GetYaxis()->SetTitle("relative uncertainty");
			this->initialize_pb(pt_name);
		}

		f_up->Close();
		f_down->Close();
	}
	//constructor for singlesided systematics
	Systematics_Container(string systematic_name,std::string folder_name, TFile *f_out, TFile *f_nominal, string filename_err, bool symmetrize = true, bool closure_test_systematic = false)
	{
		this->name = systematic_name;
		this->f_out = f_out;
		this->out_dir = f_out->GetDirectory(( folder_name + "_systematics").c_str());
		if (this->out_dir == 0)
			this->out_dir = f_out->mkdir(((folder_name + "_systematics").c_str()));
		this->out_dir->cd();
		this->symmetrize = symmetrize;
		TFile *f_err = new TFile(filename_err.c_str(), "READ");
		if (check_fit( f_err, filename_err)) {
  			std::cerr << " ... exiting." << std::endl;
			//exit(-1);
		}
		//lets see if we have a bootstrap container:
		std::string boot_file_name = filename_err;
		boot_file_name.erase(boot_file_name.end() - 5, boot_file_name.end());
		boot_file_name = boot_file_name + to_string("_bootStrap_combination.root");
		if (exist(boot_file_name.c_str()))
		{
			cout << "opening :" << boot_file_name << endl;
			f_bootstrap_up = new TFile(boot_file_name.c_str(), "READ");
			f_out->cd();
			if (f_bootstrap_up->IsZombie())
			{
				cout << "Error while opening :" << boot_file_name << endl;
				f_bootstrap_up = 0;
			}
		}

		for (const auto &wp_name : wp_names)
		{
			//cout<<"initializing "<<wp_name<<" in "<< name<<endl;
			if (closure_test_systematic)
			{
				TH1D *e_b_Postfit = (TH1D *)f_err->Get(("sf_and_beff/e_b_" + wp_name + "_Postfit").c_str());
				TH1D *e_b_Prefit = (TH1D *)f_err->Get(("sf_and_beff/e_b_" + wp_name + "_Prefit").c_str());
				f_out->cd();
				e_b_syst_up_Error[wp_name] = (TH1D *)e_b_Postfit->Clone(("e_b_" + wp_name + "_" + systematic_name + "_syst_up_Error_rel").c_str());
				e_b_syst_up_Error[wp_name]->Add(e_b_Prefit, -1);
				e_b_syst_up_Error[wp_name]->Divide(e_b_Prefit);
				e_b_syst_up_Error[wp_name]->GetYaxis()->SetTitle("relative uncertainty");
			}
			else
			{
				TH1D *e_b_Postfit_nominal = (TH1D *)f_nominal->Get(("sf_and_beff/e_b_" + wp_name + "_Postfit").c_str());
				TH1D *e_b_Postfit_err = (TH1D *)f_err->Get(("sf_and_beff/e_b_" + wp_name + "_Postfit").c_str());
				f_out->cd();
				e_b_syst_up_Error[wp_name] = (TH1D *)e_b_Postfit_err->Clone(("e_b_" + wp_name + "_" + systematic_name + "_syst_up_Error_rel").c_str());
				e_b_syst_up_Error[wp_name]->Add(e_b_Postfit_nominal, -1);
				e_b_syst_up_Error[wp_name]->Divide(e_b_Postfit_nominal);
				e_b_syst_up_Error[wp_name]->GetYaxis()->SetTitle("relative uncertainty");
			}
			e_b_syst_down_Error[wp_name] = (TH1D *)e_b_syst_up_Error[wp_name]->Clone(("e_b_" + wp_name + "_" + systematic_name + "_syst_down_Error_rel").c_str());
			if (symmetrize)
				e_b_syst_down_Error[wp_name]->Scale(-1.);
			this->initialize(wp_name);
		}
		TH1D *e_b_Postfit_nominal = (TH1D *)f_nominal->Get(("sf_and_beff/e_b_" + wp_names.at(0) + "_Postfit").c_str());
		int n_pt_bins = e_b_Postfit_nominal->GetNbinsX();
		for (int bin = 1; bin <= n_pt_bins; bin++)
		{
			string pt_name = "pt_" + to_string(bin);
			pt_names.push_back(pt_name);
			if (closure_test_systematic)
			{
				TH1D *p_b_pt_Postfit = (TH1D *)f_err->Get(("pdf/p_b_" + pt_name + "_Postfit").c_str());
				TH1D *p_b_pt_Prefit = (TH1D *)f_err->Get(("pdf/p_b_" + pt_name + "_Prefit").c_str());
				f_out->cd();
				p_b_syst_up_Error[pt_name] = (TH1D *)p_b_pt_Postfit->Clone(("p_b_" + pt_name + "_" + systematic_name + "_syst_up_Error_rel").c_str());
				p_b_syst_up_Error[pt_name]->Add(p_b_pt_Prefit, -1);
				p_b_syst_up_Error[pt_name]->Divide(p_b_pt_Prefit);
				p_b_syst_up_Error[pt_name]->GetYaxis()->SetTitle("relative uncertainty");
			}
			else
			{
				TH1D *p_b_pt_Postfit_nominal = (TH1D *)f_nominal->Get(("pdf/p_b_" + pt_name + "_Postfit").c_str());
				TH1D *p_b_pt_Postfit_up = (TH1D *)f_err->Get(("pdf/p_b_" + pt_name + "_Postfit").c_str());
				f_out->cd();
				p_b_syst_up_Error[pt_name] = (TH1D *)p_b_pt_Postfit_up->Clone(("p_b_" + pt_name + "_" + systematic_name + "_syst_up_Error_rel").c_str());
				p_b_syst_up_Error[pt_name]->Add(p_b_pt_Postfit_nominal, -1);
				p_b_syst_up_Error[pt_name]->Divide(p_b_pt_Postfit_nominal);
				p_b_syst_up_Error[pt_name]->GetYaxis()->SetTitle("relative uncertainty");
			}
			p_b_syst_down_Error[pt_name] = (TH1D *)p_b_syst_up_Error[pt_name]->Clone(("p_b_" + pt_name + "_" + systematic_name + "_syst_down_Error_rel").c_str());
			if (symmetrize)
				p_b_syst_down_Error[pt_name]->Scale(-1.);
			this->initialize_pb(pt_name);
		}
		if (f_bootstrap_up)
		{
			f_bootstrap_up->Close();
		}
		f_err->Close();
	}
	// //constructor to load from other production file
	// Systematics_Container(string systematic_name, TFile *f_out, TFile *f_in)
	// {
	// 	this->name = systematic_name + "_loaded";
	// 	this->f_out = f_out;
	// 	if (!f_in) {
	// 		std::cerr << "Input file: from other prod not found for systematic: " << systematic_name << " ... exiting." << std::endl;
	// 		exit(-1);
	// 	}
	//
	//
	// 	for (const auto &wp_name : wp_names) {
	// 		TH1D *e_b_Postfit_up = (TH1D *)f_in->Get(("e_b_" + wp_name + "_" + systematic_name + "_syst_up_Error_rel").c_str());
	// 		TH1D *e_b_Postfit_down = (TH1D *)f_in->Get(("e_b_" + wp_name + "_" + systematic_name + "_syst_down_Error_rel").c_str());
	// 		f_out->cd();
	// 		e_b_syst_up_Error[wp_name] = (TH1D *)e_b_Postfit_up->Clone(("e_b_" + wp_name + "_" + name + "_syst_up_Error_rel").c_str());
	// 		e_b_syst_down_Error[wp_name] = (TH1D *)e_b_Postfit_down->Clone(("e_b_" + wp_name + "_" + name + "_syst_down_Error_rel").c_str());
	// 		this->initialize(wp_name);
	// 	}
	// }
	//constructor to load from bootstrap
	Systematics_Container(string systematic_name, std::string folder_name, TFile *f_out, TFile *f_in)
	{
		this->name = systematic_name;
		cout << "processing: " << systematic_name << endl;
		this->f_out = f_out;
		this->out_dir = f_out->GetDirectory(( folder_name + "_systematics").c_str());
		if (this->out_dir == 0)
			this->out_dir = f_out->mkdir(((folder_name + "_systematics").c_str()));
		this->out_dir->cd();
		int n_pt_bins = 0;
		if (!f_in)
		{
			std::cerr << "Input file: from other prod not found for systematic: " << systematic_name << " ... exiting." << std::endl;
			exit(-1);
		}
		for (const auto &wp_name : wp_names)
		{
			TH1D *e_b_Postfit_up = (TH1D *)f_in->Get(("e_b_" + wp_name + "_Postfit_boot_strap_unc_rel").c_str());
			e_b_Postfit_up->GetYaxis()->SetTitle("relative uncertainty");
			n_pt_bins = e_b_Postfit_up->GetNbinsX();
			f_out->cd();
			e_b_syst_up_Error[wp_name] = (TH1D *)e_b_Postfit_up->Clone(("e_b_" + wp_name + "_" + name + "_syst_up_Error_rel").c_str());
			e_b_syst_down_Error[wp_name] = (TH1D *)e_b_Postfit_up->Clone(("e_b_" + wp_name + "_" + name + "_syst_down_Error_rel").c_str());
			e_b_syst_down_Error[wp_name]->Scale(-1);
			this->initialize(wp_name);
		}
		for (int bin = 1; bin <= n_pt_bins; bin++)
		{
			cout << "loading pt_bin: " << bin << endl;
			string pt_name = "pt_" + to_string(bin);
			pt_names.push_back(pt_name);
			TH1D *p_b_pt_Postfit_up = (TH1D *)f_in->Get(("p_b_" + pt_name + "_Postfit_boot_strap_unc_rel").c_str());
			p_b_pt_Postfit_up->GetYaxis()->SetTitle("relative uncertainty");
			f_out->cd();
			p_b_syst_up_Error[pt_name] = (TH1D *)p_b_pt_Postfit_up->Clone(("p_b_" + pt_name + "_" + name + "_syst_up_Error_rel").c_str());
			p_b_syst_down_Error[pt_name] = (TH1D *)p_b_syst_up_Error[pt_name]->Clone(("p_b_" + pt_name + "_" + systematic_name + "_syst_down_Error_rel").c_str());
			p_b_syst_down_Error[pt_name]->Scale(-1.);
			this->initialize_pb(pt_name);
		}
	}

	~Systematics_Container()
	{
	}

	TH1D *GetUpError(string wp_name)
	{
		if (e_b_syst_up_Error.find(wp_name) != e_b_syst_up_Error.end())
		{
			return e_b_syst_up_Error[wp_name];
		}
		else
		{
			cout << "Error! " << wp_name << " in " << name << " not found!" << endl;
			exit(-1);
		}
	}
	TH1D *GetPbUpError(string pt_name)
	{
		if (p_b_syst_up_Error.find(pt_name) != p_b_syst_up_Error.end())
		{
			return p_b_syst_up_Error[pt_name];
		}
		else
		{
			cout << "Error! " << pt_name << " in " << name << " not found!" << endl;
			exit(-1);
		}
	}
	TH1D *GetDownError(string wp_name)
	{
		if (e_b_syst_down_Error.find(wp_name) != e_b_syst_down_Error.end())
		{
			return e_b_syst_down_Error[wp_name];
		}
		else
		{
			cout << "Error! " << wp_name << " in " << name << " not found!" << endl;
			exit(-1);
		}
	}
	TH1D *GetPbDownError(string pt_name)
	{
		if (p_b_syst_down_Error.find(pt_name) != p_b_syst_down_Error.end())
		{
			return p_b_syst_down_Error[pt_name];
		}
		else
		{
			cout << "Error! " << pt_name << " in " << name << " not found!" << endl;
			exit(-1);
		}
	}
	TH1D *GetHighError(string wp_name)
	{
		if (e_b_syst_high_Error.find(wp_name) != e_b_syst_high_Error.end())
		{
			return e_b_syst_high_Error[wp_name];
		}
		else
		{
			cout << "Error! " << wp_name << " in " << name << " not found!" << endl;
			exit(-1);
		}
	}
	TH1D *GetPbHighError(string pt_name)
	{
		if (p_b_syst_high_Error.find(pt_name) != p_b_syst_high_Error.end())
		{
			return p_b_syst_high_Error[pt_name];
		}
		else
		{
			cout << "Error! " << pt_name << " in " << name << " not found!" << endl;
			exit(-1);
		}
	}
	TH1D *GetLowError(string wp_name)
	{
		return e_b_syst_low_Error[wp_name];
	}
	TH1D *GetPbLowError(string pt_name)
	{
		if (p_b_syst_low_Error.find(pt_name) != p_b_syst_low_Error.end())
		{
			return p_b_syst_low_Error[pt_name];
		}
		else
		{
			cout << "Error! " << pt_name << " in " << name << " not found!" << endl;
			exit(-1);
		}
	}

	TH1D *GetSymError(string wp_name)
	{
		return e_b_syst_Error[wp_name];
	}
	TH1D *GetPbSymError(string pt_name)
	{
		return p_b_syst_Error[pt_name];
	}
	string GetName()
	{
		return this->name;
	}
	void SavePlot(string wp_name, std::string releaseName, std::string taggerName, std::string WPName, std::string ChannelName, std::string dataName, std::string fitConfigName, bool used = true)
	{
		TDirectory *dir;
		dir = out_dir->GetDirectory((("e_b_wp_" + wp_name).c_str()));
		if (dir == 0)
			dir = out_dir->mkdir((("e_b_wp_" + wp_name).c_str()));
		dir->cd();
		// cout<<"try to safe: "<< name <<wp_name<<endl;
		TH1D *up_err = this->GetUpError(wp_name);
		TH1D *down_err = this->GetDownError(wp_name);
		TH1D *sym_err = this->GetSymError(wp_name);
		if (!used)
		{
			up_err = (TH1D *)up_err->Clone((string(up_err->GetName()) + "_unused").c_str());
			down_err = (TH1D *)down_err->Clone((string(down_err->GetName()) + "_unused").c_str());
			sym_err = (TH1D *)sym_err->Clone((string(sym_err->GetName()) + "_unused").c_str());
		}
		up_err->Write();
		down_err->Write();
		sym_err->Write();

		// create new canvas

		// Legend - pT plots (prefit/postfit)
		TLegend *legend_fit = new TLegend(0.206304, 0.214135, 0.482808, 0.351266);
		legend_fit->SetTextFont(42);
		legend_fit->SetBorderSize(0);  // no border
		legend_fit->SetFillColor(0);   // Legend background should be white
		legend_fit->SetTextSize(0.04); // Increase entry font size!

		// ATLAS label, sqrt(s), lumi
		TPaveText *pt = new TPaveText(0.228354, 0.841772, 0.348735, 0.909283, "brNDC");
		pt->SetBorderSize(0);
		pt->SetFillColor(0);
		pt->SetTextSize(0.05);
		pt->SetTextFont(72);
		pt->AddText("ATLAS");

		TPaveText *pt2 = new TPaveText(0.354334, 0.841772, 0.474714, 0.909283, "brNDC");
		pt2->SetBorderSize(0);
		pt2->SetFillColor(0);
		pt2->SetTextSize(0.05);
		pt2->SetTextFont(42);
		pt2->AddText("Internal");

		TPaveText *pt3 = new TPaveText(0.570201, 0.841772, 0.690544, 0.909283, "brNDC");
		pt3->SetBorderSize(0);
		pt3->SetFillColor(0);
		pt3->SetTextSize(0.05);
		pt3->SetTextFont(42);
	        pt3->AddText((to_string("#sqrt{s} = ") + CME_global + " TeV, " + lumi_global + " fb^{-1}").c_str());

		TPaveText *pt4 = new TPaveText(0.570201, 0.704641, 0.689112, 0.829114, "brNDC");
		pt4->SetBorderSize(0);
		pt4->SetFillColor(0);
		pt4->SetTextSize(0.05);
		pt4->SetTextFont(42);

		TPaveText *unused_text = new TPaveText(0.6432, 0.008438, 0.762178, 0.132911, "brNDC");
		unused_text->SetBorderSize(0);
		unused_text->SetFillColor(0);
		unused_text->SetTextSize(0.05);
		unused_text->SetTextFont(42);
		unused_text->SetTextColor(kRed + 1);
		unused_text->AddText("unused");

		TLine *h_line0 = new TLine(-1., 0, 30000, 0);

		std::string canvasname = "c_e_b_" + wp_name + "_" + name + "_syst_Error";
		TCanvas *c_obs = new TCanvas(canvasname.c_str());
		if (!pt_binning_as_eta)
			c_obs->SetLogx();
		legend_fit->Clear();
		legend_fit->AddEntry(sym_err, "(up-down)/2", "LPE");
		double max_l = sym_err->GetBinContent(sym_err->GetMaximumBin());
		if (max_l < 0.05)
			max_l = 0.05;
		else
			max_l = max_l * 1.2;
		sym_err->GetYaxis()->SetRangeUser(-1 * max_l, max_l);
		sym_err->GetXaxis()->SetRangeUser(20., 400.);
		//sym_err->GetXaxis()->SetRangeUser(5., 250.);//runVR
		sym_err->Draw("LPE");
		h_line0->DrawLine(20., 0., 400., 0);
		////h_line0->DrawLine(5., 0., 250., 0);
		down_err->SetLineColor(kGreen - 8);
		down_err->SetMarkerColor(kGreen - 8);
		if (symmetrize)
			legend_fit->AddEntry(down_err, "down - symmetrized", "LPE");
		else
			legend_fit->AddEntry(down_err, "down", "LPE");
		down_err->Draw("LPESAME");
		up_err->SetLineColor(kRed + 1);
		up_err->SetMarkerColor(kRed + 1);
		if (symmetrize)
			legend_fit->AddEntry(up_err, "up = sys-nominal", "LPE");
		else
			legend_fit->AddEntry(up_err, "up", "LPE");
		up_err->Draw("LPESAME");
		// tune and record canvas
		c_obs->cd();
		pt->Draw();
		pt2->Draw();
		pt3->Draw();
		pt4->Clear();
		pt4->AddText((releaseName + ", " + dataName + ", " + ChannelName + ", " + fitConfigName).c_str());
		pt4->AddText((taggerName + ", " + WPName + ", #epsilon_{b}^{MC} = " + wp_name + "%" + ", " + name).c_str());
		pt4->Draw();
		legend_fit->Draw();
		if (!used)
			unused_text->Draw();
		c_obs->Write();
		//cout<<"safed"<<endl;
	}
	void SavePlot_pb(string pt_name, std::string releaseName, std::string taggerName, std::string WPName, std::string ChannelName, std::string dataName, std::string fitConfigName, bool used = true)
	{
		TDirectory *dir;
		dir = out_dir->GetDirectory((("p_b_pt_" + pt_name).c_str()));
		if (dir == 0)
			dir = out_dir->mkdir((("p_b_pt_" + pt_name).c_str()));
		dir->cd();
		//cout<<"try to safe: "<< name <<wp_name<<endl;
		TH1D *up_err = this->GetPbUpError(pt_name);
		TH1D *down_err = this->GetPbDownError(pt_name);
		TH1D *sym_err = this->GetPbSymError(pt_name);
		if (!used)
		{
			up_err = (TH1D *)up_err->Clone((string(up_err->GetName()) + "_unused").c_str());
			down_err = (TH1D *)down_err->Clone((string(down_err->GetName()) + "_unused").c_str());
			sym_err = (TH1D *)sym_err->Clone((string(sym_err->GetName()) + "_unused").c_str());
		}
		up_err->Write();
		down_err->Write();
		sym_err->Write();

		// create new canvas

		// Legend - pT plots (prefit/postfit)
		TLegend *legend_fit = new TLegend(0.206304, 0.214135, 0.482808, 0.351266);
		legend_fit->SetTextFont(42);
		legend_fit->SetBorderSize(0);  // no border
		legend_fit->SetFillColor(0);   // Legend background should be white
		legend_fit->SetTextSize(0.04); // Increase entry font size!

		// ATLAS label, sqrt(s), lumi
		TPaveText *pt = new TPaveText(0.228354, 0.841772, 0.348735, 0.909283, "brNDC");
		pt->SetBorderSize(0);
		pt->SetFillColor(0);
		pt->SetTextSize(0.05);
		pt->SetTextFont(72);
		pt->AddText("ATLAS");

		TPaveText *pt2 = new TPaveText(0.354334, 0.841772, 0.474714, 0.909283, "brNDC");
		pt2->SetBorderSize(0);
		pt2->SetFillColor(0);
		pt2->SetTextSize(0.05);
		pt2->SetTextFont(42);
		pt2->AddText("Internal");

		TPaveText *pt3 = new TPaveText(0.570201, 0.841772, 0.690544, 0.909283, "brNDC");
		pt3->SetBorderSize(0);
		pt3->SetFillColor(0);
		pt3->SetTextSize(0.05);
		pt3->SetTextFont(42);
	        pt3->AddText((to_string("#sqrt{s} = ") + CME_global + " TeV, " + lumi_global + " fb^{-1}").c_str());

		TPaveText *pt4 = new TPaveText(0.570201, 0.704641, 0.689112, 0.829114, "brNDC");
		pt4->SetBorderSize(0);
		pt4->SetFillColor(0);
		pt4->SetTextSize(0.05);
		pt4->SetTextFont(42);

		TPaveText *unused_text = new TPaveText(0.6432, 0.008438, 0.762178, 0.132911, "brNDC");
		unused_text->SetBorderSize(0);
		unused_text->SetFillColor(0);
		unused_text->SetTextSize(0.05);
		unused_text->SetTextFont(42);
		unused_text->SetTextColor(kRed + 1);
		unused_text->AddText("unused");

		TLine *h_line0 = new TLine(-1., 0, 30000, 0);

		std::string canvasname = "c_p_b_" + pt_name + "_" + name + "_syst_Error";
		TCanvas *c_obs = new TCanvas(canvasname.c_str());
		//c_obs->SetLogy();
		legend_fit->Clear();
		legend_fit->AddEntry(sym_err, "(up-down)/2", "LPE");
		double max_l = sym_err->GetBinContent(sym_err->GetMaximumBin());
		if (max_l < 0.05)
			max_l = 0.05;
		else
			max_l = max_l * 1.2;
		sym_err->GetYaxis()->SetRangeUser(-1 * max_l, max_l);
		sym_err->GetXaxis()->SetRangeUser(20.,400.);
		sym_err->Draw("LPE");
		h_line0->DrawLine(20., 0., 400., 0);
		////h_line0->DrawLine(5., 0., 250., 0);
		down_err->SetLineColor(kGreen - 8);
		down_err->SetMarkerColor(kGreen - 8);
		if (symmetrize)
			legend_fit->AddEntry(down_err, "down - symmetrized", "LPE");
		else
			legend_fit->AddEntry(down_err, "down", "LPE");
		down_err->Draw("LPESAME");
		up_err->SetLineColor(kRed + 1);
		up_err->SetMarkerColor(kRed + 1);
		if (symmetrize)
			legend_fit->AddEntry(up_err, "up = sys-nominal", "LPE");
		else
			legend_fit->AddEntry(up_err, "up", "LPE");
		up_err->Draw("LPESAME");
		// tune and record canvas
		c_obs->cd();
		pt->Draw();
		pt2->Draw();
		pt3->Draw();
		pt4->Clear();
		pt4->AddText((releaseName + ", " + dataName + ", " + ChannelName + ", " + fitConfigName).c_str());
		pt4->AddText((taggerName + ", " + WPName + ", " + pt_name + ", " + name).c_str());
		pt4->Draw();
		legend_fit->Draw();
		if (!used)
			unused_text->Draw();
		c_obs->Write();
		//cout<<"safed"<<endl;
	}

	void SavePlots(std::string releaseName, std::string taggerName, std::string WPName, std::string ChannelName, std::string dataName, std::string fitConfigName,bool used = true)
	{
		for (const auto &wp_name : wp_names)
			this->SavePlot(wp_name, releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName,used);
		for (const auto &pt_name : pt_names)
			this->SavePlot_pb(pt_name, releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName, used);
	}

  private:
	void initialize(string wp_name)
	{
		e_b_syst_Error[wp_name] = (TH1D *)e_b_syst_up_Error[wp_name]->Clone(("e_b_" + wp_name + "_" + name + "_syst_Error_rel").c_str());
		e_b_syst_high_Error[wp_name] = (TH1D *)e_b_syst_up_Error[wp_name]->Clone(("e_b_" + wp_name + "_" + name + "_syst_high_Error_rel").c_str());
		e_b_syst_low_Error[wp_name] = (TH1D *)e_b_syst_up_Error[wp_name]->Clone(("e_b_" + wp_name + "_" + name + "_syst_low_Error_rel").c_str());

		//lets see if we have a bootstrap container:
		TH1D *h_boot_unc_up = 0;
		if (f_bootstrap_up)
		{
			h_boot_unc_up = (TH1D *)((TH1D *)f_bootstrap_up->Get(("e_b_" + wp_name + "_Postfit_boot_strap_unc_rel").c_str())->Clone(("e_b_" + wp_name + "_Postfit_boot_strap_unc_rel_up_tmp").c_str()));
			// h_boot_unc_up->Write();
		}
		TH1D *h_boot_unc_down = 0;
		if (f_bootstrap_down)
		{
			h_boot_unc_down = (TH1D *)((TH1D *)f_bootstrap_down->Get(("e_b_" + wp_name + "_Postfit_boot_strap_unc_rel").c_str())->Clone(("e_b_" + wp_name + "_Postfit_boot_strap_unc_rel_down_tmp").c_str()));
			// h_boot_unc_down->Write();
		}

		for (int bin = 0; bin <= e_b_syst_up_Error[wp_name]->GetNbinsX(); bin++)
		{
			//lets set unc if we have a bootstrap container:
			if (h_boot_unc_up)
			{
				e_b_syst_up_Error[wp_name]->SetBinError(bin, h_boot_unc_up->GetBinContent(bin));
			}
			else
			{
				e_b_syst_up_Error[wp_name]->SetBinError(bin, 0.);
			}
			if (h_boot_unc_down)
			{
				e_b_syst_down_Error[wp_name]->SetBinError(bin, h_boot_unc_down->GetBinContent(bin));
			}
			else
			{
				if (symmetrize)
				{
					e_b_syst_down_Error[wp_name]->SetBinError(bin, e_b_syst_up_Error[wp_name]->GetBinError(bin));
				}
				else
				{
					e_b_syst_down_Error[wp_name]->SetBinError(bin, 0);
				}
			}
		}
		if (h_boot_unc_up && !pt_binning_as_eta)
		{
			TDirectory *dir;
			dir = out_dir->GetDirectory((("e_b_wp_" + wp_name).c_str()));
			if (dir == 0)
				dir = out_dir->mkdir((("e_b_wp_" + wp_name).c_str()));
			dir->cd();
			cout << "smoothing for syst " << this->name << " wp: " << wp_name << endl;
			string up_name, down_name;
			up_name = e_b_syst_up_Error[wp_name]->GetName();
			down_name = e_b_syst_down_Error[wp_name]->GetName();
			TH1D* h_up_unc=e_b_syst_up_Error[wp_name];
			TH1D* h_down_unc=e_b_syst_down_Error[wp_name];

			//for the eta case we have to get rid of negative x-bins, the smoothing can not cope with this.
			TH1D* h_up_unc_help=0;
			TH1D* h_down_unc_help =0;
			if(pt_binning_as_eta)
			  {
			    cout << "we are using pt bins as eta, so we have to get rid of negative bins." << endl;
			    h_up_unc_help=new TH1D((up_name+"_help").c_str() ,(up_name+"_help").c_str(),h_up_unc->GetNbinsX(), 10, (h_up_unc->GetNbinsX()+1)*10);
			    h_down_unc_help=new TH1D((down_name+"_help").c_str() ,(down_name+"_help").c_str(),h_up_unc->GetNbinsX(), 10, (h_up_unc->GetNbinsX()+1)*10);
			    for (int bin=0;bin <= h_up_unc->GetNbinsX(); bin++)
			      {
				h_up_unc_help->SetBinContent(bin, h_up_unc->GetBinContent(bin));
				h_up_unc_help->SetBinError(bin, h_up_unc->GetBinError(bin));
				h_down_unc_help->SetBinContent(bin, h_down_unc->GetBinContent(bin));
				h_down_unc_help->SetBinError(bin, h_down_unc->GetBinError(bin));
			      }
			    //turn them around
			    TH1D* h_save=h_up_unc_help;
			    h_up_unc_help=h_up_unc;
			    h_up_unc= h_save;

			    h_save=h_down_unc_help;
			    h_down_unc_help=h_down_unc;
			    h_down_unc=h_save;

			    up_name=up_name+"_help";
			    down_name=down_name+"_help";
			  }


			bool smooth_down_only = false;
			if ((!h_boot_unc_down) && (!symmetrize))
			{
				smooth_down_only = true;
			}
			SmoothSyst(this->name, wp_name, dir, h_up_unc, h_down_unc, true, smooth_down_only);
			//SmoothSyst(this->name, wp_name, dir, e_b_syst_up_Error[wp_name], e_b_syst_down_Error[wp_name], true, smooth_down_only);
			e_b_syst_up_Error[wp_name] = (TH1D *)dir->Get((to_string("smoothing_") + this->name + "/" + up_name).c_str());
			e_b_syst_down_Error[wp_name] = (TH1D *)dir->Get((to_string("smoothing_") + this->name + "/" + down_name).c_str());
			cout << "done" << endl;
		}
		for (int bin = 0; bin <= e_b_syst_up_Error[wp_name]->GetNbinsX(); bin++)
		{
			/*double high_val, low_val;
			high_val = std::max(std::max(0., e_b_syst_up_Error[wp_name]->GetBinContent(bin)), e_b_syst_down_Error[wp_name]->GetBinContent(bin));
			e_b_syst_high_Error[wp_name]->SetBinContent(bin, high_val);
			e_b_syst_Error[wp_name]->SetBinError(bin, 0.);
			low_val = std::min(std::min(0., e_b_syst_up_Error[wp_name]->GetBinContent(bin)), e_b_syst_down_Error[wp_name]->GetBinContent(bin));
			e_b_syst_low_Error[wp_name]->SetBinContent(bin, low_val);
			e_b_syst_low_Error[wp_name]->SetBinError(bin, 0.);
			double up_val = e_b_syst_up_Error[wp_name]->GetBinContent(bin);
			double up_unc = e_b_syst_up_Error[wp_name]->GetBinError(bin);
			double down_val = e_b_syst_down_Error[wp_name]->GetBinContent(bin);
			double down_unc = e_b_syst_down_Error[wp_name]->GetBinError(bin);
			double average_sym = 0;
			double average_unc = 0;
			//if both have the same sign, lets take the one father away from 0. Otherwise we underestimate the unc.
			if (up_val * down_val > 0)
			{
				average_sym = up_val / abs(up_val) * std::max(abs(up_val), abs(down_val));
				average_unc = 0; //dont know what to do in this case.
			}
			//if one of them is take the other
			else if((abs(up_val) / abs(down_val)) >3){
				//upval much higher then down_val:
					average_sym = up_val;
					average_unc = up_unc;
			}	
			else if ((abs(down_val) / abs(up_val)) >3){
				//down_val much higher then up_val
				average_sym = -1* down_val; //in order to have something similar to downval but with the up sign
				average_unc = down_unc;
			}
			else
			{
				average_sym = (up_val - down_val) / 2.;
				average_unc = (up_unc + down_unc) / 2.;
			}*/
			double average_sym=(e_b_syst_up_Error[wp_name]->GetBinContent(bin) -e_b_syst_down_Error[wp_name]->GetBinContent(bin))/2.;
			double average_unc=(e_b_syst_up_Error[wp_name]->GetBinError(bin) + e_b_syst_down_Error[wp_name]->GetBinError(bin))/2.;
			e_b_syst_Error[wp_name]->SetBinContent(bin, average_sym);
			e_b_syst_Error[wp_name]->SetBinError(bin, average_unc);
			if (abs(e_b_syst_Error[wp_name]->GetBinContent(bin)) > 0.15)
			{
				std::cout << "WARNING!!!: ERROR too high!! Fit not converged?? Bin: " << bin << " wp: " << wp_name << " Error: " << e_b_syst_Error[wp_name]->GetBinContent(bin) << " systname: " << name << std::endl;
				std::cout << "e_b_syst_Error_down " << e_b_syst_down_Error[wp_name]->GetBinContent(bin) << " e_b_syst_Error_up " << e_b_syst_up_Error[wp_name]->GetBinContent(bin) << std::endl;
			}
		}
	}
	void initialize_pb(string pt_name)
	{
		p_b_syst_Error[pt_name] = (TH1D *)p_b_syst_up_Error[pt_name]->Clone(("p_b_" + pt_name + "_" + name + "_syst_Error_rel").c_str());
		p_b_syst_high_Error[pt_name] = (TH1D *)p_b_syst_up_Error[pt_name]->Clone(("p_b_" + pt_name + "_" + name + "_syst_high_Error_rel").c_str());
		p_b_syst_low_Error[pt_name] = (TH1D *)p_b_syst_up_Error[pt_name]->Clone(("p_b_" + pt_name + "_" + name + "_syst_low_Error_rel").c_str());

		//lets see if we have a bootstrap container:
		TH1D *h_boot_unc = 0;
		if (f_bootstrap_up)
		{
			h_boot_unc = (TH1D *)((TH1D *)f_bootstrap_up->Get(("p_b_" + pt_name + "_Postfit_boot_strap_unc_rel").c_str())->Clone(("p_b_" + pt_name + "_Postfit_boot_strap_unc_rel_tmp").c_str()));
			// h_boot_unc->Write();
		}

		for (int bin = 0; bin <= p_b_syst_up_Error[pt_name]->GetNbinsX(); bin++)
		{
			//lets set unc if we have a bootstrap container:
			if (h_boot_unc)
			{
				p_b_syst_up_Error[pt_name]->SetBinError(bin, h_boot_unc->GetBinContent(bin));
				p_b_syst_down_Error[pt_name]->SetBinError(bin, h_boot_unc->GetBinContent(bin));
			}
			else
			{
				p_b_syst_up_Error[pt_name]->SetBinError(bin, 0.);
				p_b_syst_down_Error[pt_name]->SetBinError(bin, 0.);
			}
			/*double high_val, low_val;
			high_val = std::max(std::max(0., p_b_syst_up_Error[pt_name]->GetBinContent(bin)), p_b_syst_down_Error[pt_name]->GetBinContent(bin));
			p_b_syst_high_Error[pt_name]->SetBinContent(bin, high_val);
			p_b_syst_Error[pt_name]->SetBinError(bin, 0.);
			low_val = std::min(std::min(0., p_b_syst_up_Error[pt_name]->GetBinContent(bin)), p_b_syst_down_Error[pt_name]->GetBinContent(bin));
			p_b_syst_low_Error[pt_name]->SetBinContent(bin, low_val);
			p_b_syst_low_Error[pt_name]->SetBinError(bin, 0.);
			// double average = abs(p_b_syst_up_Error->GetBinContent(bin)) + abs(p_b_syst_down_Error->GetBinContent(bin));
			double up_val = p_b_syst_up_Error[pt_name]->GetBinContent(bin);
			double down_val = p_b_syst_down_Error[pt_name]->GetBinContent(bin);
			double average_sym = 0;

			if (up_val * down_val > 0)
			{
				average_sym = up_val / abs(up_val) * std::max(abs(up_val), abs(down_val));
			}
			//if one of them is take the other
			else if((abs(up_val) / abs(down_val)) >3){
				//upval much higher then down_val:
					average_sym=up_val;
			}	
			else if ((abs(down_val) / abs(up_val)) >3){
				//down_val much higher then up_val
				average_sym= -1* down_val; //in order to have something similar to downval but with the up sign
			}
			else
			{
				average_sym = (up_val - down_val) / 2.;
			}*/
			double average_sym=(p_b_syst_up_Error[pt_name]->GetBinContent(bin) - p_b_syst_down_Error[pt_name]->GetBinContent(bin))/2.;
			p_b_syst_Error[pt_name]->SetBinContent(bin, average_sym);
			p_b_syst_Error[pt_name]->SetBinError(bin, 0.);
			if (abs(p_b_syst_Error[pt_name]->GetBinContent(bin)) > 0.30)
			{
				std::cout << "WARNING!!!: ERROR too high!! Fit not converged?? Bin: " << bin << " wp: " << pt_name << " Error: " << p_b_syst_Error[pt_name]->GetBinContent(bin) << " systname: " << name << std::endl;
				std::cout << "p_b_syst_Error_down " << p_b_syst_down_Error[pt_name]->GetBinContent(bin) << " p_b_syst_Error_up " << p_b_syst_up_Error[pt_name]->GetBinContent(bin) << std::endl;
			}
		}
		cout << "p_b_syst " << name << " pt: " << pt_name << endl;
		for (int i = 1; i <= p_b_syst_down_Error[pt_name]->GetNbinsX(); i++){
			cout << "Bin: " << i << " up: " << p_b_syst_up_Error[pt_name]->GetBinContent(i) << " down: " << p_b_syst_down_Error[pt_name]->GetBinContent(i) << " sym: " << p_b_syst_Error[pt_name]->GetBinContent(i) << endl;
		}
	}
};

int SystError_plottingMacro(std::string releaseName, std::string taggerName, std::string WPName, std::string ChannelName, std::string dataName, std::string dataLumi, std::string dataCME, std::string fitConfigName, std::vector<std::string> systNames, std::string inputfile_folder, std::string nominal_fit_combination_for_bin_mean)
{
	//std::string outputFile_str = "FitPlots_" + releaseName + "_" + taggerName + "_" + WPName + "_" + ChannelName + "_" + fitConfigName + "_" + systName + ".root";
	// get latest AtlasStyle package and include it in the repo
	// can be downloaded here: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PubComPlotStyle#ROOT_Style_for_official_ATLAS_pl
	SetAtlasStyle();
	gStyle->SetPaintTextFormat("1.3f");
	gStyle->SetNumberContours(12);
	bool combined_fit= false;
	if (fitConfigName == ""){
		combined_fit = true;
	}

	// 5 WPs for GN2v01
	if (taggerName == "GN2v01") {
	  wp_names.clear();
	  wp_names.emplace_back("90");
	  wp_names.emplace_back("85");
	  wp_names.emplace_back("77");
	  wp_names.emplace_back("70");
	  wp_names.emplace_back("65");
	}
	lumi_global = dataLumi;
	CME_global = dataCME;
	TLine *h_line1 = new TLine(-1., 1, 30000, 1);
	//h_line1->SetLineWidth(2);
	h_line1->SetLineStyle(2);
	h_line1->SetLineColorAlpha(1, 0.7);

	TLine *h_line0 = new TLine(-1., 0, 30000, 0);
	//h_line1->SetLineWidth(2);
	h_line1->SetLineStyle(2);
	h_line1->SetLineColorAlpha(1, 0.7);

	// ATLAS label, sqrt(s), lumi
	TPaveText *pt = new TPaveText(0.210602, 0.846316, 0.330946, 0.913684, "brNDC");
	pt->SetBorderSize(0);
	pt->SetFillColor(0);
	pt->SetTextSize(0.0631579);
	pt->SetTextFont(72);
	pt->AddText("ATLAS");

	TPaveText *pt2 = new TPaveText(0.363897, 0.848421, 0.484241, 0.915789, "brNDC");
	pt2->SetBorderSize(0);
	pt2->SetFillColor(0);
	pt2->SetTextSize(0.0631579);
	pt2->SetTextFont(42);
	pt2->AddText("Internal");

	TPaveText *pt3 = new TPaveText(0.663324, 0.848421, 0.769341, 0.915789, "brNDC");
	pt3->SetBorderSize(0);
	pt3->SetFillColor(0);
	pt3->SetTextSize(0.0631579);
	pt3->SetTextFont(42);
	pt3->AddText((to_string("#sqrt{s} = ") + CME_global + " TeV, " + lumi_global + " fb^{-1}").c_str());

	TPaveText *pt4 = new TPaveText(0.494986, 0.724211, 0.613897, 0.848421, "brNDC");
	pt4->SetBorderSize(0);
	pt4->SetFillColor(0);
	pt4->SetTextSize(0.0631579);
	pt4->SetTextFont(42);


	TPaveText *pt5 = new TPaveText(0.501433, 0.71308, 0.620344, 0.820675, "brNDC");
	pt5->SetBorderSize(0);
	pt5->SetFillColor(0);
	pt5->SetTextSize(0.035);
	pt5->SetTextFont(42);

	TPaveText *pt_2d = new TPaveText(0.200401, 0.843882, 0.360745, 0.913502, "brNDC");
	pt_2d->SetBorderSize(0);
	pt_2d->SetFillColor(0);
	pt_2d->SetTextSize(0.05);
	pt_2d->SetTextFont(72);
	pt_2d->AddText("ATLAS");

	TPaveText *pt2_2d = new TPaveText(0.355014, 0.844937, 0.475358, 0.914557, "brNDC");
	pt2_2d->SetBorderSize(0);
	pt2_2d->SetFillColor(0);
	pt2_2d->SetTextSize(0.05);
	pt2_2d->SetTextFont(42);
	pt2_2d->AddText("Internal");

	TPaveText *pt3_2d = new TPaveText(0.3149, 0.772152, 0.433811, 0.841772, "brNDC");
	pt3_2d->SetBorderSize(0);
	pt3_2d->SetFillColor(0);
	pt3_2d->SetTextSize(0.04);
	pt3_2d->SetTextFont(42);
	pt3_2d->AddText((to_string("#sqrt{s} = ") + CME_global + " TeV, " + lumi_global + " fb^{-1}").c_str());

	// Legend - pT plots (prefit/postfit)
	TLegend *legend_fit = new TLegend(0.206304, 0.4, 0.482808, 0.2);
	legend_fit->SetTextFont(42);
	legend_fit->SetBorderSize(0);  // no border
	legend_fit->SetFillColor(0);   // Legend background should be white
	legend_fit->SetTextSize(0.0547368 ); // Increase entry font size!

	TLegend *legend_eb = new TLegend(0.47851, 0.191579, 0.755014, 0.364211);
	legend_eb->SetTextFont(42);
	legend_eb->SetBorderSize(0);  // no border
	legend_eb->SetFillColor(0);   // Legend background should be white
	legend_eb->SetTextSize(0.0547368 ); // Increase entry font size!



	TLegend *legend_eb_sf = new TLegend(0.352436, 0.208861, 0.62894, 0.333333);
	legend_eb_sf->SetTextFont(42);
	legend_eb_sf->SetBorderSize(0);  // no border
	legend_eb_sf->SetFillColor(0);   // Legend background should be white
	legend_eb_sf->SetTextSize(0.0547368 ); // Increase entry font size!

	TLegend *legend_pb = new TLegend(0.401146, 0.5, 0.67765, 0.7);
	legend_pb->SetTextFont(42);
	legend_pb->SetBorderSize(0);  // no border
	legend_pb->SetFillColor(0);   // Legend background should be white
	legend_pb->SetTextSize(0.0547368 ); // Increase entry font size!



	TLegend *legend_fit_2d = new TLegend(0.325648, 0.528027, 0.602305, 0.678251);
	legend_fit_2d->SetTextFont(42);
	legend_fit_2d->SetBorderSize(0);  // no border
	legend_fit_2d->SetFillColor(0);   // Legend background should be white
	legend_fit_2d->SetTextSize(0.04); // Increase entry font size!

	// Legend - tagger output plots (b, l)
	TLegend *legend_data = new TLegend(0.40, 0.17, 0.65, 0.4);
	legend_data->SetTextFont(42);
	legend_data->SetBorderSize(0);  // no border
	legend_data->SetFillColor(0);   // Legend background should be white
	legend_data->SetTextSize(0.04); // Increase entry font size!

	TLegend *legend_data_and_unc = new TLegend(0.2, 0.3, 0.4, 0.2);
	legend_data_and_unc->SetTextFont(42);
	legend_data_and_unc->SetBorderSize(0);  // no border
	legend_data_and_unc->SetFillColor(0);   // Legend background should be white
	legend_data_and_unc->SetTextSize(0.04); // Increase entry font size!
	TH1D *dummy_data = new TH1D();
	legend_data_and_unc->AddEntry(dummy_data, dataName.c_str(), "LPE");
	TH1D *dummy_er = new TH1D();
	dummy_er->SetFillColor(kGreen - 8);
	dummy_er->SetLineColor(kGreen - 8);
	legend_data_and_unc->AddEntry(dummy_er, "stat + syst unc", "F");

	// Loading input files:

	std::string inputFile_str = inputfile_folder + "/FitPlot_" + releaseName + "_" + dataName + "_" + taggerName + "_" + WPName + "_" + ChannelName + "_";
	std::string nominal_name = "";
	if (!combined_fit)
	{
		inputFile_str = inputFile_str + fitConfigName + "_";
		nominal_name = inputFile_str + "nominal.root";
	}
	else
	{
		nominal_name = inputFile_str + "Minos" + "_" + "nominal.root";
	}
	TFile *f_nominal = new TFile((nominal_name).c_str(), "READ");
	if (f_nominal->IsZombie())
	{
		std::cerr << "Input file: " << nominal_name << " not found ... exiting." << std::endl;
		return -1;
	}
	else
	{
		std::cerr << "Opend Input file: " << nominal_name << std::endl;
	}
	if (check_fit( f_nominal, nominal_name)) {
  		std::cerr << " ... exiting." << std::endl;
  		//exit(-1);
  	}
	TFile *f_combination_for_bin_mean = new TFile((nominal_fit_combination_for_bin_mean).c_str(), "READ");
	if (f_combination_for_bin_mean->IsZombie())
	{
		std::cerr << "Input file: " << nominal_fit_combination_for_bin_mean << " not found ... exiting." << std::endl;
		return -1;
	}
	RooFitResult *nominal_fitResult = (RooFitResult *)f_nominal->Get("fitResult");
	std::string out_name = inputfile_folder + "/FinalFitPlots_" + releaseName + "_" + dataName + "_" + taggerName + "_" + WPName + "_" + ChannelName;
	if (!combined_fit)
	{
		out_name = out_name + "_" + fitConfigName;
	}
	std::string outputFile_str = out_name + ".root";
	std::cout << "Outputfile: " << outputFile_str << std::endl;
	TFile *f_output = new TFile(outputFile_str.c_str(), "RECREATE");
	TFile *f_ttbar_nominal_pdf = 0;
	TFile *f_ttbar_nominal_AF2 = 0;
	TFile *f_Singletop_nominal_pdf = 0;
	TFile *f_Singletop_nominal_AF2 = 0;
	TFile *f_zjets_nominal_pdf = 0;
	TH1D *h_temp = ((TH1D *)f_nominal->Get(("sf_and_beff/e_b_" + wp_names.at(0) + "_Postfit").c_str()));
	int n_pt_bins = h_temp->GetNbinsX();
	int n_wp_bins = wp_names.size();
	//do we use the pt bins as eta bins?
	pt_binning_as_eta = (h_temp->GetBinLowEdge(1) < 0);
	// std::string pt_label_jet1="p_{T,1} [GeV]";
	// std::string pt_label_jet2="p_{T,2} [GeV]";
	// std::string pt_label_single=pt_label_single.c_str();
	// if (pt_binning_as_eta){
	//     pt_label_jet1="\eta_{1}";
	//     pt_label_jet2="\eta_{2}";
	//     pt_label_single="\eta";
	// }
	//defining directorys:
	TDirectory * d_stat, *d_unc_summary_plots, *d_results_cumulative, *d_results_continuous, *d_bin_mean_estimation;  
	d_stat = f_output->mkdir("stat");
	d_unc_summary_plots = f_output->mkdir("unc_summary_plots");
	d_results_cumulative = f_output->mkdir("results_cumulative");
	d_results_continuous = f_output->mkdir("results_continuous");
	d_bin_mean_estimation = f_output->mkdir("bin_mean_estimation");


	// -----------------------------------------------------------------------------------------------------------------
	//symmetrize stat error:
	d_stat->cd();

	const TMatrixDSym covMatrix_input = nominal_fitResult->covarianceMatrix();

	int n_rows_input = covMatrix_input.GetNrows();
	cout << " n_rows_input:" << n_rows_input << endl;
	//Get rid of additional nps N(p1,p2)..
	int n_rows = n_pt_bins * n_wp_bins;
	TMatrixD covMatrix = TMatrixD(n_rows, n_rows);
	//where are the p_bs in the correlation matrix? :)
	if (combined_fit)
	{
		//combined fit
		//take the upper left corner
		covMatrix = covMatrix_input.GetSub(0, n_rows - 1, 0, n_rows - 1);
	}
	else
	{
		//cut based
		//take the lower right corner
		covMatrix = covMatrix_input.GetSub(n_rows_input - n_rows, n_rows_input - 1, n_rows_input - n_rows, n_rows_input - 1);
	}

	cout << " n_rows:" << covMatrix.GetNrows() << endl;
	//create covmatirx projector - adapts to the number of WPs
	TMatrixD eb_projector = TMatrixD(n_rows, n_rows);
	for (int pt_blog = 0; pt_blog < n_rows / wp_names.size(); pt_blog++)
	{
		for (int i = 0; i < wp_names.size(); i++)
		{
			for (int j = 0; j < wp_names.size(); j++)
				if (i >= j)
					eb_projector[pt_blog * wp_names.size() + i][pt_blog * wp_names.size() + j] = 1;
		}
	}
	// std::cout << "eb_projector:" <<std::endl;
	// eb_projector.Print();
	TMatrixD eb_projector_trans = TMatrixD(eb_projector);
	eb_projector_trans.T();
	TMatrixD covMatrix_eb = eb_projector * covMatrix * eb_projector_trans;

	//now we have the correlation matrix we want to diagonlize
	TMatrixDEigen covMatrix_eb_Eigen = TMatrixDEigen(covMatrix_eb);
	TVectorD eigenValues_eb = covMatrix_eb_Eigen.GetEigenValuesRe();
	int n_EigenValues = eigenValues_eb.GetNoElements();
	std::cout << "n_EigenValues: " << n_EigenValues << std::endl;
	// eigenValues_eb.Print();
	const TMatrixD eigenVectors_eb = covMatrix_eb_Eigen.GetEigenVectors();

	// lets create two vectors where we put the total stat uncertainty to check if we can find back the stat unc estimated by roofit.
	TVectorD statErrors_fromPlots = TVectorD(n_EigenValues);
	TVectorD statErrors_total_forCheck = TVectorD(n_EigenValues);
	//lets fill the one for the uncertainy from the plots:
	for (int iwp = 0; iwp < wp_names.size(); iwp++)
	{
		for (int pt_bin = 0; pt_bin < n_pt_bins; pt_bin++)
		{
			//we need those two variables to sort them in the same way then the correlation matrix is sorted:
			//this shitty cor-hist starts with bin gt 10, so 10,11,12,1,2,3,... ROOT sorts parameter names according to string not integer
			int p_var = 0;
			if (n_pt_bins >= 9)
			{
				if (pt_bin >= 9)
				{
					p_var = wp_names.size() * (pt_bin - 9);
				}
				else
				{
					p_var = wp_names.size() * (pt_bin + n_pt_bins - 9);
				}
			}
			else
			{
				p_var = wp_names.size() * pt_bin;
			}
			int n_var = p_var + iwp;
			cout << "n_var " << n_var << " p_var " << p_var << " pt_bin " << pt_bin << endl;
			std::string wp_name = wp_names.at(iwp);
			TH1D *e_b_Postfit_nominal = (TH1D *)f_nominal->Get(("sf_and_beff/e_b_" + wp_name + "_Postfit").c_str());
			statErrors_fromPlots[n_var] = e_b_Postfit_nominal->GetBinError(pt_bin + 1);
		}
	}

	for (int i = 0; i < n_EigenValues; i++)
	{
		if (eigenValues_eb[i] == 0)
			continue;
		for (int iwp = 0; iwp < wp_names.size(); iwp++)
		{
			std::string wp_name = wp_names.at(iwp);
			TH1D *e_b_Postfit_nominal = (TH1D *)f_nominal->Get(("sf_and_beff/e_b_" + wp_name + "_Postfit").c_str());
			TH1D *e_b_stat_Error = (TH1D *)e_b_Postfit_nominal->Clone(("e_b_" + wp_name + "_stat_np_" + std::to_string(i + 1) + "_Error").c_str());
			e_b_stat_Error->SetStats(0);
			e_b_stat_Error->Reset();
			for (int pt_bin = 0; pt_bin < n_pt_bins; pt_bin++)
			{
				int p_var = 0;
				if (n_pt_bins >= 9)
				{
					if (pt_bin >= 9)
					{
						p_var = wp_names.size() * (pt_bin - 9);
					}
					else
					{
						p_var = wp_names.size() * (pt_bin + n_pt_bins - 9);
					}
				}
				else
				{
					p_var = wp_names.size() * pt_bin;
				}
				int n_var = p_var + iwp;
				double err_val = sqrt(eigenValues_eb[i]) * eigenVectors_eb[n_var][i];
				e_b_stat_Error->SetBinContent(pt_bin + 1, err_val);
				// since stat unc is fully decorrelated now we can sum it in quadrature
				statErrors_total_forCheck[n_var] += err_val * err_val;
			}
			d_stat->cd();
			TDirectory *dir = d_stat->GetDirectory((("stats_wp_" + wp_name).c_str()));
			if (dir == 0)
				dir = d_stat->mkdir((("stats_wp_" + wp_name).c_str()));
			dir->cd();
			e_b_stat_Error->Write();
			TH1D *e_b_stat_Error_rel = (TH1D *)e_b_stat_Error->Clone(("e_b_" + wp_name + "_stat_np_" + std::to_string(i + 1) + "_Error_rel").c_str());
			e_b_stat_Error_rel->Divide(e_b_Postfit_nominal);
			e_b_stat_Error_rel->Write();

			std::string canvasname = "c_e_b_" + wp_name + "_stat_np_" + std::to_string(i + 1) + "_Error_rel";
			TCanvas *c_obs = new TCanvas(canvasname.c_str());
			if (!pt_binning_as_eta)
				c_obs->SetLogx();
			legend_fit->Clear();

			double max_l_l = e_b_stat_Error_rel->GetBinContent(e_b_stat_Error_rel->GetMaximumBin());
			double max_l_h = e_b_stat_Error_rel->GetBinContent(e_b_stat_Error_rel->GetMinimumBin());
			double max_l = std::max(abs(max_l_l), abs(max_l_h));
			if (max_l < 0.05)
				max_l = 0.05;
			else
				max_l = max_l * 1.2;
			e_b_stat_Error_rel->GetYaxis()->SetRangeUser(-1 * max_l, max_l);
			e_b_stat_Error_rel->GetXaxis()->SetRangeUser(20., 400.);
			//e_b_stat_Error_rel->GetXaxis()->SetRangeUser(5., 250.);//RunVR
			e_b_stat_Error_rel->Draw("HIST");
			h_line0->DrawLine(20., 0., 400., 0);
			////h_line0->DrawLine(5., 0., 250., 0);//RunVR
			legend_fit->AddEntry(e_b_stat_Error_rel, ("stat_np_" + std::to_string(i + 1)).c_str(), "l");
			// tune and record canvas
			c_obs->cd();
			pt->Draw();
			pt2->Draw();
			pt3->Draw();
			pt4->Clear();
			pt4->AddText((releaseName + ", " + dataName + ", " + ChannelName + ", " + fitConfigName).c_str());
			pt4->AddText((taggerName + ", " + WPName + ", #epsilon_{b}^{MC} = " + wp_name + "%").c_str());
			pt4->Draw();
			legend_fit->Draw();
			c_obs->Write();
		}
	}
	//check if everything worked and errors are still the same:
	for (int i = 0; i < n_EigenValues; i++)
	{
		if (abs(statErrors_fromPlots[i] - sqrt(statErrors_total_forCheck[i])) < 1.0e-3) // the accuracy depends on the numerical precision of the fit
		{
			std::cout << "Bin: " << i << " totalStatError Check valid! :) value: " << sqrt(statErrors_total_forCheck[i]) << std::endl;
		}
		else
		{
			std::cout << "Bin: " << i << " ERROR :( statError from Plot: " << statErrors_fromPlots[i] << " statErrors_total_forCheck[i] " << sqrt(statErrors_total_forCheck[i]) << std::endl;
			return -1;
		}
	}
	std::cout << "Was able to decorrelate statistical uncertanty on efficiencies with Principal component analysis." << std::endl;
	// -----------------------------------------------------------------------------------------------------------------
	//lets do the same thing for the pb stat-errors: diagonalisation of the oiginal stat matrix
	// std::cout << "covMatrix:" <<std::endl;
	// covMatrix.Print();
	n_rows = n_pt_bins * n_wp_bins;
	//create covmatirx projector - calculate the b-tagging probability for the last PCBT bin
	TMatrixD pb_projector = TMatrixD(n_rows + n_pt_bins, n_rows);
	for (int pt_blog = 0; pt_blog < n_rows / wp_names.size(); pt_blog++)
	{
		for (int i = 0; i < wp_names.size()+1; i++)
		{
			for (int j = 0; j < wp_names.size(); j++)
			{
				if (i == wp_names.size())
					pb_projector[pt_blog * (wp_names.size()+1) + i][pt_blog * wp_names.size() + j] = -1;
				else if (i == j)
					pb_projector[pt_blog * (wp_names.size()+1) + i][pt_blog * wp_names.size() + j] = 1;
			}
		}
	}
	// std::cout << "pb_projector:" <<std::endl;
	// pb_projector.Print();
	TMatrixD pb_projector_trans = TMatrixD(pb_projector);
	pb_projector_trans.T();

	TMatrixD covMatrix_pb = pb_projector * covMatrix * pb_projector_trans;
	TMatrixDSym covMatrix_pb_sym = TMatrixDSym(covMatrix_pb.GetNrows());
	if (covMatrix_pb.IsSymmetric())
	{
		std::cout << "covMatrix_pb is symmetric. :)" << std::endl;
	}
	else
	{
		std::cout << "Symmetrising covMatrix_pb..." << std::endl;
		double max_rel_diff=0;
		for (int r = 0; r < covMatrix_pb.GetNrows(); r++)
		{
			for (int c = 0; c < covMatrix_pb.GetNrows(); c++)
			{
				if (covMatrix_pb[r][c] != covMatrix_pb[c][r]) {
					std::cout << "at: " << r << " " << c << " " << covMatrix_pb[r][c] << " =! " << covMatrix_pb[c][r] << " difference: " << abs(covMatrix_pb[r][c] - covMatrix_pb[c][r]) << std::endl;
					if(covMatrix_pb[r][c] && abs((covMatrix_pb[r][c] - covMatrix_pb[c][r])/covMatrix_pb[r][c]) > max_rel_diff) {
					  max_rel_diff = abs((covMatrix_pb[r][c] - covMatrix_pb[c][r])/covMatrix_pb[r][c]);
					}
				}
				double mean = (covMatrix_pb[r][c] + covMatrix_pb[c][r]) / 2.;
				covMatrix_pb[r][c] = mean;
				covMatrix_pb[c][r] = mean;
				covMatrix_pb_sym[r][c] = mean;
				covMatrix_pb_sym[c][r] = mean;
			}
		}
		if (covMatrix_pb.IsSymmetric() && max_rel_diff<1e-2)
			std::cout << "Was able to symmetrize" << std::endl;
		else{
		  std::cerr<<"ERROR: Asymmetric covMatrix_pb!!!"<<std::endl;
		  return -1;
		}
	}
	//now we have the correlation matrix we want to diagonlize
	TMatrixDSymEigen covMatrix_pb_Eigen = TMatrixDSymEigen(covMatrix_pb_sym);
	TVectorD eigenValues_pb = covMatrix_pb_Eigen.GetEigenValues();
	int n_EigenValues_pb = eigenValues_pb.GetNoElements();
	TMatrixD eigenVectors_pb = TMatrixD(covMatrix_pb_Eigen.GetEigenVectors());
	for (int i = 0; i < n_EigenValues_pb; i++)
	{
		if (abs(eigenValues_pb[i]) < 1e-15)
		{
			eigenValues_pb[i] = 0;
			std::cout << "setted eigenvalue to 0: " << i << " due to too small values. " << std::endl;
		}
	}
	// std::cout<< "EigenVectors_pb: " <<std::endl;
	// eigenVectors_pb.Print();

	// lets create two vectors where we put the total stat uncertainty to check if we can find back the stat unc estimated by roofit.
	TVectorD statErrors_fromPlots_pb = TVectorD(n_EigenValues_pb);
	TVectorD statErrors_total_forCheck_pb = TVectorD(n_EigenValues_pb);

	for (int pt_bin = 0; pt_bin < n_pt_bins; pt_bin++)
	{
		std::string pt_name = "pt_" + to_string(pt_bin + 1);
		TH1D *p_b_Postfit_nominal = (TH1D *)f_nominal->Get(("pdf/p_b_" + pt_name + "_Postfit").c_str());
		for (int iwp = 0; iwp <= wp_names.size(); iwp++)
		{
			int p_var = 0;
			if (n_pt_bins >= 9)
			{
				if (pt_bin >= 9)
				{
					p_var = (wp_names.size() + 1) * (pt_bin - 9);
				}
				else
				{
					p_var = (wp_names.size() + 1) * (pt_bin + n_pt_bins - 9);
				}
			}
			else
			{
				p_var = (wp_names.size() + 1) * pt_bin;
			}
			int n_var = p_var + iwp;
			statErrors_fromPlots_pb[n_var] = p_b_Postfit_nominal->GetBinError(iwp + 1);
		}
	}
	for (int i = 0; i < n_EigenValues_pb; i++)
	{
		if (eigenValues_pb[i] == 0)
			continue;
		for (int pt_bin = 0; pt_bin < n_pt_bins; pt_bin++)
		{
			std::string pt_name = "pt_" + to_string(pt_bin + 1);
			TH1D *p_b_Postfit_nominal = (TH1D *)f_nominal->Get(("pdf/p_b_" + pt_name + "_Postfit").c_str());
			TH1D *p_b_stat_Error = (TH1D *)p_b_Postfit_nominal->Clone(("p_b_" + pt_name + "_stat_np_" + std::to_string(i + 1) + "_Error").c_str());
			p_b_stat_Error->SetStats(0);
			p_b_stat_Error->Reset();
			for (int iwp = 0; iwp <= wp_names.size(); iwp++)
			{   //<=
				//if (pt_bin!=0){continue;}
				int p_var = 0;
				if (n_pt_bins >= 9)
				{
					if (pt_bin >= 9)
					{
						p_var = (wp_names.size() + 1) * (pt_bin - 9);
					}
					else
					{
						p_var = (wp_names.size() + 1) * (pt_bin + n_pt_bins - 9);
					}
				}
				else
				{
					p_var = (wp_names.size() + 1) * pt_bin;
				}
				int n_var = p_var + iwp;
				double err_val = sqrt(eigenValues_pb[i]) * eigenVectors_pb[n_var][i];
				p_b_stat_Error->SetBinContent(iwp + 1, err_val);
				// since stat unc is fully decorrelated now we can sum it in quadrature
				statErrors_total_forCheck_pb[n_var] += err_val * err_val;
			}
			d_stat->cd();
			TDirectory *dir = d_stat->GetDirectory((("stats_" + pt_name).c_str()));
			if (dir == 0)
				dir = d_stat->mkdir((("stats_" + pt_name).c_str()));
			dir->cd();
			p_b_stat_Error->Write();
			TH1D *p_b_stat_Error_rel = (TH1D *)p_b_stat_Error->Clone(("p_b_" + pt_name + "_stat_np_" + std::to_string(i + 1) + "_Error_rel").c_str());
			p_b_stat_Error_rel->Divide(p_b_Postfit_nominal);
			p_b_stat_Error_rel->Write();

			std::string canvasname = "c_p_b_" + pt_name + "_stat_np_" + std::to_string(i + 1) + "_Error_rel";
			TCanvas *c_obs = new TCanvas(canvasname.c_str());
			//c_obs->SetLogy();
			legend_fit->Clear();

			double max_l_l = p_b_stat_Error_rel->GetBinContent(p_b_stat_Error_rel->GetMaximumBin());
			double max_l_h = p_b_stat_Error_rel->GetBinContent(p_b_stat_Error_rel->GetMinimumBin());
			double max_l = std::max(abs(max_l_l), abs(max_l_h));
			if (max_l < 0.05)
				max_l = 0.05;
			else
				max_l = max_l * 1.2;
			p_b_stat_Error_rel->GetYaxis()->SetRangeUser(-1 * max_l, max_l);
			//sym_err->GetXaxis()->SetRangeUser(20.,400.);
			p_b_stat_Error_rel->Draw("HIST");
			h_line0->DrawLine(20., 0., 400., 0);
			////h_line0->DrawLine(5., 0., 250., 0);
			legend_fit->AddEntry(p_b_stat_Error_rel, ("stat_np_" + std::to_string(i + 1)).c_str(), "l");
			// tune and record canvas
			c_obs->cd();
			pt->Draw();
			pt2->Draw();
			pt3->Draw();
			pt4->Clear();
			pt4->AddText((releaseName + ", " + dataName + ", " + ChannelName + ", " + fitConfigName).c_str());
			pt4->AddText((taggerName + ", " + WPName + ", " + pt_name).c_str());
			pt4->Draw();
			legend_fit->Draw();
			c_obs->Write();
		}
	}
	//check if everything worked and errors are still the same:
	for (int i = 0; i < n_EigenValues_pb; i++)
	{
		if (abs(statErrors_fromPlots_pb[i] - sqrt(statErrors_total_forCheck_pb[i])) < 1.0e-3) // the accuracy depends on the numerical precision of the fit
		{
			std::cout << "Bin: " << i << " totalStatError Check valid! :) value: " << sqrt(statErrors_total_forCheck_pb[i]) << std::endl;
		}
		else
		{
			std::cout << "Bin: " << i << " ERROR :( statError from Plot: " << statErrors_fromPlots_pb[i] << " statErrors_total_forCheck[i] " << sqrt(statErrors_total_forCheck_pb[i]) << std::endl;
			return -1;
		}
	}
	std::cout << "Was able to decorrelate statistical uncertanty on p_bs for pseudo conitious tagging with Principal component analysis." << std::endl;
	// -----------------------------------------------------------------------------------------------------------------

	//initializing all systematics for each systematic
	Systematics_Container *ttbar_Sherpa2212 = 0;
	Systematics_Container *ttbar_MatrixElement = 0;
	Systematics_Container *ttbar_Hadronisation = 0;
	Systematics_Container *ttbar_Radiation = 0;
	Systematics_Container *ttbar_fsr = 0;
	
	std::vector<string> mc_samples_to_compare;
	mc_samples_to_compare.push_back("nominal");
	std::vector<Systematics_Container *> merged_ttbar_pdf_systematics;
	std::vector<Systematics_Container *> merged_Singletop_pdf_systematics;
	std::vector<Systematics_Container *> merged_systematics;
	std::vector<Systematics_Container *> unused_systematics;
	//lets see if there is a boostrap combination for the nominal file availale:
	if (exist((inputFile_str + "nominal_bootStrap_combination.root").c_str()))
	{
		cout << "found MC_stat_unc :" << (inputFile_str + "nominal_bootStrap_combination.root").c_str() << endl;
		TFile *f_bootstrap = 0;
		f_bootstrap = new TFile((inputFile_str + "nominal_bootStrap_combination.root").c_str(), "READ");
		f_output->cd();
		if (!f_bootstrap->IsZombie())
		{
			Systematics_Container *nominal_bootstrap = new Systematics_Container("MC_stat_nominal", "other", f_output, f_bootstrap);
			cout << "corresponding plots: " << endl;
			nominal_bootstrap->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName);
			merged_systematics.push_back(nominal_bootstrap);
		}
		else
		{
			cout << "tried to open :" << (inputFile_str + "nominal_bootStrap_combination.root").c_str() << endl;
		}
	}
	else
	{
		cout << "did not find MC_stat_unc!!! :" << (inputFile_str + "nominal_bootStrap_combination.root").c_str() << endl;
	}
	cout << "adding other uncertaintes: " << endl;
	for (int i = 0; i < systNames.size(); i++)
	{
		std::string systName = systNames.at(i);
		//    std::cout << " systname: "<< systName << std::endl;
		if ((systName.substr(systName.length() - 2) == "UP" || systName.substr(systName.length() - 2) == "Up" || systName.substr(systName.length() - 4) == "_1up" || systName.find("__1up_final") != std::string::npos ) && systName != "JET_JER_SINGLE_NP__1up" && systName != "ttbar_PhPy8_hdamp_weight_mc_rad_UP")
		{   //so we have a systematic with up and down here
			// std::cout << " UP found!" << std::endl;
			std::string up_name, down_name, systName_short;
			if (systName.substr(systName.length() - 2) == "UP")
			{
				up_name = "_UP";
				down_name = "_DOWN";
				systName_short = systName.substr(0, systName.length() - 3);
			}
			else if (systName.substr(systName.length() - 2) == "Up")
			{
				up_name = "Up";
				down_name = "Down";
				systName_short = systName.substr(0, systName.length() - 2);
			}
			else if (systName.find("__1up_final") != std::string::npos)
			{
				up_name = "__1up_final";
				down_name = "__1down_final";
				systName_short = systName.substr(0, systName.length() - 11);
			}
			else
			{
				up_name = "__1up";
				down_name = "__1down";
				systName_short = systName.substr(0, systName.length() - 5);
			}
			if (find(systNames.begin(), systNames.end(), systName_short + up_name) == systNames.end())
			{
				std::cout << systName_short + up_name << " not found!" << std::endl;
				return 1;
			}

			if (find(systNames.begin(), systNames.end(), systName_short + down_name) == systNames.end())
			{
				std::cout << systName_short + down_name << " not found!" << std::endl;
				return 1;
			}
			f_output->cd();
			string folder = "other";
			if ((systName.find("FT_EFF_Eigen_C")!=std::string::npos))
			{
				folder ="FT_EFF_Eigen_C";
			}
			else if ((systName.find("FT_EFF_Eigen_Light")!=std::string::npos))
			{
				folder ="FT_EFF_Eigen_Light";
			}
			else if ( (((systName.find("leptonSF")!=std::string::npos) || (systName.find("MUON")!=std::string::npos)) || ((systName.find("EG_")!=std::string::npos) || (systName.find("trigger_EL_")!=std::string::npos))) || (systName.find("trigger_MU_")!=std::string::npos) )
			{
				folder ="Leptons";
			}
			else if ((systName.find("weight_trigger")!=std::string::npos))
			{
				folder ="Trigger";
			}
			else if ((systName.find("MET_")!=std::string::npos))
			{
				folder ="MET";
			}
			else if ((systName.find("JET_CategoryReduction_JET_JER") !=std::string::npos) || (systName.find("JET_JER") !=std::string::npos) )
			{
				folder ="JET_JER";
			}
			else if ( (((systName.find("JET_CategoryReduction_JET")!=std::string::npos) || (systName.find("JET_EffectiveNP")!=std::string::npos)) || ((systName.find("JET_Flavour")!=std::string::npos) || (systName.find("BJES")!=std::string::npos)) ) || ( ((systName.find("JET_Eta")!=std::string::npos) || (systName.find("JET_Pileup")!=std::string::npos)) || ((systName.find("JET_Flavor")!=std::string::npos) || (systName.find("JET_JES")!=std::string::npos))) || ((systName.find("JET_PunchThrough")!=std::string::npos) || systName.find("JET_InSitu")!=std::string::npos) || systName.find("JET_GroupedNP")!=std::string::npos || systName.find("JET_RelativeNonClosure")!=std::string::npos)
			{
				folder ="JET_JES";
			}
			else if ((systName.find("Singletop")!=std::string::npos) || (systName.find("singletop")!=std::string::npos))
			{
				folder ="Singletop";
			}
			else if ((systName.find("ttbar")!=std::string::npos))
			{
				folder ="ttbar";
			}
			else if ((systName.find("Diboson")!=std::string::npos))
			{
				folder ="Diboson";
			}
			else if ((systName.find("Zjets")!=std::string::npos))
			{
				folder ="Zjets";
			}
			Systematics_Container *systematic = new Systematics_Container(systName_short, folder, f_output, f_nominal, inputFile_str + systName_short + up_name + ".root", inputFile_str + systName_short + down_name + ".root");
			systematic->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName);
			merged_systematics.push_back(systematic);
		}
		else if (systName == "ttbar_PhPy8_hdamp_weight_mc_rad_UP")
		{ //we have a specail af2 radiation sample
		  //if (f_ttbar_nominal_AF2 == 0)
		  //		f_ttbar_nominal_AF2 = new TFile((inputFile_str + "ttbar_PhPy8_AF2.root").c_str(), "READ");
		  //Systematics_Container *systematic = new Systematics_Container("ttbar_mc_rad","ttbar", f_output, f_ttbar_nominal_AF2, inputFile_str + "ttbar_PhPy8_hdamp_weight_mc_rad_UP" + ".root", inputFile_str + "ttbar_PhPy8_weight_mc_rad_DOWN" + ".root", f_nominal);
		  Systematics_Container *systematic = new Systematics_Container("ttbar_mc_rad","ttbar", f_output, f_nominal, inputFile_str + "ttbar_PhPy8_nominal_weight_mc_rad_UP" + ".root", inputFile_str + "ttbar_PhPy8_weight_mc_rad_DOWN" + ".root", f_nominal);
		  systematic->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName);
		  merged_systematics.push_back(systematic);
		  ttbar_Radiation = systematic;
		} //systematic with up and down preocessed
		else if ( (systName.substr(systName.length() - 4) == "DOWN" || systName.substr(systName.length() - 6) == "_1down") || systName.find("_1down_final") != std::string::npos )
		{
			//skip systematic with down Processed if up found!
			//std::cout << " DOWN found! - skipping." << std::endl;
		}
		else
		{   //single sided systematic:
			//std::cout << " systname: "<< systName << std::endl;
			if (systName == "JET_JER_SINGLE_NP__1up")
				systName = "JET_JER_SINGLE_NP";
			bool symmetrize = true;
			bool closure_test_systematic = false;
			if (systName == "clTestMoMc_nstat")
			{
				symmetrize = false;
				closure_test_systematic = true;
			}
			string ttbar_w_mc_str_shower_np = "ttbar_weight_mc_shower_np_";
			string Singletop_w_mc_str_shower_np = "singletop_weight_mc_shower_np_";
			string ttbar_w_qcd_str_np = "ttbar_weight_mc_qcd_np_";
			string Singletop_w_qcd_str_np = "singletop_weight_mc_qcd_np_";
			if (ttbar_w_mc_str_shower_np == systName.substr(0, ttbar_w_mc_str_shower_np.size()))
			{
				//ttbar pdf systemntics https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TopMCPDFReccommendations
				if (systName != "ttbar_weight_mc_shower_np_124")
				{
					if (f_ttbar_nominal_pdf == 0)
					  f_ttbar_nominal_pdf = f_nominal; //Based on the checks of on checkMetaSG.py for ttbar new TFile((inputFile_str + "ttbar_weight_mc_shower_np_124.root").c_str(), "READ");
					Systematics_Container *systematic = new Systematics_Container(systName,"ttbar_pdf", f_output, f_ttbar_nominal_pdf, inputFile_str + systNames.at(i) + ".root", symmetrize, closure_test_systematic);
					systematic->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName);
					merged_systematics.push_back(systematic);
					merged_ttbar_pdf_systematics.push_back(systematic);
				}
				else
				{
					//lets draw some plots just to see how big the difference is between pdf sets.
					Systematics_Container *systematic = new Systematics_Container(systName,"ttbar_pdf", f_output, f_nominal, inputFile_str + systNames.at(i) + ".root", symmetrize, closure_test_systematic);
					systematic->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName, false);
					unused_systematics.push_back(systematic);
				}
			}
			else if (ttbar_w_qcd_str_np == systName.substr(0, ttbar_w_qcd_str_np.size()))
			{
			  //ttbar pdf systemntics https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TopMCPDFReccommendations
			  Systematics_Container *systematic = new Systematics_Container(systName,"ttbar_pdf", f_output, f_nominal, inputFile_str + systName + ".root", symmetrize, closure_test_systematic);
			  systematic->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName);
			  merged_systematics.push_back(systematic);
			  merged_ttbar_pdf_systematics.push_back(systematic);
			}
			else if (Singletop_w_mc_str_shower_np == systName.substr(0, Singletop_w_mc_str_shower_np.size()))
			{
				//ttbar pdf systemntics https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TopMCPDFReccommendations
				if (systName != "singletop_weight_mc_shower_np_122")
				{
					if (f_Singletop_nominal_pdf == 0)
						f_Singletop_nominal_pdf = new TFile((inputFile_str + "singletop_weight_mc_shower_np_122.root").c_str(), "READ");
					Systematics_Container *systematic = new Systematics_Container(systName,"Singletop_pdf", f_output, f_Singletop_nominal_pdf, inputFile_str + systName + ".root", symmetrize, closure_test_systematic);
					systematic->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName);
					merged_systematics.push_back(systematic);
					merged_Singletop_pdf_systematics.push_back(systematic);
				}
				else
				{
					//lets draw some plots just to see how big the difference is between pdf sets.
					Systematics_Container *systematic = new Systematics_Container(systName,"Singletop_pdf", f_output, f_nominal, inputFile_str + systNames.at(i) + ".root", symmetrize, closure_test_systematic);
					systematic->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName, false);
					unused_systematics.push_back(systematic);
				}
			}
			else if (Singletop_w_qcd_str_np == systName.substr(0, Singletop_w_qcd_str_np.size()))
			{
			  //ttbar pdf systemntics https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TopMCPDFReccommendations
			  Systematics_Container *systematic = new Systematics_Container(systName,"Singletop_pdf", f_output, f_nominal, inputFile_str + systName + ".root", symmetrize, closure_test_systematic);
			  systematic->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName);
			  merged_systematics.push_back(systematic);
			  merged_Singletop_pdf_systematics.push_back(systematic);
			}
			else if (systName.find("Singletop")!=std::string::npos)
			{
				Systematics_Container *systematic;
				if ((systName.find("Singletop_PowHW7")!=std::string::npos))
				{
				  //if (f_Singletop_nominal_AF2 == 0)
				  //f_Singletop_nominal_AF2 = new TFile((inputFile_str + "Singletop_PowPy8_AF2.root").c_str(), "READ");
				  //systematic = new Systematics_Container(systName,"Singletop", f_output, f_Singletop_nominal_AF2, inputFile_str + systName + ".root", symmetrize, closure_test_systematic);
				  systematic = new Systematics_Container(systName,"Singletop", f_output, f_nominal, inputFile_str + systName + ".root", symmetrize, closure_test_systematic);
				  systematic->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName);
				  merged_systematics.push_back(systematic);
				}
				else if ((systName.find("Singletop_aMcPy8")!=std::string::npos)	)
				{
				  //if (f_Singletop_nominal_AF2 == 0)
				  //f_Singletop_nominal_AF2 = new TFile((inputFile_str + "Singletop_PowPy8_AF2.root").c_str(), "READ");
				  //systematic = new Systematics_Container(systName,"Singletop", f_output, f_Singletop_nominal_AF2, inputFile_str + systName + ".root", symmetrize, closure_test_systematic);
				  systematic = new Systematics_Container(systName,"Singletop", f_output, f_nominal, inputFile_str + systName + ".root", symmetrize, closure_test_systematic);
				  systematic->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName, false);
				  unused_systematics.push_back(systematic);
				}		
				else
				{
				  systematic = new Systematics_Container(systName,"Singletop", f_output, f_nominal, inputFile_str + systName + ".root", symmetrize, closure_test_systematic);
				  systematic->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName);
				  merged_systematics.push_back(systematic);
				}

			}
			else if (systName.find("Diboson")!=std::string::npos)
			{	
				bool used_for_unc =true;
				if (systName.find("Diboson_PowPy8")!=std::string::npos)
				{
					used_for_unc = false;
				}
				for (int np=12;np<112;np ++)
				{
					if (systName.find("weight_mc_shower_np_"+to_string(np))!=std::string::npos)
					{
						used_for_unc = false;
					}
				}
				
				Systematics_Container *systematic = new Systematics_Container(systName,"Diboson", f_output, f_nominal, inputFile_str + systName + ".root", symmetrize, closure_test_systematic);
				systematic->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName, used_for_unc);
				if (used_for_unc)
				{
					merged_systematics.push_back(systematic);
				}
				else{
					unused_systematics.push_back(systematic);
				}
			}
			else if (systName.find("Zjets")!=std::string::npos)
			  {	
			    if (f_zjets_nominal_pdf == 0)
			      f_zjets_nominal_pdf = new TFile((inputFile_str + "Zjets_weight_mc_shower_np_11.root").c_str(), "READ"); // Coming from the checkMetaSG.py of a Sherpa 2.2.11 sample.
			    
			    bool used_for_unc =true;
			    if (systName.find("Zjets_PowPy8")!=std::string::npos)
			      {
				used_for_unc = false;
			      }
			    if (systName.find("Zjets_MGPy8")!=std::string::npos)
			      {
					used_for_unc = false;
			      }
			    for (int np=18;np<322;np ++)
			      {
				if (systName.find("weight_mc_shower_np_"+to_string(np))!=std::string::npos)
				  {
				    used_for_unc = false;
				  }
			      }
			    Systematics_Container *systematic = NULL;
			    if(systName.find("weight_mc_shower_np_")!=std::string::npos || systName.find("weight_mc_qcd_np_")!=std::string::npos ) 
			      systematic = new Systematics_Container(systName,"Zjets", f_output, f_zjets_nominal_pdf, inputFile_str + systName + ".root", symmetrize, closure_test_systematic);
			    else
			      systematic = new Systematics_Container(systName,"Zjets", f_output, f_nominal, inputFile_str + systName + ".root", symmetrize, closure_test_systematic);
			    
			    systematic->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName, used_for_unc);
			    if (used_for_unc)
			      {
				merged_systematics.push_back(systematic);
			      }
			    else{
			      unused_systematics.push_back(systematic);
			    }
			}
			else if (systName == "ttbar_Sherpa2212")
			{
				Systematics_Container *systematic = new Systematics_Container(systName,"ttbar", f_output, f_nominal, inputFile_str + "ttbar_Sherpa2212.root", symmetrize, closure_test_systematic);
				if (!combined_fit)
				{
					// add sherpa to the systematic unc in the m_lj_cut case
					systematic->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName, true);
					merged_systematics.push_back(systematic);
					mc_samples_to_compare.push_back(systName);
				}
				else
				{
					systematic->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName);
					//systematic->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName, false);
					//unused_systematics.push_back(systematic);
					merged_systematics.push_back(systematic);
					mc_samples_to_compare.push_back(systName);
				}
				ttbar_Sherpa2212 = systematic;
			}
			else if (systName == "ttbar_HW7" || systName == "ttbar_PhPy8_pthard" || systName == "ttbar_PhPy8_hdamp")
			{
			  //if (f_ttbar_nominal_AF2 == 0)
			  //f_ttbar_nominal_AF2 = new TFile((inputFile_str + "ttbar_PhPy8_AF2.root").c_str(), "READ");
			  //Systematics_Container *systematic = new Systematics_Container(systName,"ttbar", f_output, f_ttbar_nominal_AF2, inputFile_str + systNames.at(i) + ".root", symmetrize, closure_test_systematic);
			  Systematics_Container *systematic = new Systematics_Container(systName,"ttbar", f_output, f_nominal, inputFile_str + systNames.at(i) + ".root", symmetrize, closure_test_systematic);
				systematic->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName);
				merged_systematics.push_back(systematic);
				ttbar_Hadronisation = systematic;
				mc_samples_to_compare.push_back(systName);
			}
			else if (systName == "ttbar_aMcPy8")
			{
			  //if (f_ttbar_nominal_AF2 == 0)
			  //f_ttbar_nominal_AF2 = new TFile((inputFile_str + "ttbar_PhPy8_AF2.root").c_str(), "READ");
			  //	Systematics_Container *systematic = new Systematics_Container(systName,"ttbar", f_output, f_ttbar_nominal_AF2, inputFile_str + systNames.at(i) + ".root", symmetrize, closure_test_systematic);

				Systematics_Container *systematic = new Systematics_Container(systName,"ttbar", f_output, f_nominal, inputFile_str + systNames.at(i) + ".root", symmetrize, closure_test_systematic);
				
				if (!combined_fit)
				{
					// add amc to the ttbar unc in the m_lj_cut case
					systematic->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName,true);
					merged_systematics.push_back(systematic);
				}
				else{
					systematic->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName,false);
					unused_systematics.push_back(systematic);
				}
				ttbar_MatrixElement = systematic;
				mc_samples_to_compare.push_back(systName);
			}
			else if (systName == "ttbar_PowPy8" || systName == "ttbar_PhPy8_AF2")
			{
				//plots to see how much changed with switching the ttbar samples
				Systematics_Container *systematic = new Systematics_Container(systName,"ttbar", f_output, f_nominal, inputFile_str + systNames.at(i) + ".root", symmetrize, closure_test_systematic);
				systematic->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName, false);
				unused_systematics.push_back(systematic);
			}
			//else if (systName == "misstagLight_up")
			//{
			//	symmetrize = true;
			//	Systematics_Container *systematic = new Systematics_Container(systName,"other", f_output, f_nominal, inputFile_str + systNames.at(i) + ".root", symmetrize, closure_test_systematic);
			//	systematic->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName);
				//systematic->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName, false);
				//unused_systematics.push_back(systematic);
			//}
			else
			{
				Systematics_Container *systematic = new Systematics_Container(systName,"other", f_output, f_nominal, inputFile_str + systNames.at(i) + ".root", symmetrize, closure_test_systematic);
				systematic->SavePlots(releaseName, taggerName, WPName, ChannelName, dataName, fitConfigName);
				merged_systematics.push_back(systematic);
			}
		} //processed singlesided systematic
	}
	//calculate pdf_systematic efficiency errors:
	//ttbar:
	f_output->cd();
	std::vector<TH1D *> ttbar_pdf_combined_e_b_syst_error_rel_squared;
	std::vector<TH1D *> ttbar_pdf_combined_e_b_syst_error_rel_high_squared;
	std::vector<TH1D *> ttbar_pdf_combined_e_b_syst_error_rel_low_squared;
	//initializing of combine error histogramm
	bool found_ttbar_pdf_uncertanty = false;
	for (int iwp = 0; iwp < wp_names.size(); iwp++)
	{
		std::string wp_name = wp_names.at(iwp);
		TH1D *e_b_Postfit_nominal = (TH1D *)f_nominal->Get(("sf_and_beff/e_b_" + wp_name + "_Postfit").c_str());
		TH1D *combined_e_b_syst_error_rel_squared_wp = (TH1D *)e_b_Postfit_nominal->Clone(("e_b_" + wp_name + "_ttbar_pdf_combined_syst_Error_rel_squared").c_str());
		combined_e_b_syst_error_rel_squared_wp->SetStats(0);
		combined_e_b_syst_error_rel_squared_wp->Reset();
		TH1D *combined_e_b_syst_error_rel_high_squared_wp = (TH1D *)combined_e_b_syst_error_rel_squared_wp->Clone(("e_b_" + wp_name + "_ttbar_pdf_combined_syst_Error_rel_high_squared").c_str());
		TH1D *combined_e_b_syst_error_rel_low_squared_wp = (TH1D *)combined_e_b_syst_error_rel_squared_wp->Clone(("e_b_" + wp_name + "_ttbar_pdf_combined_syst_Error_rel_low_squared").c_str());
		for (const auto &systematic : merged_ttbar_pdf_systematics)
		{
			found_ttbar_pdf_uncertanty = true;
			TH1D *help = (TH1D *)systematic->GetLowError(wp_name)->Clone();
			help->Multiply(help);
			combined_e_b_syst_error_rel_low_squared_wp->Add(help);
			help = (TH1D *)systematic->GetHighError(wp_name)->Clone();
			help->Multiply(help);
			combined_e_b_syst_error_rel_high_squared_wp->Add(help);
			help = (TH1D *)systematic->GetSymError(wp_name)->Clone();
			help->Multiply(help);
			combined_e_b_syst_error_rel_squared_wp->Add(help);
		}
		ttbar_pdf_combined_e_b_syst_error_rel_low_squared.push_back(combined_e_b_syst_error_rel_low_squared_wp);
		ttbar_pdf_combined_e_b_syst_error_rel_high_squared.push_back(combined_e_b_syst_error_rel_high_squared_wp);
		ttbar_pdf_combined_e_b_syst_error_rel_squared.push_back(combined_e_b_syst_error_rel_squared_wp);
	}
	//single_top:
	f_output->cd();
	std::vector<TH1D *> Singletop_pdf_combined_e_b_syst_error_rel_squared;
	std::vector<TH1D *> Singletop_pdf_combined_e_b_syst_error_rel_high_squared;
	std::vector<TH1D *> Singletop_pdf_combined_e_b_syst_error_rel_low_squared;
	//initializing of combine error histogramm
	bool found_Singletop_pdf_uncertanty = false;
	for (int iwp = 0; iwp < wp_names.size(); iwp++)
	{
		std::string wp_name = wp_names.at(iwp);
		TH1D *e_b_Postfit_nominal = (TH1D *)f_nominal->Get(("sf_and_beff/e_b_" + wp_name + "_Postfit").c_str());
		TH1D *combined_e_b_syst_error_rel_squared_wp = (TH1D *)e_b_Postfit_nominal->Clone(("e_b_" + wp_name + "_Singletop_pdf_combined_syst_Error_rel_squared").c_str());
		combined_e_b_syst_error_rel_squared_wp->SetStats(0);
		combined_e_b_syst_error_rel_squared_wp->Reset();
		TH1D *combined_e_b_syst_error_rel_high_squared_wp = (TH1D *)combined_e_b_syst_error_rel_squared_wp->Clone(("e_b_" + wp_name + "_Singletop_pdf_combined_syst_Error_rel_high_squared").c_str());
		TH1D *combined_e_b_syst_error_rel_low_squared_wp = (TH1D *)combined_e_b_syst_error_rel_squared_wp->Clone(("e_b_" + wp_name + "_Singletop_pdf_combined_syst_Error_rel_low_squared").c_str());
		for (const auto &systematic : merged_Singletop_pdf_systematics)
		{
			found_Singletop_pdf_uncertanty = true;
			TH1D *help = (TH1D *)systematic->GetLowError(wp_name)->Clone();
			help->Multiply(help);
			combined_e_b_syst_error_rel_low_squared_wp->Add(help);
			help = (TH1D *)systematic->GetHighError(wp_name)->Clone();
			help->Multiply(help);
			combined_e_b_syst_error_rel_high_squared_wp->Add(help);
			help = (TH1D *)systematic->GetSymError(wp_name)->Clone();
			help->Multiply(help);
			combined_e_b_syst_error_rel_squared_wp->Add(help);
		}
		Singletop_pdf_combined_e_b_syst_error_rel_low_squared.push_back(combined_e_b_syst_error_rel_low_squared_wp);
		Singletop_pdf_combined_e_b_syst_error_rel_high_squared.push_back(combined_e_b_syst_error_rel_high_squared_wp);
		Singletop_pdf_combined_e_b_syst_error_rel_squared.push_back(combined_e_b_syst_error_rel_squared_wp);
	}

	//get bin means
	std::vector <double> pt_bin_means;
	std::string combined_tagger_name="";
	TH2D *sf_bb, *sf_bl, *sf_lb;
	if (combined_fit)
	{
		//combined fit - SR only!
		combined_tagger_name=ChannelName+"_m_lj_cut"+"_hist_for_fit_"+taggerName+"_"+WPName;

		//we have to correct for yield sf:
		sf_bb  = (TH2D *)f_nominal->Get("yields/SF/sf_bb");
		sf_bl  = (TH2D *)f_nominal->Get("yields/SF/sf_bl");
		sf_lb  = (TH2D *)f_nominal->Get("yields/SF/sf_lb");
	}
	else{
		combined_tagger_name=ChannelName+"_hist_for_fit_"+taggerName+"_"+WPName;
	}
	std::string hist_name_pre_pt=combined_tagger_name + "_bin_mean"+"/"+"h_"+combined_tagger_name+ "_bin_mean";
	d_bin_mean_estimation->cd();
	for (int i_pt =0; i_pt<n_pt_bins; i_pt++)
	{
		cout<<" computation of bin mean for for jet pt : " << i_pt<<endl;
		cout<<" trying to get: " << hist_name_pre_pt+"_pt1_" + to_string(i_pt+1) + "_pt2_" + to_string(i_pt+1) + "_bb"<< " from file "<<nominal_fit_combination_for_bin_mean <<endl;

		THnSparseD* h_pt_cross = (THnSparseD*) f_combination_for_bin_mean->Get((hist_name_pre_pt+"_pt1_" + to_string(i_pt+1) + "_pt2_" + to_string(i_pt+1) + "_bb") .c_str())->Clone();
		TH2D* h_pt_sum = h_pt_cross->Projection(0,2);
		h_pt_sum->SetName(("h_pt_"+to_string(i_pt+1) +"_sum").c_str());
		//h_pt_sum->Add(h_pt_cross->Projection(3));
		h_pt_sum->Reset();
		double yield_sf=1.;
		//leading jet pt first: 
		for (int j2_pt =0; j2_pt<=i_pt; j2_pt++){
			//bb events
			if (combined_fit)
				yield_sf = sf_bb->GetBinContent(i_pt+1,j2_pt+1);
			h_pt_sum->Add( ((THnSparseD*) f_combination_for_bin_mean->Get((hist_name_pre_pt+"_pt1_" + to_string(i_pt+1) + "_pt2_" + to_string(j2_pt+1) + "_bb") .c_str()))->Projection(0,2), yield_sf); 
			//bl events
			if (combined_fit)
				yield_sf = sf_bl->GetBinContent(i_pt+1,j2_pt+1);
			h_pt_sum->Add( ((THnSparseD*) f_combination_for_bin_mean->Get((hist_name_pre_pt+"_pt1_" + to_string(i_pt+1) + "_pt2_" + to_string(j2_pt+1) + "_bl") .c_str()))->Projection(0, 2), yield_sf ); 
		}
		//subleading jet 
		for (int j1_pt =i_pt; j1_pt<n_pt_bins; j1_pt++){
			//bb events
			if (combined_fit)
				yield_sf = sf_bb->GetBinContent(j1_pt+1, i_pt+1);
			h_pt_sum->Add( ((THnSparseD*) f_combination_for_bin_mean->Get((hist_name_pre_pt+"_pt1_" + to_string(j1_pt+1) + "_pt2_" + to_string(i_pt+1) + "_bb") .c_str()))->Projection(1, 3), yield_sf ); 
			//lb events
			if (combined_fit)
				yield_sf = sf_lb->GetBinContent(j1_pt+1, i_pt+1);
			h_pt_sum->Add( ((THnSparseD*) f_combination_for_bin_mean->Get((hist_name_pre_pt+"_pt1_" + to_string(j1_pt+1) + "_pt2_" + to_string(i_pt+1) + "_lb") .c_str()))->Projection(1, 3), yield_sf ); 
		}
		cout<<" done! " << i_pt<< "bin mean "<< h_pt_sum->GetMean(1) <<endl;
		cout<<" apply pseudo continous b-sf! " <<endl;
		std::string pt_name = "pt_" + to_string(i_pt+1);
		TH1D *p_b_sf_Postfit_nominal = (TH1D *) ((TH1D *)f_nominal->Get(("pdf/p_b_" + pt_name + "_Postfit").c_str()))->Clone(("p_b_sf_Postfit_nominal" + pt_name + "_dummy").c_str());
		p_b_sf_Postfit_nominal->Divide( (TH1D *)f_nominal->Get(("pdf/p_b_ttb_" + pt_name + "_MC").c_str()));
		int n_tagger_bins = p_b_sf_Postfit_nominal->GetNbinsX();
		for(int tagger_bin=1; tagger_bin <= n_tagger_bins; tagger_bin++){
			double b_sf= p_b_sf_Postfit_nominal->GetBinContent(tagger_bin);
			for (int i_small_bin=1; i_small_bin <= h_pt_sum->GetNbinsX(); i_small_bin++){
				h_pt_sum->SetBinContent(i_small_bin, tagger_bin, h_pt_sum->GetBinContent(i_small_bin, tagger_bin) *b_sf);
			}
		}
		cout<<" done! " << i_pt<< "bin mean now:"<< h_pt_sum->GetMean(1) << " ran over "<< h_pt_sum->GetNbinsX() << " bins."<<endl;
		d_bin_mean_estimation->cd();
		h_pt_sum->Write();
		pt_bin_means.push_back(h_pt_sum->GetMean());

	}

	

	//
	cout<<"jet uncertainty plot"<<endl;
	if (true)
	{	
		//JET_JES
		for (int iwp = 0; iwp < wp_names.size(); iwp++)
		{
			std::string wp_name = wp_names.at(iwp);
			TDirectory *dir = d_unc_summary_plots->GetDirectory((("e_b_wp_" + wp_name).c_str()));
			if (dir == 0)
				dir = d_unc_summary_plots->mkdir((("e_b_wp_" + wp_name).c_str()));
			dir->cd();
			TH1D * help=0;
			TH1D * sym_err=0;
			TH1D * jet_jer=0;
			TH1D * sum_of_showed=0;
			std::vector<std::string> syst_names_to_plot_on_top = { //
				"JET_Pileup_RhoTopology",
				"JET_Flavor_Response",
				"JET_Flavor_Composition",
				"JET_EtaIntercalibration_Modelling"
			};
			map<string, TH1D *> syst_to_plot; 
			for (const auto &systematic : merged_systematics)
			{
				if ((systematic->GetName().find("JET_CategoryReduction")!=std::string::npos) &&(systematic->GetName().find("JET_CategoryReduction_JET_JER")==std::string::npos) ) {
					//cout<<"jer plot: "<< systematic->GetName()<<endl;
					bool show=false;
					help = (TH1D *)systematic->GetSymError(wp_name)->Clone();
					for (const auto &syst_name : syst_names_to_plot_on_top)
					{
						if (systematic->GetName().find(syst_name)!=std::string::npos){
							syst_to_plot[syst_name]=(TH1D *) help->Clone();
							show=true;
						} 
					}

					help->Multiply(help);
					if (sym_err) sym_err->Add(help);
					else sym_err=(TH1D *) help->Clone("h_sym_jet_jes_catr");
					if (show){
						if (sum_of_showed) sum_of_showed->Add(help);
						else sum_of_showed=(TH1D *) help->Clone();
					}
				}
			}
			if (sym_err){
				n_pt_bins = sym_err->GetNbinsX();
				for (int pt_bin = 1; pt_bin <= n_pt_bins; pt_bin++)
				{
					sym_err->SetBinError(pt_bin, 0.);
					sym_err->SetBinContent(pt_bin, sqrt(sym_err->GetBinContent(pt_bin)));
					sum_of_showed->SetBinError(pt_bin, 0.);
					sum_of_showed->SetBinContent(pt_bin, sqrt(sum_of_showed->GetBinContent(pt_bin)));
				}
				std::string canvasname = "c_e_b_" + wp_name + "_" + "JET_JES_CategoryReduction" + "_syst_Error";
				TCanvas *c_obs = new TCanvas(canvasname.c_str());
				if (!pt_binning_as_eta)
					c_obs->SetLogx();
				legend_data->Clear();
				legend_data->AddEntry(sym_err, "total relative  JES uncertainty", "F");
				double max_l = sym_err->GetMaximum();
				if (max_l < 0.05)
					max_l = 0.05;
				else
					max_l = max_l * 1.2;
				sym_err->GetYaxis()->SetRangeUser(-1 * max_l, max_l);
				sym_err->GetXaxis()->SetRangeUser(20., 400.);
				//sym_err->GetXaxis()->SetRangeUser(5., 250.);//runVR
				sym_err->SetFillColor(kGreen - 8);
				sym_err->SetLineColor(kGreen - 8);
				TH1D *sym_err_neg = (TH1D *)sym_err->Clone();
				sym_err_neg->Scale(-1);
				sym_err->Draw("HIST");
				sym_err_neg->Draw("HISTSAME");				
				h_line0->DrawLine(20., 0., 400., 0);//runVR
				////h_line0->DrawLine(5., 0., 250., 0);
				int i_col=0;
				for (const auto &syst_name : syst_names_to_plot_on_top)
				{
					if(syst_to_plot.find(syst_name)!=syst_to_plot.end())
					{	
						TH1D * hPlot=syst_to_plot[syst_name];		
						hPlot->SetLineColor(nice_colours.at(i_col));
						hPlot->SetMarkerColor(nice_colours.at(i_col));
						legend_data->AddEntry(hPlot, syst_name.c_str(), "LPE");
						hPlot->Draw("LPESAME");
						i_col++;
					}
				}
				// dont show the sum for now, just to see if it matches the band...
				// if(sum_of_showed){
				// 	sum_of_showed->SetLineColor(nice_colours.at(i_col));
				// 	sum_of_showed->SetMarkerColor(nice_colours.at(i_col));
				// 	legend_data->AddEntry(sum_of_showed, "sum_of_showed", "LPE");
				// 	sum_of_showed->Draw("LPESAME");
				// 	i_col++;
				// }
				sym_err->Draw("AXISSAME");
				

				// tune and record canvas
				c_obs->cd();
				pt->Draw();
				pt2->Draw();
				pt3->Draw();
				pt4->Clear();
				pt4->AddText((releaseName + ", " + ChannelName + ", " + fitConfigName).c_str());
				pt4->AddText((taggerName + ", " + WPName + ", #epsilon_{b}^{MC} = " + wp_name + "%").c_str());
				pt4->Draw();
				legend_data->Draw();
				c_obs->Write();
			}

		}
		for (int iwp = 0; iwp < wp_names.size(); iwp++)
		{
			std::string wp_name = wp_names.at(iwp);
			TDirectory *dir = d_unc_summary_plots->GetDirectory((("e_b_wp_" + wp_name).c_str()));
			if (dir == 0)
				dir = d_unc_summary_plots->mkdir((("e_b_wp_" + wp_name).c_str()));
			dir->cd();
			TH1D * help=0;
			TH1D * sym_err=0;
			TH1D * jet_jer=0;
			TH1D * sum_of_showed=0;
			std::vector<std::string> syst_names_to_plot_on_top = { //
				"JET_JER_DataVsMC",
				"JET_JER_EffectiveNP_1",
				"JET_JER_EffectiveNP_2",
				"JET_JER_EffectiveNP_3",
				"JET_JER_EffectiveNP_4"
			};
			map<string, TH1D *> syst_to_plot; 
			for (const auto &systematic : merged_systematics)
			{
			  if ((systematic->GetName().find("JET_CategoryReduction_JET_JER")!=std::string::npos) || (systematic->GetName().find("JET_JER")!=std::string::npos) || (systematic->GetName().find("JET_Effective")!=std::string::npos) || (systematic->GetName().find("JET_Eta")!=std::string::npos) || (systematic->GetName().find("BJES")!=std::string::npos)) {
					//cout<<"jer plot: "<< systematic->GetName()<<endl;
					bool show=false;
					help = (TH1D *)systematic->GetSymError(wp_name)->Clone();
					for (const auto &syst_name : syst_names_to_plot_on_top)
					{
						if (systematic->GetName().find(syst_name)!=std::string::npos){
							syst_to_plot[syst_name]=(TH1D *) help->Clone();
							show=true;
						} 
					}

					help->Multiply(help);
					if (sym_err) sym_err->Add(help);
					else sym_err=(TH1D *) help->Clone(("h_sym_jet_jer_catr_"+wp_name).c_str());
					if (show){
						if (sum_of_showed) sum_of_showed->Add(help);
						else sum_of_showed=(TH1D *) help->Clone();
					}
				}
			}
			if (sym_err){
				n_pt_bins = sym_err->GetNbinsX();
				for (int pt_bin = 1; pt_bin <= n_pt_bins; pt_bin++)
				{
					sym_err->SetBinError(pt_bin, 0.);
					sym_err->SetBinContent(pt_bin, sqrt(sym_err->GetBinContent(pt_bin)));
					sum_of_showed->SetBinError(pt_bin, 0.);
					sum_of_showed->SetBinContent(pt_bin, sqrt(sum_of_showed->GetBinContent(pt_bin)));
				}
				std::string canvasname = "c_e_b_" + wp_name + "_" + "JET_JER_CategoryReduction" + "_syst_Error";
				TCanvas *c_obs = new TCanvas(canvasname.c_str());
				if (!pt_binning_as_eta)
					c_obs->SetLogx();
				legend_data->Clear();
				legend_data->AddEntry(sym_err, "total relative  JER uncertainty", "F");
				double max_l = sym_err->GetMaximum();
				if (max_l < 0.05)
					max_l = 0.05;
				else
					max_l = max_l * 1.2;
				sym_err->GetYaxis()->SetRangeUser(-1 * max_l, max_l);
				sym_err->GetXaxis()->SetRangeUser(20., 400.);
				//sym_err->GetXaxis()->SetRangeUser(5., 250.);//runVR
				sym_err->SetFillColor(kGreen - 8);
				sym_err->SetLineColor(kGreen - 8);
				TH1D *sym_err_neg = (TH1D *)sym_err->Clone();
				sym_err_neg->Scale(-1);
				sym_err->Draw("HIST");
				sym_err_neg->Draw("HISTSAME");				
				h_line0->DrawLine(20., 0., 400., 0);
				//h_line0->DrawLine(5., 0., 250., 0);//runVR
				int i_col=0;
				for (const auto &syst_name : syst_names_to_plot_on_top)
				{
					if(syst_to_plot.find(syst_name)!=syst_to_plot.end())
					{	
						TH1D * hPlot=syst_to_plot[syst_name];		
						hPlot->SetLineColor(nice_colours.at(i_col));
						hPlot->SetMarkerColor(nice_colours.at(i_col));
						legend_data->AddEntry(hPlot, syst_name.c_str(), "LPE");
						hPlot->Draw("LPESAME");
						i_col++;
					}
				}
				// dont show the sum for now, just to see if it matches the band...
				// if(sum_of_showed){
				// 	sum_of_showed->SetLineColor(nice_colours.at(i_col));
				// 	sum_of_showed->SetMarkerColor(nice_colours.at(i_col));
				// 	legend_data->AddEntry(sum_of_showed, "sum_of_showed", "LPE");
				// 	sum_of_showed->Draw("LPESAME");
				// 	i_col++;
				// }
				sym_err->Draw("AXISSAME");
				

				// tune and record canvas
				c_obs->cd();
				pt->Draw();
				pt2->Draw();
				pt3->Draw();
				pt4->Clear();
				pt4->AddText((releaseName + ", " + ChannelName + ", " + fitConfigName).c_str());
				pt4->AddText((taggerName + ", " + WPName + ", #epsilon_{b}^{MC} = " + wp_name + "%").c_str());
				pt4->Draw();
				legend_data->Draw();
				c_obs->Write();
				sym_err->Write();
			}

		}
	}

		//
	cout<<"FT_EFF_Eigen_C"<<endl;
	if (true)
	{	
		//FT_EFF_Eigen_C
		for (int iwp = 0; iwp < wp_names.size(); iwp++)
		{
			std::string wp_name = wp_names.at(iwp);
			TDirectory *dir = d_unc_summary_plots->GetDirectory((("e_b_wp_" + wp_name).c_str()));
			if (dir == 0)
				dir = d_unc_summary_plots->mkdir((("e_b_wp_" + wp_name).c_str()));
			dir->cd();
			TH1D * help=0;
			TH1D * sym_err=0;
			TH1D * jet_jer=0;
			TH1D * sum_of_showed=0;
			std::vector<std::string> syst_names_to_plot_on_top = { //
				"FT_EFF_Eigen_C_0",
				"FT_EFF_Eigen_C_1",
				"FT_EFF_Eigen_C_2",
				"FT_EFF_Eigen_C_3"
			};
			map<string, TH1D *> syst_to_plot; 
			for (const auto &systematic : merged_systematics)
			{
				if (systematic->GetName().find("FT_EFF_Eigen_C")!=std::string::npos)   {
					//cout<<"jer plot: "<< systematic->GetName()<<endl;
					bool show=false;
					help = (TH1D *)systematic->GetSymError(wp_name)->Clone();
					for (const auto &syst_name : syst_names_to_plot_on_top)
					{
						if (systematic->GetName().find(syst_name)!=std::string::npos){
							syst_to_plot[syst_name]=(TH1D *) help->Clone();
							show=true;
						} 
					}

					help->Multiply(help);
					if (sym_err) sym_err->Add(help);
					else sym_err=(TH1D *) help->Clone("h_sym_FT_EFF_Eigen_C_combined");
					if (show){
						if (sum_of_showed) sum_of_showed->Add(help);
						else sum_of_showed=(TH1D *) help->Clone();
					}
				}
			}
			if (sym_err){
				n_pt_bins = sym_err->GetNbinsX();
				for (int pt_bin = 1; pt_bin <= n_pt_bins; pt_bin++)
				{
					sym_err->SetBinError(pt_bin, 0.);
					sym_err->SetBinContent(pt_bin, sqrt(sym_err->GetBinContent(pt_bin)));
					sum_of_showed->SetBinError(pt_bin, 0.);
					sum_of_showed->SetBinContent(pt_bin, sqrt(sum_of_showed->GetBinContent(pt_bin)));
				}
				std::string canvasname = "c_e_b_" + wp_name + "_" + "FT_EFF_Eigen_C" + "_syst_Error";
				TCanvas *c_obs = new TCanvas(canvasname.c_str());
				if (!pt_binning_as_eta)
					c_obs->SetLogx();
				legend_data->Clear();
				legend_data->AddEntry(sym_err, "total relative FT_C uncertainty", "F");
				double max_l = sym_err->GetMaximum();
				if (max_l < 0.05)
					max_l = 0.05;
				else
					max_l = max_l * 1.2;
				sym_err->GetYaxis()->SetRangeUser(-1 * max_l, max_l);
				sym_err->GetXaxis()->SetRangeUser(20., 400.);
				//sym_err->GetXaxis()->SetRangeUser(5., 250.);//runVR
				sym_err->SetFillColor(kGreen - 8);
				sym_err->SetLineColor(kGreen - 8);
				TH1D *sym_err_neg = (TH1D *)sym_err->Clone();
				sym_err_neg->Scale(-1);
				sym_err->Draw("HIST");
				sym_err_neg->Draw("HISTSAME");				
				h_line0->DrawLine(20., 0., 400., 0);
				//h_line0->DrawLine(5., 0., 250., 0);//runVR
				int i_col=0;
				for (const auto &syst_name : syst_names_to_plot_on_top)
				{
					if(syst_to_plot.find(syst_name)!=syst_to_plot.end())
					{	
						TH1D * hPlot=syst_to_plot[syst_name];		
						hPlot->SetLineColor(nice_colours.at(i_col));
						hPlot->SetMarkerColor(nice_colours.at(i_col));
						legend_data->AddEntry(hPlot, syst_name.c_str(), "LPE");
						hPlot->Draw("LPESAME");
						i_col++;
					}
				}
				// dont show the sum for now, just to see if it matches the band...
				// if(sum_of_showed){
				// 	sum_of_showed->SetLineColor(nice_colours.at(i_col));
				// 	sum_of_showed->SetMarkerColor(nice_colours.at(i_col));
				// 	legend_data->AddEntry(sum_of_showed, "sum_of_showed", "LPE");
				// 	sum_of_showed->Draw("LPESAME");
				// 	i_col++;
				// }
				sym_err->Draw("AXISSAME");
				

				// tune and record canvas
				c_obs->cd();
				pt->Draw();
				pt2->Draw();
				pt3->Draw();
				pt4->Clear();
				pt4->AddText((releaseName + ", " + ChannelName + ", " + fitConfigName).c_str());
				pt4->AddText((taggerName + ", " + WPName + ", #epsilon_{b}^{MC} = " + wp_name + "%").c_str());
				pt4->Draw();
				legend_data->Draw();
				c_obs->Write();
			}

		}
	}

	cout<<"FT_EFF_Eigen_Light"<<endl;
	if (true)
	{	
		//JET_JES
		for (int iwp = 0; iwp < wp_names.size(); iwp++)
		{
			std::string wp_name = wp_names.at(iwp);
			TDirectory *dir = d_unc_summary_plots->GetDirectory((("e_b_wp_" + wp_name).c_str()));
			if (dir == 0)
				dir = d_unc_summary_plots->mkdir((("e_b_wp_" + wp_name).c_str()));
			dir->cd();
			TH1D * help=0;
			TH1D * sym_err=0;
			TH1D * jet_jer=0;
			TH1D * sum_of_showed=0;
			std::vector<std::string> syst_names_to_plot_on_top = { //
				"FT_EFF_Eigen_Light_0",
				"FT_EFF_Eigen_Light_1",
				"FT_EFF_Eigen_Light_2",
				"FT_EFF_Eigen_Light_3"
			};
			map<string, TH1D *> syst_to_plot; 
			for (const auto &systematic : merged_systematics)
			{
				if (systematic->GetName().find("FT_EFF_Eigen_Light")!=std::string::npos)   {
					//cout<<"jer plot: "<< systematic->GetName()<<endl;
					bool show=false;
					help = (TH1D *)systematic->GetSymError(wp_name)->Clone();
					for (const auto &syst_name : syst_names_to_plot_on_top)
					{
						if (systematic->GetName().find(syst_name)!=std::string::npos){
							syst_to_plot[syst_name]=(TH1D *) help->Clone();
							show=true;
						} 
					}

					help->Multiply(help);
					if (sym_err) sym_err->Add(help);
					else sym_err=(TH1D *) help->Clone("h_sym_FT_EFF_Eigen_Light_combined");
					if (show){
						if (sum_of_showed) sum_of_showed->Add(help);
						else sum_of_showed=(TH1D *) help->Clone();
					}
				}
			}
			if (sym_err){
				n_pt_bins = sym_err->GetNbinsX();
				for (int pt_bin = 1; pt_bin <= n_pt_bins; pt_bin++)
				{
					sym_err->SetBinError(pt_bin, 0.);
					sym_err->SetBinContent(pt_bin, sqrt(sym_err->GetBinContent(pt_bin)));
					sum_of_showed->SetBinError(pt_bin, 0.);
					sum_of_showed->SetBinContent(pt_bin, sqrt(sum_of_showed->GetBinContent(pt_bin)));
				}
				std::string canvasname = "c_e_b_" + wp_name + "_" + "FT_EFF_Eigen_Light" + "_syst_Error";
				TCanvas *c_obs = new TCanvas(canvasname.c_str());
				if (!pt_binning_as_eta)
					c_obs->SetLogx();
				legend_data->Clear();
				legend_data->AddEntry(sym_err, "total relative FT_Light uncertainty", "F");
				double max_l = sym_err->GetMaximum();
				if (max_l < 0.05)
					max_l = 0.05;
				else
					max_l = max_l * 1.2;
				sym_err->GetYaxis()->SetRangeUser(-1 * max_l, max_l);
				sym_err->GetXaxis()->SetRangeUser(20., 400.);
				//sym_err->GetXaxis()->SetRangeUser(5., 250.);//runVR
				sym_err->SetFillColor(kGreen - 8);
				sym_err->SetLineColor(kGreen - 8);
				TH1D *sym_err_neg = (TH1D *)sym_err->Clone();
				sym_err_neg->Scale(-1);
				sym_err->Draw("HIST");
				sym_err_neg->Draw("HISTSAME");				
				h_line0->DrawLine(20., 0., 400., 0);
				//h_line0->DrawLine(5., 0., 250., 0);//runVR
				int i_col=0;
				for (const auto &syst_name : syst_names_to_plot_on_top)
				{
					if(syst_to_plot.find(syst_name)!=syst_to_plot.end())
					{	
						TH1D * hPlot=syst_to_plot[syst_name];		
						hPlot->SetLineColor(nice_colours.at(i_col));
						hPlot->SetMarkerColor(nice_colours.at(i_col));
						legend_data->AddEntry(hPlot, syst_name.c_str(), "LPE");
						hPlot->Draw("LPESAME");
						i_col++;
					}
				}
				// dont show the sum for now, just to see if it matches the band...
				// if(sum_of_showed){
				// 	sum_of_showed->SetLineColor(nice_colours.at(i_col));
				// 	sum_of_showed->SetMarkerColor(nice_colours.at(i_col));
				// 	legend_data->AddEntry(sum_of_showed, "sum_of_showed", "LPE");
				// 	sum_of_showed->Draw("LPESAME");
				// 	i_col++;
				// }
				sym_err->Draw("AXISSAME");
				

				// tune and record canvas
				c_obs->cd();
				pt->Draw();
				pt2->Draw();
				pt3->Draw();
				pt4->Clear();
				pt4->AddText((releaseName + ", " + ChannelName + ", " + fitConfigName).c_str());
				pt4->AddText((taggerName + ", " + WPName + ", #epsilon_{b}^{MC} = " + wp_name + "%").c_str());
				pt4->Draw();
				legend_data->Draw();
				c_obs->Write();
			}

		}
	}

		//
	cout<<"Singletop uncertainty plot"<<endl;
	if (true)
	{	
		for (int iwp = 0; iwp < wp_names.size(); iwp++)
		{
			std::string wp_name = wp_names.at(iwp);
			TDirectory *dir = d_unc_summary_plots->GetDirectory((("e_b_wp_" + wp_name).c_str()));
			if (dir == 0)
				dir = d_unc_summary_plots->mkdir((("e_b_wp_" + wp_name).c_str()));
			dir->cd();
			TH1D * help=0;
			TH1D * sym_err=0;
			TH1D * jet_jer=0;
			TH1D * sum_of_showed=0;
			std::vector<std::string> syst_names_to_plot_on_top = { //
				"Singletop_PowHW7",
				"Singletop_aMcPy8",
				"Singletop_PhPy8_weight_mc_rad",
				"Singletop_PDF",
				"Singletop_PhPy8_weight_mc_fsr",
				"Singletop_PowPy8_DS",
				"Singletop_schan_PhPy8_weight_mc_rad",
				"Singletop_schan_PDF",
				"Singletop_schan_PhPy8_weight_mc_fsr",
				"Singletop_tchan_PhPy8_weight_mc_rad",
				"Singletop_tchan_PDF",
				"Singletop_tchan_PhPy8_weight_mc_fsr",
				"Singletop_tW_PhPy8_weight_mc_rad",
				"Singletop_tW_PDF",
				"Singletop_tW_PhPy8_weight_mc_fsr",
			};
			TH1D *Singletop_pdf_uncertanty = 0;
			map<string, TH1D *> syst_to_plot; 
			if(found_Singletop_pdf_uncertanty)
			{
				Singletop_pdf_uncertanty = (TH1D *)Singletop_pdf_combined_e_b_syst_error_rel_squared.at(iwp)->Clone();
				for (int pt_bin = 1; pt_bin <= n_pt_bins; pt_bin++)
				{
					Singletop_pdf_uncertanty->SetBinError(pt_bin, 0.);
					Singletop_pdf_uncertanty->SetBinContent(pt_bin, sqrt(Singletop_pdf_uncertanty->GetBinContent(pt_bin)));
				}
				syst_to_plot["Singletop_pdf"]=(TH1D *) Singletop_pdf_uncertanty->Clone();
				help=(TH1D *) syst_to_plot["Singletop_pdf"]->Clone();
				help->Multiply(help);
				sum_of_showed= help;
			}

			for (const auto &systematic : merged_systematics)
			{
				if (systematic->GetName().find("Singletop_")!=std::string::npos)   {
					//cout<<"jer plot: "<< systematic->GetName()<<endl;
					bool show=false;
					help = (TH1D *)systematic->GetSymError(wp_name)->Clone();
					for (const auto &syst_name : syst_names_to_plot_on_top)
					{
						if (systematic->GetName().find(syst_name)!=std::string::npos){
							syst_to_plot[syst_name]=(TH1D *) help->Clone();
							show=true;
						} 
					}

					help->Multiply(help);
					if (sym_err) sym_err->Add(help);
					else sym_err=(TH1D *) help->Clone("h_sym_Singletop_combined");
					if (show){
						if (sum_of_showed) sum_of_showed->Add(help);
						else sum_of_showed=(TH1D *) help->Clone();
					}
				}
			}
			if (sym_err){
				n_pt_bins = sym_err->GetNbinsX();
				for (int pt_bin = 1; pt_bin <= n_pt_bins; pt_bin++)
				{
					sym_err->SetBinError(pt_bin, 0.);
					sym_err->SetBinContent(pt_bin, sqrt(sym_err->GetBinContent(pt_bin)));
					sum_of_showed->SetBinError(pt_bin, 0.);
					sum_of_showed->SetBinContent(pt_bin, sqrt(sum_of_showed->GetBinContent(pt_bin)));
				}
				std::string canvasname = "c_e_b_" + wp_name + "_" + "Singletop" + "_syst_Error";
				TCanvas *c_obs = new TCanvas(canvasname.c_str());
				if (!pt_binning_as_eta)
					c_obs->SetLogx();
				legend_data->Clear();
				legend_data->AddEntry(sym_err, "total relative  Singletop uncertainty", "F");
				double max_l = sym_err->GetMaximum();
				if (max_l < 0.05)
					max_l = 0.05;
				else
					max_l = max_l * 1.2;
				sym_err->GetYaxis()->SetRangeUser(-1 * max_l, max_l);
				sym_err->GetXaxis()->SetRangeUser(20., 400.);
				//sym_err->GetXaxis()->SetRangeUser(5., 250.);//runVR
				sym_err->SetFillColor(kGreen - 8);
				sym_err->SetLineColor(kGreen - 8);
				TH1D *sym_err_neg = (TH1D *)sym_err->Clone();
				sym_err_neg->Scale(-1);
				sym_err->Draw("HIST");
				sym_err_neg->Draw("HISTSAME");				
				h_line0->DrawLine(20., 0., 400., 0);
				//h_line0->DrawLine(5., 0., 250., 0);//runVR
				int i_col=0;
				for (const auto &syst_name : syst_names_to_plot_on_top)
				{
					if(syst_to_plot.find(syst_name)!=syst_to_plot.end())
					{	
						TH1D * hPlot=syst_to_plot[syst_name];		
						hPlot->SetLineColor(nice_colours.at(i_col));
						hPlot->SetMarkerColor(nice_colours.at(i_col));
						legend_data->AddEntry(hPlot, syst_name.c_str(), "LPE");
						hPlot->Draw("LPESAME");
						i_col++;
					}
				}
				// dont show the sum for now, just to see if it matches the band...
				// if(sum_of_showed){
				// 	sum_of_showed->SetLineColor(nice_colours.at(i_col));
				// 	sum_of_showed->SetMarkerColor(nice_colours.at(i_col));
				// 	legend_data->AddEntry(sum_of_showed, "sum_of_showed", "LPE");
				// 	sum_of_showed->Draw("LPESAME");
				// 	i_col++;
				// }
				sym_err->Draw("AXISSAME");
				

				// tune and record canvas
				c_obs->cd();
				pt->Draw();
				pt2->Draw();
				pt3->Draw();
				pt4->Clear();
				pt4->AddText((releaseName + ", " + ChannelName + ", " + fitConfigName).c_str());
				pt4->AddText((taggerName + ", " + WPName + ", #epsilon_{b}^{MC} = " + wp_name + "%").c_str());
				pt4->Draw();
				legend_data->Draw();
				c_obs->Write();
			}

		}
	}

	cout<<"Diboson uncertainty plot"<<endl;
	if (true)
	{	
		//JET_JES
		for (int iwp = 0; iwp < wp_names.size(); iwp++)
		{
			std::string wp_name = wp_names.at(iwp);
			TDirectory *dir = d_unc_summary_plots->GetDirectory((("e_b_wp_" + wp_name).c_str()));
			if (dir == 0)
				dir = d_unc_summary_plots->mkdir((("e_b_wp_" + wp_name).c_str()));
			dir->cd();
			TH1D * help=0;
			TH1D * sym_err=0;
			TH1D * jet_jer=0;
			TH1D * sum_of_showed=0;
			std::vector<std::string> syst_names_to_plot_on_top = { //
				"Diboson_weight_crossec",
			};
			map<string, TH1D *> syst_to_plot; 
			for (const auto &systematic : merged_systematics)
			{
				if (systematic->GetName().find("Diboson")!=std::string::npos)   {
					//cout<<"jer plot: "<< systematic->GetName()<<endl;
					bool show=false;
					help = (TH1D *)systematic->GetSymError(wp_name)->Clone();
					for (const auto &syst_name : syst_names_to_plot_on_top)
					{
						if (systematic->GetName().find(syst_name)!=std::string::npos){
							syst_to_plot[syst_name]=(TH1D *) help->Clone();
							show=true;
						} 
					}

					help->Multiply(help);
					if (sym_err) sym_err->Add(help);
					else sym_err=(TH1D *) help->Clone("h_sym_Diboson_combined");
					if (show){
						if (sum_of_showed) sum_of_showed->Add(help);
						else sum_of_showed=(TH1D *) help->Clone();
					}
				}
			}
			if (sym_err){
				n_pt_bins = sym_err->GetNbinsX();
				for (int pt_bin = 1; pt_bin <= n_pt_bins; pt_bin++)
				{
					sym_err->SetBinError(pt_bin, 0.);
					sym_err->SetBinContent(pt_bin, sqrt(sym_err->GetBinContent(pt_bin)));
					sum_of_showed->SetBinError(pt_bin, 0.);
					sum_of_showed->SetBinContent(pt_bin, sqrt(sum_of_showed->GetBinContent(pt_bin)));
				}
				std::string canvasname = "c_e_b_" + wp_name + "_" + "Diboson" + "_syst_Error";
				TCanvas *c_obs = new TCanvas(canvasname.c_str());
				if (!pt_binning_as_eta)
					c_obs->SetLogx();
				legend_data->Clear();
				legend_data->AddEntry(sym_err, "total relative  Diboson uncertainty", "F");
				double max_l = sym_err->GetMaximum();
				if (max_l < 0.05)
					max_l = 0.05;
				else
					max_l = max_l * 1.2;
				sym_err->GetYaxis()->SetRangeUser(-1 * max_l, max_l);
				sym_err->GetXaxis()->SetRangeUser(20., 400.);
				//sym_err->GetXaxis()->SetRangeUser(5., 250.);//runVR
				sym_err->SetFillColor(kGreen - 8);
				sym_err->SetLineColor(kGreen - 8);
				TH1D *sym_err_neg = (TH1D *)sym_err->Clone();
				sym_err_neg->Scale(-1);
				sym_err->Draw("HIST");
				sym_err_neg->Draw("HISTSAME");				
				h_line0->DrawLine(20., 0., 400., 0);
				//h_line0->DrawLine(5., 0., 250., 0);//runVR
				int i_col=0;
				for (const auto &syst_name : syst_names_to_plot_on_top)
				{
					if(syst_to_plot.find(syst_name)!=syst_to_plot.end())
					{	
						TH1D * hPlot=syst_to_plot[syst_name];		
						hPlot->SetLineColor(nice_colours.at(i_col));
						hPlot->SetMarkerColor(nice_colours.at(i_col));
						legend_data->AddEntry(hPlot, syst_name.c_str(), "LPE");
						hPlot->Draw("LPESAME");
						i_col++;
					}
				}
				// dont show the sum for now, just to see if it matches the band...
				if(sum_of_showed){
					sum_of_showed->SetLineColor(nice_colours.at(i_col));
					sum_of_showed->SetMarkerColor(nice_colours.at(i_col));
					legend_data->AddEntry(sum_of_showed, "sum_of_showed", "LPE");
					sum_of_showed->Draw("LPESAME");
					i_col++;
				}
				sym_err->Draw("AXISSAME");
				

				// tune and record canvas
				c_obs->cd();
				pt->Draw();
				pt2->Draw();
				pt3->Draw();
				pt4->Clear();
				pt4->AddText((releaseName + ", " + ChannelName + ", " + fitConfigName).c_str());
				pt4->AddText((taggerName + ", " + WPName + ", #epsilon_{b}^{MC} = " + wp_name + "%").c_str());
				pt4->Draw();
				legend_data->Draw();
				c_obs->Write();
			}

		}
	}
	cout<<"Zjets uncertainty plot"<<endl;
	if (true)
	{	
		//JET_JES
		for (int iwp = 0; iwp < wp_names.size(); iwp++)
		{
			std::string wp_name = wp_names.at(iwp);
			TDirectory *dir = d_unc_summary_plots->GetDirectory((("e_b_wp_" + wp_name).c_str()));
			if (dir == 0)
				dir = d_unc_summary_plots->mkdir((("e_b_wp_" + wp_name).c_str()));
			dir->cd();
			TH1D * help=0;
			TH1D * sym_err=0;
			TH1D * jet_jer=0;
			TH1D * sum_of_showed=0;
			std::vector<std::string> syst_names_to_plot_on_top = { //
				"Zjets_weight_crossec",
				"Zjets_weight_mc_qcd_np_4",
				"Zjets_weight_mc_qcd_np_5",
				"Zjets_weight_mc_qcd_np_6",
			};
			map<string, TH1D *> syst_to_plot; 
			for (const auto &systematic : merged_systematics)
			{
				if (systematic->GetName().find("Zjets")!=std::string::npos)   {
					//cout<<"jer plot: "<< systematic->GetName()<<endl;
					bool show=false;
					help = (TH1D *)systematic->GetSymError(wp_name)->Clone();
					for (const auto &syst_name : syst_names_to_plot_on_top)
					{
						if (systematic->GetName().find(syst_name)!=std::string::npos){
							syst_to_plot[syst_name]=(TH1D *) help->Clone();
							show=true;
						} 
					}

					help->Multiply(help);
					if (sym_err) sym_err->Add(help);
					else sym_err=(TH1D *) help->Clone("h_sym_Zjets_combined");
					if (show){
						if (sum_of_showed) sum_of_showed->Add(help);
						else sum_of_showed=(TH1D *) help->Clone();
					}
				}
			}
			if (sym_err){
				n_pt_bins = sym_err->GetNbinsX();
				for (int pt_bin = 1; pt_bin <= n_pt_bins; pt_bin++)
				{
					sym_err->SetBinError(pt_bin, 0.);
					sym_err->SetBinContent(pt_bin, sqrt(sym_err->GetBinContent(pt_bin)));
					sum_of_showed->SetBinError(pt_bin, 0.);
					sum_of_showed->SetBinContent(pt_bin, sqrt(sum_of_showed->GetBinContent(pt_bin)));
				}
				std::string canvasname = "c_e_b_" + wp_name + "_" + "Zjets" + "_syst_Error";
				TCanvas *c_obs = new TCanvas(canvasname.c_str());
				if (!pt_binning_as_eta)
					c_obs->SetLogx();
				legend_data->Clear();
				legend_data->AddEntry(sym_err, "total relative  Zjets uncertainty", "F");
				double max_l = sym_err->GetMaximum();
				if (max_l < 0.05)
					max_l = 0.05;
				else
					max_l = max_l * 1.2;
				sym_err->GetYaxis()->SetRangeUser(-1 * max_l, max_l);
				sym_err->GetXaxis()->SetRangeUser(20., 400.);
				//sym_err->GetXaxis()->SetRangeUser(5., 250.);//runVR
				sym_err->SetFillColor(kGreen - 8);
				sym_err->SetLineColor(kGreen - 8);
				TH1D *sym_err_neg = (TH1D *)sym_err->Clone();
				sym_err_neg->Scale(-1);
				sym_err->Draw("HIST");
				sym_err_neg->Draw("HISTSAME");				
				h_line0->DrawLine(20., 0., 400., 0);
				//h_line0->DrawLine(5., 0., 250., 0);//runVR
				int i_col=0;
				for (const auto &syst_name : syst_names_to_plot_on_top)
				{
					if(syst_to_plot.find(syst_name)!=syst_to_plot.end())
					{	
						TH1D * hPlot=syst_to_plot[syst_name];		
						hPlot->SetLineColor(nice_colours.at(i_col));
						hPlot->SetMarkerColor(nice_colours.at(i_col));
						legend_data->AddEntry(hPlot, syst_name.c_str(), "LPE");
						hPlot->Draw("LPESAME");
						i_col++;
					}
				}
				// dont show the sum for now, just to see if it matches the band...
				// if(sum_of_showed){
				// 	sum_of_showed->SetLineColor(nice_colours.at(i_col));
				// 	sum_of_showed->SetMarkerColor(nice_colours.at(i_col));
				// 	legend_data->AddEntry(sum_of_showed, "sum_of_showed", "LPE");
				// 	sum_of_showed->Draw("LPESAME");
				// 	i_col++;
				// }
				sym_err->Draw("AXISSAME");
				

				// tune and record canvas
				c_obs->cd();
				pt->Draw();
				pt2->Draw();
				pt3->Draw();
				pt4->Clear();
				pt4->AddText((releaseName + ", " + ChannelName + ", " + fitConfigName).c_str());
				pt4->AddText((taggerName + ", " + WPName + ", #epsilon_{b}^{MC} = " + wp_name + "%").c_str());
				pt4->Draw();
				legend_data->Draw();
				c_obs->Write();
			}

		}
	}
	

	cout<<"ttbar uncertainty plot"<<endl;
	if (true)
	{	
		for (int iwp = 0; iwp < wp_names.size(); iwp++)
		{
			std::string wp_name = wp_names.at(iwp);
			TDirectory *dir = d_unc_summary_plots->GetDirectory((("e_b_wp_" + wp_name).c_str()));
			if (dir == 0)
				dir = d_unc_summary_plots->mkdir((("e_b_wp_" + wp_name).c_str()));
			dir->cd();
			TH1D * help=0;
			TH1D * sym_err=0;
			TH1D * jet_jer=0;
			TH1D * sum_of_showed=0;
			std::vector<std::string> syst_names_to_plot_on_top = { //
				"ttbar_HW7",
				"ttbar_Sherpa2212",
				"ttbar_aMcPy8",
				"ttbar_PhPy8_weight_mc_fsr",
				"ttbar_PhPy8_weight_mc_rad",
				"ttbar_qcd_pdf",
			};
			TH1D *ttbar_pdf_uncertanty = 0;
			map<string, TH1D *> syst_to_plot; 
			if(found_ttbar_pdf_uncertanty)
			{
				ttbar_pdf_uncertanty = (TH1D *)ttbar_pdf_combined_e_b_syst_error_rel_squared.at(iwp)->Clone();
				for (int pt_bin = 1; pt_bin <= n_pt_bins; pt_bin++)
				{
					ttbar_pdf_uncertanty->SetBinError(pt_bin, 0.);
					ttbar_pdf_uncertanty->SetBinContent(pt_bin, sqrt(ttbar_pdf_uncertanty->GetBinContent(pt_bin)));
				}
				syst_to_plot["ttbar_qcd_pdf"]=(TH1D *) ttbar_pdf_uncertanty->Clone();
				help=(TH1D *) syst_to_plot["ttbar_qcd_pdf"]->Clone();
				help->Multiply(help);
				sum_of_showed= help;
			}

			for (const auto &systematic : merged_systematics)
			{
				if (systematic->GetName().find("ttbar_")!=std::string::npos)   {
					//cout<<"jer plot: "<< systematic->GetName()<<endl;
					bool show=false;
					help = (TH1D *)systematic->GetSymError(wp_name)->Clone();
					for (const auto &syst_name : syst_names_to_plot_on_top)
					{
						if (systematic->GetName().find(syst_name)!=std::string::npos){
							syst_to_plot[syst_name]=(TH1D *) help->Clone();
							show=true;
						} 
					}

					help->Multiply(help);
					if (sym_err) sym_err->Add(help);
					else sym_err=(TH1D *) help->Clone(("h_sym_ttbar_combined_"+wp_name).c_str());
					if (show){
						if (sum_of_showed) sum_of_showed->Add(help);
						else sum_of_showed=(TH1D *) help->Clone();
					}
				}
			}
			if (sym_err){
				n_pt_bins = sym_err->GetNbinsX();
				for (int pt_bin = 1; pt_bin <= n_pt_bins; pt_bin++)
				{
					sym_err->SetBinError(pt_bin, 0.);
					sym_err->SetBinContent(pt_bin, sqrt(sym_err->GetBinContent(pt_bin)));
					sum_of_showed->SetBinError(pt_bin, 0.);
					sum_of_showed->SetBinContent(pt_bin, sqrt(sum_of_showed->GetBinContent(pt_bin)));
				}
				std::string canvasname = "c_e_b_" + wp_name + "_" + "ttbar" + "_syst_Error";
				TCanvas *c_obs = new TCanvas(canvasname.c_str());
				if (!pt_binning_as_eta)
					c_obs->SetLogx();
				legend_data->Clear();
				legend_data->AddEntry(sym_err, "total relative  ttbar uncertainty", "F");
				double max_l = sym_err->GetMaximum();
				if (max_l < 0.05)
					max_l = 0.05;
				else
					max_l = max_l * 1.2;
				sym_err->GetYaxis()->SetRangeUser(-1 * max_l, max_l);
				sym_err->GetXaxis()->SetRangeUser(20., 400.);
				//sym_err->GetXaxis()->SetRangeUser(5., 250.);//runVR
				sym_err->SetFillColor(kGreen - 8);
				sym_err->SetLineColor(kGreen - 8);
				TH1D *sym_err_neg = (TH1D *)sym_err->Clone();
				sym_err_neg->Scale(-1);
				sym_err->Draw("HIST");
				sym_err_neg->Draw("HISTSAME");				
				h_line0->DrawLine(20., 0., 400., 0);
				//h_line0->DrawLine(5., 0., 250., 0);//runVR
				int i_col=0;
				for (const auto &syst_name : syst_names_to_plot_on_top)
				{
					if(syst_to_plot.find(syst_name)!=syst_to_plot.end())
					{	
						TH1D * hPlot=syst_to_plot[syst_name];		
						hPlot->SetLineColor(nice_colours.at(i_col));
						hPlot->SetMarkerColor(nice_colours.at(i_col));
						legend_data->AddEntry(hPlot, syst_name.c_str(), "LPE");
						hPlot->Draw("LPESAME");
						i_col++;
					}
				}
				// dont show the sum for now, just to see if it matches the band...
				// if(sum_of_showed){
				// 	sum_of_showed->SetLineColor(nice_colours.at(i_col));
				// 	sum_of_showed->SetMarkerColor(nice_colours.at(i_col));
				// 	legend_data->AddEntry(sum_of_showed, "sum_of_showed", "LPE");
				// 	sum_of_showed->Draw("LPESAME");
				// 	i_col++;
				// }
				sym_err->Draw("AXISSAME");
				

				// tune and record canvas
				c_obs->cd();
				pt->Draw();
				pt2->Draw();
				pt3->Draw();
				pt4->Clear();
				pt4->AddText((releaseName + ", " + ChannelName + ", " + fitConfigName).c_str());
				pt4->AddText((taggerName + ", " + WPName + ", #epsilon_{b}^{MC} = " + wp_name + "%").c_str());
				pt4->Draw();
				legend_data->Draw();
				c_obs->Write();
				sym_err->Write();
			}

		}
	}

	//calculate all efficiency errors:
	d_results_cumulative->cd();
	std::vector<TH1D *> combined_e_b_syst_error_rel_squared;
	std::vector<TH1D *> combined_e_b_syst_error_rel_high_squared;
	std::vector<TH1D *> combined_e_b_syst_error_rel_low_squared;
	//initializing of combine error histogramm
	for (int iwp = 0; iwp < wp_names.size(); iwp++)
	{
		std::string wp_name = wp_names.at(iwp);
		TH1D *e_b_Postfit_nominal = (TH1D *)f_nominal->Get(("sf_and_beff/e_b_" + wp_name + "_Postfit").c_str());
		TH1D *combined_e_b_syst_error_rel_squared_wp = (TH1D *)e_b_Postfit_nominal->Clone(("e_b_" + wp_name + "_combined_syst_Error_rel_squared").c_str());
		combined_e_b_syst_error_rel_squared_wp->SetStats(0);
		combined_e_b_syst_error_rel_squared_wp->Reset();

		TH1D *combined_e_b_syst_error_rel_high_squared_wp = (TH1D *)e_b_Postfit_nominal->Clone(("e_b_" + wp_name + "_combined_syst_Error_rel_high_squared").c_str());
		combined_e_b_syst_error_rel_high_squared_wp->SetStats(0);
		combined_e_b_syst_error_rel_high_squared_wp->Reset();

		TH1D *combined_e_b_syst_error_rel_low_squared_wp = (TH1D *)e_b_Postfit_nominal->Clone(("e_b_" + wp_name + "_combined_syst_Error_rel_low_squared").c_str());
		combined_e_b_syst_error_rel_low_squared_wp->SetStats(0);
		combined_e_b_syst_error_rel_low_squared_wp->Reset();
		for (const auto &systematic : merged_systematics)
		{
			TH1D *help = (TH1D *)systematic->GetLowError(wp_name)->Clone();
			help->Multiply(help);
			combined_e_b_syst_error_rel_low_squared_wp->Add(help);
			help = (TH1D *)systematic->GetHighError(wp_name)->Clone();
			help->Multiply(help);
			combined_e_b_syst_error_rel_high_squared_wp->Add(help);
			help = (TH1D *)systematic->GetSymError(wp_name)->Clone();
			help->Multiply(help);
			combined_e_b_syst_error_rel_squared_wp->Add(help);
		}
		combined_e_b_syst_error_rel_low_squared.push_back(combined_e_b_syst_error_rel_low_squared_wp);
		combined_e_b_syst_error_rel_high_squared.push_back(combined_e_b_syst_error_rel_high_squared_wp);
		combined_e_b_syst_error_rel_squared.push_back(combined_e_b_syst_error_rel_squared_wp);
	}

	//final efficiency plot:
	std::cout << "final efficiency plot: " << std::endl;
	for (int iwp = 0; iwp < wp_names.size(); iwp++)
	{
		std::string wp_name = wp_names.at(iwp);
		TH1D *e_b_Postfit_nominal = (TH1D *)f_nominal->Get(("sf_and_beff/e_b_" + wp_name + "_Postfit").c_str());
		TH1D *e_b_syst_Error_squared = (TH1D *)combined_e_b_syst_error_rel_squared.at(iwp)->Clone(("e_b_" + wp_name + "_combined_syst_Error_squared").c_str());
		e_b_syst_Error_squared->Multiply(e_b_Postfit_nominal);
		e_b_syst_Error_squared->Multiply(e_b_Postfit_nominal);
		int n_pt_bins = e_b_Postfit_nominal->GetNbinsX();
		std::vector<double> x_nom, y_nom, xl, xh, yl, yh, yl_stat;
		for (int pt_bin = 1; pt_bin <= n_pt_bins; pt_bin++)
		{
			double x_nom_v = pt_bin_means.at(pt_bin-1);
			x_nom.push_back(x_nom_v);
			xl.push_back(x_nom_v - e_b_Postfit_nominal->GetXaxis()->GetBinLowEdge(pt_bin));
			xh.push_back(e_b_Postfit_nominal->GetXaxis()->GetBinUpEdge(pt_bin) - x_nom_v);
			double y_nom_v = e_b_Postfit_nominal->GetBinContent(pt_bin);
			double y_nom_stat_er = e_b_Postfit_nominal->GetBinError(pt_bin);
			y_nom.push_back(y_nom_v);
			double yerr = sqrt(e_b_syst_Error_squared->GetBinContent(pt_bin) + pow(y_nom_stat_er, 2));
			//std::cout << "P_t_bin: "<<pt_bin<<" yerr " << yerr << " y_stat " << y_nom_stat_er << " y_syst "<< sqrt( e_b_syst_Error_squared->GetBinContent(pt_bin)) <<std::endl;
			yl.push_back(yerr);
			yh.push_back(yerr);
			yl_stat.push_back(abs(y_nom_stat_er));
		}
		const TVectorD vx(x_nom.size(), &x_nom[0]);
		const TVectorD vy(y_nom.size(), &y_nom[0]);
		const TVectorD vexl(xl.size(), &xl[0]);
		const TVectorD vexh(xh.size(), &xh[0]);
		const TVectorD veyl(yl.size(), &yl[0]);
		const TVectorD veyh(yh.size(), &yh[0]);
		const TVectorD ve_stat(yl_stat.size(), &yl_stat[0]);

		TGraphAsymmErrors *er_background = new TGraphAsymmErrors(vx, vy, vexl, vexh, veyl, veyh);
		er_background->SetName(("g_e_b_" + wp_name + "_er_background").c_str());
		
		TGraphAsymmErrors *g_nominal = new TGraphAsymmErrors(vx, vy, vexl, vexh, ve_stat, ve_stat);
		g_nominal->SetName(("g_e_b_" + wp_name + "_nominal").c_str());
		g_nominal->SetLineWidth(2);
		// create new canvas
		std::string canvasname = "c_e_b_" + wp_name + "_nominal";
		TCanvas *c_obs = new TCanvas(canvasname.c_str());
		//c_obs->SetLogx();
		e_b_Postfit_nominal->GetYaxis()->SetRangeUser(0.35, 1.19);
		e_b_Postfit_nominal->GetYaxis()->SetTitle("b-jet tagging efficiency");
		e_b_Postfit_nominal->GetXaxis()->SetTitle("Jet p_{T} [GeV]");
		e_b_Postfit_nominal->GetXaxis()->SetRangeUser(20., 400.);
		//e_b_Postfit_nominal->GetXaxis()->SetRangeUser(5., 250.);//runVR

		e_b_Postfit_nominal->GetYaxis()->SetNdivisions(507);
		e_b_Postfit_nominal->GetYaxis()->SetTitleSize(0.06);
		e_b_Postfit_nominal->GetYaxis()->SetTitleOffset(1.17);
		e_b_Postfit_nominal->GetYaxis()->SetLabelOffset(0.005);
		e_b_Postfit_nominal->GetYaxis()->SetLabelSize(0.06);
		e_b_Postfit_nominal->GetXaxis()->SetNdivisions(510);
		e_b_Postfit_nominal->GetXaxis()->SetTitleSize(0.06);
		e_b_Postfit_nominal->GetXaxis()->SetTitleOffset(1.2);
		e_b_Postfit_nominal->GetXaxis()->SetLabelOffset(0.005);
		e_b_Postfit_nominal->GetXaxis()->SetLabelSize(0.06);
		e_b_Postfit_nominal->GetXaxis()->SetMoreLogLabels();
		e_b_Postfit_nominal->Draw("AXIS");
		er_background->SetFillColor(kGreen - 8);
		er_background->SetLineColor(kGreen - 8);
		er_background->Draw("2SAME");


			TFile *c_file = new TFile((inputFile_str  + "nominal.root").c_str(), "READ");
			if (!c_file)
			{
				std::cerr << "Input file: " << inputFile_str  +  "nominal.root"
						  << " not found ... exiting." << std::endl;
				return -1;
			}
			d_results_cumulative->cd();
			TH1D *e_b_Prefit_c = (TH1D *)((TH1D *)c_file->Get(("MCeff/h_effMC_ttbar_b_" + wp_name + "wp").c_str()))->Clone(("h_effMC_ttbar_b_" + wp_name + "wp_c_nominal2" ).c_str());
			e_b_Prefit_c->SetLineColor(nice_colours.at(0));
			e_b_Prefit_c->SetLineStyle(1);
			c_obs->cd();
			string name_for_legend = "ttbar_PhPy8 (nominal)";
			e_b_Prefit_c->Draw("HISTSAME");
			//std::cout << "closing compare file." << std::endl;

			e_b_Prefit_c->Write();
			c_file->Close();
		g_nominal->Draw("pSAME");
		//h_line1->DrawLine(e_b_Postfit_nominal->GetXaxis()->GetXmin(), 1., e_b_Postfit_nominal->GetXaxis()->GetXmax(), 1);
		//e_b_Postfit_nominal->Draw("LPESAME");
		e_b_Postfit_nominal->Draw("AXISSAME");
		// tune and record canvas
		c_obs->cd();
		pt->Draw();
		pt2->Draw();
		pt3->Draw();
		pt4->Clear();
		pt4->Clear();
		if (taggerName=="MV2c10")
		{
			taggerName="MV2";
		}
		pt4->AddText((taggerName +",  #epsilon_{b} = " + wp_name + "% Single Cut OP").c_str());
		pt4->Draw();
		legend_eb->Clear();
		legend_eb->AddEntry(e_b_Postfit_nominal, "Data (stat. unc.)", "LPE");
		legend_eb->AddEntry(er_background, "Data (total unc.)", "F");
		legend_eb->AddEntry(e_b_Prefit_c, "t#bar{t} MC", "l");
		

		legend_eb->Draw();
		d_results_cumulative->cd();
		c_obs->Write();
		e_b_Postfit_nominal->Write();
		g_nominal->Write();

		//now with log scaling and last bin
		c_obs->SetName((canvasname + "_log").c_str());
		if (!pt_binning_as_eta)
		{
			c_obs->SetLogx();
			d_results_cumulative->cd();
			c_obs->Write();
		}

		//c_obs->SaveAs((out_name + canvasname + ".pdf").c_str());
		//lets add some mc values to compare with:
		//std::cout << "final efficiency plot: adding compare names:" << std::endl;
		canvasname = "c_e_b_" + wp_name + "_true_mc_comparison";
		TCanvas *c_e_b_true = (TCanvas *)c_obs->Clone((canvasname).c_str());
		c_e_b_true->SetName(canvasname.c_str());
		legend_fit->Clear();
		legend_fit->AddEntry(e_b_Postfit_nominal, "data", "LPE");
		legend_fit->AddEntry(er_background, "stat + syst unc", "F");
		e_b_Postfit_nominal->SetYTitle("estimated b-efficiency");
		e_b_Postfit_nominal->Draw("Axissame");
		for (int n_c = 0; n_c < mc_samples_to_compare.size(); n_c++)
		{
			TFile *c_file = new TFile((inputFile_str + mc_samples_to_compare.at(n_c) + ".root").c_str(), "READ");
			if (!c_file)
			{
				std::cerr << "Input file: " << mc_samples_to_compare.at(n_c) + ".root"
						  << " not found ... exiting." << std::endl;
				return -1;
			}
			d_results_cumulative->cd();
			TH1D *e_b_Prefit_c = (TH1D *)((TH1D *)c_file->Get(("MCeff/h_effMC_ttbar_b_" + wp_name + "wp").c_str()))->Clone(("h_effMC_ttbar_b_" + wp_name + "wp_c_" + mc_samples_to_compare.at(n_c)).c_str());
			e_b_Prefit_c->SetLineColor(nice_colours.at(n_c));
			e_b_Prefit_c->SetLineStyle(1 + n_c);
			c_e_b_true->cd();
			string name_for_legend = mc_samples_to_compare.at(n_c);
			string ftag2 = "";
			if (name_for_legend == "nominal")
				name_for_legend = "ttbar_PhPy8 (nominal)";
			if (ftag2 == name_for_legend.substr(0, ftag2.size()))
				name_for_legend = name_for_legend.substr(ftag2.size());
			legend_fit->AddEntry(e_b_Prefit_c, (name_for_legend).c_str(), "l");
			e_b_Prefit_c->Draw("HISTSAME");
			//std::cout << "closing compare file." << std::endl;

			e_b_Prefit_c->Write();
			c_file->Close();
		}
		c_e_b_true->cd();
		//std::cout << "Drawing legend:" << std::endl;
		legend_fit->Draw();
		g_nominal->Draw("pSAME");
		e_b_Postfit_nominal->Draw("Axissame");
		d_results_cumulative->cd();
		//std::cout << "Writing:" << std::endl;
		c_e_b_true->Write();

		//lets do the same thing for the estimated efficiencys.
		std::cout << "final efficiency plot: adding estiamted efficiencies." << std::endl;
		canvasname = "c_e_b_" + wp_name + "_estimated_mc_comparison";
		TCanvas *c_e_b_estimated = (TCanvas *)c_obs->Clone((canvasname).c_str());
		c_e_b_estimated->SetName(canvasname.c_str());
		legend_fit->Clear();
		legend_fit->AddEntry(e_b_Postfit_nominal, "ttbar_PhPy8 (nominal)", "LPE");
		legend_fit->AddEntry(er_background, "stat + syst unc", "F");
		for (int n_c = 0; n_c < mc_samples_to_compare.size(); n_c++)
		{
			TFile *c_file = new TFile((inputFile_str + mc_samples_to_compare.at(n_c) + ".root").c_str(), "READ");
			if (!c_file)
			{
				std::cerr << "Input file: " << mc_samples_to_compare.at(n_c) + ".root"
						  << " not found ... exiting." << std::endl;
				return -1;
			}
			d_results_cumulative->cd();
			TH1D *e_b_Prefit_c = (TH1D *)((TH1D *)c_file->Get(("sf_and_beff/e_b_" + wp_name + "_Postfit").c_str()))->Clone(("h_e_b_Postfit_ttbar_b_" + wp_name + "wp_c_" + mc_samples_to_compare.at(n_c)).c_str());
			e_b_Prefit_c->SetLineColor(nice_colours.at(n_c));
			//e_b_Prefit_c->SetLineStyle(1 + n_c);
			c_e_b_estimated->cd();
			string name_for_legend = mc_samples_to_compare.at(n_c);
			string ftag2 = "FTAG2_";
			if (name_for_legend == "nominal")
			{
				continue;
			}
			if (ftag2 == name_for_legend.substr(0, ftag2.size()))
				name_for_legend = name_for_legend.substr(ftag2.size());
			legend_fit->AddEntry(e_b_Prefit_c, (name_for_legend).c_str(), "l");
			e_b_Prefit_c->Draw("HISTSAME");
			c_file->Close();
		}
		c_e_b_estimated->cd();
		legend_fit->Draw();
		g_nominal->Draw("pSAME");
		e_b_Postfit_nominal->Draw("AXISSAME");
		d_results_cumulative->cd();
		c_e_b_estimated->Write();
		//lets do a ratio true efficiency mc/data.
		std::cout << "final efficiency plot: ratios:" << std::endl;
		canvasname = "c_e_b_" + wp_name + "_mc_div_data";
		TCanvas *c_mc_div_data = new TCanvas(canvasname.c_str());
		legend_fit->Clear();
		e_b_Postfit_nominal->SetYTitle("#epsilon_{b}^{MC} / #epsilon_{b}^{data}");
		for (int n_c = 0; n_c < mc_samples_to_compare.size(); n_c++)
		{
			TFile *c_file = new TFile((inputFile_str + mc_samples_to_compare.at(n_c) + ".root").c_str(), "READ");
			if (!c_file)
			{
				std::cerr << "Input file: " << mc_samples_to_compare.at(n_c) + ".root"
						  << " not found ... exiting." << std::endl;
				return -1;
			}
			d_results_cumulative->cd();
			TH1D *e_b_Prefit_c = (TH1D *)((TH1D *)c_file->Get(("MCeff/h_effMC_ttbar_b_" + wp_name + "wp").c_str()))->Clone(("h_effMC_ttbar_b_" + wp_name + "wp_c_" + mc_samples_to_compare.at(n_c)).c_str());
			e_b_Prefit_c->SetLineColor(nice_colours.at(n_c));
			e_b_Prefit_c->SetLineStyle(1 + n_c);
			c_mc_div_data->cd();
			string name_for_legend = mc_samples_to_compare.at(n_c);
			string ftag2 = "FTAG2_";
			if (name_for_legend == "nominal")
				name_for_legend = "ttbar_PhPy8 (nominal)";
			if (ftag2 == name_for_legend.substr(0, ftag2.size()))
				name_for_legend = name_for_legend.substr(ftag2.size());
			legend_fit->AddEntry(e_b_Prefit_c, (name_for_legend).c_str(), "l");
			e_b_Prefit_c->SetYTitle("#epsilon_{b}^{MC} / #epsilon_{b}^{data}");
			e_b_Prefit_c->Divide(e_b_Postfit_nominal);
			e_b_Prefit_c->GetYaxis()->SetRangeUser(0.5, 1.3);
			e_b_Prefit_c->Draw("HISTSAME");
			c_file->Close();
		}
		c_mc_div_data->cd();
		legend_fit->Draw();
		d_results_cumulative->cd();
		c_mc_div_data->Write();
	}
	//
	//
	//final scalfactor plot:
	std::cout << "final e_b scalfactor plot: " << std::endl;
	for (int iwp = 0; iwp < wp_names.size(); iwp++)
	{
		std::string wp_name = wp_names.at(iwp);
		TH1D *e_b_Postfit_nominal = (TH1D *)f_nominal->Get(("sf_and_beff/e_b_" + wp_name + "_Postfit").c_str());
		TH1D *sf_b_Postfit_nominal = (TH1D *)e_b_Postfit_nominal->Clone(("sf_b_" + wp_name + "_nominal_calculated_in_systplot").c_str());
		TH1D *e_b_ttb_nominal = (TH1D *)f_nominal->Get(("MCeff/h_effMC_ttbar_b_" + wp_name + "wp").c_str()); //denominator
		sf_b_Postfit_nominal->Divide(e_b_ttb_nominal);
		TH1D *sf_b_Postfit_nominal_form_fitPlot = (TH1D *)f_nominal->Get(("sf_and_beff/sf_b_" + wp_name + "_Postfit").c_str());
		TH1D *sf_b_Postfit_syst_rel = (TH1D *)sf_b_Postfit_nominal_form_fitPlot->Clone(("sf_b_" + wp_name + "_syst_Error_combined_rel").c_str());
		sf_b_Postfit_syst_rel->SetStats(0);
		sf_b_Postfit_syst_rel->Reset();
		TH1D *sf_b_Postfit_syst_high_rel = (TH1D *)sf_b_Postfit_nominal_form_fitPlot->Clone(("sf_b_" + wp_name + "_syst_Error_high_rel").c_str());
		sf_b_Postfit_syst_high_rel->SetStats(0);
		sf_b_Postfit_syst_high_rel->Reset();
		TH1D *sf_b_Postfit_syst_low_rel = (TH1D *)sf_b_Postfit_nominal_form_fitPlot->Clone(("sf_b_" + wp_name + "_syst_Error_low_rel").c_str());
		sf_b_Postfit_syst_low_rel->SetStats(0);
		sf_b_Postfit_syst_low_rel->Reset();
		TH1D *sf_b_Postfit_stat_rel = (TH1D *)sf_b_Postfit_nominal_form_fitPlot->Clone(("sf_b_" + wp_name + "_stat_Error_rel").c_str());
		sf_b_Postfit_stat_rel->SetStats(0);
		sf_b_Postfit_stat_rel->Reset();
		sf_b_Postfit_nominal->GetXaxis()->SetTitle(sf_b_Postfit_nominal_form_fitPlot->GetXaxis()->GetTitle());
		sf_b_Postfit_nominal->GetYaxis()->SetTitle(sf_b_Postfit_nominal_form_fitPlot->GetYaxis()->GetTitle());
		TH1D *e_b_syst_Error_rel_squared = (TH1D *)combined_e_b_syst_error_rel_squared.at(iwp)->Clone(("sf_b_" + wp_name + "_combined_syst_Error_squared").c_str());
		TH1D *sf_b_Postfit_stat_plus_syst_rel = (TH1D *)sf_b_Postfit_stat_rel->Clone(("sf_b_" + wp_name + "_stat_plus_syst_Error_rel").c_str());
		std::vector<double> x_nom, y_nom, xl, xh, yl, yh, yl_stat;
		for (int pt_bin = 1; pt_bin <= n_pt_bins; pt_bin++)
		{
			if (abs(sf_b_Postfit_nominal->GetBinContent(pt_bin) - sf_b_Postfit_nominal_form_fitPlot->GetBinContent(pt_bin)) > 0.00000000001)
				cout << "Error with claculated scalefactors! systplot: " << sf_b_Postfit_nominal_form_fitPlot->GetBinContent(pt_bin) << " calculated now: " << sf_b_Postfit_nominal->GetBinContent(pt_bin) << " wp " << wp_name << " pt " << pt_bin << endl;
			sf_b_Postfit_syst_high_rel->SetBinContent(pt_bin, sqrt(combined_e_b_syst_error_rel_high_squared.at(iwp)->GetBinContent(pt_bin)));
			sf_b_Postfit_syst_low_rel->SetBinContent(pt_bin, sqrt(combined_e_b_syst_error_rel_low_squared.at(iwp)->GetBinContent(pt_bin)));
			e_b_ttb_nominal->SetBinError(pt_bin, 0.);
			double x_nom_v = pt_bin_means.at(pt_bin-1);
			x_nom.push_back(x_nom_v);
			xl.push_back(x_nom_v - e_b_Postfit_nominal->GetXaxis()->GetBinLowEdge(pt_bin));
			xh.push_back(e_b_Postfit_nominal->GetXaxis()->GetBinUpEdge(pt_bin) - x_nom_v);
			double y_nom_v = sf_b_Postfit_nominal_form_fitPlot->GetBinContent(pt_bin);
			double y_nom_stat_er = sf_b_Postfit_nominal_form_fitPlot->GetBinError(pt_bin);
			double y_nom_stat_er_rel = y_nom_stat_er / y_nom_v;
			double y_syst_err_rel = sqrt(e_b_syst_Error_rel_squared->GetBinContent(pt_bin));
			double y_syst_err = y_syst_err_rel * y_nom_v;
			double yerr = sqrt(pow(y_syst_err, 2) + pow(y_nom_stat_er, 2));

			sf_b_Postfit_stat_rel->SetBinContent(pt_bin, y_nom_stat_er_rel);
			sf_b_Postfit_syst_rel->SetBinContent(pt_bin, y_syst_err_rel);
			sf_b_Postfit_syst_rel->SetBinError(pt_bin, 0.);
			sf_b_Postfit_stat_plus_syst_rel->SetBinContent(pt_bin, yerr);
			sf_b_Postfit_stat_plus_syst_rel->SetBinError(pt_bin, 0.);
			//yerr= yerr / e_b_ttb_nominal->GetBinContent(pt_bin);
			//std::cout << "P_t_bin: "<<pt_bin<<" yerr " << yerr << " y_stat " << y_nom_stat_er << " y_syst "<< sqrt( e_b_syst_Error_squared->GetBinContent(pt_bin)) <<std::endl;
			y_nom.push_back(y_nom_v);
			yl.push_back(yerr);
			yh.push_back(yerr);
			yl_stat.push_back(abs(y_nom_stat_er));
		}
		const TVectorD vx(x_nom.size(), &x_nom[0]);
		const TVectorD vy(y_nom.size(), &y_nom[0]);
		const TVectorD vexl(xl.size(), &xl[0]);
		const TVectorD vexh(xh.size(), &xh[0]);
		const TVectorD veyl(yl.size(), &yl[0]);
		const TVectorD veyh(yh.size(), &yh[0]);
		const TVectorD ve_stat(yl_stat.size(), &yl_stat[0]);

		TGraphAsymmErrors *er_background = new TGraphAsymmErrors(vx, vy, vexl, vexh, veyl, veyh);
		er_background->SetName(("g_e_b_sf_" + wp_name + "_er_background").c_str());
		
		TGraphAsymmErrors *g_nominal = new TGraphAsymmErrors(vx, vy, vexl, vexh, ve_stat, ve_stat);
		g_nominal->SetName(("g_e_b_sf_" + wp_name + "_nominal").c_str());
		g_nominal->SetLineWidth(2);

		// create new canvas
		std::string canvasname = "c_e_b_sf_" + wp_name + "_nominal";
		TCanvas *c_obs = new TCanvas(canvasname.c_str());
		c_obs->cd();

		sf_b_Postfit_nominal_form_fitPlot->GetYaxis()->SetRangeUser(0.8, 1.15);
		sf_b_Postfit_nominal_form_fitPlot->GetXaxis()->SetRangeUser(20., 400.);
		//sf_b_Postfit_nominal_form_fitPlot->GetXaxis()->SetRangeUser(5., 250.);//runVR

		sf_b_Postfit_nominal_form_fitPlot->GetYaxis()->SetTitle("b-jet tagging efficiency SF");
		sf_b_Postfit_nominal_form_fitPlot->GetYaxis()->SetNdivisions(507);
		sf_b_Postfit_nominal_form_fitPlot->GetYaxis()->SetTitleSize(0.06);
		sf_b_Postfit_nominal_form_fitPlot->GetYaxis()->SetTitleOffset(1.17);
		sf_b_Postfit_nominal_form_fitPlot->GetYaxis()->SetLabelOffset(0.005);
		sf_b_Postfit_nominal_form_fitPlot->GetYaxis()->SetLabelSize(0.06);
		sf_b_Postfit_nominal_form_fitPlot->GetXaxis()->SetTitle("Jet p_{T} [GeV]");
		sf_b_Postfit_nominal_form_fitPlot->GetXaxis()->SetNdivisions(510);
		sf_b_Postfit_nominal_form_fitPlot->GetXaxis()->SetTitleSize(0.06);
		sf_b_Postfit_nominal_form_fitPlot->GetXaxis()->SetTitleOffset(1.2);
		sf_b_Postfit_nominal_form_fitPlot->GetXaxis()->SetLabelOffset(0.005);
		sf_b_Postfit_nominal_form_fitPlot->GetXaxis()->SetLabelSize(0.06);
		sf_b_Postfit_nominal_form_fitPlot->GetXaxis()->SetMoreLogLabels();
		sf_b_Postfit_nominal_form_fitPlot->Draw("Axis");
		er_background->SetFillColor(kGreen - 8);
		er_background->SetLineColor(kGreen - 8);
		er_background->Draw("2SAME");
		h_line1->DrawLine(sf_b_Postfit_nominal_form_fitPlot->GetXaxis()->GetXmin(), 1., sf_b_Postfit_nominal_form_fitPlot->GetXaxis()->GetXmax(), 1);
		g_nominal->Draw("pSAME");
		sf_b_Postfit_nominal_form_fitPlot->Draw("AXISSAME");
		// tune and record canvas
		c_obs->cd();
		pt->Draw();
		pt2->Draw();
		pt3->Draw();
		pt4->Clear();
		if (taggerName=="MV2c10")
		{
			taggerName="MV2";
		}
		pt4->AddText((taggerName +",  #epsilon_{b} = " + wp_name + "% Single Cut OP").c_str());
		pt4->Draw();
		legend_eb_sf->Clear();
		legend_eb_sf->AddEntry(sf_b_Postfit_nominal_form_fitPlot, "Scale Factor (stat. unc.)", "LPE");
		legend_eb_sf->AddEntry(er_background, "Scale factor (total unc.)", "F");
		legend_eb_sf->Draw();
		d_results_cumulative->cd();
		c_obs->Write();

		//now with log scaling and last bin
		canvasname = canvasname + "_log";
		c_obs->SetName((canvasname).c_str());
		if (!pt_binning_as_eta)
			c_obs->SetLogx();

		d_results_cumulative->cd();
		c_obs->Write();
		g_nominal->Write();
		sf_b_Postfit_syst_rel->Write();
		sf_b_Postfit_stat_rel->Write();
		sf_b_Postfit_nominal_form_fitPlot->Write();
		sf_b_Postfit_syst_high_rel->Write();
		sf_b_Postfit_syst_low_rel->Write();
		sf_b_Postfit_stat_plus_syst_rel->Write();

		//draw a canvas to compare all the other errors
		canvasname = "c_sf_e_b_" + wp_name + "_combined_errors";
		c_obs = new TCanvas(canvasname.c_str());
		c_obs->cd();
		if (!pt_binning_as_eta)
			c_obs->SetLogx();

		legend_fit_2d->Clear();
		legend_fit_2d->AddEntry(sf_b_Postfit_stat_plus_syst_rel, "stat + syst", "LPE");
		sf_b_Postfit_stat_plus_syst_rel->GetYaxis()->SetRangeUser(0., 0.12);
		sf_b_Postfit_stat_plus_syst_rel->GetYaxis()->SetTitle("relative uncertainty");
		sf_b_Postfit_stat_plus_syst_rel->GetXaxis()->SetRangeUser(20., 400.);
		//sf_b_Postfit_stat_plus_syst_rel->GetXaxis()->SetRangeUser(5., 250.);//runVR
		sf_b_Postfit_stat_plus_syst_rel->Draw("LPE");
		h_line0->DrawLine(20., 0., 400., 0);
		////h_line0->DrawLine(5., 0., 250., 0);//runVR

		sf_b_Postfit_syst_rel->SetLineColor(kGreen - 8);
		sf_b_Postfit_syst_rel->SetMarkerColor(kGreen - 8);
		legend_fit_2d->AddEntry(sf_b_Postfit_syst_rel, "syst", "LPE");
		sf_b_Postfit_syst_rel->Draw("LPESAME");

		// sf_b_Postfit_syst_low_rel->SetLineColor(kGreen - 8);
		// sf_b_Postfit_syst_low_rel->SetMarkerColor(kGreen - 8);
		// legend_fit_2d->AddEntry(sf_b_Postfit_syst_low_rel, "low Errors *(-1)", "LPE");
		// sf_b_Postfit_syst_low_rel->Draw("LPESAME");

		// sf_b_Postfit_syst_high_rel->SetLineColor(kRed + 1);
		// sf_b_Postfit_syst_high_rel->SetMarkerColor(kRed + 1);
		// legend_fit_2d->AddEntry(sf_b_Postfit_syst_high_rel, "high Error", "LPE");
		// sf_b_Postfit_syst_high_rel->Draw("LPESAME");

		sf_b_Postfit_stat_rel->SetLineColor(kBlue + 1);
		sf_b_Postfit_stat_rel->SetMarkerColor(kBlue + 1);
		legend_fit_2d->AddEntry(sf_b_Postfit_stat_rel, "stat", "LPE");
		sf_b_Postfit_stat_rel->Draw("LPESAME");

		sf_b_Postfit_stat_plus_syst_rel->Draw("LPESAME");
		// tune and record canvas
		c_obs->cd();
		pt->Draw();
		pt2->Draw();
		pt3->Draw();
		pt4->Clear();
		pt4->AddText((releaseName + ", " + ChannelName + ", " + fitConfigName).c_str());
		pt4->AddText((taggerName + ", " + WPName + ", #epsilon_{b}^{MC} = " + wp_name + "%").c_str());
		pt4->Draw();
		legend_fit_2d->Draw();
		sf_b_Postfit_stat_plus_syst_rel->Draw("AXISSAME");
		d_results_cumulative->cd();
		c_obs->Write();
	}

	//calculate all p_b errors:
	std::cout << "calculating p_b errors: " << std::endl;
	d_results_continuous->cd();
	TH1D *e_b_Postfit_nominal = (TH1D *)f_nominal->Get(("sf_and_beff/e_b_" + wp_names.at(0) + "_Postfit").c_str());
	std::vector<TH1D *> combined_p_b_syst_error_rel_squared;
	std::vector<TH1D *> combined_p_b_syst_error_rel_high_squared;
	std::vector<TH1D *> combined_p_b_syst_error_rel_low_squared;
	//initializing of combine error histogramm
	for (int bin = 1; bin <= n_pt_bins; bin++)
	{
		std::string pt_name = "pt_" + to_string(bin);
		TH1D *p_b_Postfit_nominal = (TH1D *)f_nominal->Get(("pdf/p_b_" + pt_name + "_Postfit").c_str());
		TH1D *combined_p_b_syst_error_rel_squared_pt = (TH1D *)p_b_Postfit_nominal->Clone(("p_b_" + pt_name + "_combined_syst_Error_rel_squared").c_str());
		combined_p_b_syst_error_rel_squared_pt->SetStats(0);
		combined_p_b_syst_error_rel_squared_pt->Reset();

		TH1D *combined_p_b_syst_error_rel_high_squared_pt = (TH1D *)p_b_Postfit_nominal->Clone(("p_b_" + pt_name + "_combined_syst_Error_rel_high_squared").c_str());
		combined_p_b_syst_error_rel_high_squared_pt->SetStats(0);
		combined_p_b_syst_error_rel_high_squared_pt->Reset();

		TH1D *combined_p_b_syst_error_rel_low_squared_pt = (TH1D *)p_b_Postfit_nominal->Clone(("p_b_" + pt_name + "_combined_syst_Error_rel_low_squared").c_str());
		combined_p_b_syst_error_rel_low_squared_pt->SetStats(0);
		combined_p_b_syst_error_rel_low_squared_pt->Reset();
		for (const auto &systematic : merged_systematics)
		{
			TH1D *help = (TH1D *)systematic->GetPbLowError(pt_name)->Clone();
			help->Multiply(help);
			combined_p_b_syst_error_rel_low_squared_pt->Add(help);
			help = (TH1D *)systematic->GetPbHighError(pt_name)->Clone();
			help->Multiply(help);
			combined_p_b_syst_error_rel_high_squared_pt->Add(help);
			help = (TH1D *)systematic->GetPbSymError(pt_name)->Clone();
			help->Multiply(help);
			combined_p_b_syst_error_rel_squared_pt->Add(help);
			/*
			if(bin==1){ // debug
			  std::cout<<systematic->GetName()<<" ";
			  for(int i_test=1;i_test<=combined_p_b_syst_error_rel_low_squared_pt->GetNbinsX();i_test++){
			    std::cout<<combined_p_b_syst_error_rel_low_squared_pt->GetBinContent(i_test)<<" ";
			    std::cout<<combined_p_b_syst_error_rel_high_squared_pt->GetBinContent(i_test)<<" ";
			    std::cout<<combined_p_b_syst_error_rel_squared_pt->GetBinContent(i_test)<<" ";
			  }
			  std::cout<<std::endl;
			}*/
		}
		combined_p_b_syst_error_rel_low_squared.push_back(combined_p_b_syst_error_rel_low_squared_pt);
		combined_p_b_syst_error_rel_high_squared.push_back(combined_p_b_syst_error_rel_high_squared_pt);
		combined_p_b_syst_error_rel_squared.push_back(combined_p_b_syst_error_rel_squared_pt);
	}

	TH1D *e_b_Postfit_nominal_dummy = (TH1D *)f_nominal->Get(("sf_and_beff/e_b_" + wp_names.at(0) + "_Postfit").c_str());
	//final p_b plots:
	std::cout << "final p_b plot:" << std::endl;
	for (int bin = 1; bin <= n_pt_bins; bin++)
	{
		std::string pt_name = "pt_" + to_string(bin);
		double pt_low_edge = e_b_Postfit_nominal_dummy->GetXaxis()->GetBinLowEdge(bin);
		double pt_high_edge = e_b_Postfit_nominal_dummy->GetXaxis()->GetBinUpEdge(bin);
		string pt_label = std::to_string((int)pt_low_edge) + " GeV < Jet p_{T} < " + std::to_string((int)pt_high_edge) + " GeV";
		TH1D *p_b_Postfit_nominal = (TH1D *)f_nominal->Get(("pdf/p_b_" + pt_name + "_Postfit").c_str());
		TH1D *p_b_syst_Error_squared = (TH1D *)combined_p_b_syst_error_rel_squared.at(bin - 1)->Clone(("p_b_" + pt_name + "_combined_syst_Error_squared").c_str());
		p_b_syst_Error_squared->Multiply(p_b_Postfit_nominal);
		p_b_syst_Error_squared->Multiply(p_b_Postfit_nominal);
		std::vector<double> x_nom, y_nom, xl, xh, yl, yh;
		for (int wp_bin = 1; wp_bin <= wp_names.size() + 1; wp_bin++)
		{
			double x_nom_v = p_b_Postfit_nominal->GetXaxis()->GetBinCenter(wp_bin);
			x_nom.push_back(x_nom_v);
			xl.push_back(x_nom_v - p_b_Postfit_nominal->GetXaxis()->GetBinLowEdge(wp_bin));
			xh.push_back(p_b_Postfit_nominal->GetXaxis()->GetBinUpEdge(wp_bin) - x_nom_v);
			double y_nom_v = p_b_Postfit_nominal->GetBinContent(wp_bin);
			double y_nom_stat_er = p_b_Postfit_nominal->GetBinError(wp_bin);
			y_nom.push_back(y_nom_v);
			double yerr = sqrt(p_b_syst_Error_squared->GetBinContent(wp_bin) + pow(y_nom_stat_er, 2));
			//std::cout << "P_t_bin: "<<wp_bin<<" yerr " << yerr << " y_stat " << y_nom_stat_er << " y_syst "<< sqrt( p_b_syst_Error_squared->GetBinContent(wp_bin)) <<std::endl;
			yl.push_back(yerr);
			yh.push_back(yerr);
		}
		const TVectorD vx(x_nom.size(), &x_nom[0]);
		const TVectorD vy(y_nom.size(), &y_nom[0]);
		const TVectorD vexl(xl.size(), &xl[0]);
		const TVectorD vexh(xh.size(), &xh[0]);
		const TVectorD veyl(yl.size(), &yl[0]);
		const TVectorD veyh(yh.size(), &yh[0]);

		TGraphAsymmErrors *er_background = new TGraphAsymmErrors(vx, vy, vexl, vexh, veyl, veyh);
		// create new canvas
		std::string canvasname = "c_p_b_" + pt_name + "_nominal";
		TCanvas *c_obs = new TCanvas(canvasname.c_str());
		//c_obs->SetLogx();
		c_obs->cd();
		p_b_Postfit_nominal->GetYaxis()->SetRangeUser(0, 1.);
		p_b_Postfit_nominal->GetYaxis()->SetTitle("b-jet tagging probability");
		p_b_Postfit_nominal->GetXaxis()->SetTitle(("D_{" + taggerName + "}").c_str());
		p_b_Postfit_nominal->GetYaxis()->SetNdivisions(507);
		p_b_Postfit_nominal->GetYaxis()->SetTitleSize(0.06);
		p_b_Postfit_nominal->GetYaxis()->SetTitleOffset(1.17);
		p_b_Postfit_nominal->GetYaxis()->SetLabelOffset(0.005);
		p_b_Postfit_nominal->GetYaxis()->SetLabelSize(0.06);
		p_b_Postfit_nominal->GetXaxis()->SetTitleSize(0.06);
		p_b_Postfit_nominal->GetXaxis()->SetTitleOffset(1.2);
		p_b_Postfit_nominal->GetXaxis()->SetLabelOffset(0.005);
		p_b_Postfit_nominal->GetXaxis()->SetLabelSize(0.08);
		p_b_Postfit_nominal->Draw("LPE");
		er_background->SetFillColor(kGreen - 8);
		er_background->SetLineColor(kGreen - 8);
		er_background->Draw("2SAME");
			TFile *c_file = new TFile((inputFile_str +  "nominal.root").c_str(), "READ");
			if (!c_file)
			{
				std::cerr << "Input file: " << "nominal.root"
						  << " not found ... exiting." << std::endl;
				return -1;
			}
			d_results_continuous->cd();
			TH1D *p_b_Prefit_c = (TH1D *)((TH1D *)c_file->Get(("pdf/p_b_ttb_" + pt_name + "_MC").c_str()))->Clone(("p_b_ttb_" + pt_name + "_MC_" +"nominal2").c_str());
			p_b_Prefit_c->SetLineColor(nice_colours.at(0));
			p_b_Prefit_c->SetLineStyle(1 );
			c_obs->cd();
			p_b_Prefit_c->Draw("HISTSAME");
			c_file->Close();
		//h_line1->DrawLine(p_b_Postfit_nominal->GetXaxis()->GetXmin(), 1., p_b_Postfit_nominal->GetXaxis()->GetXmax(), 1);
		p_b_Postfit_nominal->Draw("LPESAME");
		p_b_Postfit_nominal->Draw("AXISSAME");
		// tune and record canvas
		c_obs->cd();
		pt->Draw();
		pt2->Draw();
		pt3->Draw();
		pt4->Clear();
		if (taggerName=="MV2c10")
		{
			taggerName="MV2";
		}
		pt4->AddText((taggerName +", " + pt_label).c_str());
		pt4->Draw();
		legend_pb->Clear();
		legend_pb->AddEntry(p_b_Postfit_nominal, "Data (stat. unc.)", "LPE");
		legend_pb->AddEntry(er_background, "Data (total unc.)", "F");
		legend_pb->AddEntry(p_b_Prefit_c, "t#bar{t} MC", "l");
		legend_pb->Draw();
		//  legend_fit->Draw();
		d_results_continuous->cd();
		c_obs->Write();
		//c_obs->SaveAs((out_name + canvasname + ".pdf").c_str());
		//lets add some mc values to compare with:
		canvasname = "c_p_b_" + pt_name + "_true_mc_comparison";
		TCanvas *c_p_b_true = (TCanvas *)c_obs->Clone((canvasname).c_str());
		c_p_b_true->SetName(canvasname.c_str());
		for (int n_c = 0; n_c < mc_samples_to_compare.size(); n_c++)
		{
			TFile *c_file = new TFile((inputFile_str + mc_samples_to_compare.at(n_c) + ".root").c_str(), "READ");
			if (!c_file)
			{
				std::cerr << "Input file: " << mc_samples_to_compare.at(n_c) + ".root"
						  << " not found ... exiting." << std::endl;
				return -1;
			}
			d_results_continuous->cd();
			TH1D *p_b_Prefit_c = (TH1D *)((TH1D *)c_file->Get(("pdf/p_b_ttb_" + pt_name + "_MC").c_str()))->Clone(("p_b_ttb_" + pt_name + "_MC_" + mc_samples_to_compare.at(n_c)).c_str());
			p_b_Prefit_c->SetLineColor(nice_colours.at(n_c));
			p_b_Prefit_c->SetLineStyle(1 + n_c);
			c_p_b_true->cd();
			string name_for_legend = mc_samples_to_compare.at(n_c);
			string ftag2 = "FTAG2_";
			if (name_for_legend == "nominal")
				name_for_legend = "ttbar_PhPy8 (nominal)";
			if (ftag2 == name_for_legend.substr(0, ftag2.size()))
				name_for_legend = name_for_legend.substr(ftag2.size());
			legend_fit->AddEntry(p_b_Prefit_c, (name_for_legend).c_str(), "l");
			p_b_Prefit_c->Draw("HISTSAME");
			c_file->Close();
		}
		c_p_b_true->cd();
		legend_fit->Draw();
		// ->SetYTitle("estimated b-efficiency");
		e_b_Postfit_nominal->Draw("LPESAME");
		d_results_continuous->cd();
		c_p_b_true->Write();
		//lets do the same thing for the estimated efficiencys.
		canvasname = "c_p_b_" + pt_name + "_estimated_mc_comparison";
		TCanvas *c_p_b_estimated = (TCanvas *)c_obs->Clone((canvasname).c_str());
		c_p_b_estimated->SetName(canvasname.c_str());
		legend_fit->Clear();
		legend_fit->AddEntry(p_b_Postfit_nominal, "ttbar_PhPy8 (nominal)", "LPE");
		legend_fit->AddEntry(er_background, "stat + syst unc", "F");
		for (int n_c = 0; n_c < mc_samples_to_compare.size(); n_c++)
		{
			TFile *c_file = new TFile((inputFile_str + mc_samples_to_compare.at(n_c) + ".root").c_str(), "READ");
			if (!c_file)
			{
				std::cerr << "Input file: " << mc_samples_to_compare.at(n_c) + ".root"
						  << " not found ... exiting." << std::endl;
				return -1;
			}
			d_results_continuous->cd();
			TH1D *p_b_Postfit_c = (TH1D *)((TH1D *)c_file->Get(("pdf/p_b_" + pt_name + "_Postfit").c_str()))->Clone(("h_p_b_Postfit_" + pt_name + "_c_" + mc_samples_to_compare.at(n_c)).c_str());
			p_b_Postfit_c->SetLineColor(nice_colours.at(n_c));
			c_p_b_estimated->cd();
			string name_for_legend = mc_samples_to_compare.at(n_c);
			string ftag2 = "FTAG2_";
			if (name_for_legend == "nominal")
			{
				continue;
			}
			if (ftag2 == name_for_legend.substr(0, ftag2.size()))
				name_for_legend = name_for_legend.substr(ftag2.size());
			legend_fit->AddEntry(p_b_Postfit_c, (name_for_legend).c_str(), "l");
			p_b_Postfit_c->Draw("HISTSAME");
			c_file->Close();
		}
		c_p_b_estimated->cd();
		legend_fit->Draw();
		p_b_Postfit_nominal->Draw("LPESAME");
		p_b_Postfit_nominal->Draw("AXISSAME");
		d_results_continuous->cd();
		c_p_b_estimated->Write();
		// //lets do a ratio true efficiency mc/data.
		canvasname = "c_p_b_" + pt_name + "_mc_div_data";
		TCanvas *c_p_b_mc_div_data = new TCanvas(canvasname.c_str());
		legend_fit->Clear();
		for (int n_c = 0; n_c < mc_samples_to_compare.size(); n_c++)
		{
			TFile *c_file = new TFile((inputFile_str + mc_samples_to_compare.at(n_c) + ".root").c_str(), "READ");
			if (!c_file)
			{
				std::cerr << "Input file: " << mc_samples_to_compare.at(n_c) + ".root"
						  << " not found ... exiting." << std::endl;
				return -1;
			}
			d_results_continuous->cd();
			TH1D *p_b_div_data = (TH1D *)((TH1D *)c_file->Get(("pdf/p_b_ttb_" + pt_name + "_MC").c_str()))->Clone(("p_b_ttb_" + pt_name + "_MC_" + mc_samples_to_compare.at(n_c) + "_div_data").c_str());
			p_b_div_data->SetLineColor(nice_colours.at(n_c));
			p_b_div_data->SetLineStyle(1 + n_c);
			c_p_b_mc_div_data->cd();
			string name_for_legend = mc_samples_to_compare.at(n_c);
			string ftag2 = "FTAG2_";
			if (name_for_legend == "nominal")
				name_for_legend = "ttbar_PhPy8 (nominal)";
			if (ftag2 == name_for_legend.substr(0, ftag2.size()))
				name_for_legend = name_for_legend.substr(ftag2.size());
			legend_fit->AddEntry(p_b_div_data, (name_for_legend).c_str(), "l");
			p_b_div_data->SetYTitle("p_{b}^{MC} / p_{b}^{data}");
			p_b_div_data->Divide(p_b_Postfit_nominal);
			p_b_div_data->GetYaxis()->SetRangeUser(0.5, 1.3);
			p_b_div_data->Draw("HISTSAME");
			c_file->Close();
		}
		c_p_b_mc_div_data->cd();
		legend_fit->Draw();
		d_results_continuous->cd();
		c_p_b_mc_div_data->Write();
	}
	//
	//final p_b scalfactor plot:
	std::cout << "final p_b sf plot: " << std::endl;
	for (int bin = 1; bin <= n_pt_bins; bin++)
	{
		std::string pt_name = "pt_" + to_string(bin);
		double pt_low_edge = e_b_Postfit_nominal_dummy->GetXaxis()->GetBinLowEdge(bin);
		double pt_high_edge = e_b_Postfit_nominal_dummy->GetXaxis()->GetBinUpEdge(bin);
		string pt_label = std::to_string((int)pt_low_edge) + " GeV < Jet p_{T} < " + std::to_string((int)pt_high_edge) + " GeV";
		TH1D *p_b_Postfit_nominal = (TH1D *)f_nominal->Get(("pdf/p_b_" + pt_name + "_Postfit").c_str());
		int n_t_bins = p_b_Postfit_nominal->GetNbinsX();
		TH1D *p_b_ttb_nominal = (TH1D *)f_nominal->Get(("pdf/p_b_ttb_" + pt_name + "_MC").c_str()); //denominator
		for (int t_bin = 1; t_bin <= n_t_bins; t_bin++)
			p_b_ttb_nominal->SetBinError(t_bin, 0.);
		TH1D *p_b_sf_Postfit_nominal = (TH1D *)p_b_Postfit_nominal->Clone(("p_b_sf_" + pt_name + "_nominal").c_str());
		p_b_sf_Postfit_nominal->Divide(p_b_ttb_nominal);
		p_b_sf_Postfit_nominal->GetYaxis()->SetTitle("b-jet PDF sf");

		TH1D *p_b_sf_Postfit_syst_rel = (TH1D *)p_b_sf_Postfit_nominal->Clone(("p_b_sf_" + pt_name + "_syst_Error_combined_rel").c_str());
		p_b_sf_Postfit_syst_rel->SetStats(0);
		p_b_sf_Postfit_syst_rel->Reset();
		p_b_sf_Postfit_syst_rel->GetYaxis()->SetTitle("relative uncertainty");
		TH1D *p_b_sf_Postfit_syst_high_rel = (TH1D *)p_b_sf_Postfit_syst_rel->Clone(("p_b_sf_" + pt_name + "_syst_Error_high_rel").c_str());
		p_b_sf_Postfit_syst_high_rel->SetStats(0);
		p_b_sf_Postfit_syst_high_rel->Reset();
		TH1D *p_b_sf_Postfit_syst_low_rel = (TH1D *)p_b_sf_Postfit_syst_rel->Clone(("p_b_sf_" + pt_name + "_syst_Error_low_rel").c_str());
		p_b_sf_Postfit_syst_low_rel->SetStats(0);
		p_b_sf_Postfit_syst_low_rel->Reset();
		TH1D *p_b_sf_Postfit_stat_rel = (TH1D *)p_b_sf_Postfit_syst_rel->Clone(("p_b_sf_" + pt_name + "_stat_Error_rel").c_str());
		p_b_sf_Postfit_stat_rel->SetStats(0);
		p_b_sf_Postfit_stat_rel->Reset();
		std::vector<double> x_nom, y_nom, xl, xh, yl, yh;
		for (int t_bin = 1; t_bin <= n_t_bins; t_bin++)
		{
			p_b_sf_Postfit_syst_high_rel->SetBinContent(t_bin, sqrt(combined_p_b_syst_error_rel_high_squared.at(bin - 1)->GetBinContent(t_bin)));
			p_b_sf_Postfit_syst_low_rel->SetBinContent(t_bin, sqrt(combined_p_b_syst_error_rel_low_squared.at(bin - 1)->GetBinContent(t_bin)));
			double x_nom_v = p_b_Postfit_nominal->GetXaxis()->GetBinCenter(t_bin);
			x_nom.push_back(x_nom_v);
			xl.push_back(x_nom_v - p_b_Postfit_nominal->GetXaxis()->GetBinLowEdge(t_bin));
			xh.push_back(p_b_Postfit_nominal->GetXaxis()->GetBinUpEdge(t_bin) - x_nom_v);
			double y_nom_v = p_b_sf_Postfit_nominal->GetBinContent(t_bin);
			double y_nom_stat_er = p_b_sf_Postfit_nominal->GetBinError(t_bin);
			double y_nom_stat_er_rel = y_nom_stat_er / y_nom_v;
			double y_syst_err_rel = sqrt(combined_p_b_syst_error_rel_squared.at(bin - 1)->GetBinContent(t_bin));
			double y_syst_err = y_syst_err_rel * y_nom_v;
			double yerr = sqrt(pow(y_syst_err, 2) + pow(y_nom_stat_er, 2));

			p_b_sf_Postfit_stat_rel->SetBinContent(t_bin, y_nom_stat_er_rel);
			p_b_sf_Postfit_stat_rel->SetBinError(t_bin, 0.);
			p_b_sf_Postfit_syst_rel->SetBinContent(t_bin, y_syst_err_rel);
			p_b_sf_Postfit_syst_rel->SetBinError(t_bin, 0.);

			//yerr= yerr / e_b_ttb_nominal->GetBinContent(t_bin);
			//std::cout << "P_t_bin: "<<t_bin<<" yerr " << yerr << " y_stat " << y_nom_stat_er << " y_syst "<< sqrt( e_b_syst_Error_squared->GetBinContent(t_bin)) <<std::endl;
			y_nom.push_back(y_nom_v);
			yl.push_back(yerr);
			yh.push_back(yerr);
		}
		const TVectorD vx(x_nom.size(), &x_nom[0]);
		const TVectorD vy(y_nom.size(), &y_nom[0]);
		const TVectorD vexl(xl.size(), &xl[0]);
		const TVectorD vexh(xh.size(), &xh[0]);
		const TVectorD veyl(yl.size(), &yl[0]);
		const TVectorD veyh(yh.size(), &yh[0]);

		TGraphAsymmErrors *er_background = new TGraphAsymmErrors(vx, vy, vexl, vexh, veyl, veyh);
		// create new canvas
		std::string canvasname = "c_p_b_sf_" + pt_name + "_nominal";
		TCanvas *c_obs = new TCanvas(canvasname.c_str());
		c_obs->cd();
		p_b_sf_Postfit_nominal->GetYaxis()->SetRangeUser(0.5 , 1.5);
		p_b_sf_Postfit_nominal->GetYaxis()->SetTitle("b-jet tagging probability SF");
		p_b_sf_Postfit_nominal->GetXaxis()->SetTitle(("D_{" + taggerName + "}").c_str());
		p_b_sf_Postfit_nominal->GetYaxis()->SetNdivisions(507);
		p_b_sf_Postfit_nominal->GetYaxis()->SetTitleSize(0.06);
		p_b_sf_Postfit_nominal->GetYaxis()->SetTitleOffset(1.17);
		p_b_sf_Postfit_nominal->GetYaxis()->SetLabelOffset(0.005);
		p_b_sf_Postfit_nominal->GetYaxis()->SetLabelSize(0.06);
		p_b_sf_Postfit_nominal->GetXaxis()->SetTitleSize(0.06);
		p_b_sf_Postfit_nominal->GetXaxis()->SetTitleOffset(1.2);
		p_b_sf_Postfit_nominal->GetXaxis()->SetLabelOffset(0.005);
		p_b_sf_Postfit_nominal->GetXaxis()->SetLabelSize(0.08);
		p_b_sf_Postfit_nominal->Draw("LPE");
		er_background->SetFillColor(kGreen - 8);
		er_background->SetLineColor(kGreen - 8);
		er_background->Draw("2SAME");
		h_line1->DrawLine(p_b_sf_Postfit_nominal->GetXaxis()->GetXmin(), 1., p_b_sf_Postfit_nominal->GetXaxis()->GetXmax(), 1);
		p_b_sf_Postfit_nominal->Draw("LPESAME");
		p_b_sf_Postfit_nominal->Draw("AXISSAME");
		// tune and record canvas
		c_obs->cd();
		pt->Draw();
		pt2->Draw();
		pt3->Draw();
		pt4->Clear();
		if (taggerName=="MV2c10")
		{
			taggerName="MV2";
		}
		pt4->AddText((taggerName +", " + pt_label).c_str());
		pt4->Draw();
		legend_eb_sf->Clear();
		legend_eb_sf->AddEntry(p_b_Postfit_nominal, "Data (stat. unc.)", "LPE");
		legend_eb_sf->AddEntry(er_background, "Data (total unc.)", "F");
		legend_eb_sf->Draw();

		//  legend_fit->Draw();
		d_results_continuous->cd();
		c_obs->Write();
		//c_obs->SaveAs((out_name + canvasname + ".pdf").c_str());
		p_b_sf_Postfit_syst_rel->Write();
		p_b_sf_Postfit_nominal->Write();
		p_b_sf_Postfit_syst_high_rel->Write();
		p_b_sf_Postfit_syst_low_rel->Write();

		//draw a canvas to compare all the other errors
		canvasname = "c_p_b_sf_" + pt_name + "_combined_errors";
		c_obs = new TCanvas(canvasname.c_str());
		c_obs->cd();
		legend_fit_2d->Clear();
		legend_fit_2d->AddEntry(p_b_sf_Postfit_syst_rel, "sym syst Errors", "LPE");
		p_b_sf_Postfit_syst_rel->GetYaxis()->SetRangeUser(0., 0.5);
		p_b_sf_Postfit_syst_rel->Draw("LPE");
		p_b_sf_Postfit_syst_low_rel->SetLineColor(kGreen - 8);
		p_b_sf_Postfit_syst_low_rel->SetMarkerColor(kGreen - 8);
		legend_fit_2d->AddEntry(p_b_sf_Postfit_syst_low_rel, "low Errors *(-1)", "LPE");
		p_b_sf_Postfit_syst_low_rel->Draw("LPESAME");
		p_b_sf_Postfit_syst_high_rel->SetLineColor(kRed + 1);
		p_b_sf_Postfit_syst_high_rel->SetMarkerColor(kRed + 1);
		legend_fit_2d->AddEntry(p_b_sf_Postfit_syst_high_rel, "high Error", "LPE");
		p_b_sf_Postfit_syst_high_rel->Draw("LPESAME");
		p_b_sf_Postfit_stat_rel->SetLineColor(kBlue + 1);
		p_b_sf_Postfit_stat_rel->SetMarkerColor(kBlue + 1);
		legend_fit_2d->AddEntry(p_b_sf_Postfit_stat_rel, "stat Error", "LPE");
		p_b_sf_Postfit_stat_rel->Draw("LPESAME");
		// tune and record canvas
		c_obs->cd();
		pt->Draw();
		pt2->Draw();
		pt3->Draw();
		pt4->Clear();
		pt4->AddText((releaseName + ", " + ChannelName + ", " + fitConfigName).c_str());
		pt4->AddText((taggerName + ", " + WPName + ", " + pt_label).c_str());
		pt4->Draw();
		legend_fit_2d->Draw();
		d_results_continuous->cd();
		c_obs->Write();
	}
	std::cout << " closing output file....." << std::endl;
	f_output->Close();
	std::cout << " done." << std::endl;
	return 0;
}
