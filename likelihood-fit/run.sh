#!/bin/bash
# To combine histograms for fit
python combine_histos_for_fit_with_syst.py 2>&1 | tee log.combine

# To fit
python fit.py --rrF 2>&1 | tee log.fit

# To plot the scale factors
cd plot_sf/
python get_results.py 2>&1 | tee log.get_results
cd ../

# To get the CDI file and the uncertainty breakdown
cd results/
python writeCDI_PCBT.py 2>&1 | tee log.writeCDI_PCBT
python sf_uncert_from_txt.py -c settings_Run3.yaml 2>&1 | tee log.sf_uncert_from_txt
