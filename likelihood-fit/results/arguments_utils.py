import configparser
import argparse
import ast
import yaml

def args_from_config(args, parser):
    """Read arguments from a YAML settings file, overwrite with command-line arguments, and return args."""

    # Load YAML configuration
    with open(args.config, "r") as file:
        config = yaml.safe_load(file)

    # Set defaults
    parser.set_defaults(**config["defaults"])
    args = parser.parse_args()  # Overwrite arguments

    # Set list of CDI txt input filenames
    flavour_details = {
        "flavour": config["defaults"]["flavour"],
        "title": config["defaults"]["title"],
        "colour": config["defaults"]["colour"],
        "group": config["defaults"]["group"],
    }
    args.flavour_details = flavour_details

    # 4. Set CDI txt file paths by year
    args.years = config.get("years", {})

    # 5. Input/output directories
    args.input_dir = config["defaults"]["input_dir"]
    args.plot_dir = config["defaults"]["plot_dir"]

    return args


def parser_arguments():
    parser = argparse.ArgumentParser(description="Input arguments defined in settings.ini file")
    parser.add_argument(
        "--config",
        #abbreviated as -s
        "-c",
        type=str,
        required=True,
        help="Configuration files with script settings in ini standard",
    )

    return parser
