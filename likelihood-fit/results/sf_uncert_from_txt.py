# Written by Lorenzo Santi: https://gitlab.cern.ch/atlas-ftag-calibration/comb_CDI/-/blob/losanti_GN2_dev/sf_uncert_from_txt.py?ref_type=heads
import re
import os
import math
import colorsys
import arguments_utils
import matplotlib.colors as mcolors
from collections import defaultdict
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
import yaml
import atlasify as al
import numpy as np
from matplotlib.legend_handler import HandlerLine2D, HandlerPatch



def main():
    parser = arguments_utils.parser_arguments()
    args = parser.parse_args()

    if args.config:
        args = arguments_utils.args_from_config(args, parser)  # Load settings from YAML
    else:
        print("Please, provide a settings.yaml file")
        exit(1)

    # create output directory if it does not exist
    os.makedirs(args.plot_dir, exist_ok=True)

    for wp in args.wp:
        print(f"Plotting {wp} working point for {args.flavour_details['title']}")        
        plot_sf_and_uncertainty(wp, args)

    make_table_breakdown_pt_vs_wp(
        args)
    
    print(f"Eveything is done! Plots are saved in {args.plot_dir}")

def plot_sf_and_uncertainty(wp, args):
    fig_sf, sf_axes = setup_plot_grid("Scale Factor", wp, args)
    fig_uncertainty, unc_axes = setup_plot_grid("Systematic Uncertainty", wp, args)

    flavour = args.flavour_details["title"]
    for year in args.years:
        print(f"Plotting {year} year")
        args.year = year
        pt_bins, scale_factors, uncertainties, sys_breakdown = extract_sf_and_sys(
            flavour, wp, year, args
        )
        pt_centers, pt_errors = calculate_pt_bins(pt_bins)

        # Plot Scale Factors
        plot_sf_with_errorbars(
            sf_axes[args.years[year]['subplot_pos']],
            pt_centers,
            scale_factors,
            pt_errors,
            uncertainties,
            flavour,
            wp,
            year,
            args,
        )

        # create .tex table of breakdown
        make_table_breakdown_wp_vs_pt(
            pt_bins,
            scale_factors,
            sys_breakdown,
            flavour,
            wp,
            year,
            args)

        # Plot Systematic Breakdown
        plot_sys_breakdown(
            unc_axes[args.years[year]['subplot_pos']],
            pt_bins,
            sys_breakdown,
            flavour,
            year,
            args,
        )

    save_plots(fig_sf, fig_uncertainty, wp, args)


def setup_plot_grid(title, wp, args):
    fig, axes = plt.subplots(len(list(args.years)),1, 
                            figsize=(7, max(len(list(args.years))*3, 5)),
                            sharex=True, dpi=300)#, gridspec_kw={"hspace": 0.05}, dpi=300)
    # set grid spec if len(list(args.years)) > 1
    if len(list(args.years)) > 1:
        fig.subplots_adjust(hspace=0.05)

    # if axes is not a list make it a list
    if not isinstance(axes, list):
        axes = [axes]
    x_label = r"Jet $p_T$"
    if title == "Scale Factor":
        x_label = f"{x_label} [GeV]"
    else:
        #fig.subplots_adjust(right)
        x_label = f"{x_label} bin [GeV]"
    reference = args.years[list(args.years)[0]]
    al.atlasify(args.ATLAS_label,
                           f"{reference['run']}, "r"$\sqrt{s}$="f"{reference['energy']}, GN2 "r"$b$"f"-tagging, {wp.split('-')[1]}%<"r"$\varepsilon_b$<"f"{wp.split('-')[0]}%",
                           font_size=20, label_font_size=16, sub_font_size=14,
                           axes=axes[0], outside=True
                           )
    axes[-1].set_xlabel(x_label, fontsize=16, loc="right")
    
    # ticks internal to the plot
    axes[-1].tick_params(axis='x', which='both', labelsize=12, direction='in')
    for (axis,yr) in zip(axes, list(args.years)):
        axis.tick_params(axis="x", which="both", bottom=True, top=False)
        axis.tick_params(axis="y", which="major", left=True, right=False, direction='in')
        #remov minor ticks
        axis.tick_params(axis="y", which="minor", left=False, right=False)
        axis.text(0.05, 0.95, f'Data {args.years[yr]["name"]} L={args.years[yr]["lumi"]/1e3:.1f} fb'r'$^{-1}$',
            transform=axis.transAxes, fontsize=9, verticalalignment='top')
    
    axes[-1].tick_params(axis="x", which="both", bottom=False, top=False, labelbottom=True)

    return fig, axes


def plot_sf_with_errorbars(
    axis, pt_centers, scale_factors, pt_errors, uncertainties, flavour, wp, year, args
):
    axis.set_ylabel(f"{flavour[0]}-jets SF", fontsize=16, loc="center")
    eb1 = axis.errorbar(
        pt_centers,
        scale_factors,
        xerr=pt_errors,
        yerr=uncertainties[0],
        fmt="none",  
        label='Total uncertainty',
        ecolor=args.flavour_details['colour'],
        elinewidth=3,
    )
    eb1[-1][0].set_linestyle("-")
    eb1[-1][1].set_linestyle("-")

    eb2 = axis.errorbar(
        pt_centers,
        scale_factors,
        xerr=pt_errors,
        yerr=uncertainties[1],
        fmt="none",  # Suppress central markers
        label='Statistical uncertainty',
        ecolor='black',
        elinewidth=1
    )
    eb2[-1][0].set_linestyle("-")
    eb2[-1][1].set_linestyle("-")

    ycenters = np.array(scale_factors)
    yerrors = np.array(uncertainties[0])
    ymin = min(ycenters - yerrors) - max(yerrors)/8
    ymax = max(ycenters + yerrors) + max(yerrors)/2.5
    
    axis.set_ylim(min(1, ymin), max(1, ymax))
    axis.axhline(
            y=1, 
            color="grey", linestyle="--", linewidth=1
        )

    # Aggiungi la legenda utilizzando la mappa degli handler personalizzata
    if args.years[year]['subplot_pos'] == 0:
        axis.legend(
            handles=[eb1, eb2],
            labels=['Total uncertainty', 'Statistical uncertainty'],
            loc='best',
            frameon=False,
            fontsize=12,   
        )
        #in the legend remove caps
        
        #axis.legend(handler_map=handler_map, loc='best', frameon=False, fontsize=12)

        
        #make legend symbols larger
        #for line in axis.get_legend().get_lines():
         #   line.set_linewidth(8.0)
         #   line.set_markersize(8.0)

def make_table_breakdown_wp_vs_pt(pt_bins, scale_factors, sys_breakdown, flavour, wp, year, args):

    table_file = os.path.join(args.plot_dir, f"Breakdown_Table_{wp}_{year}.tex")
    with open(table_file, "w") as file:
        file.write("\\begin{table}[htb!]\n")
        file.write("\\setlength{\\tabcolsep}{12pt}\n")
        file.write("\\centering\n")
        file.write("\\begin{tabular}{l")
        for _ in range(len(pt_bins)-1):
            file.write("c")
        file.write("}\n")
        file.write("\\hline\n")
        file.write(f"Source of uncertainty & \\multicolumn{{{len(pt_bins)-1}}}{{c}}{{\\makecell{{Absolute uncertainty [\\%] on \\\\ b-tagging efficiency per $p_T$ bin [GeV]}}}}")
        file.write("\\\\\n")
        file.write(f"\\cmidrule(lr){{2-{len(pt_bins)}}}")
        file.write("\\\\\n")
        # merge all the other columns 

        for pt_idx, pt in enumerate(pt_bins[:-1]):
            file.write(f" &  {pt_bins[pt_idx]}-{pt_bins[pt_idx+1]} ")
        file.write("\\\\\n")
        file.write("\\toprule\n")
        # use sys_breakdown[0] as reference for the keys 
        for sys_name in sys_breakdown[0].keys():
            if sys_name == "Total Uncertainty" or sys_name == "Statistical uncertainty" or sys_name == "Other sources combined":
                continue
            file.write(f"{sys_name} ")
            for sys_components, sf in zip(sys_breakdown, scale_factors):
                file.write(f" & {sys_components[sys_name]*sf:.1f} ")
            file.write("\\\\\n")
        
        # print other source 
        file.write("Other sources combined ")
        for sys_components, sf in zip(sys_breakdown, scale_factors):
            file.write(f" & {sys_components.get('Other sources combined', 0)*sf:.1f} ")
        file.write("\\\\\n")
        file.write("\\hline\n")
        # total systematics
        file.write("Total Systematics ")
        for sys_components, sf in zip(sys_breakdown, scale_factors):
            # difference in quadrature of tot and stat
            syst_unc = math.sqrt((sys_components['Total Uncertainty']*sf)**2 - (sys_components['Statistical uncertainty']*sf)**2)
            file.write(f" & {syst_unc:.1f} ")
        file.write("\\\\\n")

        # print statistical uncertainty
        file.write("Statistical uncertainty ")
        for sys_components, sf in zip(sys_breakdown, scale_factors):
            file.write(f" & {sys_components['Statistical uncertainty']*sf:.1f} ")
        file.write("\\\\\n")
        file.write("\\toprule\n")

        # total uncertainty
        file.write("\\textbf{{Total Uncertainty}} ")
        for sys_components, sf in zip(sys_breakdown, scale_factors):
            file.write(f" & {sys_components['Total Uncertainty']*sf:.1f} ")
        file.write("\\\\\n")
        file.write("\\hline\n")
        file.write("\\end{tabular}\n")
        if "22_23" in year:
            printyear = "Run 3 (2022-2023)"
        elif "15_18" in year:
            printyear = "Run 2 (2015-2018)"
        else:
            printyear = year.split("_")[1]
        file.write(f"\\caption{{Uncertainty breakdown of systematics uncertainty on the {flavour} tagging efficiency for the {wp.split('-')[1]}\\%"r"$<\varepsilon_b<$"f"{wp.split('-')[0]}\\% b-tagging pseudo-continuous working point in {printyear} data.}}\n")
        file.write("\\end{table}\n")

def make_table_breakdown_pt_vs_wp(args):
    
    for year in args.years:
        # make a breakdown table for each pt bin with all the WP in that ptbin
        # read pt bins from the first WP
        
        sfs_vs_wp = {}
        sys_breakdown_vs_wp = {}
        
        for wp in args.wp:
            pt_bins, sfs, _, sys_breakdown = extract_sf_and_sys(
            args.flavour_details["title"], wp, year, args
            )
            sfs_vs_wp[wp] = sfs
            sys_breakdown_vs_wp[wp] = sys_breakdown
        systlist = sys_breakdown[0].keys()
        #print(sfs_vs_wp)

        print(f"Making table for {year} year")
        for i, pt in enumerate(pt_bins[:-1]):
            table_file = os.path.join(args.plot_dir, f"Breakdown_Table_{year}_pt_{pt}-{pt_bins[i+1]}.tex")
            with open(table_file, "w") as file:

                file.write("\\begin{table}[htb!]\n")
                file.write("\\centering\n")
                file.write("\\begin{tabular}{l")
                for _ in range(len(args.wp)):
                    file.write("c")
                file.write("}\n")
                file.write("\\hline\n")
                file.write(f"Source of uncertainty & \\multicolumn{{{len(args.wp)}}}{{c}}{{\\makecell{{Absolute uncertainty [\\%] on \\\\ b-tagging efficiency per pseudo-continuous WP}}}}")
                file.write("\\\\\n")
                file.write(f"\\cmidrule(lr){{2-{len(args.wp)+1}}}")
                file.write("\\\\\n")

                for wp in args.wp:
                    file.write(f" &  {wp} ")
                file.write("\\\\\n")
                file.write("\\toprule\n")

#
                for sys_name in systlist:
                    if sys_name == "Total Uncertainty" or sys_name == "Statistical uncertainty" or sys_name == "Other sources combined":
                        continue
                    file.write(f"{sys_name} ")
                    for wp in args.wp:
                        file.write(f" & {sys_breakdown_vs_wp[wp][i][sys_name]*sfs_vs_wp[wp][i]:.1f} ")
                    file.write("\\\\\n")

                file.write("Other sources combined ")
                for wp in args.wp:
                    file.write(f" & {sys_breakdown_vs_wp[wp][i].get('Other sources combined', 0)*sfs_vs_wp[wp][i]:.1f} ")
                file.write("\\\\\n")

                file.write("Total Systematics ")
                for wp in args.wp:
                    syst_unc = math.sqrt((sys_breakdown_vs_wp[wp][i]['Total Uncertainty']*sfs_vs_wp[wp][i])**2 - (sys_breakdown_vs_wp[wp][i]['Statistical uncertainty']*sfs_vs_wp[wp][i])**2)
                    file.write(f" & {syst_unc:.1f} ")
                file.write("\\\\\n")

                file.write("Statistical uncertainty ")
                for wp in args.wp:
                    file.write(f" & {sys_breakdown_vs_wp[wp][i]['Statistical uncertainty']*sfs_vs_wp[wp][i]:.1f} ")
                file.write("\\\\\n")
                file.write("\\toprule\n")
                
            
                file.write("\\textbf{{Total Uncertainty}} ")
                for wp in args.wp:
                    file.write(f" & {sys_breakdown_vs_wp[wp][i]['Total Uncertainty']*sfs_vs_wp[wp][i]:.1f} ")
                file.write("\\\\\n")

                file.write("\\hline\n")
                file.write("\\end{tabular}\n")
                if "22_23" in year:
                    printyear = "Run 3 (2022-2023)"
                elif "15_18" in year:
                    printyear = "Run 2 (2015-2018)"
                else:
                    printyear = year.split("_")[1]
                file.write(f"\\caption{{Uncertainty breakdown of systematics uncertainty on the {args.flavour_details['title']} tagging efficiency for the {pt}-{pt_bins[i+1]} GeV $p_T$ bin in {printyear} data.}}\n")
                file.write("\\end{table}\n")



def plot_sys_breakdown(axis, pt_bins, sys_breakdown, flavour, year, args):
    axis.set_ylabel(f"Relative uncertainty [%]", fontsize=16, loc="top")

    bin_width = 0.2
    max_uncertainty = 0
    for s in sys_breakdown:
        max_uncertainty = s["Total Uncertainty"] if s["Total Uncertainty"] > max_uncertainty else max_uncertainty
    bin_position = 0

    for pt, sys_components in zip(pt_bins, sys_breakdown):
        plot_single_pt_breakdown(
            axis,
            sys_components,
            bin_position,
            bin_width,
            max_uncertainty,
            pt,
            year,
            args,
        )
        bin_position = bin_position + (len(sys_components)+1) * (bin_width)
    #plot the legend only once
    if args.years[year]['subplot_pos'] == 0:
        set_axis_legend(axis, flavour)


    binpositions = [ x * (len(sys_components)+1) * (bin_width) - 0.1 for x in range(len(pt_bins)-1)]

    if args.years[year]['subplot_pos'] == len(list(args.years))-1:
        axis.set_xticks(binpositions)
        axis.set_xticklabels([f"{pt_bins[i]}-{pt_bins[i+1]}" for i in range(len(pt_bins)-1)], size=9, ha='left')

def plot_single_pt_breakdown(
    axis,
    sys_components,
    bin_position,
    bin_width,
    max_uncertainty,
    pt,
    year,
    args,
):
    for index, (sys_name, sys_value) in enumerate(sys_components.items()):
        colour = args.colour_palette[sys_name] #if args.flavour_details['colour_palette'][sys_name] else "red"
        axis.bar(
            bin_position + index * bin_width,
            sys_value,
            width=bin_width,
            color=colour,
            label=sys_name,
        )
    axis.axvline(
        x=bin_position - bin_width, color="grey", linestyle="--", linewidth=0.5
    )
    #axis.text(
    #    bin_position - bin_width + 0.1,
    #    max_uncertainty,
    #    f"{str(pt)} GeV",
    #    f"{(bin_position - bin_width + 0.1):.1f}",
    #    #f"{str(bin_position - bin_width + 0.1):.1f},
    #    color="grey",
    #    fontsize=8,
    #)
    #axis.tick_params(axis="x", which="both", bottom=False, top=False, labelbottom=False)


    # set 
    # set xticks
    axis.set_ylim(0, max_uncertainty*1.2)

def set_axis_legend(axis, flavour):
    handles, labels = axis.get_legend_handles_labels()
    legend_items = dict(zip(labels, handles))
    legend_columns = 1 if flavour == "light" else 2
    axis.legend(
        legend_items.values(),
        legend_items.keys(),
        #bbox_to_anchor=(1.01, 1),
        loc="upper right",
        ncol=legend_columns,
        fontsize=8.5,
        frameon=False,
    )


def save_plots(fig_sf, fig_uncertainty, wp, args):
    
    plot_dir = args.plot_dir
    fig_sf.savefig(os.path.join(plot_dir, f"SF_{wp}.png"), dpi=300)
    fig_sf.savefig(os.path.join(plot_dir, f"SF_{wp}.pdf"), dpi=300)
    fig_uncertainty.savefig(os.path.join(plot_dir, f"Uncertainty_{wp}.png"), dpi=300)
    fig_uncertainty.savefig(os.path.join(plot_dir, f"Uncertainty_{wp}.pdf"), dpi=300)
    # copy from plot_dir ../index.php to the plot_dir
    # os.system(f"cp {args.plot_dir}/../index.php {plot_dir}")


def calculate_pt_bins(pt_bins):
    centers = [(pt_bins[i] + pt_bins[i + 1]) / 2 for i in range(len(pt_bins) - 1)]
    errors = [(pt_bins[i + 1] - pt_bins[i]) / 2 for i in range(len(pt_bins) - 1)]
    return centers, errors



def extract_sf_and_sys(flavour, wp, year, args):
    bin_reg, SF_reg, syst_reg = regex_expressions(wp)

    file_name = os.path.join(args.input_dir, args.years[year]["txt_file"])

    pt_bins = []
    scale_factors = []
    total_uncertainties = []
    stat_uncertainties = []
    uncert_breakdown = []

    SF_bins = get_bins_content(file_name, bin_reg)
    for SF_bins_i in SF_bins:
        pt_min = int(SF_bins_i[0])
        pt_max = int(SF_bins_i[1])
        bin_content = SF_bins_i[5]

        # Append pt range to pt_bins
        if pt_min not in pt_bins:
            pt_bins.append(pt_min)
        if pt_max not in pt_bins:
            pt_bins.append(pt_max)
        # Extract central_value for this bin
        central_value_match = SF_reg.search(bin_content)
        if central_value_match:
            central_value = float(central_value_match.group(1))
            stat_percent = central_value_match.group(2)
            scale_factors.append(central_value)
            # Extract all sys and usys values for this bin
            sys_dict = syst_reg.findall(bin_content)
            sys_dict.append(("stat", stat_percent))

            # Group items by systematics group
            grouped_systs = group_by_prefix(flavour, sys_dict, args)

            # Output the grouped list
            syst_breakdown = {}
            for prefix, items in grouped_systs.items():
                syst_breakdown[prefix] = [
                    central_value * float(p[1]) / 100 for p in items
                ]
                syst_breakdown[prefix] = sum_in_quadrature(syst_breakdown[prefix])

            # Calculate the effective scale factor error for this bin
            total_percentages = [p for p in syst_breakdown.values()]
            tot_SF_value = sum_in_quadrature(total_percentages)
            total_uncertainties.append(tot_SF_value)
            
            for prefix, items in syst_breakdown.items():
                absolute_sys = syst_breakdown[prefix]
                syst_breakdown[prefix] = 100 * absolute_sys / central_value

            syst_breakdown["Total Uncertainty"] = 100 * tot_SF_value / central_value
            
            stat_SF_value = central_value * syst_breakdown["Statistical uncertainty"] / 100
            stat_uncertainties.append(stat_SF_value)

            uncert_breakdown.append(syst_breakdown)

    # Sort pt_bins and remove duplicates
    pt_bins = sorted(set(pt_bins))

    # the uncertainties are relative to the central value
    return pt_bins, scale_factors, (total_uncertainties,stat_uncertainties), uncert_breakdown


def get_bins_content(file_name, bin_reg):
    with open(file_name, "r") as file:
        content = file.read()
        return bin_reg.findall(content)


def regex_expressions(wp):
    # 1. look only for WP bin we want to plot
    PCBT_edges = re.compile(r"(\d+)-(\d+)")
    PCBT_bin = PCBT_edges.findall(wp)

    if PCBT_bin[0][0] == "100":
        wp_reg = rf"tagweight<FixedCutBEff_{PCBT_bin[0][1]}"
    elif PCBT_bin[0][1] == "0":
        wp_reg = rf"FixedCutBEff_{PCBT_bin[0][0]}<tagweight"
    else:
        wp_reg = (
            rf"FixedCutBEff_{PCBT_bin[0][0]}<tagweight<FixedCutBEff_{PCBT_bin[0][1]}"
        )

    # 2. extract pT and eta bin
    pt_reg = r"(\d+)<pt<(\d+)"
    eta_reg = r"([\d.]+)<abseta<([\d.]+)"

    # 3. dump bin content given pT, eta, WP bin
    bin_reg = re.compile(rf"bin\({pt_reg},\s*{eta_reg},\s*({wp_reg})\)\s*{{([^}}]*)}}")

    # 4. value of SF and stat uncertainty
    SF_reg = re.compile(r"central_value\(\s*([\d.]+)\s*,\s*([\d.]+)%?\s*\)")

    # 5. each systematic name and relative % value
    syst_reg = re.compile(r"u?sys\(([a-zA-Z0-9_-]+),-?(\d+.\d+)%\)")

    return bin_reg, SF_reg, syst_reg


def group_by_prefix(flavour, data, args):
    """Function to group by prefixes"""
    prefix_groups = defaultdict(list)

    min_prefix_length = args.flavour_details['group']

    for item in data:
        name, value = item
        name = name.replace("FT_EFF_", "")

        if min_prefix_length == 0:
            prefix_groups[name].append(item)

        prefix = name[:min_prefix_length]
        # for c and b the stat uncertainty comes from cov
        # so it is a separate term
        if "stat" in name:
            prefix = "Statistical uncertainty"
        elif "JET" in name:
            prefix = "Jet systematics"
        elif "ttbar" in name:
            prefix = "ttbar modelling"
        elif "Singletop" in name:
            prefix = "Single top modelling"
        elif "MUON" in name or "EGAMMA" in name or "EG" in name:
            prefix = "Lepton systematics"
        #elif "correctFakes" in name:
        #    prefix = "Fake leptons modelling"
        else:
            prefix = "Other sources combined"

        prefix_groups[prefix].append(item)
    
    # sort to have Jet, Lepton, ttbar, Single top, other, stat
    sorted_keys = ["Jet systematics", "Lepton systematics", "ttbar modelling", "Single top modelling", "Other sources combined", "Statistical uncertainty"]
    prefix_groups = dict(sorted(prefix_groups.items(), key=lambda item: sorted_keys.index(item[0])))

    return dict(prefix_groups)


def sum_in_quadrature(values):
    """calculate sum in quadrature"""
    return math.sqrt(sum(val**2 for val in values))


if __name__ == "__main__":
    main()
