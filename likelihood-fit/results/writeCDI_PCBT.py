import ROOT
import os
import math
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
import argparse
import sys
sys.path.insert(0, '../../FinalSelection/')
from options_file import options


taggers=["GN2v01"]
cuts=["FixedCutBEff"]
workingpoints=["90", "85", "77", "70", "65"]
workingpoints_plus=list(workingpoints)
workingpoints_plus.append("0")

#be carefull if you change this! do you really use the new one? Change it in the TSelectors!!
cdi_input_name="2023-02_MC23_CDI_GN2v01-noSF.root"
cdi_input_path="/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13p6TeV/"
workdir=options.plot_dir+"FitPlots_3SB/"
#outdir=options.output_dir
outdir="./"+"cdi-input-files/"+options.data_name+"/cdi-input-files-combined_fit/"
os.system("mkdir -p %s"%outdir)
version="v1.0"
rtag = "r24"

pt_binedges=['20', '40', '60', '140', '250', '400'] # ["20","30","40","60","85","110","140","175","250","600"]                                                                                             

#outdir=options.output_dir
outdir=outdir+"/continuous/"
os.system("mkdir -p %s"%outdir)
nbins = len(pt_binedges)
dataName=options.data_name

#parser = argparse.ArgumentParser(
#    description='Writes the scale factors and relative uncetainties into .txt files for the CDI file')
#parser.add_argument('-t',"--threshold",default="0.0",
#        help='Sets the threshold at which relative systematic uncertainties are dropped.')
#args = parser.parse_args()
#this does not work!
#if args.threshold:
#    threshold = args.threshold
syst_names_to_replace={
	"pileup" : "PRW_DATASF",
	"jvt" : "JET_JvtEfficiency",
	"leptonSF_MU_SF_Trigger_STAT" : "MUON_EFF_TrigStatUncertainty",
	"leptonSF_MU_SF_Trigger_SYST" : "MUON_EFF_TrigSystUncertainty",
	"leptonSF_MU_SF_ID_STAT" : "MUON_EFF_RECO_STAT",
	"leptonSF_MU_SF_ID_SYST" : "MUON_EFF_RECO_SYS",
	"leptonSF_MU_SF_ID_STAT_LOWPT" : "MUON_EFF_RECO_STAT_LOWPT",
	"leptonSF_MU_SF_ID_SYST_LOWPT" : "MUON_EFF_RECO_SYS_LOWPT",
	"leptonSF_MU_SF_TTVA_STAT" : "MUON_EFF_TTVA_STAT",
	"leptonSF_MU_SF_TTVA_SYST" : "MUON_EFF_TTVA_SYS",
	"leptonSF_MU_SF_Isol_STAT" : "MUON_EFF_ISO_STAT",
	"leptonSF_MU_SF_Isol_SYST" : "MUON_EFF_ISO_SYS",
	"leptonSF_EL_SF_Trigger" : "EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR",
	"leptonSF_EL_SF_Reco" : "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR",
	"leptonSF_EL_SF_ID" : "EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR",
	"leptonSF_EL_SF_Isol" : "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR",
    "hdamp_mc_rad" : "FT_EFF_ttbar_PhPy8Rad",
    "MC_stat_nominal" : "FT_EFF_MC_stat_nominal",
    "misstagLight_up" : "FT_EFF_misstagLight_up",
    "clTestMoMc_nstat" : "FT_EFF_closure",
    "correctFakes" : "FT_EFF_correctFakes",
    "singletop": "Singletop",
}
threshold = -1 # 0.0005

unc_directorys=[
    "other_systematics",
    # "Leptons_systematics",
    "MET_systematics",
    "JET_JES_systematics",
    "JET_JER_systematics",
    # "Zjets_systematics",
    # "Diboson_systematics",
    "ttbar_systematics",
    "Singletop_systematics",
]


for t in taggers:
    for c in cuts:
        inputFilename = workdir+f"FinalFitPlots_{rtag}_"+dataName+"_"+t+"_"+c+"_emu_OS_J2.root"
        if not os.path.isfile(inputFilename):
            print("File "+inputFilename+" not found!")
            break
        inputFile=ROOT.TFile(inputFilename,"read")
        keyList=inputFile.GetListOfKeys()
        keyList.Print()
        outputFilename = "btag_ttbarPDF_"+version+"_24-2-02_"+t+"_"+c+"_"+"Continuous"+".txt"
        outputFile = open(outdir + outputFilename, "w")
        CDIstring = "Analysis(ttbar_PDF,bottom,"+t+","+"Continuous"+",AntiKt4EMPFlowJets){\n\n"
        CDIstring += "\tmeta_data_s (Hadronization, Pythia8EvtGen)\n"
        for wp in workingpoints:
            cutvalue = c+"_"+wp+"_"+cdi_input_name.replace(".root","")
            if c== "FixedCutBEff":
                cdiFile=ROOT.TFile(cdi_input_path+cdi_input_name,"read")
                cutval=cdiFile.Get(t+"/AntiKt4EMPFlowJets/FixedCutBEff_"+wp+"/cutvalue")
                cutvalue=cutval.Max()
            CDIstring += "\tmeta_data_s (TagWeightBinEdge,"+c+"_"+wp+","+str(cutvalue)+")\n"
        CDIstring +="\n"
        #now starting block for all the 3d bins:
        for pt_bin in range(1, nbins):
            wp_n=0
            tagweight_name=""
            for wp in workingpoints_plus:
                wp_n +=1
                if wp=="0":
                    tagweight_name += "tagweight"
                else:
                    tagweight_name += "tagweight<"+c+"_"+wp

                CDIstring += "\tbin("+pt_binedges[pt_bin-1]+"<pt<"+pt_binedges[pt_bin]+",0<abseta<2.5," + tagweight_name + ")\n"
                #set tagweighname for next round
                tagweight_name=c+"_"+wp+"<"
                CDIstring += "\t{\n"
                TotalRelativeSysError = 0.
                sys_string = ""
                for unc_folder in unc_directorys:
                    TotalRelativeSysError_folder = 0.
                    sys_dir=inputFile.Get(unc_folder+"/p_b_pt_pt_"+str(pt_bin))
                    print("Reading systematic folder:\t"+unc_folder)
                    sys_keyList=sys_dir.GetListOfKeys()
                    for ikey in range(1, sys_keyList.GetSize()):
                        obj=sys_dir.Get(sys_keyList.At(ikey).GetName())
                        className=obj.ClassName()
                        if className[:2]=="TH":
                            hist_name=obj.GetName()
                            if hist_name.find("_syst_Error_rel")>0 and hist_name.find(str(pt_bin))>0 and hist_name.find("unused")==-1 and hist_name.find("MET_SoftTrk")==-1:
                                #print "Reading syst histogam:\t"+hist_name
                                hist_sys = sys_dir.Get(hist_name)
                                sys_value = hist_sys.GetBinContent(wp_n)
                                if (abs(sys_value) < threshold):
                                    print("Dropping this systematic, because its absolute value "+str(abs(sys_value))+" is smaller than threshold "+str(threshold))
                                    continue
                                
                                TotalRelativeSysError += math.pow(sys_value,2)
                                TotalRelativeSysError_folder+= math.pow(sys_value,2)
                                syst_name=(hist_name.replace("_syst_Error_rel","")).replace("p_b_"+"pt_"+str(pt_bin)+"_","")
                                syst_name=syst_name.replace("JET_21NP_","").replace("CategoryReduction_JET_","").replace("FTAG2_","FT_EFF_")
                                syst_name_cp=syst_name.replace("weight_","")
                                if "MC_stat_nominal" in syst_name_cp:
                                    print("... MC_stat_nominal" , str(round(100*sys_value,2))+"%)" )
                                if "correctFakes" in syst_name_cp:
                                    print("... fake" , str(round(100*sys_value,2))+"%)" )
                                for key in syst_names_to_replace:
                                    if key in syst_name_cp:
                                        syst_name_cp = syst_name_cp.replace(key,syst_names_to_replace[key])
                                syst_name_cp = syst_name_cp.replace("singletop","Singletop")
                                sys_string += "\t\tsys("+syst_name_cp+","+str(round(100*sys_value,2))+"%)\n"
                                
                    print("...", unc_folder, str(round(100*math.sqrt(TotalRelativeSysError_folder),2))+"%)")
                # pdf_sys_dir=inputFile.Get("ttbar_pdf_systematics"+"/p_b_pt_pt_"+str(pt_bin))
                # pdf_sys_keyList=pdf_sys_dir.GetListOfKeys()
                # countSystematic=0
                # for ikey in range(1, pdf_sys_keyList.GetSize()):
                #     obj=pdf_sys_dir.Get(pdf_sys_keyList.At(ikey).GetName())
                #     className=obj.ClassName()
                #     if className[:2]=="TH":
                #         hist_name=obj.GetName();
                #         if hist_name.find("_syst_Error_rel")>0 and hist_name.find(str(pt_bin))>0 and hist_name.find("unused")==-1 and hist_name.find("MET_SoftTrk")==-1:
                #             #print "Reading syst histogam:\t"+hist_name
                #             hist_sys = pdf_sys_dir.Get(hist_name)
                #             sys_value = hist_sys.GetBinContent(wp_n)
                #             if (abs(sys_value) < threshold):
                #                 print("Dropping this systematic, because its absolute value "+str(abs(sys_value))+" is smaller than threshold "+str(threshold))
                #                 continue
                #             TotalRelativeSysError += math.pow(sys_value,2)
                #             syst_name_anaTop=(hist_name.replace("_syst_Error_rel","")).replace("p_b_"+"pt_"+str(pt_bin)+"_","")
                #             syst_name_anaTop=syst_name_anaTop.replace("mc_shower","PDF4LHC").replace("mc_qcd","QCD")
                #             syst_name_cp=syst_name_anaTop.replace("ttbar_weight_","")
                #             np=int(syst_name_cp.replace("PDF4LHC_np_","").replace("QCD_np_",""))
                #             if np == 124:
                #                 continue
                #             if "PDF4LHC" in syst_name_cp:
                #                 countSystematic+=1
                #                 syst_name_cp="FT_EFF_ttbar_PDF4LHC_np_"+str(countSystematic)
                #             else:
                #                 syst_name_cp="FT_EFF_ttbar_QCD_np_"+str(np)
                #             sys_string += "\t\tsys("+syst_name_cp+","+str(round(100*sys_value,2))+"%)\n"
                # pdf_sys_dir=inputFile.Get("Singletop_pdf_systematics"+"/p_b_pt_pt_"+str(pt_bin))
                # pdf_sys_keyList=pdf_sys_dir.GetListOfKeys()
                # for ikey in range(1, pdf_sys_keyList.GetSize()):
                #     obj=pdf_sys_dir.Get(pdf_sys_keyList.At(ikey).GetName())
                #     className=obj.ClassName()
                #     if className[:2]=="TH":
                #         hist_name=obj.GetName();
                #         if hist_name.find("_syst_Error_rel")>0 and hist_name.find(str(pt_bin))>0 and hist_name.find("unused")==-1 and hist_name.find("MET_SoftTrk")==-1:
                #             print("Reading syst histogam:\t"+hist_name)
                #             hist_sys = pdf_sys_dir.Get(hist_name)
                #             sys_value = hist_sys.GetBinContent(wp_n)
                #             if (abs(sys_value) < threshold):
                #                 print("Dropping this systematic, because its absolute value "+str(abs(sys_value))+" is smaller than threshold "+str(threshold))
                #                 continue
                #             TotalRelativeSysError += math.pow(sys_value,2)
                #             syst_name_anaTop=(hist_name.replace("_syst_Error_rel","")).replace("p_b_"+"pt_"+str(pt_bin)+"_","")
                #             syst_name_anaTop=syst_name_anaTop.replace("mc_shower","PDF4LHC").replace("mc_qcd","QCD")
                #             syst_name_cp=syst_name_anaTop.replace("singletop_weight_","")
                #             np=int(syst_name_cp.replace("PDF4LHC_np_","").replace("QCD_np_",""))
                #             if np == 122:
                #                 continue
                #             if "PDF4LHC" in syst_name_cp:
                #                 countSystematic+=1
                #                 syst_name_cp="FT_EFF_singletop_PDF4LHC_np_"+str(countSystematic)
                #             else:
                #                 syst_name_cp="FT_EFF_singletop_QCD_np_"+str(np)
                #             sys_string += "\t\tsys("+syst_name_cp+","+str(round(100*sys_value,2))+"%)\n"
                TotalRelativeStatError = 0.
                stat_string = ""
                stat_dir=inputFile.Get("stat/stats_pt_"+str(pt_bin))
                stat_keyList=stat_dir.GetListOfKeys()
                for ikey in range(1, stat_keyList.GetSize()):
                    obj=stat_dir.Get(stat_keyList.At(ikey).GetName())
                    className=obj.ClassName()
                    if className[:2]=="TH":
                        hist_name=obj.GetName();
                        if hist_name.find("Error_rel")>0 and hist_name.find(str(pt_bin))>0:
                            #print "Reading stat histogam:\t"+hist_name
                            hist_stat = stat_dir.Get(hist_name)
                            stat_value = hist_stat.GetBinContent(wp_n)
                            if (abs(stat_value) < threshold):
                                print("Dropping this statistical error, because its absolute value "+str(abs(stat_value))+" is smaller than threshold "+str(threshold))
                                continue
                            TotalRelativeStatError += math.pow(stat_value,2)
                            sys_string += "\t\tsys("+(hist_name.replace("_Error_rel","")).replace("p_b_pt_"+str(pt_bin)+"_","FT_EFF_b-PDF_")+","+str(round(100*stat_value,2))+"%)\n"
                #Get SF here
                string_nominal = "p_b_sf_pt_"+str(pt_bin)+"_nominal"
                hist_nominal = inputFile.Get("results_continuous/"+string_nominal)
                central_value = hist_nominal.GetBinContent(wp_n)
                totalstat_value = hist_nominal.GetBinError(wp_n)
                totalstat_value /= central_value
                string_totalsys = "p_b_sf_pt_"+str(pt_bin)+"_syst_Error_combined_rel"
                hist_totalsys = inputFile.Get("results_continuous/"+string_totalsys)
                totalsys_value = hist_totalsys.GetBinContent(wp_n)
                totalerr_value = math.sqrt(math.pow(totalsys_value,2) + math.pow(totalstat_value,2))
                TotalRelativeError = math.sqrt(TotalRelativeSysError + TotalRelativeStatError)
                TotalRelativeSysError = math.sqrt(TotalRelativeSysError)
                TotalRelativeStatError = math.sqrt(TotalRelativeStatError)
                print("Summed up total relative syst. error for pt_bin "+str(pt_bin)+" wp_bin "+str(wp_n)+":\t"+str(100.*TotalRelativeSysError)+"%")
                print("Summed up total relative stat. error for pt_bin "+str(pt_bin)+" wp_bin "+str(wp_n)+":\t"+str(100.*TotalRelativeStatError)+"%")
                print("Total error for pt_bin "+str(pt_bin)+" wp_bin "+str(wp_n)+":\t"+str(100.*TotalRelativeError)+"%")
                print("Cross check: reading total relative syst. error for pt_bin "+str(pt_bin)+" wp_bin "+str(wp_n)+" from file: "+str(100.*totalsys_value)+"%")
                print("Cross check: reading total relative stat. error for pt_bin "+str(pt_bin)+" wp_bin "+str(wp_n)+" from file: "+str(100.*totalstat_value)+"%")
                print("Cross check: reading total relative error for pt_bin "+str(pt_bin)+" wp_bin "+str(wp_n)+" from file: "+str(100.*totalerr_value)+"%")
                print("Cross check: absolut stat error for pt_bin "+str(pt_bin)+" wp_bin "+str(wp_n)+" from file: "+str(central_value*totalstat_value)+" from here: "+str(TotalRelativeStatError*central_value))
                #CDIstring += "\t\tcentral_value("+str(round(central_value,2))+","+str(round(100.*TotalRelativeStatError,2))+"%)\n"
                #CDIstring += "\t\tcentral_value("+str(round(central_value,2))+","+str(-999)+")\n"
                CDIstring += "\t\tcentral_value("+str(round(central_value,2))+","+str(0)+")\n"
                # CDIstring += "\t\tmeta_data(N_jets total,-999,-999)\n"
                # CDIstring += "\t\tmeta_data(N_jets tagged,-999,-999)\n"
                CDIstring += sys_string
                CDIstring += "\t}\n"
        CDIstring += "}"
        print("Writing to file "+outputFilename)
        outputFile.write(CDIstring)
        outputFile.close()
        inputFile.Close()
        print("File "+inputFilename+" closed.")
print("Done")
