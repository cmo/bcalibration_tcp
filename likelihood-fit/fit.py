import os
import sys
import subprocess
import argparse
from multiprocessing.pool import ThreadPool
sys.path.insert(0, '../FinalSelection/')

from options_file import options
from utils import SystematicsManager

def work(input_file,work_dir,rlTag,syst,channel,tagger,workingPointName,additional_options=[]):
    out_name= workspace_dir+ "/Workspace_"+ rlTag +"_" + dataName + "_" + tagger + "_" + workingPointName +"_"+ channel + "_"+ syst
    ##print( "got additional option:", additional_options
    print( "Calculating fit for Systematic: ", syst + "  " + out_name)
    s_additional_options=''
    for s_additional_options_single in additional_options:
        s_additional_options=s_additional_options+"_"+s_additional_options_single
    s_additional_options=s_additional_options.replace("-","").replace("+","")
    print( s_additional_options)
    print('log file:', out_name+s_additional_options+".log")
    log=open(out_name+s_additional_options+".log","w")
    print('command:')
    print( " ".join(["./build/2jet_pt_combined_fit","-i",input_file,"-o", work_dir, "-r", rlTag,"-d", dataName, "-s",syst,"-c", channel,"-t",tagger,"-w",workingPointName]+additional_options), flush=True)
    print('command end')
    p=subprocess.Popen(["./build/2jet_pt_combined_fit","-i",input_file,"-o", work_dir, "-r", rlTag,"-d", dataName, "-s",syst,"-c", channel,"-t",tagger,"-w",workingPointName]+additional_options,stdout=log) #,stdout=log "--allow_neg_sf_xx"
    p.wait()


parser = argparse.ArgumentParser(
    description='Calculates all fits for a given Tagger, workingpoint and cahnnel.')
parser.add_argument('-c',"--channel",default="emu_OS_J2",
                    help='Channel to run over')
parser.add_argument('-t',"--tagger",default="GN2v01", #MV2c10
                    help='tagger to run over')
parser.add_argument('-w',"--workingPointName",default="FixedCutBEff",
                    help='workingPointName to run over')
parser.add_argument('-r',"--rlTag",default="r24",
                    help='releaseTag')
parser.add_argument('-m',"--MC_label",default="mc23a",
                    help='MC label e.g. mc23a')
parser.add_argument('-d',"--dataName",default=options.data_name,
                    help='dataName')
parser.add_argument('--rrF', action='store_true',
                    help='rerun all the Fits (default: false)')
parser.add_argument('--boot_strap',action='store_true',
                    help='run with mc stat systematics takes really long!.')
parser.add_argument("--plot_dir",default=options.plot_dir,
                    help='use in order to change working dir, usefull for combination of different years.')

num_of_cores_to_use = 1#8 # None  # set to the number of workers you want (it defaults to the cpu count of your machine)
args = parser.parse_args()
channel=args.channel
tagger=args.tagger
workingPointName=args.workingPointName
rlTag=args.rlTag
MC_label=args.MC_label
dataName=args.dataName
work_dir = args.plot_dir

systematics = []
# branch systematics
for systematic in SystematicsManager.get_systematics():
    syst = systematic.name_for_histos
    if 'JET_JER' in syst:
        # pair the ones with and w/o "PseudoData" in the name
        if 'PseudoData' in syst:
            continue
        elif syst.replace('__1down', '_PseudoData__1down').replace('__1up', '_PseudoData__1up') in SystematicsManager.get_systematics():
            systematics.append(syst.replace("1down", "1down_final").replace("1up", "1up_final"))
        # if the pair doesn't exist, just add the one that does
        else:
            systematics.append(syst)
    else:
        systematics.append(syst)

# mc weight systematics
for sample_name, systs in SystematicsManager.get_mc_weight_systematics().items():
    for syst in systs:
        systematics.append(syst.name_for_histos)

syst_samples = [vals for key, vals in options.alternative_samples.items()]


workspace_dir = work_dir+"/Workspaces_3SB"
fitplots_dir = work_dir+"/FitPlots_3SB"
if not os.path.exists(workspace_dir):
    os.makedirs(workspace_dir)
if not os.path.exists(fitplots_dir):
    os.makedirs(fitplots_dir)

s_data_lumi="{:.1f}".format(options.data_lumi) 
CME=options.data_CME
options_for_all_fits=[]

options_for_all_fits=options_for_all_fits+["-l", s_data_lumi, "-m", MC_label, "--allow_neg_sf_xx", 
                                        #    "-e", CME 
                                           ]
fit_config=''


#main part of the script. Otherwise we can use it to calculate a single bootstrap untcertainty. Usefull to have shorter jobs. 
if args.rrF:
    tp = ThreadPool(num_of_cores_to_use)
    """
    Nominal Fit:
    """
    print( "Nominal Fit: ")
    input_file=work_dir+"combination_sys_"+options.nominal_samples['ttbar'].name+"/"+"combination_for_fit_"+"nominal"+".root"
    print( "input file:", input_file)

    tp.apply_async(work, (input_file,work_dir,rlTag,"nominal",channel,tagger,workingPointName, options_for_all_fits[:] + ["--use_Minos"]))

    # a nominal version without Minos
    tp.apply_async(work, (input_file,work_dir,rlTag,"nominal",channel,tagger,workingPointName, options_for_all_fits[:]))


    """
    Systematics Fit
    """
    #............detector systematics, pdf systematics... systematics:
    for syst in systematics:
        input_file=work_dir+"combination_sys_"+options.nominal_samples['ttbar'].name+"/"+"combination_for_fit_"+syst+".root"
        tp.apply_async(work, (input_file,work_dir,rlTag,syst,channel,tagger,workingPointName,options_for_all_fits[:] ))
        print( "Fit for syst: ", syst)

    """
    Sample systematics Fit
    """
    for syst_sample in syst_samples:
        input_file=work_dir+"combination_sys_"+options.nominal_samples['ttbar'].name+"/"+"combination_for_fit_"+syst_sample.name+".root"
        tp.apply_async(work, (input_file, work_dir, rlTag, syst_sample.name, channel, tagger, workingPointName,options_for_all_fits[:] ))
        print( "Fit for syst: ", syst_sample.name)
    
    tp.close()
    tp.join()


# For plotting
syst_for_final_plot=[]  #will be a list of all systematics wich shall be included in the final plots.
for syst in systematics:
    syst_for_final_plot.append(syst)
for syst_sample in syst_samples:
    syst_for_final_plot.append(syst_sample.name)
# in case of double counting of systematics
syst_for_final_plot = list(set(syst_for_final_plot))
command=" ".join(["./build/final_syst_plot", "-i", fitplots_dir ,"-r", rlTag, "-d", dataName, "-c", channel ,"-t",tagger,"-w",workingPointName, "-f", fit_config, "-l", s_data_lumi, "-n", work_dir+"combination_sys_"+options.nominal_samples['ttbar'].name+"/"+"combination_for_fit_"+"nominal"+".root"]+syst_for_final_plot)
print( "command for plotting: ",command)
p=subprocess.Popen(["./build/final_syst_plot", "-i", fitplots_dir ,"-r", rlTag, "-d", dataName, "-c", channel ,"-t",tagger,"-e", CME, "-w",workingPointName, "-f", fit_config, "-l", s_data_lumi, "-n", work_dir+"combination_sys_"+options.nominal_samples['ttbar'].name+"/"+"combination_for_fit_"+"nominal"+".root"]+syst_for_final_plot)
p.wait()
print("done fitting")
