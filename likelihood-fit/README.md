# Installation
```bash
source setup.sh
mkdir build
cd build
cmake ..
make
```

# Run the fit and get the results
The full pipeline can be run with `./run.sh`. You can also run every command one by one, as explained below

## combine the input files.
```bash
python combine_histos_for_fit_with_syst.py
```
to combine the relevant histogram for the fits. It runs a compiled c-script (Combine_histograms_for_fit.cxx) for every systematic. This script prepares the inputs needed for one liklihoodfit and if set up, corresponding bootstrap weights.  At the moment You have to uncomment parts you don't want here (no systematics for example).
This we could easily submit to a batch system as well. 

now you have hopefully everything in place.


## Run the Fit:
```bash
python fit.py 
```

## Plot the scale factors
```bash
cd plot_sf/
python get_results.py 
```

## Get the CDI file and the uncertainty breakdown
```bash
cd results/
python writeCDI_PCBT.py 2
python sf_uncert_from_txt.py -c settings_Run3.yaml
```
